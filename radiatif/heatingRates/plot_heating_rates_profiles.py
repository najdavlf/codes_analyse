import netCDF4 as nc
import numpy   as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys

g0 = 9.81       # m/s2
Rt = 6371*1000. # m
Cp = 1004       # J/(kg.K)
day2sec = 24.*3600

mpl.rcParams["figure.figsize"] = [15, 6]
mpl.rcParams["figure.titlesize"] = 30
mpl.rcParams["figure.subplot.wspace"] = .3
mpl.rcParams["figure.subplot.left"] = 0.05
mpl.rcParams["figure.subplot.right"] = .99

ecdir = "/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/hrates/"
ecinp = "i3rc_mls_cumulus"
ecfic = [ecdir+ecinp+"_spartacus"+ext+"_out.nc" for ext in ["","_noatm"]]
eccld = ecdir+ecinp+"_sza.nc"

dscld = nc.Dataset(eccld,"r")
cldfr = dscld.variables["cloud_fraction"][0,:]
hlvls = dscld.variables["height_hl"][0,:]
cosza = dscld.variables["cos_solar_zenith_angle"][:]
dscld.close()
flvls = (hlvls[:-1]+hlvls[1:])/2.
CC,ZZ = np.meshgrid(cosza,flvls)

press=[]
dn_sw=[]
dn_sw_clear=[]
up_sw=[]
up_sw_clear=[]
dn_lw=[]
dn_lw_clear=[]
up_lw=[]
up_lw_clear=[]

for fic in ecfic:
    print fic
    dset = nc.Dataset(fic,"r")
    press.append(dset.variables["pressure_hl"][:].transpose())
    dn_sw.append(dset.variables["flux_dn_sw"][:].transpose())
    dn_sw_clear.append(dset.variables["flux_dn_sw_clear"][:].transpose())
    up_sw.append(dset.variables["flux_up_sw"][:].transpose())
    up_sw_clear.append(dset.variables["flux_up_sw_clear"][:].transpose())
    dn_lw.append(dset.variables["flux_dn_lw"][:].transpose())
    dn_lw_clear.append(dset.variables["flux_dn_lw_clear"][:].transpose())
    up_lw.append(dset.variables["flux_up_lw"][:].transpose())
    up_lw_clear.append(dset.variables["flux_up_lw_clear"][:].transpose())

net_sw = [ dn-up for dn,up in zip(dn_sw,up_sw)]
net_sw_clear = [ dn-up for dn,up in zip(dn_sw_clear,up_sw_clear)]
net_lw = [ dn-up for dn,up in zip(dn_lw,up_lw)]
net_lw_clear = [ dn-up for dn,up in zip(dn_lw_clear,up_lw_clear)]

def hr(vecnet,vecpress):
 return [ -day2sec*g0/(Cp*(1.+np.tile(flvls,(net.shape[1],1)).transpose()/Rt)**2)*(net[1:,:]-net[:-1,:])/(pr[1:,:]-pr[:-1,:]) for net,pr in zip(vecnet,vecpress)]

# dF / dP
# dP > pour indices croissants
hr_sw = hr(net_sw,press)
hr_sw_clear = hr(net_sw_clear,press)
hr_lw = hr(net_lw,press)
hr_lw_clear = hr(net_lw_clear,press)

plt.figure()
ax=plt.subplot(141)
plt.plot(cldfr,flvls)
plt.xlabel("Cloud fraction")
plt.ylim(0,10000)
plt.subplot(142,sharey=ax)
plt.plot(hr_sw[0][:,0]-hr_sw_clear[0][:,0],flvls)
plt.plot(hr_sw[0][:,20]-hr_sw_clear[0][:,20],flvls)
plt.plot(hr_sw[0][:,40]-hr_sw_clear[0][:,40],flvls)
plt.legend(["SZA %g" %(np.arccos(cosza[i])*180./np.pi) for i in [0,20,40]])
plt.xlabel("cloud contrib")
plt.ylim(0,10000)
plt.subplot(143,sharey=ax)
plt.plot(hr_sw[0][:,0],flvls)
plt.plot(hr_sw[0][:,20],flvls)
plt.plot(hr_sw[0][:,40],flvls)
plt.xlabel("total contrib")
plt.ylim(0,10000)
plt.subplot(144)
plt.plot(hr_sw[0][:,0],flvls)
plt.plot(hr_sw[0][:,20],flvls)
plt.plot(hr_sw[0][:,40],flvls)
plt.xlabel("total contrib")
plt.suptitle("SW heating rates (K/d)")
plt.savefig("sw_hr.pdf")

plt.figure()
ax=plt.subplot(141)
plt.plot(cldfr,flvls)
plt.xlabel("Cloud fraction")
plt.ylim(0,10000)
plt.subplot(142,sharey=ax)
plt.plot(hr_lw[0][:,0]-hr_lw_clear[0][:,0],flvls)
plt.xlabel("cloud contrib")
plt.ylim(0,10000)
plt.subplot(143,sharey=ax)
plt.plot(hr_lw[0][:,0],flvls)
plt.xlabel("total contrib")
plt.ylim(0,10000)
plt.subplot(144)
plt.plot(hr_lw[0][:,0],flvls)
plt.xlabel("total contrib")
plt.suptitle("LW heating rates (K/d)")
plt.savefig("lw_hr.pdf")

plt.figure()
ax=plt.subplot(141)
plt.plot(cldfr,flvls)
plt.xlabel("Cloud fraction")
plt.ylim(0,10000)
plt.subplot(142,sharey=ax)
plt.plot(hr_sw[0][:,0]-hr_sw_clear[0][:,0]+hr_lw[0][:,0]-hr_lw_clear[0][:,0],flvls)
plt.plot(hr_sw[0][:,20]-hr_sw_clear[0][:,20]+hr_lw[0][:,0]-hr_lw_clear[0][:,0],flvls)
plt.plot(hr_sw[0][:,40]-hr_sw_clear[0][:,40]+hr_lw[0][:,0]-hr_lw_clear[0][:,0],flvls)
plt.legend(["SZA %g" %(np.arccos(cosza[i])*180./np.pi) for i in [0,20,40]])
plt.ylim(0,10000)
plt.xlabel("cloud contrib")
plt.subplot(143,sharey=ax)
plt.plot(hr_sw[0][:,0]+hr_lw[0][:,0],flvls)
plt.plot(hr_sw[0][:,20]+hr_lw[0][:,0],flvls)
plt.plot(hr_sw[0][:,40]+hr_lw[0][:,0],flvls)
plt.ylim(0,10000)
plt.xlabel("total contrib")
plt.subplot(144)
plt.plot(hr_sw[0][:,0]+hr_lw[0][:,0],flvls)
plt.plot(hr_sw[0][:,20]+hr_lw[0][:,0],flvls)
plt.plot(hr_sw[0][:,40]+hr_lw[0][:,0],flvls)
plt.xlabel("total contrib")
plt.suptitle("Net heating rates (K/d)")
plt.savefig("net_hr.pdf")
