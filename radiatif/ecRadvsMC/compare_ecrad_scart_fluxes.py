import netCDF4 as ncdf
import numpy as np
from matplotlib import pyplot as plt
from mycolors import *

class Obj(object) :
  def __init__(self,dic) :
    for k in dic :
        setattr(self,k,dic[k])
    setattr(self,'dic',dic)

  def __getattribute__(self,n) :
    if n=='dn' : return self.dic[dn]
    elif n=='dr' : return self.dic[dr]
    if n=='sdn' : return self.dic[sdn]
    elif n=='sdr' : return self.dic[sdr]
    elif n=='zz' : return self.dic[zz]
    else : return object.__getattribute__(self,n)
    
dn="flux_dn_sw"
dr="flux_dn_direct_sw"
sdn="sflux_dn_sw"
sdr="sflux_dn_direct_sw"
zz="z"

def get_var_1D(nc,name) :
  var =  nc.variables[name]
  return var[:].reshape(var.shape)

def get_var_4D(nc,name) :
  return get_var_1D(nc,name)[:,:,0,0]

def model_ec(ztab, vec, zi):
  i=-1
  while (ztab[i+1]>zi & i < length(ztab)): i=i+1
  if (i==-1) : i=0
  a = (vec[i+1]-vec[i])/(ztab[i+1]-ztab[i])
  return vec[i]+a*(zi-ztab[i]) 

def get_ecrad(ecfile) :
  nc  = ncdf.Dataset(ecfile, 'r')
  var = nc.variables[dn]
  ecdn = var[:].reshape(var.shape)
  var = nc.variables[dr]
  ecdir = var[:].reshape(var.shape)
  nc.close()
  return {dn : ecdn, dr : ecdir}

def get_input(infile) :
  nc  = ncdf.Dataset(infile, 'r')
  var = nc.variables[ "cos_solar_zenith_angle"]
  cos = var[:].reshape(var.shape)
  var = nc.variables["height_hl"]
  ecz = var[:].reshape(var.shape)[0,:]
  nc.close()
  return {"mu":cos, zz:ecz}
  
def get_mtcrl(mcfile):
  nc  = ncdf.Dataset(mcfile, 'r')
  mcz = get_var_1D(nc,"vertical_levels")
  mcdn = get_var_4D(nc,dn)
  smcdn = get_var_4D(nc,"std_"+dn)
  mcdir = get_var_4D(nc,dr)
  smcdir = get_var_4D(nc,"std_"+dr)
  nc.close()
  return {zz:mcz, dn:mcdn, dr:mcdir, 's'+dn:smcdn, 's'+dr:smcdir}

def set_mtcrl(mcfile):
  mcdat = Obj(get_mtcrl(mcfile))
  mcdat.ratio = mcdat.dr/mcdat.dn
  mcdat.minratio = (mcdat.dr-3.*mcdat.sdr)/(mcdat.dn+3*mcdat.sdn)
  mcdat.maxratio = (mcdat.dr+3*mcdat.sdr)/(mcdat.dn-3*mcdat.sdn)
  return mcdat

def plt_mtcrl(vecsza,mcdat,color,label,met='ratio'):
  if met=='ratio' :
    plt.plot(vecsza,mcdat.ratio[:,0],color=color,label=label)
    plt.fill_between(vecsza,mcdat.minratio[:,0],mcdat.maxratio[:,0],color=color,alpha=.1)
  elif met==dn :
    smet = 's'+met
    plt.plot(vecsza,mcdat.dn[:,0],color=color,label=label)
    plt.fill_between(vecsza,mcdat.dn[:,0]-3*mcdat.sdn[:,0],mcdat.dn[:,0]+3*mcdat.sdn[:,0],color=color,alpha=.1)
  elif met==dr :
    plt.plot(vecsza,mcdat.dr[:,0],color=color,label=label)
    plt.fill_between(vecsza,mcdat.dr[:,0]-3*mcdat.sdr[:,0],mcdat.dr[:,0]+3*mcdat.sdr[:,0],color=color,alpha=.1)

ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_out.nc'  
ecdat0 = Obj(get_ecrad(ecfile))
ecdat0.ratio = ecdat0.dr/ecdat0.dn

ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_scaled_overlap_out.nc'  
ecdat1 = Obj(get_ecrad(ecfile))
ecdat1.ratio = ecdat1.dr/ecdat1.dn

infile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza.nc'  
indat = Obj(get_input(infile))


mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0.nc'
mcdat0 = set_mtcrl(mcfile)

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc1.nc'
mcdat1 = set_mtcrl(mcfile)

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0_instru.nc'
mcdat2 = set_mtcrl(mcfile)

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0_cutnose.nc'
mcdat3 = set_mtcrl(mcfile)

vecsza = np.arccos(indat.mu)*180./np.pi

plt.figure()
plt.xlim(0.,90.)
plt.xlabel(r"Solar Zenith Angle [$^o$]")
plt.ylabel(r"SW direct [W.m$^{-2}$]")
plt.plot(vecsza,ecdat0.dr[:,-1],color=basic3[0],label="ecRad",linestyle='--')
plt.plot(vecsza,ecdat1.dr[:,-1],color=basic3[1],label="ecRad scaled overlap",linewidth=2)
plt_mtcrl(vecsza,mcdat0,basic3[2],"Monte Carlo Mie",met=dr)
plt_mtcrl(vecsza,mcdat1,basic3[3],"Monte Carlo delta scaled HG",met=dr)
plt_mtcrl(vecsza,mcdat3,basic3[5],"Monte Carlo Mie cut nose",met=dr)
plt.legend(loc="best")
plt.savefig("test_direct.pdf")

plt.figure()
plt.xlim(0.,90.)
plt.xlabel(r"Solar Zenith Angle [$^o$]")
plt.ylabel(r"SW total [W.m$^{-2}$]")
plt.plot(vecsza,ecdat0.dn[:,-1],color=basic3[0],label="ecRad",linestyle='--')
plt.plot(vecsza,ecdat1.dn[:,-1],color=basic3[1],label="ecRad scaled overlap",linewidth=2)
plt_mtcrl(vecsza,mcdat0,basic3[2],"Monte Carlo Mie",met=dn)
plt_mtcrl(vecsza,mcdat1,basic3[3],"Monte Carlo delta scaled HG",met=dn)
plt_mtcrl(vecsza,mcdat3,basic3[5],"Monte Carlo Mie cut nose",met=dn)
plt.legend(loc="best")
plt.savefig("test_total.pdf")

plt.figure()
plt.xlim(0.,90.)
plt.ylim(0.,1.)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.xlabel(r"Solar Zenith Angle [$^o$]",fontsize=22)
plt.ylabel(r"SW direct / global [W.m$^{-2}$]",fontsize=22)
plt.title("SW ecRad vs Monte Carlo on a LES\ncumulus field (ARM-Cumulus, cover 26%)",fontsize=22)
plt.plot(vecsza,ecdat1.ratio[:,-1],color=basic3[0],label="ecRad",linewidth=2,linestyle='--')
#plt.plot(vecsza,ecdat1.ratio[:,-1],color=basic3[1],label="ecRad scaled overlap",linewidth=2)
plt_mtcrl(vecsza,mcdat0,basic3[1],"Monte Carlo Mie")
plt_mtcrl(vecsza,mcdat3,basic3[3],"Monte Carlo Mie cut nose")
plt_mtcrl(vecsza,mcdat1,basic3[2],"Monte Carlo delta scaled HG")
plt.legend(loc="best",fontsize=20)
plt.tight_layout()
plt.savefig("test_ratio.pdf")

w,h = plt.figaspect(1./3)
fig=plt.figure(figsize=(w,h))
ax=fig.add_subplot(131)
plt.xlim(0.,90.)
plt.xlabel(r"Solar Zenith Angle [$^o$]")
plt.ylabel(r"SW direct [W.m$^{-2}$]")
ax.plot(vecsza,ecdat0.dr[:,-1],color=basic3[0],label="ecRad",linestyle='--')
ax.plot(vecsza,ecdat1.dr[:,-1],color=basic3[0],label="ecRad scaled overlap",linewidth=2)
plt_mtcrl(vecsza,mcdat0,basic3[1],"Monte Carlo Mie",met=dr)
plt_mtcrl(vecsza,mcdat1,basic3[2],"Monte Carlo delta scaled HG",met=dr)
plt_mtcrl(vecsza,mcdat3,basic3[3],"Monte Carlo Mie cut nose",met=dr)

ax=fig.add_subplot(132)
plt.xlim(0.,90.)
plt.xlabel(r"Solar Zenith Angle [$^o$]")
plt.ylabel(r"SW total [W.m$^{-2}$]")
ax.plot(vecsza,ecdat0.dn[:,-1],color=basic3[0],label="ecRad",linestyle='--')
ax.plot(vecsza,ecdat1.dn[:,-1],color=basic3[0],label="ecRad scaled overlap",linewidth=2)
plt_mtcrl(vecsza,mcdat0,basic3[1],"Monte Carlo Mie",met=dn)
plt_mtcrl(vecsza,mcdat1,basic3[2],"Monte Carlo delta scaled HG",met=dn)
plt_mtcrl(vecsza,mcdat3,basic3[3],"Monte Carlo Mie cut nose",met=dn)

ax=fig.add_subplot(133)
plt.xlim(0.,90.)
plt.ylim(0.,1.)
plt.xlabel(r"Solar Zenith Angle [$^o$]")
plt.ylabel(r"SW direct / global [W.m$^{-2}$]")
ax.plot(vecsza,ecdat0.ratio[:,-1],color=basic3[0],label="ecRad",linestyle='--')
ax.plot(vecsza,ecdat1.ratio[:,-1],color=basic3[0],label="ecRad scaled overlap",linewidth=2)
plt_mtcrl(vecsza,mcdat0,basic3[1],"Monte Carlo Mie")
plt_mtcrl(vecsza,mcdat1,basic3[2],"Monte Carlo delta scaled HG")
plt_mtcrl(vecsza,mcdat3,basic3[3],"Monte Carlo Mie cut nose")
plt.legend(loc="best")

plt.tight_layout()
plt.savefig("subplot_test_ratio.pdf")


"""
plt.figure()
plt.xlim(-0.,2000.)
plt.ylim(0.,90.)
plt.xlabel("Flux down [W/m^2]")
plt.ylabel("Height [km]")
plt.title("Radiative downward fluxes [W/m^2]")
print ecdn[0,:].shape,ecz.shape
for sza in range(len(ecdn[:,0])) :
  plt.plot(mcdn[sza,:],mcz,color=basic3[1])
  plt.plot(ecdn[sza,:],ecz/1000.,color=basic3[1],linestyle='--')
  plt.plot(mcdir[sza,:],mcz,color=basic3[2])
  plt.plot(ecdir[sza,:],ecz/1000.,color=basic3[2],linestyle='--')

plt.legend(loc="best",labels=["ecRad","Monte Carlo","Total","Direct"])
plt.savefig("down_fluxes_ecrad_scart.pdf")

"""
