import netCDF4 as ncdf
import numpy as np
from matplotlib import pyplot as plt
from mycolors import *

path   = '/home/villefranquen/Work/NCDF/GEM/'
path   = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/'
ecfile = path+'gem_pacific_1D_16_mono_encr_out.nc'
ecfile = path+'i3rc_mls_cumulus_ARMCu008_homogeneous2_sza_homogene_spartacus_out_modif2.nc'
ecfile1 = path+'i3rc_mls_cumulus_ARMCu008b_sza_spartacus_out_modif2.nc'
ecfile2 = path+'i3rc_mls_cumulus_ARMCu008b_sza_samecov_spartacus_out_modif2.nc'
ecfile3 = path+'i3rc_mls_cumulus_ARMCu008b_sza_defaultoverlap_spartacus_out_modif2.nc'
zfile  = path+'gem_pacific_1D_16.nc'
zfile  = path+'i3rc_mls_cumulus_ARMCu008_sza.nc'
path   = '/home/villefranquen/Work/NCDF/ARMCU/'
path   = '/home/villefranquen/Work/SCART/scart_project/build/'
mcfile = [path+'GEM.016out_sza%s_dscale1.nc' %s for s in ['0','44','70']]
mcfile = [path+'ARMCU.008out_sza%s_dscale0_1M.nc' %s for s in ['0','44','70']]
mcfile = [path+'test_armcu_homog2_nodscale_sza%s_ssa_alb0.nc' %s for s in ['0','44','70']]


dset = ncdf.Dataset(ecfile1,'r')
ecdn = dset.variables['flux_up_sw'][:].reshape(dset.variables['flux_up_sw'].shape)
ecdn1 = ecdn
dset.close()
dset = ncdf.Dataset(ecfile2,'r')
ecdn = dset.variables['flux_up_sw'][:].reshape(dset.variables['flux_up_sw'].shape)
ecdn2 = ecdn
dset.close()
dset = ncdf.Dataset(ecfile3,'r')
ecdn = dset.variables['flux_up_sw'][:].reshape(dset.variables['flux_up_sw'].shape)
ecdn3 = ecdn
dset.close()
dset = ncdf.Dataset(ecfile,'r')
ecdn = dset.variables['flux_up_sw'][:].reshape(dset.variables['flux_up_sw'].shape)
ecdn = ecdn
dset.close()

dset = ncdf.Dataset(zfile,'r')
ecz = dset.variables['height_hl'][:].reshape(dset.variables['height_hl'].shape)[0,:]
ecf = dset.variables['cloud_fraction']
ecf = ecf[:].reshape(ecf.shape)[0,:]
dset.close()

eczf = 0.5*(ecz[:-1]+ecz[1:])
ecsza=[0,1,2]
ecsza=[0,22,35]
for i,f in enumerate(mcfile) : 
  dset = ncdf.Dataset(f,'r')
  if f==mcfile[0] : 
      mcz = dset.variables['vertical_levels'][:].reshape(dset.variables['vertical_levels'].shape)
      mcdn = np.zeros((len(ecsza),len(mcz)))
  mcdn[i,:] = dset.variables['flux_up_sw'][:].reshape(dset.variables['flux_up_sw'].shape)[:,0,0]
  dset.close()

vecsza=[0,44,70]
plt.figure()
plt.ylim(0.,3.)
plt.xlabel("Albedo [-]")
plt.ylabel("Height [km]")
print ecdn.shape
plt.plot([1,1],[1,1],color='black')
plt.plot([1,1],[1,1],color='black',linestyle='--')
#plt.plot([1,1],[1,1],color='black',linestyle='-.')
#plt.plot([1,1],[1,1],color='black',linestyle=':')
plt.plot([1,1],[1,1],color=basic3[1])
plt.plot([1,1],[1,1],color=basic3[2])
plt.plot([1,1],[1,1],color=basic3[3])
for sza in range(len(ecsza)) :
  plt.plot(mcdn[sza,:],mcz,color=basic3[sza+1],label='Monte Carlo SZA %i' %vecsza[sza])
  plt.plot(ecdn[ecsza[sza],:]/ecdn[ecsza[sza],0],ecz/1000.,color=basic3[sza+1],linestyle='--',label='ecRad, overlap LES, SZA %i' %vecsza[sza])
#  plt.plot(ecdn1[ecsza[sza],:]/ecdn1[ecsza[sza],0],ecz/1000.,color=basic3[sza+1],linestyle='--',label='ecRad, overlap LES, SZA %i' %vecsza[sza])
#  plt.plot(ecdn2[ecsza[sza],:]/ecdn2[ecsza[sza],0],ecz/1000.,color=basic3[sza+1],linestyle='-.',label='ecRad, overlap scaled, SZA %i' %vecsza[sza])
#  plt.plot(ecdn3[ecsza[sza],:]/ecdn3[ecsza[sza],0],ecz/1000.,color=basic3[sza+1],linestyle=':',label='ecRad, overlap default, SZA %i' %vecsza[sza])
#plt.legend(['Monte Carlo','overlap from LES','scaled overlap', 'default overlap','SZA 0','SZA 44','SZA 70'],loc="best",ncol=2)
plt.legend(['Monte Carlo','ECRAD','SZA 0','SZA 44','SZA 70'],loc="best",ncol=1)
#,colors=['black','black','black','black']+basic3[1:3]
#plt.savefig("down_fluxes_ecrad_scart_SLAB.pdf")
plt.savefig("up_fluxes_ecrad_scart_SLAB.pdf")
plt.close()

"""
f = path+'gem_pacific_3D_16_3DMC_noint.nc'
dset = ncdf.Dataset(f,'r')
rc = dset.variables['RCT']
rc = rc[:].reshape(rc.shape)
zz = dset.variables['mid_levels']
zz = zz[:].reshape(zz.shape)
dset.close()
mask = rc*0
mask[rc>1.0e-6]=1
cf = np.sum(mask,axis=(1,2))/(400*400)
plt.figure()
plt.ylim(0,12)
plt.plot(cf,zz)
plt.plot(ecf,eczf/1000.)
plt.show()

cols = np.where(np.max(mask,axis=0)>0)
print cols
print len(cols[0])
print len(cols[0])/(400.*400.)
"""
