import netCDF4 as ncdf
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import colors as clr
from mycolors import *
import matplotlib as mpl
mpl.rcParams.update({'font.size': 18})


class Obj(object) :
  def __init__(self,dic) :
    for k in dic :
        setattr(self,k,dic[k])
    setattr(self,'dic',dic)

  def __getattribute__(self,n) :
    if n=='dn' : return self.dic[dn]
    elif n=='dr' : return self.dic[dr]
    if n=='sdn' : return self.dic[sdn]
    elif n=='sdr' : return self.dic[sdr]
    elif n=='zz' : return self.dic[zz]
    else : return object.__getattribute__(self,n)
    
dn="flux_dn_sw"
dr="flux_dn_direct_sw"
sdn="sflux_dn_sw"
sdr="sflux_dn_direct_sw"
zz="z"

def get_var_1D(nc,name) :
  var =  nc.variables[name]
  return var[:].reshape(var.shape)

def get_var_4D(nc,name) :
  return get_var_1D(nc,name)[:,:,0,0]

def model_ec(ztab, vec, zi):
  i=-1
  while (ztab[i+1]>zi & i < length(ztab)): i=i+1
  if (i==-1) : i=0
  a = (vec[i+1]-vec[i])/(ztab[i+1]-ztab[i])
  return vec[i]+a*(zi-ztab[i]) 

def get_ecrad(ecfile) :
  nc  = ncdf.Dataset(ecfile, 'r')
  var = nc.variables[dn]
  ecdn = var[:].reshape(var.shape)
  var = nc.variables[dr]
  ecdir = var[:].reshape(var.shape)
  nc.close()
  return {dn : ecdn, dr : ecdir}

def get_input(infile) :
  nc  = ncdf.Dataset(infile, 'r')
  var = nc.variables[ "cos_solar_zenith_angle"]
  cos = var[:].reshape(var.shape)
  var = nc.variables["height_hl"]
  ecz = var[:].reshape(var.shape)[0,:]
  nc.close()
  return {"mu":cos, zz:ecz}
  
def get_mtcrl(mcfile):
  nc  = ncdf.Dataset(mcfile, 'r')
  mcz = get_var_1D(nc,"vertical_levels")
  mcdn = get_var_4D(nc,dn)
  smcdn = get_var_4D(nc,"std_"+dn)
  mcdir = get_var_4D(nc,dr)
  smcdir = get_var_4D(nc,"std_"+dr)
  nc.close()
  return {zz:mcz, dn:mcdn, dr:mcdir, 's'+dn:smcdn, 's'+dr:smcdir}

def set_mtcrl(mcfile):
  mcdat = Obj(get_mtcrl(mcfile))
  mcdat.ratio = mcdat.dr/mcdat.dn
  mcdat.minratio = (mcdat.dr-3.*mcdat.sdr)/(mcdat.dn+3*mcdat.sdn)
  mcdat.maxratio = (mcdat.dr+3*mcdat.sdr)/(mcdat.dn-3*mcdat.sdn)
  return mcdat

def plt_mtcrl(vecsza,mcdat,color,label,linestyle='-',linewidth=2,met='ratio'):
  if met=='ratio' :
    toplt = mcdat.ratio[:,0]
    topltmin = mcdat.minratio[:,0]
    topltmax = mcdat.maxratio[:,0]
  elif met==dn :
    smet = 's'+met
    toplt = mcdat.dn[:,0]
    topltmin = mcdat.dn[:,0]-3*mcdat.sdn[:,0]
    topltmax = mcdat.dn[:,0]+3*mcdat.sdn[:,0]
  elif met==dr :
    smet = 's'+met
    toplt = mcdat.dr[:,0]
    topltmin = mcdat.dr[:,0]-3*mcdat.sdr[:,0]
    topltmax = mcdat.dr[:,0]+3*mcdat.sdr[:,0]
  plt.plot(vecsza,toplt,color=color,linestyle=linestyle,linewidth=linewidth)
  plt.fill_between(vecsza,topltmin,topltmax,label=label,color=color,alpha=.4)

ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_spartacus_out.nc'  
ecdat1 = Obj(get_ecrad(ecfile))
ecdat1.ratio = ecdat1.dr/ecdat1.dn
ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_miscaling_spartacus_out.nc'  
ecdat11 = Obj(get_ecrad(ecfile))
ecdat11.ratio = ecdat11.dr/ecdat11.dn
ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_samecov_spartacus_out.nc'  
ecdat12 = Obj(get_ecrad(ecfile))
ecdat12.ratio = ecdat12.dr/ecdat12.dn

ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_tripleclouds_out.nc'  
ecdat2 = Obj(get_ecrad(ecfile))
ecdat2.ratio = ecdat2.dr/ecdat2.dn
ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_miscaling_tripleclouds_out.nc'  
ecdat21 = Obj(get_ecrad(ecfile))
ecdat21.ratio = ecdat21.dr/ecdat21.dn
ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_samecov_tripleclouds_out.nc'  
ecdat22 = Obj(get_ecrad(ecfile))
ecdat22.ratio = ecdat22.dr/ecdat22.dn

infile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza.nc'  
indat = Obj(get_input(infile))

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0.nc'
mcdat0 = set_mtcrl(mcfile)

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc1.nc'
mcdat1 = set_mtcrl(mcfile)

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0_instru.nc'
mcdat2 = set_mtcrl(mcfile)

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0_cutnose.nc'
mcdat3 = set_mtcrl(mcfile)
mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0_HG.nc'
mcdat4 = set_mtcrl(mcfile)




ncfl='/home/villefranquen/Work/NCDF/ARMCU/TIMESTEP/ARM1630.nc'
dset = ncdf.Dataset(ncfl, 'r')
tmp = dset.variables['RCT']
rcz = tmp[0,:,:,:]
rc  = np.sum(rcz,axis=0)
rho_w = 1000.
r_eff = 10.0e-6
ke  = 3./2.*rc/(rho_w*r_eff)
od  = ke*25. # meters (vertical resolution)
xx=np.linspace(0,6.4,257)
yy=np.linspace(0,6.4,257)
r=range(0,256,40)
print xx[r]
#w, h = plt.figaspect(1./1.8)

plt.figure(figsize=(23,6.5))
ax1 = plt.subplot2grid((3, 3), (0, 0), rowspan=3)
ax2 = plt.subplot2grid((3, 3), (0, 1), colspan=2,rowspan=3)

im=ax1.imshow(od,cmap=mpl.cm.Blues,norm=clr.LogNorm(vmin=1,vmax=10),origin='lower')
ax1.contour(od,[0],origin="lower",colors="gray")
ax1.plot([0,128], [4/0.025]*2,'k--',linewidth=3)
ax1.set_xlim(0,256)
ax1.set_ylim(0,256)
ax1.set_aspect(1.,'box')
ax1.set_xticklabels(['%.1f'%xx[u] for u in range(0,251,50)])
ax1.set_yticklabels(['%.1f'%xx[u] for u in range(0,251,50)])
ax1.set_xlabel(r"$x$ [km]",fontsize=22)
ax1.set_ylabel(r"$y$ [km]",fontsize=22)
cbar=plt.colorbar(im,ax=ax1,shrink=.8)
cbar.ax.tick_params(labelsize=18)
cbar.set_label("Optical depth",labelpad=-20,fontsize=20)

vecsza = np.arccos(indat.mu)*180./np.pi
ax2.set_xlim(0.,90.)
ax2.set_xlabel(r"Solar Zenith Angle [$^o$]",fontsize=22)
ax2.set_ylabel(r"SW direct / global [W.m$^{-2}$]",fontsize=22)
#plt.title("SW direct / global at the surface, ecRad vs Monte Carlo \n ARM-Cu (Brown et al. 2002) LES MesoNH (Lac et al. 2018)",fontsize=20)
#plt.plot(vecsza,ecdat1.ratio[:,-1],color=basic3[1],label="ecRad SPARTACUS",linewidth=1.2,linestyle='--')
#plt.plot(vecsza,ecdat2.ratio[:,-1],color=basic3[1],label="ecRad TripleClouds",linewidth=1.2,linestyle='-.')
#plt.plot(vecsza,ecdat11.ratio[:,-1],color=basic3[2],label="ecRad SPARTACUS miscaling",linewidth=1.2,linestyle='--')
#plt.plot(vecsza,ecdat21.ratio[:,-1],color=basic3[2],label="ecRad TripleClouds miscaling",linewidth=1.2,linestyle='-.')
ax2.plot(vecsza,ecdat22.ratio[:,-1],color=basic3[0],label="ecRad 1D (TripleClouds)",linewidth=3,linestyle='-.')
ax2.plot(vecsza,ecdat12.ratio[:,-1],color=basic3[0],label="ecRad 3D (SPARTACUS)",linewidth=3,linestyle='--')
#plt_mtcrl(vecsza,mcdat3,basic3[2],"Monte Carlo Mie cut 2.5$^o$",linestyle='-')
plt_mtcrl(vecsza,mcdat0,basic3[1],"Monte Carlo Mie $\pm 3\sigma$",linewidth=.5,linestyle='-')
plt_mtcrl(vecsza,mcdat1,basic3[3],"Monte Carlo delta scaled HG $\pm 3\sigma$",linewidth=.5,linestyle='-')
#plt_mtcrl(vecsza,mcdat4,basic3[2],"Monte Carlo HG $\pm 3\sigma$",linewidth=.5,linestyle='-')
plt.legend(loc="best",fontsize=20)
#plt.tight_layout()
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")
plt.subplots_adjust(top=.95,left=0.1,wspace=.15,hspace=0.05)
plt.savefig("test_ratio_ecRad_SCART.png")
plt.show()
