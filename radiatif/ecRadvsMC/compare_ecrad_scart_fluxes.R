library("ncdf4")

ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza_spartacus_out.nc'  
nc  = nc_open(ecfile)
ecdn = ncvar_get(nc, "flux_dn_sw")
ecdir = ncvar_get(nc, "flux_dn_direct_sw")
nc_close(nc)
ecfile = '/home/villefranquen/Work/codes_analyse/ecrad-1.0.14/test/naj/i3rc_mls_cumulus_ARMCu008_sza.nc'  
nc  = nc_open(ecfile)
cos = ncvar_get(nc, "cos_solar_zenith_angle")
ecz=ncvar_get(nc,"height_hl")
ecz=ecz[,1]
nc_close(nc)

mcfile='/home/villefranquen/Work/SCART/scart_project/build/ratio/ARM008_atm1_dsc0.nc'
nc  = nc_open(mcfile)
mcz = ncvar_get(nc,"vertical_levels")
mcdn = ncvar_get(nc,"flux_dn_sw")
mcdir =  ncvar_get(nc,"flux_dn_direct_sw")
nc_close(nc)

vecsza = acos(cos)*180./pi

model.ec <- function(ztab, vec, zi)
{
  i=0
  while (ztab[i+1]>zi & i+1 < length(ztab))
  {
    i=i+1
  }
  if (i==0) { i=1 }
  a = (vec[i+1]-vec[i])/(ztab[i+1]-ztab[i])
  return(vec[i]+a*(zi-ztab[i]))
}

pdf("down_fluxes_ecrad_scart.pdf")
plot(c(-0.,2000.),c(0.,4.),type="n", xlab = "Flux down [W/m^2]", ylab="z [km]", main="Radiative downward fluxes [W/m^2]")
for (sza in seq(length(ecdn[1,]))) {
lines(mcdn[,sza],mcz,type="l",col="purple")
lines(ecdn[,sza],ecz/1000.,type="l",col="purple",lty=2)
#lines(mcdir[,sza],mcz,type="l",col="black")
#lines(ecdir[,sza],ecz/1000.,type="l",col="black",lty=2)
}
legend("bottomright",legend=c("ecRad","Monte Carlo","Total","Direct"), col=c("purple","purple","black","black"), lty=c(1,2,1,2))
dev.off()

mcratio = mcdir/mcdn
ecratio = ecdir/ecdn

pdf("ratio.pdf")
plot(c(0,90),c(0.,1.),type="n")
lines(vecsza, head(mcratio,1),col="purple")
lines(vecsza, tail(ecratio,1),col="black")
legend("topright",legend=c("ecRad","Monte Carlo"), col=c("black","purple"),lty=1)
dev.off()

pdf("zoom_down_fluxes_ecrad_scart.pdf")
par(mfrow=c(2,2))
plot(c(-0.,2000.),c(0.,4.),type="n", xlab = "Flux down [W/m^2]", ylab="z [km]", main="Radiative downward fluxes [W/m^2]")
for (sza in seq(length(ecdn[1,]))) {
  lines(mcdn[,sza],mcz,type="l",col="purple")
}
plot(c(-0.,2000.),c(0.,4.),type="n", xlab = "Flux down [W/m^2]", ylab="z [km]", main="Radiative downward fluxes [W/m^2]")
for (sza in seq(length(ecdn[1,]))) {
  lines(ecdn[,sza],ecz/1000.,type="l",col="purple",lty=2)
}
plot(c(-0.,2000.),c(0.,4.),type="n", xlab = "Flux down [W/m^2]", ylab="z [km]", main="Radiative downward fluxes [W/m^2]")
for (sza in seq(length(ecdn[1,]))) {
  lines(mcdir[,sza],mcz,type="l",col="black")
}
plot(c(-0.,2000.),c(0.,4.),type="n", xlab = "Flux down [W/m^2]", ylab="z [km]", main="Radiative downward fluxes [W/m^2]")
for (sza in seq(length(ecdn[1,]))) {
  lines(ecdir[,sza],ecz/1000.,type="l",col="black",lty=2)
}
legend("bottomright",legend=c("ecRad","Monte Carlo","Total","Direct"), col=c("purple","purple","black","black"), lty=c(1,2,1,2))
dev.off()

