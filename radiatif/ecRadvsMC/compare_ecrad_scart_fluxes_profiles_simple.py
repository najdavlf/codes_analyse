import netCDF4 as ncdf
import numpy as np
from matplotlib import pyplot as plt
from mycolors import *
import sys

if len(sys.argv)!=4 :
  print "Usage: python",sys.argv[0],"ecrad_in ecrad_out mc_out"
  exit(1)

zfile=sys.argv[1]
ecfile=sys.argv[2]
mcfile=sys.argv[3]

dset = ncdf.Dataset(zfile,'r')
ecz = np.squeeze(dset.variables['height_hl'][:])
dset.close()

dset = ncdf.Dataset(ecfile,'r')
ecdn = np.squeeze(dset.variables['flux_dn_sw'][:])
ecup = np.squeeze(dset.variables['flux_up_sw'][:])
dset.close()

dset = ncdf.Dataset(mcfile,'r')
mcz = np.squeeze(dset.variables['vertical_levels'][:])
mcdn = np.squeeze(dset.variables['flux_dn_sw'][:])
mcup = np.squeeze(dset.variables['flux_up_sw'][:])
dset.close()

plt.figure()
plt.xlabel("Flux down [W/m2]")
plt.ylabel("Height [km]")
plt.plot(mcdn,mcz,color=basic3[1],label='Monte Carlo')
plt.plot(ecdn,ecz/1000.,color=basic3[2],label='ecRad')
#plt.plot(mcup,mcz,color=basic3[1])
#plt.plot(ecup,ecz/1000.,color=basic3[2])
#plt.plot(mcdn-mcup,mcz,color=basic3[1])
#plt.plot(ecdn-ecup,ecz/1000.,color=basic3[2])
plt.legend(loc="best",ncol=1)
plt.savefig("up_dn_fluxes_ecrad_scart.pdf")
plt.show()
plt.close()
