import netCDF4 as ncdf
import numpy as np
from matplotlib import pyplot as plt
from mycolors import *
import sys

if len(sys.argv)<4 :
  print "Usage: python",sys.argv[0],"ecrad_in ecrad_out mc_out [figname_prefix]"
  exit(1)

zfile=sys.argv[1]
ecfile=sys.argv[2]
mcfile=sys.argv[3]
if len(sys.argv)==5 : figname_prefix=sys.argv[4]

dset = ncdf.Dataset(zfile,'r')
ecz = np.squeeze(dset.variables['height_hl'][:])
dset.close()

dset = ncdf.Dataset(ecfile,'r')
ecdn = np.squeeze(dset.variables['flux_dn_sw'][:])
ecdi = np.squeeze(dset.variables['flux_dn_direct_sw'][:]) # unscattered flux
ecup = np.squeeze(dset.variables['flux_up_sw'][:])
dset.close()

dset = ncdf.Dataset(mcfile,'r')
mcz = np.squeeze(dset.variables['vertical_levels'][:])
mcdn = np.squeeze(dset.variables['flux_dn_sw'][:])
std_mcdn = np.squeeze(dset.variables['std_flux_dn_sw'][:])
mcdi = np.squeeze(dset.variables['flux_dn_direct_sw'][:])
std_mcdi = np.squeeze(dset.variables['std_flux_dn_direct_sw'][:])
mcup = np.squeeze(dset.variables['flux_up_sw'][:])
dset.close()

plt.figure()
plt.xlabel("Diffuse flux down [W/m2]")
plt.ylabel("Height [km]")
plt.plot(mcdn-mcdi,mcz,color=basic3[1],label='Monte Carlo')
plt.plot(ecdn-ecdi,ecz/1000.,color=basic3[2],label='ecRad')
#plt.plot(mcdn-mcup,mcz,color=basic3[1])
#plt.plot(ecdn-ecup,ecz/1000.,color=basic3[2])
plt.legend(loc="best",ncol=1)
plt.savefig(figname_prefix+"dif_fluxes_ecrad_scart.pdf")
plt.show()

plt.figure()
plt.xlabel("Direct flux down [W/m2]")
plt.ylabel("Height [km]")
plt.plot(mcdi,mcz,color=basic3[1],label='Monte Carlo')
plt.plot(ecdi,ecz/1000.,color=basic3[2],label='ecRad')
plt.legend(loc="best",ncol=1)
plt.savefig(figname_prefix+"dir_fluxes_allprof_ecrad_scart.pdf")
plt.ylim(-0.01,0.1)
plt.plot([mcdi[0]-std_mcdi[0],mcdi[0]+std_mcdi[0]], [mcz[0],mcz[0]],"b--")
plt.text(1225,.05,"ec-mc surf : %g"%(ecdi[-1]-mcdi[0]))
plt.savefig(figname_prefix+"dir_fluxes_zoomsurf_ecrad_scart.pdf")
plt.show()

plt.figure()
plt.xlabel("ecRad - MC [W/m2]")
plt.ylabel("Height [km]")
plt.plot(np.interp(mcz,ecz[::-1]/1000.,ecdi[::-1])-mcdi,mcz, color=basic2[1],label="direct")
plt.fill_betweenx(mcz,-3*std_mcdi,3*std_mcdi,color=basic2[1],alpha=.5)
plt.plot(np.interp(mcz,ecz[::-1]/1000.,ecdn[::-1])-mcdn,mcz, linestyle="--",color=basic2[2],label="total")
plt.fill_betweenx(mcz,-3*std_mcdn,3*std_mcdn,color=basic2[2],alpha=.5)
plt.legend()
plt.show()

plt.figure()
plt.xlabel("Total flux down [W/m2]")
plt.ylabel("Height [km]")
plt.plot(mcdn,mcz,color=basic3[1],label='Monte Carlo')
plt.plot(ecdn,ecz/1000.,color=basic3[2],label='ecRad')
#plt.plot(mcdn-mcup,mcz,color=basic3[1])
#plt.plot(ecdn-ecup,ecz/1000.,color=basic3[2])
plt.legend(loc="best",ncol=1)
plt.savefig(figname_prefix+"tot_fluxes_ecrad_scart.pdf")
plt.show()
