
from matplotlib import pyplot as plt
import netCDF4 as ncdf
import numpy as np 

ncpath='/home/villefranquen/Work/NCDF/MIE/'
ncfile=ncpath+'Mie_LUT_Cloud.nc'

dset = ncdf.Dataset(ncfile,"r")

pdf = dset.variables["pdf"]
pdf = pdf[:].reshape(pdf.shape)[0,:,:]
the = dset.variables["th"]
the = the[:].reshape(the.shape)
lam = dset.variables["lambda"]
lam = lam[:].reshape(lam.shape)
pdf800 = pdf[lam==800,:]
pdf800 = pdf800[0,:]

cth = np.cos(the)
print cth
dmu = -(cth[1:]-cth[:-1])
midpdf = .5*(pdf800[1:]+pdf800[:-1])
midcth = .5*(cth[1:]+cth[:-1])
mupdf = midpdf*midcth
mupdfdmu = mupdf*dmu

g = np.sum(mupdfdmu)
f = g*g
dscaled_g1 = (g-f)/(1.-f)
f = .5*g*g
dscaled_g2 = (g-f)/(1.-f)

k=0
for i in range(len(midcth)):
    if abs(np.sum(mupdfdmu[i:])/dscaled_g1 -1.) < .01 or abs(np.sum(mupdfdmu[i:])/dscaled_g2 -1.) < .02:
      print np.arccos(midcth[i])*180./np.pi, np.sum(mupdfdmu[i:])
      if k==0 : l='.5g$^2$'
      if k==1 : l='g$^2$'
      plt.plot(midcth[i:],np.cumsum(mupdfdmu[i:]),label='lambda=800nm;cut angle: '+str(np.arccos(midcth[i])*180./np.pi)+', f='+l)
      k+=1

pdf800 = pdf[lam==300,:]
pdf800 = pdf800[0,:]

cth = np.cos(the)
print cth
dmu = -(cth[1:]-cth[:-1])
midpdf = .5*(pdf800[1:]+pdf800[:-1])
midcth = .5*(cth[1:]+cth[:-1])
mupdf = midpdf*midcth
mupdfdmu = mupdf*dmu

g = np.sum(mupdfdmu)
f = g*g
dscaled_g1 = (g-f)/(1.-f)
f = .5*g*g
dscaled_g2 = (g-f)/(1.-f)

k=0
for i in range(len(midcth)):
    if abs(np.sum(mupdfdmu[i:])/dscaled_g1 -1.) < .015 or abs(np.sum(mupdfdmu[i:])/dscaled_g2 -1.) < .03:
      print np.arccos(midcth[i])*180./np.pi, np.sum(mupdfdmu[i:])
      if k==0 : l='.5g$^2$'
      if k==1 : l='g$^2$'
      plt.plot(midcth[i:],np.cumsum(mupdfdmu[i:]),label='lambda=300nm; cut angle: '+str(np.arccos(midcth[i])*180./np.pi)+', f='+l,linestyle='--')
      k+=1

plt.xlabel("cos theta")
plt.ylabel("cumulative pdf * costheta * dcostheta")
plt.legend(loc="best")
plt.show()

