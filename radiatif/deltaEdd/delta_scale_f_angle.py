
from mycolors import basic2
from matplotlib import pyplot as plt
import netCDF4 as ncdf
import numpy as np 

ncpath='/home/villefranquen/Work/NCDF/MIE/'
ncfile=ncpath+'Mie_LUT_Cloud.nc'

def gds(g,a): # returns delta scaled g as a function of g and a with f=agg
    f = g*g*a
    return (g-f)/(1.-f)

def inv(g,mpd,mmu): 
    # mpd(mu) is the mu*pdf(mu)*dmu function of mu. Its integral from mu=-1 to mu=1 is unscaled g. 
    # The function returns the mu' that is the upper bound of the integral that gives integral of mpd(mu) from mu=-1 to mu=mu' equals to scaled g
    for i in range(len(mmu)):
      if abs(np.sum(mupdfdmu[i:])/dsg - 1.) < .01:
        print g, np.arccos(midcth[i])*180./np.pi, np.sum(mupdfdmu[i:])
        return mmu[i]
    return 1.
    
def pdfSintDt(midpdf,midth,dth) :
  pdfsinth = midpdf*np.sin(midth)
  pdfsinthdth = pdfsinth*dth
  return pdfsinthdth

def pdfCostDt(midpdf,midcth,dcth) :
  pdfcosth = midpdf*midcth
  pdfcosthdth = pdfcosth*(-dcth)
  return pdfcosthdth

def HG(g,midth) :
  return 1./2.*(1.-g*g)/((1.+g*g -2.*g*np.cos(midth))**(3./2.))

def ktoa(k):
  f=1.-k
  g=.86 # f=a*g*g
  a1=f/(g*g)
  return ["%.2f"%(v) for v in a1]

def atok(a):
  g=.86
  f=a*g*g
  return 1.-f


dset = ncdf.Dataset(ncfile,"r")

pdf = dset.variables["pdf"]
pdf = pdf[:].reshape(pdf.shape)[0,:,:]
the = dset.variables["th"]
the = the[:].reshape(the.shape)
lam = dset.variables["lambda"]
lam = lam[:].reshape(lam.shape)
cth = np.cos(the)

dth = (the[1:]-the[:-1])
midth = .5*(the[1:]+the[:-1])
dcth = (cth[1:]-cth[:-1])
midcth = .5*(cth[1:]+cth[:-1])
lamvec = np.linspace(400,900,2)
angle=np.linspace(0.1,15.)
maxthe = the[:300]
print len(the)
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twinx()
ymin=.2
newtickslocations=np.linspace(ymin,1.,15)

for il,l in enumerate(lamvec) :
  kscaleMie = 0.*maxthe
  kscaleHG  = 0.*maxthe
  for ia,a in enumerate(maxthe):
    pdfMie=pdf[lam==l]
    midpdfMie = (pdfMie[0,1:]+pdfMie[0,:-1])*.5
    gMie      = np.sum(pdfCostDt(midpdfMie, midcth, dcth))
    midpdfMie = pdfSintDt(midpdfMie, midth, dth)
    midpdfHG  = HG(gMie,midth)
    midpdfHG  = pdfSintDt(midpdfHG , midth, dth)
    kscaleMie[ia] = 1.-np.sum(midpdfMie[:ia])
    kscaleHG[ia]  = 1.-np.sum(midpdfHG[:ia])
  ax1.plot(maxthe*180./np.pi,kscaleMie,linewidth=2.,color=basic2[il+1],label='Mie, lambda='+str(int(l))+'nm, g=%3.3f'%(gMie),linestyle='-')
# ax1.plot(maxthe*180./np.pi,kscaleHG ,linewidth=2.,color=basic2[il+1],label='HG,  lambda='+str(int(l))+'nm, g=%3.3f'%(gMie),linestyle='--')
ax1.axvline(x=2.85,linewidth=2.,color="black")
ax1.axhline(y=atok(1.),linestyle=':',linewidth=2.,color="black")
#ax1.axhline(y=atok(.5),linewidth=1.,linestyle=':',color="black")
ax1.fill_between([ax1.get_xlim()[0],ax1.get_xlim()[1]],atok(.571),atok(.627),color=basic2[3])
ax1.text(2.9,.22,'2.85')
#ax1.axhline(y=atok(.62),linewidth=1.,color="black")
ax1.legend(fontsize=16)
ax2.set_ylim(ax1.get_ylim())
ax2.set_yticks(newtickslocations)
ax2.set_yticklabels(ktoa(newtickslocations))
ax1.set_ylabel("k",fontsize=18)
ax2.set_ylabel("$\\alpha$, with $k=1-\\alpha g^2$, $g=.86$",fontsize=18)
ax1.set_ylim(ymin,1)
ax1.set_xlabel("Half opening angle [$^o$]",fontsize=18)
ax1.set_title("Delta Eddington scaling factor, $\\tau'=k\\tau$",fontsize=20)
plt.tight_layout()
plt.savefig("delta_scale_analysis.svg")
plt.savefig("delta_scale_analysis.png")
plt.show()
