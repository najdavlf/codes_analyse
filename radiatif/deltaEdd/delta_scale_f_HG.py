
from matplotlib import pyplot as plt
import netCDF4 as ncdf
import numpy as np 

def gds(g,a): # returns delta scaled g as a function of g and a with f=agg
    f = g*g*a
    return (g-f)/(1.-f)

def inv(g,mpd,mmu): 
    # mpd(mu) is the mu*pdf(mu)*dmu function of mu. Its integral from mu=-1 to mu=1 is unscaled g. 
    # The function returns the mu' that is the upper bound of the integral that gives integral of mpd(mu) from mu=-1 to mu=mu' equals to scaled g
    for i in range(len(mmu)):
      if abs(np.sum(mupdfdmu[i:])/dsg - 1.) < .01:
        print g, np.arccos(midcth[i])*180./np.pi, np.sum(mupdfdmu[i:])
        return mmu[i]
    return 1.
    
def dir(g,midth,dth) :
  midpdf=1./2.*(1.-g*g)/((1.+g*g -2.*g*np.cos(midth))**(3./2.))
  pdfsinth = midpdf*np.sin(midth)
  pdfsinthdth = pdfsinth*dth
  return pdfsinthdth

the = np.linspace(0,np.pi,800)
dth = (the[1:]-the[:-1])
midth = .5*(the[1:]+the[:-1])
gvec = np.linspace(0.852,0.866,2)
angle=np.linspace(0.1,15.)

for g in gvec :
  kscale = 0.*the[:100]
  for ia,a in enumerate(the[:100]):
    pdfstdt=dir(g,midth,dth)
    print np.sum(pdfstdt)
    kscale[ia]=1.-np.sum(pdfstdt[:ia])
  #plt.plot(np.arccos(mu)*180./np.pi,(1.-avec*g*g),label='g='+str(g))
  plt.plot(the[:100]*180./np.pi,kscale,label='g='+str(g))
#plt.plot(angle/angle*2.5,kscale,linewidth=2.,color="black")
plt.legend()
#plt.xlim(0,5)
plt.ylim(.4,1)
plt.show()

"""
plt.ylim(0,1)
plt.xlabel("cos theta")
plt.ylabel("cumulative pdf * costheta * dcostheta")
plt.legend(loc="best")
plt.show()
"""
