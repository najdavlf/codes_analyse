
from matplotlib import pyplot as plt
import netCDF4 as ncdf
import numpy as np 

def gds(g,a): # returns delta scaled g as a function of g and a with f=agg
    f = g*g*a
    return (g-f)/(1.-f)

def inv(g,mpd,mmu): 
    # mpd(mu) is the mu*pdf(mu)*dmu function of mu. Its integral from mu=-1 to mu=1 is unscaled g. 
    # The function returns the mu' that is the upper bound of the integral that gives integral of mpd(mu) from mu=-1 to mu=mu' equals to scaled g
    for i in range(len(mmu)):
      if abs(np.sum(mupdfdmu[i:])/dsg - 1.) < .01:
        print g, np.arccos(midcth[i])*180./np.pi, np.sum(mupdfdmu[i:])
        return mmu[i]
    return 1.
    
    
def dir(g,midcth) :
  pdf = 1./2.*(1.-g*g)/((1.+g*g -2.*g*cth)**(3./2.))
  midpdf = .5*(pdf[1:]+pdf[:-1])
  mupdf = midpdf*midcth
  mupdfdmu = mupdf*dmu
  return mupdfdmu


the = np.linspace(0,3.14,800)
cth = np.cos(the)
dmu = -(cth[1:]-cth[:-1])
midcth = .5*(cth[1:]+cth[:-1])
gvec = np.linspace(0.852,0.866,2)
avec=np.linspace(0,1.)

for g in gvec :
  mu = 0.*avec
  for ia,a in enumerate(avec):
    dsg = gds(g,a)
    mupdfdmu=dir(g,midcth)
    mu[ia]=inv(dsg,mupdfdmu,midcth)
  #plt.plot(np.arccos(mu)*180./np.pi,(1.-avec*g*g),label='g='+str(g))
  plt.plot((mu),(1.-avec*g*g),label='g='+str(g))
plt.plot(avec/avec*np.cos(2.5/180.*np.pi),avec,linewidth=2.,color="black")
plt.legend()
#plt.xlim(0,5)
plt.ylim(.4,1)
plt.show()

"""
plt.ylim(0,1)
plt.xlabel("cos theta")
plt.ylabel("cumulative pdf * costheta * dcostheta")
plt.legend(loc="best")
plt.show()
"""
