from mycolors import basic6 as cols
import matplotlib.pyplot as plt
import matplotlib as mpl
import netCDF4 as nc
import numpy as np
import sys

plt.style.use("manuscript")
#mpl.rcParams["lines.linewidth"]=1
mpl.rcParams["axes.edgecolor"]=cols[-3]
mpl.rcParams["grid.color"]=cols[-4]
mpl.rcParams["grid.linestyle"]="-"

# En argument, liste des netcdf out de Monte Carlo avec SZA concatene
if len(sys.argv) < 2 : print "Usage: python",sys.argv[0],"theta_vec" ; exit(1)

f = open(sys.argv[1])
sza = [float(u) for u in f]
print sza

# BOMEX 004 007 010 013 
tsteps=["004","007","008","009","010","013"]
# besoin des sorties de MC dans hightune
htpath="/home/villefranquen/Work/HIGHTUNE/HighTune/"
les1Dp=htpath+"LES1D_ecRad/"
ecpath="/home/villefranquen/Work/ecrad-1.1.0/test/amas/"
for c,(pre,cas) in enumerate(zip(["VMFTH","CERK4"],["BOMEX","ARMCU"])):
  i=1
  w,h = plt.figaspect(1)
  plt.figure(figsize=(1.5*w,1.5*h))
  ax1 = plt.subplot2grid((4,3),(0,0),rowspan=1,colspan=3)
  ax5 = plt.subplot2grid((4,3),(1,0),rowspan=1,colspan=3,sharex=ax1)
  ax2 = plt.subplot2grid((4,3),(2,0),rowspan=2)
  ax3 = plt.subplot2grid((4,3),(2,1),rowspan=2,sharey=ax2)
  ax4 = plt.subplot2grid((4,3),(2,2),rowspan=2,sharey=ax3)

  mcpath=htpath+"RAD/"+cas+"/REF/"
  for t in tsteps: 
    print cas,t
    f=mcpath+"RAD"+t+".nc"
    d = nc.Dataset(f,"r")
    dntoa  = np.squeeze(d.variables["flux_dn_sw"][:,-1])
    dnsurf = np.squeeze(d.variables["flux_dn_sw"][:,0])
    ax1.plot(sza,dnsurf/dntoa,color=cols[i],label=" ".join([cas,t]))
    d.close()

    mctran = dnsurf/dntoa

    f=ecpath+"out_"+cas+"-REF-"+t+"_1D_sza.nc"
    d = nc.Dataset(f,"r")
    dntoa  = np.squeeze(d.variables["flux_dn_sw"][:,0])
    dnsurf = np.squeeze(d.variables["flux_dn_sw"][:,-1])
    ax5.plot(sza,(dnsurf/dntoa-mctran)/mctran*100.,color=cols[i],linestyle="-",linewidth=1)
    d.close()

    f=ecpath+"out_default_"+cas+"-REF-"+t+"_1D_sza.nc"
    d = nc.Dataset(f,"r")
    dntoa  = np.squeeze(d.variables["flux_dn_sw"][:,0])
    dnsurf = np.squeeze(d.variables["flux_dn_sw"][:,-1])
    ax5.plot(sza,(dnsurf/dntoa-mctran)/mctran*100.,color=cols[i],linestyle="--",linewidth=1)
    d.close()

    f=les1Dp+pre+".1."+cas+"."+t+"_1D.nc"
    d = nc.Dataset(f,"r")
    vlv = np.squeeze(d.variables["height_hl"])[::-1]/1000.
    fsd = np.squeeze(d.variables["fractional_std"])[::-1]
    ovp = np.squeeze(d.variables["overlap_param"])[::-1]
    csc = 1./np.squeeze(d.variables["inv_cloud_effective_size"])[::-1]
    mid = .5*(vlv[1:]+vlv[:-1])
    ax2.plot(fsd[fsd>0],mid[fsd>0],color=cols[i])
    ax2.plot(fsd*0.+.75,mid,color=cols[-3],linestyle="--",linewidth=.5)
    ax3.plot(ovp[(ovp>0)&(ovp<1)],vlv[1:-1][(ovp>0)&(ovp<1)],color=cols[i])
    ax3.plot(np.exp(ovp*0.-25./2000.),vlv[1:-1],color=cols[-3],linestyle="--",linewidth=.5)
    ax4.plot(csc,mid,color=cols[i])
    ax4.plot(fsd*0.+1000,mid,color=cols[-3],linestyle="--",linewidth=.5)

    i+=1
  ax1.set_title("Reference transmissivity (Monte Carlo)")
  ax5.set_title("Relative error in transmissivity (ecRad - Monte Carlo)")
  ax2.set_title("Fractional std")
  ax3.set_title("Overlap param")
  ax4.set_title("Cloud scale [m]")
  ax2.set_ylabel("Height [km]")
  ax2.set_ylim(0,4)
  ax3.set_ylim(0,4)
  ax3.set_xlim(0,1.1)
  ax4.set_ylim(0,4)
  ax4.set_xlim(0,1100)
  ax1.legend(ncol=3,loc="best")
  plt.tight_layout()
  plt.savefig(cas+"_dnsurf_caracs.svg")
plt.show()
