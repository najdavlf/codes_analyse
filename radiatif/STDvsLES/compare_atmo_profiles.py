
import netCDF4 as nc
import numpy as np
from matplotlib import pyplot as plt

# plot les profils de temp, press et q pour i3rc (utilise pour produire les donnees gaz), standard tropical atmo et ensemble des LES (pour l'instant juste ARMCu mais rajouter les autres cas)
# => quelles diff sur les profils de ka ? sur les profils de flux ?

fi3rc = "/home/villefranquen/Work/ecrad-1.0.1/test/i3rc/i3rc_mls_cumulus.nc"
ftrop = "/home/villefranquen/Work/NCDF/ATM1D/STROATM.nc"
farmc = "/home/villefranquen/Work/NCDF/ARMCU/CERK4/CERK4.1.ARMCu.000KCL.nc"

ni3rc = nc.Dataset(fi3rc,"r")
ntrop = nc.Dataset(ftrop,"r")
narmc = nc.Dataset(farmc,"r")

z = ni3rc.variables['height_hl']
zi3rc = z[0,:]/1000.
zi3rc_fl = 0.5*(zi3rc[:-1]+zi3rc[1:])
z = ntrop.variables['vertical_levels']
ztrop = z[:].reshape(z.shape)
z = narmc.variables['vertical_levels']
zarmc = z[:].reshape(z.shape)/1000.

t = ni3rc.variables['temperature_hl']
ti3rc = t[0,:]
t = ntrop.variables['MEAN_T']
ttrop = t[:].reshape(t.shape)
t = narmc.variables['temp']
tarmc = t[:].reshape(t.shape)


p = ni3rc.variables['pressure_hl']
pi3rc = p[0,:]/100.
p = ntrop.variables['MEAN_P']
ptrop = p[:].reshape(p.shape)
p = narmc.variables['pf']
parmc = p[:].reshape(p.shape)/100.

v = ni3rc.variables['q']
vi3rc = v[0,:]*1000.
v = ntrop.variables['MEAN_VC']
vtrop = v[:].reshape(v.shape)
v = narmc.variables['qv']
varmc = v[:].reshape(v.shape)*1000.

ni3rc.close()
ntrop.close()

plt.figure()

plt.subplot(131)
plt.plot(ti3rc,zi3rc,'r--',linewidth=2)
plt.plot(ttrop,ztrop,'k-',linewidth=2)
for it in range(tarmc.shape[0]):
    plt.plot(tarmc[it,:,0,0],zarmc,'gray',alpha=.1)
plt.ylim(0,50)
#plt.xlim(260,320)
plt.legend(labels=["i3rc","std trop","ARMCu"],loc="upper right")
plt.grid()
plt.xlabel("T (K)")
plt.ylabel("z (km)")
plt.title("Temperature")

plt.subplot(132)
plt.plot(pi3rc,zi3rc,'r--',linewidth=2)
plt.plot(ptrop,ztrop,'k-',linewidth=2)
for it in range(tarmc.shape[0]):
    plt.plot(parmc[it,:,0,0],zarmc,'gray',alpha=.1)
plt.ylim(0,50)
#plt.xlim(500,1100)
plt.legend(labels=["i3rc","std trop","ARMCu"],loc="upper right")
plt.grid()
plt.xlabel("P (hPa)")
plt.ylabel("z (km)")
plt.title("Pressure")

plt.subplot(133)
plt.plot(vi3rc,zi3rc_fl,'r--',linewidth=2)
plt.plot(vtrop,ztrop,'k-',linewidth=2)
for it in range(tarmc.shape[0]):
    plt.plot(varmc[it,:,0,0],zarmc,'gray',alpha=.1)
plt.ylim(0,50)
plt.legend(labels=["i3rc","std trop","ARMCu"],loc="upper right")
plt.grid()
plt.xlabel("q (g/kg)")
plt.ylabel("z (km)")
plt.title("Specific humidity")

plt.tight_layout()
plt.savefig("compare_atmo_profiles.svg")
plt.show()

