
import netCDF4 as nc
import numpy as np
from matplotlib import pyplot as plt

# plot les profils de temp, press et q pour i3rc (utilise pour produire les donnees gaz), standard tropical atmo et ensemble des LES (pour l'instant juste ARMCu mais rajouter les autres cas)
# => quelles diff sur les profils de ka ? sur les profils de flux ?

fvlv = "i3rc_mls_cumulus.nc"
fref = "i3rc_mls_cumulus_out.nc"
fstd = "i3rc_mls_cumulus_STROATM_out.nc"
ftp  = "i3rc_mls_cumulus_TP_out.nc"

nvlv = nc.Dataset(fvlv,"r")
nref = nc.Dataset(fref,"r")
nstd = nc.Dataset(fstd,"r")
ntp  = nc.Dataset(ftp,"r")

z = nvlv.variables['height_hl'][0,:]/1000.
print len(z)
nvlv.close()

dn=np.zeros((3,len(z)))
up=np.zeros((3,len(z)))

for i,ncdf in enumerate([nref,nstd,ntp]):
    dn[i,:]=ncdf.variables['flux_dn_lw'][:]
    up[i,:]=ncdf.variables['flux_up_lw'][:]

nt = dn-up

nref.close()
nstd.close()
ntp.close()

plt.figure()

plt.subplot(131)
plt.plot(dn[0,:],z,'r--',linewidth=2)
plt.plot(dn[1,:],z,'k-',linewidth=2)
plt.plot(dn[2,:],z,'orange',linewidth=2)
plt.legend(labels=["i3rc","T,P,q trop","T,P trop"],loc="upper right")
plt.grid()
plt.xlabel(r"Down fluxes (W/m$^2$)")
plt.ylabel("z (km)")
plt.title("Down fluxes")
plt.ylim(0,20)

plt.subplot(132)
plt.plot(up[0,:],z,'r--',linewidth=2)
plt.plot(up[1,:],z,'k-',linewidth=2)
plt.plot(up[2,:],z,'orange',linewidth=2)
plt.legend(labels=["i3rc","T,P,q trop","T,P trop"],loc="upper right")
plt.grid()
plt.xlabel(r"Up fluxes (W/m$^2$)")
plt.ylabel("z (km)")
plt.title("Up fluxes")
plt.ylim(0,20)

plt.subplot(133)
plt.plot(nt[0,:],z,'r--',linewidth=2)
plt.plot(nt[1,:],z,'k-',linewidth=2)
plt.plot(nt[2,:],z,'orange',linewidth=2)
plt.legend(labels=["i3rc","T,P,q trop","T,P trop"],loc="upper right")
plt.grid()
plt.xlabel(r"Net fluxes (W/m$^2$)")
plt.ylabel("z (km)")
plt.title("Net fluxes")
plt.ylim(0,20)

plt.tight_layout()
plt.savefig("compare_flux_profiles.svg")
plt.show()

