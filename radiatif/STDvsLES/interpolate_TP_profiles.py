
import netCDF4 as nc
import numpy as np
from matplotlib import pyplot as plt

# construct profiles on the ecRad grid but with values interpolated from the standard atmospheric profile STROATM
# this will be used to compute optical properties and fluxes profiles that should then be compared to i3rc values

def interp(f,z,zc):
    n = zc*0.
    for iz,zz in enumerate(zc):
        grt = np.where(z>=zz)[0]
        if len(grt)>0:
          isup=grt[0]
          if isup==0: n[iz]=f[0]
          else:       n[iz]=f[isup-1]+(f[isup]-f[isup-1])/(z[isup]-z[isup-1])*(zz-z[isup-1])
        else: n[iz]=np.float("nan")
    return n

ftrop = "/home/villefranquen/Work/NCDF/ATM1D/STROATM.nc"
fnewp = "/home/villefranquen/Work/NCDF/ECRAD/i3rc_mls_cumulus_TP.nc"

rtrop = nc.Dataset(ftrop,"r")
wnewp = nc.Dataset(fnewp,"r+")

z = wnewp.variables['height_hl']
zi3rc = z[0,:]
zi3rc_fl=.5*(zi3rc[:-1]+zi3rc[1:])

ti3rc = wnewp.variables['temperature_hl'][0,:]
pi3rc = wnewp.variables['pressure_hl'][0,:]

z = rtrop.variables['vertical_levels']
ztrop = z[:].reshape(z.shape)*1000.

t = rtrop.variables['MEAN_T']
ttrop = t[:].reshape(t.shape)
tnew = interp(ttrop,ztrop,zi3rc)
indx=np.where(np.isnan(tnew))[0]
tnew[np.isnan(tnew)]=ti3rc[indx]

p = rtrop.variables['MEAN_P']
ptrop = p[:].reshape(p.shape)*100.
pnew = interp(ptrop,ztrop,zi3rc)
indx=np.where(np.isnan(pnew))[0]
pnew[np.isnan(pnew)]=pi3rc[indx]

wnewp.variables['temperature_hl'][0,:] = tnew
wnewp.variables['pressure_hl'][0,:] = pnew

rtrop.close()
wnewp.close()
