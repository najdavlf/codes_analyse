# @desc Utils functions to analyse angular distribution computed by SCART simulations
# @date 23/03/2018
# @author N. Villefranque

buildColors = function() {
  color = grDevices::colors()
  color = color[grep('gr(a|e)y|white|light', color, invert = T)]
  color = color[grep('1|2|3|4|5|6', color, invert=T)]
  color = color[seq(-7,-1,1)]
  return(color)
}

renameData = function(data) {
  names(data)[1] = "ID"
  names(data)[2] = "weight"
  names(data)[3] = "ux"
  names(data)[4] = "uy"
  names(data)[5] = "uz"
  names(data)[6] = "scattered"
  return(data)
}

extractBOA = function(data) {
  return(data[data$weight>0,])
}

dataOrders = function(data) {
  return(sort(unique(data$scattered)))
}

muOrder = function(data,order) {
  return(-data$uz[data$scattered==order])
}

datagtOrder = function(data,order) {
  return(data[data$scattered>order,])
}

mugtOrder = function(data,order) {
  return(-data$uz[data$scattered>order])
}

mugtx = function(mu,x) {
  return(mu[mu>x])
}

nbdir = function(mu, mumin) {
  return(length(mu[mu>mumin]))
}
