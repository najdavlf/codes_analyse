#!/usr/bin/python
# -*- coding: latin-1 -*-

R_=8.31447   # constante gaz parfaits [J/K/mol]
a_=0.        # facteur de linearite de variation enthalpie temperature
M_=0.018     # masse molaire eau [kg/mol]
L_=2.26e6    # chaleur latente de vaporisation [J/kg]
P0_=1013.    # pression normale [hPa]
T0_=373.     # temperature normale [K]
Tk_=273.15   # K to °C
Cp_=1004.    # chaleur massique de l'air [J/K/kg]
Ma_=28.98e-3 # masse molaire de l'air [kg/mol]
Ps_=1000     # pression standard [hPa]
