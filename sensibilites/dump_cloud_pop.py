#!/usr/bin/python
# -*- coding: latin-1 -*-

# 14/01/2019 
# N. Villefranque

import sys, os
p='/home/villefranquen/objects/src/'
p='/cnrm/tropics/commun/DATACOMMUN/OBJECTS/objects/src/'
sys.path.append(p)
import netCDF4 as nc
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime
from identification_methods import identify
from mycolors import basic4 as cols
import matplotlib as mpl
from constants import *
import pickle

f=open("data_clouds.txt","w")

plt.style.use("manuscript")
mpl.rcParams["axes.edgecolor"]=cols[-3]
mpl.rcParams["grid.color"]=cols[-4]
mpl.rcParams["grid.linestyle"]="-"

markers =  {'.': 'point', ',': 'pixel', 'o': 'circle', 'v': 'triangle_down', '^': 'triangle_up', '<': 'triangle_left', '>': 'triangle_right', '1': 'tri_down', '2': 'tri_up', '3': 'tri_left', '4': 'tri_right', '8': 'octagon', 's': 'square', 'p': 'pentagon', '*': 'star', 'h': 'hexagon1', 'H': 'hexagon2', '+': 'plus', 'x': 'x', 'D': 'diamond', 'd': 'thin_diamond', '|': 'vline', '_': 'hline',  0: 'tickleft', 1: 'tickright', 2: 'tickup', 3: 'tickdown', 4: 'caretleft', 5: 'caretright', 6: 'caretup', 7: 'caretdown',  10: 'caretupbase'}
mymarkers = [m for m in list(markers)]

# Read vars

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

def cloudMask(dictVars,thrs) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>thrs]=1 # cell is in cloud if liquid water mixing ratio is greater than thrs
    return mask

def coreMask(dictVars,thrs) :
    rc = dictVars['RCT']
    wt = dictVars['WT']
    mask=rc*0.   # same shape as RCT
    mask[(rc>thrs)&(wt>0)]=1 # cell is in cloud if liquid water mixing ratio is greater than thrs
    return mask


listVarsNames = ["RCT","WT"]
path = "/home/villefranquen/Work/NCDF/"
path = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/"

# To find all the files
rep = "BOMEX"
reps = ["ARM_LES"]
reps = ["ARMCu","BOMEX","RICO","SCMS"]
reps = ["ARM_LES","BOMEX_LES","RICO_LES","SCMS_LES"]
pref = ["CERK4"]
pref = ["CERK4","VMFTH","THL3D","CER25"]
pref = ["VMFTH", "D100m", "L25km", "L12km"]
cas = "BOMEX"
case = ["ARMCu"]
case = ["ARMCu","BOMEX","RICO","SCMS"]
nb = 15
nbfs = [4,4,4,4]
nbfs = [14,15,15,12]
dxs = [.025,.1,.1,.025]
dys = [.025,.1,.1,.025]
dzs = [.025,.04,.04,.025]

w,h = plt.figaspect(1)
fig = plt.figure(figsize=(w*2,h*2.5))
ax01 = fig.add_subplot(421)  # smallest big cloud eq radius
ax02 = fig.add_subplot(422)  # percent remaining clouds 
ax11 = fig.add_subplot(423)  # cloud number
ax12 = fig.add_subplot(424,sharey=ax11) # big cloud number
ax21 = fig.add_subplot(425)  # cloud cover
ax22 = fig.add_subplot(426,sharey=ax21) # big cloud cover
ax31 = fig.add_subplot(427)  # base top 
ax32 = fig.add_subplot(428)  # big base top

fig1 = plt.figure(figsize=(w*2.,h))
ax41 = fig1.add_subplot(121) # cloud number
ax42 = fig1.add_subplot(122) # cloud cover

fig2 = plt.figure(figsize=(w*1,h))
ax43 = fig2.add_subplot(111) # smallest big cloud eq radius

fig3 = plt.figure(figsize=(w*2.,h))
ax44 = fig3.add_subplot(121) # percent of remaining clouds
ax45 = fig3.add_subplot(122) # number of remaining clouds 

fig4 = plt.figure(figsize=(w*2.,h))
ax46 = fig4.add_subplot(121) # cloud cover big clouds
ax47 = fig4.add_subplot(122) # cloud volume fraction big clouds

fig5 = plt.figure(figsize=(w*2.,h))
ax48 = fig5.add_subplot(121) # all clouds base and top
ax49 = fig5.add_subplot(122) # big clouds base and top

figvol = plt.figure(figsize=(w*2.0,h*2.0))
ax51 = figvol.add_subplot(221) # cumulative volume fraction at each timestep
ax52 = figvol.add_subplot(222) # cumulative volume fraction at each timestep
ax53 = figvol.add_subplot(223) # cumulative volume fraction at each timestep
ax54 = figvol.add_subplot(224) # cumulative volume fraction at each timestep
axes5 = {pref[0]:ax51,pref[1]:ax52,pref[2]:ax53,pref[3]:ax54}

figasp = plt.figure(figsize=(w*2.0,h*2.0)) # aspect ratio scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax61 = figasp.add_subplot(gs[1:4,0:3])
ax61x = figasp.add_subplot(gs[0,0:3],sharex=ax61)
ax61y = figasp.add_subplot(gs[1:4,3],sharey=ax61) 
ax62 = figasp.add_subplot(gs[1:4,5:8]) 
ax62x = figasp.add_subplot(gs[0,5:8],sharex=ax62) 
ax62y = figasp.add_subplot(gs[1:4,8],sharey=ax62) 
ax63 = figasp.add_subplot(gs[6:9,0:3]) 
ax63x = figasp.add_subplot(gs[5,0:3],sharex=ax63) 
ax63y = figasp.add_subplot(gs[6:9,3],sharey=ax63) 
ax64 = figasp.add_subplot(gs[6:9,5:8]) 
ax64x = figasp.add_subplot(gs[5,5:8],sharex=ax64) 
ax64y = figasp.add_subplot(gs[6:9,8],sharey=ax64) 
axes6 = {pref[0]:(ax61,ax61x,ax61y),pref[1]:(ax62,ax62x,ax62y),pref[2]:(ax63,ax63x,ax63y),pref[3]:(ax64,ax64x,ax64y)}

figtop = plt.figure(figsize=(w*2.0,h*2.0)) # tops scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax71 = figtop.add_subplot(gs[1:4,0:3])
ax71x = figtop.add_subplot(gs[0,0:3],sharex=ax71)
ax71y = figtop.add_subplot(gs[1:4,3],sharey=ax71) 
ax72 = figtop.add_subplot(gs[1:4,5:8]) 
ax72x = figtop.add_subplot(gs[0,5:8],sharex=ax72) 
ax72y = figtop.add_subplot(gs[1:4,8],sharey=ax72) 
ax73 = figtop.add_subplot(gs[6:9,0:3]) 
ax73x = figtop.add_subplot(gs[5,0:3],sharex=ax73) 
ax73y = figtop.add_subplot(gs[6:9,3],sharey=ax73) 
ax74 = figtop.add_subplot(gs[6:9,5:8]) 
ax74x = figtop.add_subplot(gs[5,5:8],sharex=ax74) 
ax74y = figtop.add_subplot(gs[6:9,8],sharey=ax74) 
axes7 = {pref[0]:(ax71,ax71x,ax71y),pref[1]:(ax72,ax72x,ax72y),pref[2]:(ax73,ax73x,ax73y),pref[3]:(ax74,ax74x,ax74y)}

figdep = plt.figure(figsize=(w*2.0,h*2.0))  # depth scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax81 = figdep.add_subplot(gs[1:4,0:3])
ax81x = figdep.add_subplot(gs[0,0:3],sharex=ax81)
ax81y = figdep.add_subplot(gs[1:4,3],sharey=ax81) 
ax82 = figdep.add_subplot(gs[1:4,5:8]) 
ax82x = figdep.add_subplot(gs[0,5:8],sharex=ax82) 
ax82y = figdep.add_subplot(gs[1:4,8],sharey=ax82) 
ax83 = figdep.add_subplot(gs[6:9,0:3]) 
ax83x = figdep.add_subplot(gs[5,0:3],sharex=ax83) 
ax83y = figdep.add_subplot(gs[6:9,3],sharey=ax83) 
ax84 = figdep.add_subplot(gs[6:9,5:8]) 
ax84x = figdep.add_subplot(gs[5,5:8],sharex=ax84) 
ax84y = figdep.add_subplot(gs[6:9,8],sharey=ax84) 
axes8 = {pref[0]:(ax81,ax81x,ax81y),pref[1]:(ax82,ax82x,ax82y),pref[2]:(ax83,ax83x,ax83y),pref[3]:(ax84,ax84x,ax84y)}


figbas = plt.figure(figsize=(w*2.0,h*2.0)) # base scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax91 = figbas.add_subplot(gs[1:4,0:3])
ax91x = figbas.add_subplot(gs[0,0:3],sharex=ax91)
ax91y = figbas.add_subplot(gs[1:4,3],sharey=ax91) 
ax92 = figbas.add_subplot(gs[1:4,5:8]) 
ax92x = figbas.add_subplot(gs[0,5:8],sharex=ax92) 
ax92y = figbas.add_subplot(gs[1:4,8],sharey=ax92) 
ax93 = figbas.add_subplot(gs[6:9,0:3]) 
ax93x = figbas.add_subplot(gs[5,0:3],sharex=ax93) 
ax93y = figbas.add_subplot(gs[6:9,3],sharey=ax93) 
ax94 = figbas.add_subplot(gs[6:9,5:8]) 
ax94x = figbas.add_subplot(gs[5,5:8],sharex=ax94) 
ax94y = figbas.add_subplot(gs[6:9,8],sharey=ax94) 
axes9 = {pref[0]:(ax91,ax91x,ax91y),pref[1]:(ax92,ax92x,ax92y),pref[2]:(ax93,ax93x,ax93y),pref[3]:(ax94,ax94x,ax94y)}

i=0
#for i,(rep,pre,cas,nbf) in enumerate(zip(reps,pref,case,nbfs)):
for i,(pre,dx,dy,dz)in enumerate(zip(pref,dxs,dys,dzs)):
  tsteps = range(4,nb+1)
  nbf=len(tsteps)
  nc3dList = [path+rep+"/"+pre+"/"+pre+".1."+cas+".%03i"%(h)+".nc" for h in tsteps]
  nc3dList = [path+rep+"/"+pre+".1."+cas+".%03i"%(h)+".diaKCL.nc" for h in tsteps]
  # Characteristics
  number=np.zeros(nbf,dtype=int) # Number of clouds at a timestep
  bignumber=np.zeros(nbf,dtype=int) # Number of big clouds at a timestep
  tccovr=np.zeros(nbf) # Total cloud cover at a timestep
  bigtccovr=np.zeros(nbf) # Big cloud cover at a timestep
  minbas=np.zeros(nbf) # Min cloud base at each timestep
  maxtop=np.zeros(nbf) # Max cloud top at each timestep
  bigbas=np.zeros(nbf) # Min cloud base of the 50% bigger clouds at each timestep
  bigtop=np.zeros(nbf) # Max cloud top of the 50% bigger clouds at each timestep
  thresh=np.zeros(nbf) # Threshold in volume for the 90 percentile
  percen=np.zeros(nbf) # Percentage of clouds that make the 90% of the volume
  alleqlen=[]
  alldepth=[]
  allmeantp=[]
  allmaxtop=[]
  allmdep=[]
  allmeanbs=[]
  allminbas=[]
  for i3d,nc3d in enumerate(nc3dList) :
    print pre,cas,i3d
    print nc3d
    if "clouds" in nc.Dataset(nc3d).variables.keys(): clouds=nc.Dataset(nc3d).variables["clouds"][:]
    else : 
      clouds,toto=identify(nc3d, listVarsNames, cloudMask, argsCalcMask=(1.e-6), name="clouds", write=True, overwrite=True)
    if np.max(clouds)>0 :
      ax5=axes5[pre] # volumes cumul
      (ax6,ax6x,ax6y)=axes6[pre] # aspect ratios
      (ax7,ax7x,ax7y)=axes7[pre] # tops 
      (ax8,ax8x,ax8y)=axes8[pre] # depth
      (ax9,ax9x,ax9y)=axes9[pre] # base
      nbCellsDomainCover = clouds.shape[-1]*clouds.shape[-2]
      nbCellsDomainVolume= clouds.shape[-1]*clouds.shape[-2]*clouds.shape[-3]
      number[i3d] = len(np.unique(clouds[clouds>0]))
      # Total cover and fraction are in %
      tccovr[i3d] = 100.*float(len(np.where(np.sum(clouds,axis=-3)>0)[0]))/nbCellsDomainCover
      nbcols = np.zeros((number[i3d])) # List of cloud number of columns at this timestep
      nbcell = np.zeros((number[i3d])) # List of cloud number of cells at this timestep
      basesh = np.zeros((number[i3d])) # List of cloud base heights at this timestep
      ctopsh = np.zeros((number[i3d])) # List of cloud top heights at this timestep
      basesh = np.zeros((number[i3d])) # List of cloud base heights at this timestep
      ctopsh = np.zeros((number[i3d])) # List of cloud max top heights at this timestep
      meantp = np.zeros((number[i3d])) # List of cloud mean top heights at this timestep
      meanbs = np.zeros((number[i3d])) # List of cloud mean base heights at this timestep
      bigclouds = 1.*clouds
      for k,c in enumerate(np.unique(clouds[clouds>0])) : # 1 value per cloud
        inds = np.where(clouds==c)
        uniq_cols = list(set(zip(inds[-1],inds[-2])))
        nbcols[k] = len(uniq_cols)
        nbcell[k] = len(inds[0])
        alltops = dz*np.array([max([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))])
        allbase = dz*np.array([min([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))])
        basesh[k] = dz*1000.*min(inds[-3])
        ctopsh[k] = dz*1000.*max(inds[-3])
        meantp[k] = 1000.*np.mean(alltops)
        meanbs[k] = 1000.*np.mean(allbase)
     
      # Cumulative sum of volumic cloud fractions : the last point is the total volumic cloud fraction at the timestep
      nbCellsDomainVolume=nbCellsDomainCover*(np.max(ctopsh)/dz/1000. - np.min(basesh)/dz/1000.)
      y=np.cumsum((np.sort(nbcell)[::-1])/float(nbCellsDomainVolume)*100.) # cumulative sorted from biggest to smallest of volume cloud fraction in %
      j90=np.searchsorted(y,.9*max(y))                                     # where cumulated volume fraction is ~ 90% of total volume fraction
      nbcell90 = np.sort(nbcell)[::-1][j90]                                # size (nb of cells) of the cloud just before that 90% percentile
      thresh[i3d]=(nbcell90*dx*dy*dz*3./4./np.pi)**(1./3.)*1000.           # radius [m] of a sphere of same volume than the smallest cloud in the 90% volume fraction pool
      x=np.array(range(number[i3d]))/float(number[i3d])*100.
      percen[i3d] = x[j90]                                                 # Percentage of clouds in the 90% pool
      pickle.dump(x,f)
      pickle.dump(y,f)
      ax5.plot(x,y,linewidth=.5+.1*i3d,color=cols[i+1])
      pickle.dump(x[j90],f)
      pickle.dump(y[j90],f)
      ax5.plot(x[j90],y[j90],marker='+',color="black",markersize=2*i3d)
      ax5.set_title(pre)
      ax5.set_xlabel("Cloud [% of number of clouds]")
      ax5.set_ylabel("Cumul vol frac [%]")

      for k,c in enumerate(np.unique(clouds[clouds>0])) : 
        if nbcell[k]<=nbcell90 : bigclouds[bigclouds==c]=0
      bignumber[i3d] = len(nbcell[nbcell>nbcell90])
      bigtccovr[i3d] = 100.*float(len(np.where(np.sum(bigclouds,axis=-3)>0)[0]))/nbCellsDomainCover

      # list of '1 value per cloud' values at each timestep
      nbcols=list(nbcols)
      nbcell=list(nbcell)
      # one value per timestep
      minbas[i3d] = np.min(basesh)/1000.
      maxtop[i3d] = np.max(ctopsh)/1000.
      bigbas[i3d] = np.mean(basesh[nbcell>nbcell90])/1000.
      bigtop[i3d] = np.mean(ctopsh[nbcell>nbcell90])/1000.

      depth = (ctopsh[nbcell>nbcell90] - basesh[nbcell>nbcell90])*1e-3
      nbcols=np.array(nbcols); nbcell=np.array(nbcell)
      areas=nbcols[nbcell>nbcell90]*dx*dy
      alleqlen+=list(np.sqrt(areas))
      alldepth+=list(depth)
      pickle.dump(np.sqrt(areas),f)
      pickle.dump(depth,f)
      ax6.plot(np.sqrt(areas),depth, linestyle="", marker='+',markersize=1.5*i3d,color=cols[i+1])
      ax6.set_xlabel("Equivalent length [km]")
      ax6.set_ylabel("Depth [km]")

      moy = meantp[nbcell>nbcell90]*1.e-3
      top = ctopsh[nbcell>nbcell90]*1.e-3
      allmeantp+=list(moy)
      allmaxtop+=list(top)
      pickle.dump(moy,f)
      pickle.dump(top,f)
      ax7.plot(moy,top,linestyle="", marker="+",markersize=1.5*i3d,color=cols[i+1])
      ax7.set_xlabel("Mean top [km]")
      ax7.set_ylabel("Max top [km]")
      
      mdep = (meantp[nbcell>nbcell90]-meanbs[nbcell>nbcell90])
      allmdep+=list(mdep*1.e-3)
      pickle.dump(mdep*1.e-3,f)
      pickle.dump(depth,f)
      ax8.plot(mdep*1.e-3,depth,linestyle="", marker='+',markersize=1.5*i3d,color=cols[i+1])
      ax8.set_xlabel("Mean depth [km]")
      ax8.set_ylabel("Max depth [km]")

      moy = meanbs[nbcell>nbcell90]*1.e-3
      bas = basesh[nbcell>nbcell90]*1.e-3
      allmeanbs+=list(moy)
      allminbas+=list(bas)
      pickle.dump(bas,f)
      pickle.dump(moy,f)
      ax9.plot(bas,moy,linestyle="", marker='+',markersize=1.5*i3d,color=cols[i+1])
      ax9.set_xlabel("Min base [km]")
      ax9.set_ylabel("Mean base [km]")

    if i3d==nbf-1 :
      pickle.dump(sum(bignumber),f)
      ax6x.set_title(pre+" - %i big clouds"%sum(bignumber))
      plt.setp(ax6x.get_xticklabels(),visible=False)
      [label.set_visible(False) for label in ax6x.get_yticklabels()[1:-1]]
      [label.set_visible(False) for label in ax6y.get_xticklabels()[1:-1]]
      plt.setp(ax6y.get_yticklabels(),visible=False)
      ax6.set_xlim(0.,2.)
      ax6.set_ylim(0.,2.)
      pickle.dump([0.,2.],f)
      pickle.dump([0.,2.],f)
      ax6.plot([0.,2.],[0.,2.],linewidth=1.,color=cols[-3])
      pickle.dump(alleqlen,f)
      pickle.dump(alldepth,f)
      ax6x.hist(alleqlen,bins="auto",color=cols[i+1],alpha=.9)
      ax6y.hist(alldepth,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
      pickle.dump(sum(bignumber),f)
      ax7x.set_title(pre+" - %i big clouds"%sum(bignumber))
      plt.setp(ax7x.get_xticklabels(),visible=False)
      [label.set_visible(False) for label in ax7x.get_yticklabels()[1:-1]]
      [label.set_visible(False) for label in ax7y.get_xticklabels()[1:-1]]
      plt.setp(ax7y.get_yticklabels(),visible=False)
      ax7.set_xlim(0.,3.)
      ax7.set_ylim(0.,3.)
      pickle.dump([0.,3.],f)
      pickle.dump([0.,3.],f)
      ax7.plot([0.,3.],[0.,3.],linewidth=1.,color=cols[-3])
      pickle.dump(allmeantp,f)
      pickle.dump(allmaxtop,f)
      ax7x.hist(allmeantp,bins="auto",color=cols[i+1],alpha=.9)
      ax7y.hist(allmaxtop,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
      pickle.dump(sum(bignumber),f)
      ax8x.set_title(pre+" - %i big clouds"%sum(bignumber))
      plt.setp(ax8x.get_xticklabels(),visible=False)
      [label.set_visible(False) for label in ax8x.get_yticklabels()[1:-1]]
      [label.set_visible(False) for label in ax8y.get_xticklabels()[1:-1]]
      plt.setp(ax8y.get_yticklabels(),visible=False)
      ax8.set_xlim(0.,1.8)
      ax8.set_ylim(0.,1.8)
      pickle.dump([0.,1.8],f)
      pickle.dump([0.,1.8],f)
      ax8.plot([0.,1.8],[0.,1.8],linewidth=1.,color=cols[-3])
      pickle.dump(allmdep,f)
      pickle.dump(alldepth,f)
      ax8x.hist(allmdep,bins="auto",color=cols[i+1],alpha=.9)
      ax8y.hist(alldepth,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
      pickle.dump(sum(bignumber),f)
      ax9x.set_title(pre+" - %i big clouds"%sum(bignumber))
      plt.setp(ax9x.get_xticklabels(),visible=False)
      [label.set_visible(False) for label in ax9x.get_yticklabels()[1:-1]]
      [label.set_visible(False) for label in ax9y.get_xticklabels()[1:-1]]
      plt.setp(ax9y.get_yticklabels(),visible=False)
      ax9.set_xlim(0.4,2.)
      ax9.set_ylim(0.4,2.)
      pickle.dump([0.4,2.],f)
      pickle.dump([0.4,2.],f)
      ax9.plot([0.4,2.],[0.4,2.],linewidth=1.,color=cols[-3])
      pickle.dump(allminbas,f)
      pickle.dump(allmeanbs,f)
      ax9x.hist(allminbas,bins="auto",color=cols[i+1],alpha=.9)
      ax9y.hist(allmeanbs,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
 
  # volume threshold and percentage of remaining clouds
  pickle.dump(tsteps,f)
  pickle.dump(thresh,f)
  ax01.plot(tsteps, thresh, color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(percen,f)
  ax02.plot(tsteps, percen, color=cols[i+1],label=pre)
  ax02.fill_between(tsteps, percen, color=cols[i+1],label=pre,alpha=.33)
  # same but on separate fig 
  pickle.dump(tsteps,f)
  pickle.dump(thresh,f)
  ax43.plot(tsteps, thresh, color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(percen,f)
  ax44.plot(tsteps, percen, color=cols[i+1],label=pre)

  # number total and number of 90% total volume
  pickle.dump(tsteps,f)
  pickle.dump(number,f)
  ax11.plot(tsteps,number,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(bignumber,f)
  ax12.plot(tsteps,bignumber,color=cols[i+1],label=pre)
  # same but on separate fig 
  pickle.dump(tsteps,f)
  pickle.dump(number,f)
  ax41.plot(tsteps,number,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(bignumber,f)
  ax45.plot(tsteps,bignumber,color=cols[i+1],label=pre)

  # cover total and cover of 90% total volume
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  ax21.plot(tsteps,tccovr,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  ax22.plot(tsteps,tccovr,color=cols[i+1],label=pre,linewidth=1.)
  pickle.dump(tsteps,f)
  pickle.dump(bigtccovr,f)
  ax22.plot(tsteps,bigtccovr,color=cols[i+1],label=pre)
  # same but on separate fig 
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  ax42.plot(tsteps,tccovr,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  ax46.plot(tsteps,tccovr,color=cols[i+1],label=pre,linewidth=.5)
  pickle.dump(tsteps,f)
  pickle.dump(bigtccovr,f)
  ax46.plot(tsteps,bigtccovr,color=cols[i+1],label=pre)

  # base min / top max all clouds
  pickle.dump(tsteps,f)
  pickle.dump(minbas,f)
  ax31.plot(tsteps,minbas,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(maxtop,f)
  ax31.plot(tsteps,maxtop,color=cols[i+1],label=pre)
  ax31.fill_between(tsteps,minbas,maxtop,color=cols[i+1],label=pre,alpha=.33)
  ax31.grid(axis="y")
  # same but on separate fig
  pickle.dump(tsteps,f)
  pickle.dump(minbas,f)
  ax48.plot(tsteps,minbas,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(maxtop,f)
  ax48.plot(tsteps,maxtop,color=cols[i+1],label=pre)
  ax48.fill_between(tsteps,minbas,maxtop,color=cols[i+1],label=pre,alpha=.33)
  ax48.grid(axis="y")

  # base mean / top mean, clouds 90% total volume
  pickle.dump(tsteps,f)
  pickle.dump(bigbas,f)
  ax32.plot(tsteps,bigbas,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(bigtop,f)
  ax32.plot(tsteps,bigtop,color=cols[i+1],label=pre)
  ax32.fill_between(tsteps,bigbas,bigtop,color=cols[i+1],label=pre,alpha=.33)
  ax32.grid(axis="y")
  # same but separated fig
  pickle.dump(tsteps,f)
  pickle.dump(bigbas,f)
  ax49.plot(tsteps,bigbas,color=cols[i+1],label=pre)
  pickle.dump(tsteps,f)
  pickle.dump(bigtop,f)
  ax49.plot(tsteps,bigtop,color=cols[i+1],label=pre)
  ax49.fill_between(tsteps,bigbas,bigtop,color=cols[i+1],label=pre,alpha=.33)
  ax49.grid(axis="y")

#figdep.tight_layout()
figdep.savefig("depths.svg")

#figtop.tight_layout()
figtop.savefig("tops.svg")

#figbas.tight_layout()
figbas.savefig("bases.svg")

#figasp.tight_layout()
figasp.savefig("aspect_ratios.svg")

figvol.tight_layout()
figvol.savefig("cumul_volumes.svg")
#----------#
# ax1  ax3 #
# ax2  ax4 #
#----------#
ax12.legend(loc="best")
ax01.set_ylabel("Smallest big cloud\neq. sphere radius [m]")
ax02.set_ylabel("% of big clouds")
ax02.yaxis.tick_right()
ax02.yaxis.set_label_position("right")
ax11.set_title("All clouds")
ax12.set_title("Biggest clouds")
ax12.yaxis.tick_right()
ax12.yaxis.set_label_position("right")
ax11.set_ylabel("Number of clouds")
ax21.set_ylabel("Cloud cover [%]")
ax22.yaxis.tick_right()
ax31.set_ylabel("Cloud min base and\nmax top heights [km]")
ax32.set_ylabel("Big clouds mean base\nand top heights [km]")
ax32.yaxis.tick_right()
ax32.yaxis.set_label_position("right")
ax31.set_xlabel("Simulation hour [#]")
ax32.set_xlabel("Simulation hour [#]")
fig.tight_layout()
fig.savefig("cloud_pop.svg")


ax41.set_ylabel("Cloud number")
ax41.set_xlabel("Simulation hour [#]")
ax42.set_ylabel("Cloud cover [%]")
ax42.set_xlabel("Simulation hour [#]")
ax42.legend(loc="best")

ax43.set_ylabel("Smallest big cloud\neq. sphere radius [m]")
ax43.set_xlabel("Simulation hour [#]")
ax43.legend(loc="best")

ax44.set_ylabel("% of big clouds")
ax45.set_ylabel("Number of big clouds")
ax44.set_xlabel("Simulation hour [#]")
ax45.set_xlabel("Simulation hour [#]")
ax45.legend(loc="best")

ax46.set_ylabel("Cloud cover [%]")
ax47.set_ylabel("Cloud volume fraction [%]")
ax46.set_xlabel("Simulation hour [#]")
ax47.set_xlabel("Simulation hour [#]")
ax47.legend(loc='best')

ax48.set_ylabel("All clouds\nmin base, max top")
ax49.set_ylabel("Big clouds\nmean base and top")
ax48.set_xlabel("Simulation hour [#]")
ax49.set_xlabel("Simulation hour [#]")
ax49.legend(loc="best")

fig1.tight_layout()
fig1.savefig("cloud_num_cov.svg")
fig2.tight_layout()
fig2.savefig("smallest_big_radius.svg")
fig3.tight_layout()
fig3.savefig("remaining_big_clouds.svg")
fig4.tight_layout()
fig4.savefig("cover_big_clouds.svg")
fig5.tight_layout()
fig5.savefig("bases_tops.svg")

plt.show()
f.close()
