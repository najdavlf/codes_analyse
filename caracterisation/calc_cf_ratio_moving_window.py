
import numpy   as np
import netCDF4 as nc
from matplotlib import pyplot as plt

obspath = '/home/villefranquen/Work/NCDF/CLOUDNET/'
obspath = '/cnrm/tropics/user/villefranquen/NCDF/OBS/ARM/SGP/'
obsfile = 'arm-sgp_classification_June2009.nc'
obsfile = 'arm-sgp_maskzwang_June2017.nc'
obs = nc.Dataset(obspath+obsfile, "r")

vlev_lmdz = np.array([0.00000000e+00, 2.14453201e+01, 4.53599701e+01, 7.20312195e+01 , 1.01773483e+02, 1.34935135e+02, 1.71904327e+02, 2.13112839e+02 , 2.59040344e+02, 3.10218994e+02, 3.67239716e+02, 4.30756897e+02 , 5.01495422e+02, 5.80257507e+02, 6.67930115e+02, 7.65498169e+02 , 8.74054932e+02, 9.94808899e+02, 1.12908655e+03, 1.27835046e+03 , 1.44421497e+03, 1.62844421e+03, 1.83293457e+03, 2.05974902e+03 , 2.31115601e+03, 2.58992285e+03, 2.89925659e+03, 3.24208667e+03 , 3.62116846e+03, 4.03952344e+03, 4.50041943e+03, 5.00708447e+03 , 5.56235547e+03, 6.16858984e+03, 6.82761182e+03, 7.54052881e+03 , 8.30765039e+03, 9.12836230e+03, 1.00011299e+04, 1.09235684e+04 , 1.18925664e+04, 1.29044746e+04, 1.39553594e+04, 1.50412021e+04 , 1.61581152e+04, 1.73025215e+04, 1.84712383e+04, 1.96615781e+04 , 2.08713340e+04, 2.20988340e+04, 2.33428672e+04, 2.46027188e+04 , 2.58780566e+04, 2.71690000e+04, 2.84760254e+04, 2.98000117e+04 , 3.11421797e+04, 3.25042305e+04, 3.38881875e+04, 3.52966406e+04 , 3.67326445e+04, 3.81999219e+04, 3.97029844e+04, 4.12472188e+04 , 4.28391836e+04, 4.44869102e+04, 4.62003242e+04, 4.79917695e+04 , 4.98769102e+04, 5.18760000e+04, 5.40155625e+04, 5.63313047e+04 , 5.88724531e+04, 6.17091836e+04, 6.49450664e+04, 6.87392344e+04 , 7.33463438e+04, 7.91848828e+04, 8.72277109e+04])

vlev_obs = obs.variables['height'][:]

vlev_LES = np.linspace(0,4000.,160)

# vertical levels
plt.figure()
plt.plot(vlev_lmdz,'k',label='LMDz')
plt.plot(vlev_LES,'b',label='LES')
plt.plot(vlev_obs,'r',label='ARM SGP') 
plt.legend()
plt.savefig('levels_lmdz_les_arm.png')
plt.close()

# cloud classif in Cloudnet
var='target_classification'
#obs_cloudnet = obs.variables[var][:].reshape(obs.variables[var].shape)
# cloud mask in ARM
var='cloud_mask'
obs_arm = obs.variables[var][:].reshape(obs.variables[var].shape)
nb_profiles = obs_arm.shape[0]
nb_heights  = len(vlev_obs[vlev_obs<=4.00])
#obs_cloudnet = obs_cloudnet.reshape((nb_profiles,nb_heights))
#mask_obs = obs_cloudnet*0
#mask_obs[obs_cloudnet==1]=1
mask_obs = obs_arm[:,vlev_obs<=4.00]
print mask_obs.shape

plt.figure()
plt.contourf(mask_obs.transpose())
plt.colorbar()
plt.savefig('original_mask.png')
plt.close()

# horizontal interpolation
# based on assumptions on mean wind speed
ut=10 #m/s
dt=30 #s/p
dx=ut*dt # m/p
dom=6400 #m
pts_in_dom = dom/dx
nb_pts = nb_profiles/pts_in_dom
# in 1 month of 30 sec data we sample ~ 4055 LES domains
nbx = pts_in_dom
midobs = range(nbx,nb_profiles-nbx,pts_in_dom)

def cloudfrac_ratio(nbz):
  cloudfrac_s = np.zeros((nb_profiles-nbx,nb_heights-nbz))
  cloudfrac_v = np.zeros((nb_profiles-nbx,nb_heights-nbz))
  for icf in range(nb_profiles-nbx):
    for jcf in range(nb_heights-nbz):
      this = mask_obs[icf:icf+nbx,jcf:jcf+nbz]
      if np.ma.is_masked(this):
        cloudfrac_v[icf,jcf]=-999.
        cloudfrac_s[icf,jcf]=-999.
      else: 
        cloudfrac_v[icf,jcf] = np.sum(this)*1.0/(pts_in_dom*nbz)
        cloudfrac_s[icf,jcf] = len(np.where(np.sum(this,axis=1)>0)[0])*1.0/(pts_in_dom)
  return cloudfrac_v,cloudfrac_s

dz = vlev_obs[2]-vlev_obs[1]
f=open('ratio_cfv_cfs.txt','w')
for nbz in range(1,12):
    print 'delta z',dz*nbz
    cfv,cfs = cloudfrac_ratio(nbz)
    ratio = cfv
    ratio[cfs>0]/=cfs[cfs>0]
    ratio[cfs<=0]=np.float('nan')
    print 'mean cfv/cfs', np.nanmean(ratio)
#   ratio[cfs==0]=0.
#   f.write(str(dz*nbz)+'\n')
#   [f.write('%f '%i) for i in ratio.reshape(ratio.size)]
#   f.write('\n')
f.close()
