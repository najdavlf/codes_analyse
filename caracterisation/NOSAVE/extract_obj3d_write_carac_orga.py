#!/usr/bin/python
# -*- coding: latin-1 -*-

"""
Lit un fichier netcdf contenant des objets (nuages) numérotés
Lit un fichier netcdf LES associé, extrait certaines variables (champs 3D)
Calcule des moyennes et std des variables extraites dans chaque nuage détecté à chaque instant avec information verticale
Calcule des caractéristiques morphologiques pour chaque nuage avec info verticale
Ecrit les donnees dans un fichier netcdf
"""

import sys
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from matplotlib import pyplot as plt
import seaborn as sns
sns.set(color_codes=True)

# Lit le fichier objets, nombre d'objets, dimension des champs
objfile = sys.argv[1]
objdata = ncdf.Dataset(objfile, "r")
objets = objdata.variables["objets"][:]
objets = objets.reshape(objets.shape)
numobj = np.unique(objets[objets>0])
nbobjs = len(numobj)
print nbobjs, "objects in the field"

xvec = objdata.variables['W_E_direction'][:]
yvec = objdata.variables['S_N_direction'][:]
zvec = objdata.variables['vertical_levels'][:]
objdata.close()

dx = xvec[1]-xvec[0]
dy = yvec[1]-yvec[0]
ds = dx*dy
xlen = xvec.max()-xvec.min()+dx
ylen = yvec.max()-yvec.min()+dy

# Ecrit le nouveau fichier, dimensions, variables 
newfile = sys.argv[2]
newdata = ncdf.Dataset(newfile, "w")
zdim = newdata.createDimension('vertical_levels', len(zvec))
tdim = newdata.createDimension('time',1)
ndim = newdata.createDimension('cloud',nbobjs)
cdim = newdata.createDimension('carac',4)

volum = newdata.createVariable("volume", 'f8', ('time','cloud','carac','vertical_levels'))
frvol = newdata.createVariable("volumic_frac", "f8", ('time','cloud','carac','vertical_levels'))
tophe = newdata.createVariable("top","f8", ('time','cloud','carac','vertical_levels'))
baseh = newdata.createVariable("base","f8",('time','cloud','carac','vertical_levels'))
etend = newdata.createVariable("area","f8",('time','cloud','carac','vertical_levels'))
eproj = newdata.createVariable("cover","f8",('time','cloud','carac','vertical_levels'))
distv = newdata.createVariable("neighbor_dist","f8",('time','cloud','carac','vertical_levels'))
xbary = newdata.createVariable("W_E_barycenter","f8",('time','cloud','carac','vertical_levels'))
ybary = newdata.createVariable("S_N_barycenter","f8",('time','cloud','carac','vertical_levels'))

varxbary = np.empty(xbary.shape)
varybary = np.empty(ybary.shape)
varvolum = np.empty(volum.shape)
varfrvol = np.empty(frvol.shape)
vartophe = np.empty(tophe.shape)
varbaseh = np.empty(baseh.shape)
varetend = np.empty(etend.shape)
vareproj = np.empty(eproj.shape)
vardistv = np.empty(distv.shape)

color=np.array(nbobjs*("k",))

already = False

# Boucle sur les champs dont on veut calculer les caracs 
lesfile = sys.argv[3]
lesdata = ncdf.Dataset(lesfile, "r")
for (indvar,var) in enumerate(lesdata.variables.keys()):
  print var
  # Lit champ dans le netcdf
  field = lesdata.variables[var][:]
  field = field.reshape(field.shape)
  if (var!="vertical_levels" and len(field.shape)!=3 and var!="time") :  
    if (var=='W_E_direction'):
      field = np.broadcast_to(field, (1,len(zvec),len(yvec),len(xvec)))
    if (var=='S_N_direction'):
      field = np.broadcast_to(field, (1,len(zvec),len(yvec),len(xvec)))
      field = np.swapaxes(field,2,3)
    newfi = newdata.createVariable(var, 'f8', ('time','cloud','carac','vertical_levels'))
    tmpfield = np.empty(newfi.shape)
    dz = 0.*zvec
    cumdz=0.
    for (iz,z) in enumerate(zvec) :
      if (indvar==0):
        if (iz==0):
          dz[iz]=2*z
          cumdz=dz[iz]+cumdz
        else :
          dz[iz]=2*(z-cumdz)
          cumdz=dz[iz]+cumdz
      if (objets[0,iz,:,:].max()>0):
        zobjet = objets[0,iz,:,:]
        zfield = field[0,iz,:,:]
        for (num,cloud) in enumerate(numobj) :
          if (len(zobjet[zobjet==cloud])>0):
            masque = np.ma.array(zfield,mask=np.ma.masked_not_equal(zobjet,cloud).mask)
            tmpfield[0,num,0,iz] = np.ma.mean(masque)
            tmpfield[0,num,1,iz] = np.ma.min(masque)
            tmpfield[0,num,2,iz] = np.ma.max(masque)
            tmpfield[0,num,3,iz] = np.ma.std(masque)

            if (var=="W_E_direction" or var=="S_N_direction"):
              if (var=="W_E_direction"):
                valmin=xvec.min()
                valmax=xvec.max()
                vallen=xlen
              else :
                valmin=yvec.min()
                valmax=yvec.max()
                vallen=ylen
              if (masque.min()==valmin and masque.max()==valmax):
                  # cyclic
                  color[num]="g"
                  tmpfield[0,num,1,iz] = masque[(masque> vallen/2.)].min()
                  tmpfield[0,num,2,iz] = masque[(masque<=vallen/2.)].max()
                  bary1 = np.ma.mean(masque[(masque> vallen/2.)])
                  bary2 = np.ma.mean(masque[(masque<=vallen/2.)])
                  lenba = vallen-(bary1-bary2)
                  ratio = masque[(masque<=vallen/2.)].count()/float(masque.count())
                  tmpfield[0,num,0,iz] = (bary1+ratio*lenba)%vallen

            if (indvar==0):
              volume = (xlen*ylen)*((zvec.max()+ dz[-1]/2.)-(zvec.min()-dz[0]/2.))
              varetend[:,num,0,iz] = masque.count()*ds   
              varvolum[0,num,0,0] = varvolum[0,num,0,0]  + varetend[0,num,0,iz]*dz[iz]
              varfrvol[0,num,0,0] = varfrvol[0,num,0,0]  + varvolum[0,num,0,0] /volume

    if (var=="W_E_direction" or var=="S_N_direction"):
      if (var=="W_E_direction"):
          vallen=xlen
      else : 
          vallen=ylen
      for (num,cloud) in enumerate(numobj):
        tcl,zcl,ycl,xcl = np.where(objets==cloud)
        bary = tmpfield[0,num,0,np.unique(zcl)]
        if (bary.max()-bary.min() > vallen/2.):
            # cyclic
            color[num]="r"
            bary1 = np.mean(bary[bary> vallen/2.])
            bary2 = np.mean(bary[bary<=vallen/2.])
            lenba = vallen - (bary1-bary2)
            ratio = len(bary[bary<=vallen/2.])/float(len(bary))
            varbary = (bary1+ratio*lenba)%vallen
        else :
            varbary = np.mean(bary)
        if (var=="W_E_direction"):
          varxbary[0,num,0,0] = varbary
        else :
          varybary[0,num,0,0] = varbary
    newfi[:,:,:,:]=tmpfield

etend[:,:,:,:]=varetend
volum[:,:,:,:]=varvolum
frvol[:,:,:,:]=varfrvol
xbary[:,:,:,:]=varxbary
ybary[:,:,:,:]=varybary

for (num,cloud) in enumerate(numobj):
  a = np.array(zip(ycl,xcl))
  xylist = [list(x) for x in set(tuple(x) for x in a)]
  topc = np.empty(len(xylist))
  basc = np.empty(len(xylist))
  for (c,(y,x)) in enumerate(xylist):
    topc[c] = zvec[zcl[np.where((ycl==y) * (xcl==x))]].max()
    basc[c] = zvec[zcl[np.where((ycl==y) * (xcl==x))]].min()
  vareproj[0,num,0,0] = len(xylist)*ds 
  vartophe[0,num,0,0] = np.mean(topc)
  vartophe[0,num,1,0] = topc.min()
  vartophe[0,num,2,0] = zvec[zcl.max()]
  vartophe[0,num,3,0] = np.std(topc)
  varbaseh[0,num,0,0] = np.mean(basc)
  varbaseh[0,num,1,0] = zvec[zcl.min()]
  varbaseh[0,num,2,0] = basc.max()
  varbaseh[0,num,3,0] = np.std(basc)
  distvois = np.zeros(len(numobj))
  distvois2D = np.zeros(len(numobj))
  print "distance nuage", cloud
  for (numvois,cloudvois) in enumerate(numobj):
      zvois = np.where(objets==cloudvois)[1]
      xdist = abs(varxbary[0,numvois,0,0]-varxbary[0,num,0,0]) 
      ydist = abs(varybary[0,numvois,0,0]-varybary[0,num,0,0])
      zdist = abs(np.mean(zvec[zcl]) - np.mean(zvec[zvois]))
      # cyclic forcing 
      if (xdist>(xlen/2)) :
        xdist = xlen - xdist
      if (ydist>(ylen/2)) :
        ydist = ylen - ydist
      distvois[numvois] = np.sqrt(xdist**2+ydist**2+zdist**2)
      distvois2D[numvois] = np.sqrt(xdist**2+ydist**2)
#   sns.distplot(distvois);    
#   plt.show()
  vardistv[0,num,0,0] = np.mean(distvois2D[distvois2D!=0])  
  vardistv[0,num,1,0] = np.min(distvois2D[distvois2D!=0])
  vardistv[0,num,2,0] = np.max(distvois2D[distvois2D!=0])
  vardistv[0,num,3,0] = np.std(distvois2D[distvois2D!=0])
  vardistv[0,num,0,1] = np.mean(distvois[distvois!=0])  
  vardistv[0,num,1,1] = np.min(distvois[distvois!=0])
  vardistv[0,num,2,1] = np.max(distvois[distvois!=0])
  vardistv[0,num,3,1] = np.std(distvois[distvois!=0])
  print '... done'

distv[:,:,:,:]=vardistv
tophe[:,:,:,:]=vartophe
baseh[:,:,:,:]=varbaseh
eproj[:,:,:,:]=vareproj

plt.figure()
plt.scatter(varxbary[0,:,0,0],varybary[0,:,0,0],s=vareproj[0,:,0,0]*784,c=color,alpha=0.7)
plt.plot([0,12.8],[0,0],"k-")
plt.plot([0,0],[0,12.8],"k-")
plt.plot([0,12.8],[12.8,12.8],"k-")
plt.plot([12.8,12.8],[0,12.8],"k-")
plt.savefig("field_detect.pdf")


newdata.close()
