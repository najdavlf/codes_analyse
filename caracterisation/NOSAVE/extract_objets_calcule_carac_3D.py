#!/usr/bin/python
# -*- coding: latin-1 -*-

# N. Villefranque
# 01 / 12 / 2016 
# Modifs :
# 03/03/2017 -> Ajout cas de cumulus SCMS 
# 18/08/2017 -> Grands domaines (12.8km)

"""
Lit un fichier netcdf contenant des objets (nuages) numérotés
Lit des fichiers netcdf LES, extrait certaines variables (champs 3D)
Calcule des moyennes et std des variables extraites dans chaque nuage détecté à chaque instant et dans chaque simu
Calcule des caractéristiques morphologiques pour chaque nuage
Ecrit les données calculées dans un fichier : une ligne par nuage, une colonne par variable (simu, heure, num nuage, morpho, thermo, meteo)
"""


import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time, datetime
import six
from matplotlib import colors
import matplotlib.patches as mpatches

colors_ = list(six.iteritems(colors.cnames))
sys.setrecursionlimit(50000)

######################################## FUNCTIONS ########################################

def extrait_objets(filename,indt):
  print "Lecture objets"
  ncfile = ncdf.Dataset(filename,'r') # whole path must be given in filename
  t = ncfile.variables['time'][:]
  objets_float = ncfile.variables['objets'][indt,:,:,:]
  objets = np.array(objets_float,dtype=int)
  ncfile.close()
  return [t,objets]

def extrait_var(filename,indt):
  print "Extraction champs 3D"
  ncfile = ncdf.Dataset(filename,'r') # whole path must be given in filename
  z = ncfile.variables['VLEV'][:,0,0]
  y = ncfile.variables['S_N_direction'][:]
  x = ncfile.variables['W_E_direction'][:]
  th = ncfile.variables['THT'][indt,:,:,:] # t, z, y, x
  rv = ncfile.variables['RVT'][indt,:,:,:]
  rc = ncfile.variables['RCT'][indt,:,:,:]
  rr = ncfile.variables['RRT'][indt,:,:,:]
  pr = ncfile.variables['PABST'][indt,:,:,:]
  ke = ncfile.variables['TKET'][indt,:,:,:]
  ut = ncfile.variables['UT'][indt,:,:,:]
  vt = ncfile.variables['VT'][indt,:,:,:]
  wt = ncfile.variables['WT'][indt,:,:,:]
  sv = ncfile.variables['SVT001'][indt,:,:,:]
  ncfile.close()

  return [x,y,z,th,rv,rc,rr,pr,ke,ut,vt,wt,sv]

def calcule_mean_min_max_std(var,objets,nb_obj):
  """ calcule pour chaque nuage :
  moyenne et ecart type de la var"""
  # var : variable dont on veut calculer la moyenne et la var pour chaque nuage champ 3D
  # objets : objets numerotes en 3D
  mean = np.zeros(nb_obj,dtype=float)
  nmin = np.zeros(nb_obj,dtype=float)
  nmax = np.zeros(nb_obj,dtype=float)
  std = np.zeros(nb_obj,dtype=float)
  for num_obj in range(1,nb_obj+1):
    mean[num_obj-1] = np.mean(var[objets==num_obj])
    nmin[num_obj-1] = var[objets==num_obj].min()
    nmax[num_obj-1] = var[objets==num_obj].max()
    std[num_obj-1] = np.std(var[objets==num_obj])
  
  return [mean,nmin,nmax,std]


def calcule_morpho(objets,xx,yy,zz):
  """ calcule pour chaque nuage : 
  etendue max et moy , epaisseur max et moy , volume """
  # objets : objets numérotés en 3D
  # x,y,z : vecteurs extraits des fichiers nc
  nb_obj = objets.max()
  dx=xx[1]-xx[0]
  dy=yy[1]-yy[0]
  dz=zz[1]-zz[0]
  ds=dx*dy
  dv=ds*dz
  volume_simu = (xx[-1]-xx[0]+dx)*(yy[-1]-yy[0]+dy)*(zz[-1]-zz[0]+dz) # coordonnées aux centres des mailles
  
  # MEAN MIN MAX STD
  etendue = np.zeros((4,nb_obj),dtype=float)
  base = np.zeros((4,nb_obj),dtype=float)
  top = np.zeros((4,nb_obj),dtype=float)
  hauteur = np.zeros((4,nb_obj),dtype=float)
  volume = np.zeros(nb_obj,dtype=float) 
  fraction_volume = np.zeros(nb_obj,dtype=float) 
  etendue_projetee = np.zeros(nb_obj,dtype=float) 
  xmin = np.zeros(nb_obj, dtype=float)
  xmax = np.zeros(nb_obj, dtype=float)
  ymin = np.zeros(nb_obj, dtype=float)
  ymax = np.zeros(nb_obj, dtype=float)

  for num in range(1,nb_obj+1):
    tmp = objets*(objets==num)
    ind = np.nonzero(tmp)           # liste des indices (z,y,x) des cellules de l'objet numero "num"
    zind = ind[0]
    yind = ind[1]
    xind = ind[2]
    z_list = np.unique(zind)        # liste des indices (z) des niveaux verticaux de l'objet numero "num"
    nz = len(z_list)                # nombre de niveaux verticaux de l'objet numero "num"

    volume[num-1] = len(zind)*dv    # volume = nombre de cellules de l'objet * volume d'une cellule
    fraction_volume[num-1] = volume[num-1]/volume_simu

    base[1,num-1] = zz[z_list.min()]-dz/2.
    top[2,num-1] = zz[z_list.max()]+dz/2.

    xmin[num-1] = (xind.min() - .5)*dx          # positions min et max 
    xmax[num-1] = (xind.max() + .5)*dx          # des nuages dans les 2
    ymin[num-1] = (yind.min() - .5)*dy          # direction horizontales
    ymax[num-1] = (yind.max() + .5)*dy          # (pour les bounding box)

    etendue_z = np.zeros(nz,dtype=float)

    for (i,z) in enumerate(z_list):             # a chaque niveau vertical de l'objet 
      etendue_z[i] = len(zind[zind==z])*ds      # l'etendue du niveau = le nombre de cellules de
                                                # l'objet à ce niveau * la surface d'une cellule

    etendue[0,num-1] = np.mean(etendue_z)       # moyenne des etendues de chaque niveau
    etendue[1,num-1] = etendue_z.min()          # min des etendues de chaque niveau
    etendue[2,num-1] = etendue_z.max()          # max des etendues de chaque niveau
    etendue[3,num-1] = np.std(etendue_z)        # ecart type des etendues de chaque niveau
    
    a = np.array(zip(yind,xind))                # liste des indices (y,x) des colonnes de l'objet
    xy_list = np.unique(a.view(np.dtype((np.void, a.dtype.itemsize*a.shape[1])))).view(a.dtype).reshape(-1, a.shape[1])
                                                # liste unique des indices (http://stackoverflow.com/questions/16970982/find-unique-rows-in-numpy-array)

    etendue_projetee[num-1] = len(xy_list)*ds   # etendue projetee = nombre de colonnes * surface d'une colonne
                                                # ATTENTION c'est l'etendue projetee compactee...
    
    nc = len(xy_list)                           # nombre de colonnes dans l'objet
    hauteur_xy = np.zeros(nc, dtype=float)      # hauteur de chaque colonne
    base_xy = np.zeros(nc, dtype=float)         # niveau de la base de chaque colonne
    top_xy = np.zeros(nc, dtype=float)          # niveau du top de chaque colonne

    for (c,(y,x)) in enumerate(xy_list):
      tmpc = np.array(objets[:,y,x])            # extraction de la colonne
      hauteur_xy[c] = len(tmpc[tmpc==num])*dz   # hauteur = nombre de mailles de la colonne * hauteur d'une maille
      base_xy[c] = zz[np.array(np.nonzero(tmpc==num)).min()] - dz/2.
      top_xy[c] = zz[np.array(np.nonzero(tmpc==num)).max()]  + dz/2.

    hauteur[0,num-1] = np.mean(hauteur_xy)
    hauteur[1,num-1] = hauteur_xy.min()
    hauteur[2,num-1] = hauteur_xy.max()
    hauteur[3,num-1] = np.std(hauteur_xy)
    base[0,num-1] = np.mean(base_xy)
    base[2,num-1] = base_xy.max()
    base[3,num-1] = np.std(base_xy)
    top[0,num-1] = np.mean(top_xy)
    top[1,num-1] = top_xy.min()
    top[3,num-1] = np.std(top_xy)

  return [ volume, fraction_volume, etendue_projetee, etendue, hauteur, base, top, xmin, xmax, ymin, ymax ] 

def write_data(filename, idSimu, idCas, indt, heure, all_data):
  """ Ajoute à la fin d'un fichier, les lignes correspondant à la simulation en cours 
  à une heure donnée. Une ligne par nuage. Une colonne par variable calculée"""
  # filename : nom du fichier de donnees
  # all_data : contient les variables thermo / meteo et la morpho
  datafile = open(filename, "a")
  [ mean, nmin, nmax, std, morpho ] = all_data
  [ volume, fraction_volume, etendue_projetee, etendue, hauteur, base, top, xmin, xmax, ymin, ymax ] = morpho
  nb_obj = len(volume)

  for num_obj in range(1,nb_obj+1):
    # indicatif simu et nuage , volume et fraction volumique
    datafile.write("%s\t%s\t%i\t%s\t%i\t%.3e\t%.3e\t%.3e\t" %(idCas,idSimu,indt+1,heure,num_obj,volume[num_obj-1],fraction_volume[num_obj-1],etendue_projetee[num_obj-1]))
    # morpho mean min max et std
    for var_morph in (etendue, hauteur, base, top):
      datafile.write("%.3e\t%.3e\t%.3e\t%.3e\t" %(var_morph[0,num_obj-1],var_morph[1,num_obj-1],var_morph[2,num_obj-1],var_morph[3,num_obj-1]))
    # champs LES mean min max et std
    for (var_mean, var_min, var_max, var_std) in zip(mean[:,num_obj-1],nmin[:,num_obj-1],nmax[:,num_obj-1],std[:,num_obj-1]):
      datafile.write("%.3e\t%.3e\t%.3e\t%.3e\t" %(var_mean, var_min, var_max, var_std))
    datafile.write("%.3e\t%.3e\t%.3e\t%.3e" %(xmin[num_obj-1],xmax[num_obj-1],ymin[num_obj-1],ymax[num_obj-1]))
    datafile.write('\n')
  datafile.close()


def main_calcul(path, pathSimu, idSimu, idCas, datafile):
  """ Calcule et écrit dans un fichier pour UNE simu """
  # idSimu : ["ARM","BMX","RIC","SCM"]
  objetfilename = path+pathSimu+idSimu+'.1.'+idCas+'.OBJ.nc'
  print ""
  print "*******************************"
  print "**********", idSimu," - ", idCas, "**********"
  print "*******************************"
  
  # recuperer le vecteur temps
  [t_tmp,objets0] = extrait_objets(objetfilename,0)
 
  ncfilename = path+pathSimu+idSimu+'.1.'+idCas+'.ALL.nc'
  
  nb_time = len(t_tmp)
  t = t_tmp
  heures = []
  for tt in t:                                              # manipulation du vecteur temps  
    date = datetime.timedelta(seconds=int(tt))              # pour enlever le jour
    day = date.days
    ss = tt - day*24.*3600.                                 # commence à l'heure initiale UTC
    heures.append(str(datetime.timedelta(seconds=int(ss)))) # format H:M:S
  
  for (indt,tt) in enumerate(t):
    print "---", idCas, "--", heures[indt], "---"
    [null,objets] = extrait_objets(objetfilename,indt)
    
    if (objets.max() > 0):
      print objets.max(), "nuage(s) détecté(s)"
      [x,y,z,th,rv,rc,rr,pr,ke,ut,vt,wt,sv] = extrait_var(ncfilename,indt)
      wind = np.sqrt(ut**2 + vt**2)
      list_var = [th,rv,rc,rr,pr,ke,wind,ut,vt,wt,sv]
      nb_var = len(list_var)
      nb_obj = objets.max()
      
      print "Calcul des caractéristiques"
      mean = np.zeros((nb_var,nb_obj),dtype=float)
      nmin = np.zeros((nb_var,nb_obj),dtype=float)
      nmax = np.zeros((nb_var,nb_obj),dtype=float)
      std = np.zeros((nb_var,nb_obj),dtype=float)
      # mean nmin nmax et std : des tableaux (nb_var , nb_objets)
      for (indv, var) in enumerate(list_var):
              [ mean[indv,:] , nmin[indv,:] , nmax[indv,:], std[indv,:] ] = calcule_mean_min_max_std(var,objets,nb_obj)
      morpho = calcule_morpho(objets,x,y,z)
     
      all_data = [ mean, nmin, nmax, std, morpho ]
      datafilename = path+datafile
      write_data(datafilename, idSimu, idCas, indt, heures[indt], all_data)
    else:
      print "Aucun nuage détecté"
  
  return 0
  
###########################################################################################
###########################################################################################
######################################## PROGRAMME ########################################
###########################################################################################
###########################################################################################

start0 = time.clock()
# PATH
path = "/cnrm/moana/user/villefranquen/CAS_LES/SIMULATIONS/"

# LISTES 
listPathSimu = ["ARM_LES/L12km/","BOMEX_LES/L12km/","SCMS_LES/L12km/","RICO_LES/L12km/"]
listIdSimu   = ["L12km","L12km","L12km","L12km"]      #["CERK4","VMFTH","THL3D","CER25"]
listIdCas    = ["ARMCU","BOMEX","SCMS","RICO"]      #["ARM","BMX","SCM","RIC"]
# DATAFILE
datafile = "datafile_L12km"

# SI EXISTANT, RENAME PREVIOUS DATAFILE
if (os.path.isfile(path+datafile)): 
  os.rename(path+datafile, path+"/DATAFILES/"+datafile+'_'+str(os.path.getmtime(path+datafile)))

# EN-TETES VARIABLES
dfile = open(path+datafile,"w")
dfile.write("ID\tSIMU\tnH\tH\tN\tVOLUME\tVOLUME_frac\tETENDUE_proj\tETENDUE_mean\tETENDUE_min\tETENDUE_max\tETENDUE_std\tHAUTEUR_mean\tHAUTEUR_min\tHAUTEUR_max\tHAUTEUR_std\tBASE_mean\tBASE_min\tBASE_max\tBASE_std\tTOP_mean\tTOP_min\tTOP_max\tTOP_std\tTH_mean\tTH_min\tTH_max\tTH_std\tRV_mean\tRV_min\tRV_max\tRV_std\tRC_mean\tRC_min\tRC_max\tRC_std\tRR_mean\tRR_min\tRR_max\tRR_std\tPR_mean\tPR_min\tPR_max\tPR_std\tTKE_mean\tTKE_min\tTKE_max\tTKE_std\tWIND_mean\tWIND_min\tWIND_max\tWIND_std\tU_mean\tU_min\tU_max\tU_std\tV_mean\tV_min\tV_max\tV_std\tW_mean\tW_min\tW_max\tW_std\tSV1_mean\tSV1_min\tSV1_max\tSV1_std\txmin\txmax\tymin\tymax\n")
dfile.close()

# BOUCLE SUR LES SIMU
for (pathSimu,idSimu,idCas) in zip(listPathSimu,listIdSimu,listIdCas):
  # APPEL MAIN FUNCTION
  start = time.clock()
  print "*--------------------------------------------*"
  main_calcul(path, pathSimu, idSimu, idCas, datafile)
  stop = time.clock()
  print ""
  print "Fin execution ", idSimu
  print "Duree execution :", stop-start, "secondes (", (stop-start)/60.," minutes )"
  print ""
  print "*--------------------------------------------*"

stop0 = time.clock()

print "FIN EXECUTION"
print "DUREE TOTALE EX : ",stop0-start0, "secondes (soit", (stop0-start0)/3600., "heures)"
