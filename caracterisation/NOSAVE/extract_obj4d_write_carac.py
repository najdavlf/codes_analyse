#!/usr/bin/python
# -*- coding: latin-1 -*-

"""
Lit un fichier netcdf contenant des objets (nuages) numérotés
Lit un fichier netcdf LES associé, extrait certaines variables (champs 3D)
Calcule des moyennes et std des variables extraites dans chaque nuage détecté à chaque instant avec information verticale
Calcule des caractéristiques morphologiques pour chaque nuage avec info verticale
Ecrit les donnees dans un fichier netcdf
"""

import sys
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from matplotlib import pyplot as plt

if (len(sys.argv) < 2) :
    print "Usage : python", sys.argv[0], "objfile [ncdfile]"
    exit()

# Lit le fichier objets, nombre d'objets, dimension des champs
objfile = sys.argv[1]
objdata = ncdf.Dataset(objfile, "r")
objets = objdata.variables["objets"][:]
print "t, z, y, x :", objets.shape
objets = objets.reshape(objets.shape)
nbtime = objets.shape[0]
numobj = np.unique(objets[objets>0])
nbobjs = len(numobj)
print nbobjs, "objects in the field"

tvec = objdata.variables['time'][:]
xvec = objdata.variables['W_E_direction'][:]
yvec = objdata.variables['S_N_direction'][:]
#zvec = objdata.variables['VLEV'][:,0,0]
zvec = objdata.variables['vertical_levels'][:]
objdata.close()

dx = xvec[1]-xvec[0]
dy = yvec[1]-yvec[0]
ds = dx*dy
xlen = xvec.max()-xvec.min()+dx
ylen = yvec.max()-yvec.min()+dy

# Ecrit le nouveau fichier, dimensions, variables 
newfile = objfile.split(".obj.nc")[0]+".carac.nc"
newdata = ncdf.Dataset(newfile, "w")
zdim = newdata.createDimension('vertical_levels', len(zvec))
tdim = newdata.createDimension('time',nbtime)
ndim = newdata.createDimension('cloud',nbobjs)
cdim = newdata.createDimension('carac',4)


dz = 0.*zvec
cumdz=0.
for (iz,z) in enumerate(zvec) :
  if (iz==0):
    dz[iz]=2*z
    cumdz=dz[iz]+cumdz
  else :
    dz[iz]=2*(z-cumdz)
    cumdz=dz[iz]+cumdz
 
# Boucle sur les champs dont on veut calculer les caracs 
if (len(sys.argv)==3) :
    lesfile = sys.argv[2]
else : 
    lesfile = objfile.split(".obj.nc")[0]+".nc"
lesdata = ncdf.Dataset(lesfile, "r")

listField=[]
listVar=[]
for (indvar,var) in enumerate(lesdata.variables.keys()):
  if (var != "VLEV" and var!="vertical_levels"  and var!="time") :  
    # creation du champ correspondant dans le netcdf
    if ((len(lesdata.variables[var].shape)!=3) and (len(lesdata.variables[var].shape)!=2) and (len(lesdata.variables[var].shape)!=0)):
      print var
      # besoin de reshape w_e et s_n
      if (var=='W_E_direction'):
        wefield = lesdata.variables[var][:]
        wefield = np.broadcast_to(wefield, (nbtime,len(zvec),len(yvec),len(xvec)))
      elif (var=='S_N_direction'):
        snfield = lesdata.variables[var][:]
        snfield = np.broadcast_to(snfield, (nbtime,len(zvec),len(yvec),len(xvec)))
        snfield = np.swapaxes(snfield,2,3)
      else :
        listVar = np.append(listVar,var)

allFields = np.empty((len(zvec),4,nbobjs,nbtime,len(listVar)+10))
      
for (it, t) in enumerate(tvec):
  if (objets[it,:,:,:].max()>0) :
    print "Computing for time ",t,"..."
    start=time.time()
    for (num,cloud) in enumerate(numobj) :
      zcl,ycl,xcl = np.where(objets[it,:,:,:]==cloud)
      if (len(zcl>0)):
        for (iz,z) in zip(np.unique(zcl),zvec[np.unique(zcl)]) :
            zobjet = objets[it,iz,:,:]
            maskoz = np.ma.masked_not_equal(zobjet,cloud).mask
            for (indvar,var) in enumerate(listVar):
              # Lit champ en entier dans le netcdf
              field = lesdata.variables[var][it,iz,:,:]
              field = field.reshape(field.shape)
              masque = np.ma.array(field,mask=maskoz)
              # et on écrit toutes les caractéristiques dans la variable temporaire
              allFields[iz,0,num,it,indvar] = np.ma.mean(masque)
              allFields[iz,1,num,it,indvar] = np.ma.min(masque)
              allFields[iz,2,num,it,indvar] = np.ma.max(masque)
              allFields[iz,3,num,it,indvar] = np.ma.std(masque)
            indvar+=1
            # "W_E_direction" len(listVar)+1
            masque = np.ma.array(wefield[it,iz,:,:,],mask=maskoz)
            valmin = xvec.min()
            valmax = xvec.max()
            vallen = xlen
            if (masque.min()==valmin and masque.max()==valmax):
              allFields[iz,1,num,it,indvar] = masque[(masque> vallen/2.)].min()
              allFields[iz,2,num,it,indvar] = masque[(masque<=vallen/2.)].max()
              bary1 = np.ma.mean(masque[(masque> vallen/2.)])
              bary2 = np.ma.mean(masque[(masque<=vallen/2.)])
              lenba = vallen-(bary1-bary2)
              ratio = masque[(masque<=vallen/2.)].count()/float(masque.count())
              allFields[iz,0,num,it,indvar] = (bary1+ratio*lenba)%vallen
#             allFields[iz,3,num,it,indvar] = np.std([masque[masque> vallen/2.-vallen],masque[masque<=vallen/2.]])
            else :
              allFields[iz,0,num,it,indvar] = np.ma.mean(masque)
              allFields[iz,1,num,it,indvar] = np.ma.min(masque)
              allFields[iz,2,num,it,indvar] = np.ma.max(masque)
              allFields[iz,3,num,it,indvar] = np.ma.std(masque)
            indvar+=1
            # "S_N_direction" len(listVar)+2 
            masque = np.ma.array(snfield[it,iz,:,:],mask=maskoz)
            valmin = yvec.min()
            valmax = yvec.max()
            vallen = ylen
            if (masque.min()==valmin and masque.max()==valmax):
              allFields[iz,1,num,it,indvar] = masque[(masque> vallen/2.)].min()
              allFields[iz,2,num,it,indvar] = masque[(masque<=vallen/2.)].max()
              bary1 = np.ma.mean(masque[(masque> vallen/2.)])
              bary2 = np.ma.mean(masque[(masque<=vallen/2.)])
              lenba = vallen-(bary1-bary2)
              ratio = masque[(masque<=vallen/2.)].count()/float(masque.count())
              allFields[iz,0,num,it,indvar] = (bary1+ratio*lenba)%vallen
#             allFields[iz,0,num,it,indvar] = np.std([masque[masque> vallen/2.-vallen],masque[masque<=vallen/2.]])
            else :
              allFields[iz,0,num,it,indvar] = np.ma.mean(masque)
              allFields[iz,1,num,it,indvar] = np.ma.min(masque)
              allFields[iz,2,num,it,indvar] = np.ma.max(masque)
              allFields[iz,3,num,it,indvar] = np.ma.std(masque)
            volume = (xlen*ylen)*((zvec.max()+ dz[-1]/2.)-(zvec.min()-dz[0]/2.))
            indvar+=1
            # etendue  len(listVar)+3  
            allFields[iz,0,num,it,indvar] = masque.count()*ds   
            indvar+=1
            # volume len(listVar)+4
            allFields[0,0,num,it,indvar] = allFields[0,0,num,it,indvar] + allFields[iz,0,num,it,indvar-1]*dz[iz]
            indvar+=1
            # fraction volumique len(listVar)+5
            allFields[0,0,num,it,indvar] = allFields[0,0,num,it,indvar] + allFields[iz,0,num,it,indvar-1] /volume
        a = np.array(zip(ycl,xcl))
        xylist = [list(x) for x in set(tuple(x) for x in a)]
        topc = np.empty(len(xylist))
        basc = np.empty(len(xylist))
        for (c,(y,x)) in enumerate(xylist):
           ind_zcl = zcl[np.where((ycl==y) * (xcl==x))]
           topc[c] = zvec[ind_zcl].max()+dz[ind_zcl.max()]/2.
           basc[c] = zvec[ind_zcl].min()-dz[ind_zcl.min()]/2.
        indvar+=1
        # etendue projetee len(listVar) + 6
        allFields[0,0,num,it,indvar] = len(xylist)*ds 
        indvar+=1
        # top height len(listVar) + 7
        allFields[0,0,num,it,indvar] = np.mean(topc)
        allFields[0,1,num,it,indvar] = topc.min()
        allFields[0,2,num,it,indvar] = zvec[zcl.max()]
        allFields[0,3,num,it,indvar] = np.std(topc)
        indvar+=1
        # base height len(listVar) + 8
        allFields[0,0,num,it,indvar] = np.mean(basc)
        allFields[0,1,num,it,indvar] = zvec[zcl.min()]
        allFields[0,2,num,it,indvar] = basc.max()
        allFields[0,3,num,it,indvar] = np.std(basc)
        wevar=len(listVar)
        # "W_E_direction" len(listVar)
        vallen=xlen
        bary = allFields[np.unique(zcl),0,num,it,wevar]
        indvar+=1
        # xbarycenter len(listVar)+9
        if (bary.max()-bary.min() > vallen/2.):
            # cyclic
            bary1 = np.mean(bary[bary> vallen/2.])
            bary2 = np.mean(bary[bary<=vallen/2.])
            lenba = vallen - (bary1-bary2)
            ratio = len(bary[bary<=vallen/2.])/float(len(bary))
            allFields[0,0,num,it,indvar] = (bary1+ratio*lenba)%vallen
            allFields[0,1,num,it,indvar] = min(bary[bary> vallen/2.])
            allFields[0,2,num,it,indvar] = max(bary[bary<=vallen/2.])
#           allFields[0,3,num,it,indvar] = np.std([bary[bary> vallen/2.]-vallen , bary[bar<=vallen/2.]])
        else :
            allFields[0,0,num,it,indvar] = np.mean(bary)
            allFields[0,1,num,it,indvar] = min(bary)
            allFields[0,2,num,it,indvar] = max(bary)
#           allFields[0,3,num,it,indvar] = np.std(bary)
        snvar=len(listVar)+1
        # "S_N_direction" len(listVar)+2
        vallen=ylen
        bary = allFields[np.unique(zcl),0,num,it,snvar]
        indvar+=1
        # ybarycenter len(listVar)+10
        if (bary.max()-bary.min() > vallen/2.):
            # cyclic
            bary1 = np.mean(bary[bary> vallen/2.])
            bary2 = np.mean(bary[bary<=vallen/2.])
            lenba = vallen - (bary1-bary2)
            ratio = len(bary[bary<=vallen/2.])/float(len(bary))
            allFields[0,0,num,it,indvar] = (bary1+ratio*lenba)%vallen
            allFields[0,1,num,it,indvar] = min(bary[bary> vallen/2.])
            allFields[0,2,num,it,indvar] = max(bary[bary<=vallen/2.])
#           allFields[0,3,num,it,indvar] = np.std([bary[bary> vallen/2.]-vallen , bary[bar<=vallen/2.]])
        else :
            allFields[0,0,num,it,indvar] = np.mean(bary)
            allFields[0,1,num,it,indvar] = min(bary)
            allFields[0,2,num,it,indvar] = max(bary)
#           allFields[0,3,num,it,indvar] = np.std(bary)

    print "OK ! ",time.time()-start, "s"

for (indvar,var) in enumerate(listVar):
    newfi = newdata.createVariable(var, 'f8', ('vertical_levels','carac','cloud','time'))
    newfi[:,:,:,:] = allFields[:,:,:,:,indvar]

indvar+=1
wevar = newdata.createVariable("W_E_direction", 'f8', ('vertical_levels','carac','cloud','time'))
wevar[:,:,:,:] = allFields[:,:,:,:,indvar]

indvar+=1
snvar = newdata.createVariable("S_N_direction", 'f8', ('vertical_levels','carac','cloud','time'))
snvar[:,:,:,:] = allFields[:,:,:,:,indvar]

indvar+=1
etend = newdata.createVariable("area","f8",('vertical_levels','carac','cloud','time'))
etend[:,:,:,:] = allFields[:,:,:,:,indvar]

indvar+=1
volum = newdata.createVariable("volume", 'f8', ('cloud','time'))
volum[:,:]=allFields[0,0,:,:,indvar]

indvar+=1
frvol = newdata.createVariable("volumic_frac", "f8", ('cloud','time'))
frvol[:,:]=allFields[0,0,:,:,indvar]

indvar+=1
eproj = newdata.createVariable("cover","f8",('cloud','time'))
eproj[:,:]=allFields[0,0,:,:,indvar]

indvar+=1
tophe = newdata.createVariable("top","f8", ('carac','cloud','time'))
tophe[:,:,:] = allFields[0,:,:,:,indvar]

indvar+=1
baseh = newdata.createVariable("base","f8",('carac','cloud','time'))
baseh[:,:,:] = allFields[0,:,:,:,indvar]

indvar+=1
xbary = newdata.createVariable("W_E_barycenter","f8",('carac','cloud','time'))
xbary[:,:,:] = allFields[0,:,:,:,indvar]

indvar+=1
ybary = newdata.createVariable("S_N_barycenter","f8",('carac','cloud','time'))
ybary[:,:,:] = allFields[0,:,:,:,indvar]

newdata.close()

