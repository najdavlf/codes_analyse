#!/usr/bin/python
# -*- coding: latin-1 -*-

"""
Lit un fichier netcdf contenant des objets (nuages) numérotés
Lit un fichier netcdf LES associé, extrait certaines variables (champs 3D)
Calcule des moyennes et std des variables extraites dans chaque nuage détecté à chaque instant avec information verticale
Calcule des caractéristiques morphologiques pour chaque nuage avec info verticale
Ecrit les donnees dans un fichier netcdf
"""

import sys
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from matplotlib import pyplot as plt

# Lit le fichier objets, nombre d'objets, dimension des champs
objfile = sys.argv[1]
objdata = ncdf.Dataset(objfile, "r")
objets = objdata.variables["objets"][:]
objets = objets.reshape(objets.shape)
nbtime = objets.shape[0]
numobj = np.unique(objets[objets>0])
nbobjs = len(numobj)
print nbobjs, "objects in the field"

tvec = objdata.variables['time'][:]
xvec = objdata.variables['W_E_direction'][:]
yvec = objdata.variables['S_N_direction'][:]
zvec = objdata.variables['vertical_levels'][:]
objdata.close()

dx = xvec[1]-xvec[0]
dy = yvec[1]-yvec[0]
ds = dx*dy
xlen = xvec.max()-xvec.min()+dx
ylen = yvec.max()-yvec.min()+dy

# Ecrit le nouveau fichier, dimensions, variables 
newfile = objfile.split(".nc")[0]+"_carac.nc"
newdata = ncdf.Dataset(newfile, "w")
zdim = newdata.createDimension('vertical_levels', len(zvec))
tdim = newdata.createDimension('time',nbtime)
ndim = newdata.createDimension('cloud',nbobjs)
cdim = newdata.createDimension('carac',4)

volum = newdata.createVariable("volume", 'f8', ('time','cloud','carac','vertical_levels'))
frvol = newdata.createVariable("volumic_frac", "f8", ('time','cloud','carac','vertical_levels'))
tophe = newdata.createVariable("top","f8", ('time','cloud','carac','vertical_levels'))
baseh = newdata.createVariable("base","f8",('time','cloud','carac','vertical_levels'))
etend = newdata.createVariable("area","f8",('time','cloud','carac','vertical_levels'))
eproj = newdata.createVariable("cover","f8",('time','cloud','carac','vertical_levels'))
xbary = newdata.createVariable("W_E_barycenter","f8",('time','cloud','carac','vertical_levels'))
ybary = newdata.createVariable("S_N_barycenter","f8",('time','cloud','carac','vertical_levels'))

varxbary = np.empty((nbobjs,4,len(zvec)))
varybary = np.empty((nbobjs,4,len(zvec)))
varvolum = np.empty((nbobjs,4,len(zvec)))
varfrvol = np.empty((nbobjs,4,len(zvec)))
vartophe = np.empty((nbobjs,4,len(zvec)))
varbaseh = np.empty((nbobjs,4,len(zvec)))
varetend = np.empty((nbobjs,4,len(zvec)))
vareproj = np.empty((nbobjs,4,len(zvec)))

color=np.array(nbobjs*("k",))

# Boucle sur les champs dont on veut calculer les caracs 
lesfile = sys.argv[2]
lesdata = ncdf.Dataset(lesfile, "r")

for (indvar,var) in enumerate(lesdata.variables.keys()):
  print var
  # Lit champ en entier dans le netcdf
  field = lesdata.variables[var][:]
  field = field.reshape(field.shape)
  # seuls les champs 4D et w_e et s_n doivent être traités
  if (var!="vertical_levels" and len(field.shape)!=3 and var!="time") :  
    # besoin de reshape w_e et s_n
    if (var=='W_E_direction'):
      field = np.broadcast_to(field, (nbtime,len(zvec),len(yvec),len(xvec)))
    if (var=='S_N_direction'):
      field = np.broadcast_to(field, (nbtime,len(zvec),len(yvec),len(xvec)))
      field = np.swapaxes(field,2,3)
    # creation du champ correspondant dans le netcdf
    newfi = newdata.createVariable(var, 'f8', ('time','cloud','carac','vertical_levels'))
    dz = 0.*zvec
    cumdz=0.
    # boucle sur les instantannés 
    for (it, t) in enumerate(tvec):
     # s'il y a des nuages à ce pas de temps :
     if (objets[it,:,:,:].max()>0) :
      print t
      # extaire le pas de temps correspondant
      tobjets = objets[it,:,:,:]
      tfield = field[it,:,:,:]
      # creer un array à remplir à chaque pas de temps on copie cet array dans le fichier netcdf
      tmpfield = np.empty((nbobjs,4,len(zvec)))
      # boucle sur les nuages
      for (num,cloud) in enumerate(numobj) :
        # boucle sur les niveaux d'altitude
        for (iz,z) in enumerate(zvec) :
          # besoin de connaitre la grille verticale (dz variable ?)
          if (indvar==0 and it==0 and num==0):
           if (iz==0):
            dz[iz]=2*z
            cumdz=dz[iz]+cumdz
           else :
            dz[iz]=2*(z-cumdz)
            cumdz=dz[iz]+cumdz
          if (tobjets[iz,:,:].max()>0) :
           zobjet = tobjets[iz,:,:]
           zfield = tfield[iz,:,:]
           if (len(zobjet[zobjet==cloud])>0):
            # si ce nuage est présent parmis les nuages de ce niveau d'altitude et de ce pas de temps
            # alors on prend les maills dans le champ qui correspondent à ce nuage à cette altitude à ce pas de temps
            masque = np.ma.array(zfield,mask=np.ma.masked_not_equal(zobjet,cloud).mask)
            # et on écrit toutes les caractéristiques dans la variable temporaire
            tmpfield[num,0,iz] = np.ma.mean(masque)
            tmpfield[num,1,iz] = np.ma.min(masque)
            tmpfield[num,2,iz] = np.ma.max(masque)
            tmpfield[num,3,iz] = np.ma.std(masque)

            # pour les barycentres, traitement spécial
            if (var=="W_E_direction" or var=="S_N_direction"):
              # longueur du domaine dans les deux directions :
              if (var=="W_E_direction"):
                valmin=xvec.min()
                valmax=xvec.max()
                vallen=xlen
              else :
                valmin=yvec.min()
                valmax=yvec.max()
                vallen=ylen
              # traitement particulier si le nuage est cyclique à cette altitude
              if (masque.min()==valmin and masque.max()==valmax):
                  color[num]="g"
                  tmpfield[num,1,iz] = masque[(masque> vallen/2.)].min()
                  tmpfield[num,2,iz] = masque[(masque<=vallen/2.)].max()
                  bary1 = np.ma.mean(masque[(masque> vallen/2.)])
                  bary2 = np.ma.mean(masque[(masque<=vallen/2.)])
                  lenba = vallen-(bary1-bary2)
                  ratio = masque[(masque<=vallen/2.)].count()/float(masque.count())
                  tmpfield[num,0,iz] = (bary1+ratio*lenba)%vallen
            if (indvar==0):
              # à faire une seule fois au début : volume, etendue à chaque altitude
              volume = (xlen*ylen)*((zvec.max()+ dz[-1]/2.)-(zvec.min()-dz[0]/2.))
              varetend[num,0,iz] = masque.count()*ds   
              varvolum[num,0,0] = varvolum[num,0,0]  + varetend[num,0,iz]*dz[iz]
              varfrvol[num,0,0] = varfrvol[num,0,0]  + varvolum[num,0,0] /volume
       
        # sortie de la boucle sur les altitudes (besoin du nuage en entier)
        if (var=="W_E_direction" or var=="S_N_direction"):
         if (var=="W_E_direction"):
          vallen=xlen
         else : 
          vallen=ylen
         zcl,ycl,xcl = np.where(tobjets==cloud)
         if (len(zcl)>0):
          bary = tmpfield[num,0,np.unique(zcl)]
          a = np.array(zip(ycl,xcl))
          xylist = [x for x in set(tuple(x) for x in a)]
          topc = np.empty(len(xylist))
          basc = np.empty(len(xylist))
          for (c,(y,x)) in enumerate(xylist):
           topc[c] = zvec[zcl[np.where((ycl==y) * (xcl==x))]].max()
           basc[c] = zvec[zcl[np.where((ycl==y) * (xcl==x))]].min()
          vareproj[num,0,0] = len(xylist)*ds 
          vartophe[num,0,0] = np.mean(topc)
          vartophe[num,1,0] = topc.min()
          vartophe[num,2,0] = zvec[zcl.max()]
          vartophe[num,3,0] = np.std(topc)
          varbaseh[num,0,0] = np.mean(basc)
          varbaseh[num,1,0] = zvec[zcl.min()]
          varbaseh[num,2,0] = basc.max()
          varbaseh[num,3,0] = np.std(basc)
          if (bary.max()-bary.min() > vallen/2.):
            # cyclic
            color[num]="r"
            bary1 = np.mean(bary[bary> vallen/2.])
            bary2 = np.mean(bary[bary<=vallen/2.])
            lenba = vallen - (bary1-bary2)
            ratio = len(bary[bary<=vallen/2.])/float(len(bary))
            varbary = (bary1+ratio*lenba)%vallen
          else :
            varbary = np.mean(bary)
          if (var=="W_E_direction"):
            varxbary[num,0,0] = varbary
          else :
            varybary[num,0,0] = varbary
      newfi[it,:,:,:]=tmpfield
      if (indvar==0):
       etend[it,:,:,:]=varetend
       volum[it,:,:,:]=varvolum
       frvol[it,:,:,:]=varfrvol
       xbary[it,:,:,:]=varxbary
       ybary[it,:,:,:]=varybary
       tophe[it,:,:,:]=vartophe
       baseh[it,:,:,:]=varbaseh
       eproj[it,:,:,:]=vareproj

newdata.close()
