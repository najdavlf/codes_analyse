#!/usr/bin/python
# -*- coding: latin-1 -*-

# 01 / 12 / 2016 

"""
Lit un champ RC 4D depuis un fichier netcdf
Crée un fichier netcdf sur les mêmes dimensions avec une variable objets
Détecte les nuages du champs de RC et crée un champ objet en identifiant les nuages
Ecrit le champ objet dans le nouveau netcdf
"""


import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time, datetime
import six
from matplotlib import colors
import matplotlib.patches as mpatches

colors_ = list(six.iteritems(colors.cnames))
sys.setrecursionlimit(50000)
######################################## FUNCTIONS ########################################
def extrait_RC_ecrit_objets(filename,wfile,idSimu):
  print "Lecture fichier netcdf"
  ncfile = ncdf.Dataset(filename,'r') # whole path must be given in filename
  t = ncfile.variables['time'][:]
  z = ncfile.variables['vertical_levels'][:]
# z = ncfile.variables['VLEV'][:,0,0]
  y = ncfile.variables['S_N_direction'][:]
  x = ncfile.variables['W_E_direction'][:]
  dx = x[1]-x[0] # (km)
  dy = y[1]-y[0] # (km)
  dz = z[1]-z[0] # (km)
  dt = t[1]-t[0] # (s)

  dv = dx*dy*dz
  seuil = int((.05*.05*.05)/dv)

  print "Creation nouveau fichier"

  ncnew=ncdf.Dataset(wfile, "w", format="NETCDF3_CLASSIC")
  Tdim = ncfile.dimensions['time']
  Zdim = ncfile.dimensions['vertical_levels']
  Ydim = ncfile.dimensions['S_N_direction']
  Xdim = ncfile.dimensions['W_E_direction']


  T = ncnew.createDimension('time',len(t))
  Z = ncnew.createDimension('vertical_levels',len(z))
  Y = ncnew.createDimension('S_N_direction',len(y))
  X = ncnew.createDimension('W_E_direction',len(x))

  T=Tdim
  Z=Zdim
  Y=Ydim
  X=Xdim

  time = ncnew.createVariable('time','f8',('time',))
  time.units = "seconds since 2004-01-01 00:00:00 UTC"
  time[:] = t
  objets = ncnew.createVariable('objets','f8',('time','vertical_levels','S_N_direction','W_E_direction',))
  
  print "Identification objets"
  for (indt,tc) in enumerate(t) :
    print "---", idSimu, "--", str(datetime.timedelta(seconds=int(tc))), "---"
    rc =  ncfile.variables['RCT'][indt,:,:,:]
    masque3d = masque_RC(rc)
    objets3d = numerote_rec(masque3d,x,y,z,t,seuil)
    objets[indt,:,:,:] = objets3d

  ncfile.close()
  ncnew.close()

  return 0 


def masque_RC(rc):
  masque=np.zeros(rc.shape,dtype=int)
  masque[rc>10.**(-6)]=1
  
  return masque

def numerote_rec(masque,x,y,z,t,seuil):
  objets = np.zeros(masque.shape,dtype=int)
  nuagesNonNum = masque[(masque==1)&(objets==0)]
  count = 1
  while (len(nuagesNonNum)>0):
    indNuageux = np.nonzero(masque) ## indices 3D où masque vaut 1 
    valObjNuageux = objets[indNuageux] ## numéro nuage des cellules désignées par ces indices
    indNum = np.nonzero(valObjNuageux) ## indices du vecteur indNuageux où la cellule est déjà num dans objets
    newIndNu = np.delete(np.array(indNuageux).transpose(), indNum, 0) ## on enleve les indices 3D correspondants
    cellule = (newIndNu[0,:][0] , newIndNu[0,:][1] , newIndNu[0,:][2]) ## on prend le premier set indice 3D nuageux non numerote
    objets[cellule]=count
    
    objets = diffuse_voisins_rec(cellule,masque,objets,count,x,y,z)
    count=count+1
    nuagesNonNum = masque[(masque==1)&(objets==0)]
    
  nbObj=len(np.unique(objets))-1
  num=1
  while (num < objets.max()+1):
    indNuage = np.where(objets==num)
    taille = len(indNuage[0])
    if (taille < seuil):
      objets[objets==num] = 0
      objets[objets>num] = objets[objets>num]-1
    else:
      num=num+1
  
  nbObj=objets.max()
  print nbObj, "nuage(s) détecté(s)"
    
  return objets

def diffuse_voisins_rec(cellule,masque,objets,count,xx,yy,zz):
    
    zi,yi,xi = cellule

    indVoisins = identifie_voisins(xi,yi,zi,xx,yy,zz,masque,objets,count)
    listVois=[]
    
    for (z,y,x) in indVoisins:
      if ((masque[z,y,x]==1) & (objets[z,y,x]!=count)):
        objets[z,y,x] = count
        listVois.append((z,y,x))
    
    for (z,y,x) in listVois:
      cellule = (z,y,x)
      objets = diffuse_voisins_rec(cellule,masque,objets,count,xx,yy,zz)
    
    return objets

def identifie_voisins(xi,yi,zi,xx,yy,zz,masque,objets,count):
  if (xi < len(xx)-1): xd = xi+1
  else: xd = 0
  if (xi > 0): xg = xi-1
  else : xg = len(xx)-1
  if (yi < len(yy)-1): yd = yi+1
  else : yd = 0
  if (yi > 0): yg = yi-1
  else : yg = len(yy)-1
  if (zi < len(zz)-1): zd = zi+1
  else : zd = 0  
  if (zi > 0): zg = zi-1
  else : zg = len(zz)-1
  indVoisins=np.array([ (z,y,x) for z in [zg,zi,zd] for y in [yg,yi,yd] for x in [xg,xi,xd] if (((x!=xi)|(y!=yi)|(z!=zi))) ])
  
  return indVoisins


def main_calcul(path, pathSimu, idSimu, idCas):
  """ Calcule et écrit dans un fichier pour UNE simu """
  # idCas : ["ARM","BMX","RIC","SCM"]
  ncfilename = path+pathSimu+idSimu+'.1.'+idCas+'.ALL.nc'
  wfile = path+pathSimu+idSimu+'.1.'+idCas+'.OBJ.nc'
  print ""
  print "*******************************"
  print "********", idSimu,' - ', idCas, "********"
  print "*******************************"

  print ncfilename

  extrait_RC_ecrit_objets(ncfilename,wfile,idSimu)
  
  return 0

###########################################################################################
###########################################################################################
######################################## PROGRAMME ########################################
###########################################################################################
###########################################################################################

start0 = time.clock()
# PATH
path = "/cnrm/moana/user/villefranquen/CAS_LES/SIMULATIONS/"

# LISTES 
listPathSimu = ["ARM_LES/L12km/","BOMEX_LES/L12km/","SCMS_LES/L12km/","RICO_LES/L12km/"]
listIdSimu   = ["L12km","L12km","L12km","L12km"]      #["CERK4","VMFTH","THL3D","CER25"]
listIdCas    = ["ARMCU","BOMEX","SCMS","RICO"]      #["ARM","BMX","SCM","RIC"]

# BOUCLE SUR LES SIMU
for (pathSimu,idSimu, idCas) in zip(listPathSimu,listIdSimu,listIdCas):
  # APPEL MAIN FUNCTION
  start = time.clock()
  main_calcul(path, pathSimu, idSimu, idCas)
  stop = time.clock()
  print ""
  print "Fin execution ", idSimu, " - ", idCas
  print "Duree execution :", stop-start, "secondes (", (stop-start)/60.," minutes )"
  print ""
  print "*--------------------------------------------*"

stop0 = time.clock()

print "FIN EXECUTION"
print "DUREE EX : ",stop0-start0, "secondes (soit", (stop0-start0)/3600., "heures)"
