#!/usr/bin/python
# -*- coding: latin-1 -*-

import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from scipy.ndimage import label, generate_binary_structure

t0=time.time()

print ""
print "*** Begin cloud identification ***"
print ""

# variables
## BOOLEEN 
cyclic   = True  # force conditions cycliques aux bords
supprime = True  # supprime les objets trop petits
renomme  = False # supprime les objets trop petits et renomme pour avoir des étiquettes consecutives
trie     = False  # classe en deux catégories suivant un critere
## FILENAME
# file to read
filename = "/cnrm/moana/user/villefranquen/CAS_LES/SIMULATIONS/BOMEX_LES/L12km/L12km.1.BOMEX.114.nc"
# file to write
wfile = "/cnrm/moana/user/villefranquen/CAS_LES/SIMULATIONS/BOMEX_LES/L12km/L12km.1.BOMEX.114_fastfilter_cyclic2_obj_scipy.nc"
## CRITERES SEUILS
# minimum number of cells in a cloud
seuil_taille=8
# minimum top height of type 2 cloud (m)
seuil_top = 4000.

# generate element to represent connectivity rule (horizontal and diagonal) :
struct=generate_binary_structure(3,3)

# open ncdf file and extract dimensions and 3D field
t1 = time.time()
print "Read netcdf input data file..."
nc = ncdf.Dataset(filename,'r') # whole path must be given in filename
Zdim = nc.dimensions['vertical_levels']
Ydim = nc.dimensions['S_N_direction']
Xdim = nc.dimensions['W_E_direction']
z = nc.variables['vertical_levels'][:]
y = nc.variables['S_N_direction'][:]
x = nc.variables['W_E_direction'][:]
rc = nc.variables['RCT'][:,:,:]
nc.close()
print "... OK ! (", time.time()-t1, "s )"

# create new ncdf file and set same dimensions 
t1 = time.time()
print "Create netcdf output data file..."
ncnew=ncdf.Dataset(wfile, "w", format="NETCDF3_CLASSIC")
Z = ncnew.createDimension('vertical_levels',len(z))
Y = ncnew.createDimension('S_N_direction',len(y))
X = ncnew.createDimension('W_E_direction',len(x))
Z=Zdim
Y=Ydim
X=Xdim
zz = ncnew.createVariable('vertical_levels','f8',('vertical_levels',))
yy = ncnew.createVariable('S_N_direction','f8',('S_N_direction',))
xx = ncnew.createVariable('W_E_direction','f8',('W_E_direction',))
zz[:]=z
yy[:]=y
xx[:]=x
print "... OK ! (", time.time()-t1, "s )"

# create mask with condition on 3D field
mask=np.zeros(rc.shape,dtype=int)
mask[rc>10.**(-6)]=1

# use ndimage to identify objetcs in mask
t1 = time.time()
print "Label clouds..."
objects, nb_objects = label(mask, structure=struct)
print "... OK ! (", time.time()-t1, "s )"

# force cyclic conditions on lateral boundaries
if (cyclic):
  t1=time.time()
  print "Force cyclic conditions..."
  for k in range(len(z)) :
    # bord droit : x=x[-1], (y,z)
    for j in range(len(y)) :
      if ( (mask[k,j,len(x)-1]==1) and (mask[k,j,0]==1) and (objects[k,j,0]!=objects[k,j,len(x)-1]) ):
        objects[objects==objects[k,j,0]] = objects[k,j,len(x)-1]
    # bord fond  : y=y[-1], (x,z)
    for i in range(len(x)) :
      if ( (mask[k,len(y)-1,i]==1) and (mask[k,0,i]==1) and (objects[k,0,i]!=objects[k,len(y)-1,i]) ):
        objects[objects==objects[k,0,i]] = objects[k,len(y)-1,i]
  print "... OK ! (", time.time()-t1, "s )"

# filter small objects
# and analyse each object with criterion to decide which type it is e.g. cumulus (top < 4km) vs cumulonimbus (top >= 4km)
listObj = np.unique(objects[objects>0])
if (supprime) :
  t1 = time.time()
  print "Filter clouds according to size..."
  for num in listObj :
    taille = len(objects[objects==num])
    if (taille < seuil_taille):
      objects[objects==num] = 0
  print "... OK ! (", time.time()-t1, "s )"
elif (renomme) :
  t1 = time.time()
  print "Filter and rename clouds according to size..."
  while (len(listObj)):
    num=listObj[0]
    taille = len(objects[objects==num])
    if (taille < seuil_taille):
      objects[objects==num] = 0
      objects[objects==listObj[-1]] = num
      listObj=listObj[:-1]
    else:
      listObj=listObj[1:]
  print "... OK ! (", time.time()-t1, "s )"
if (trie):
  t1 = time.time()
  print "Classify clouds according to top height..."
  typeobjects=np.zeros(rc.shape,dtype=int)
  listObj = np.unique(objects[objects>0])
  for num in listObj :
    criterion = z[np.where(objects==num)[0].max()] < seuil_top
    if (criterion):
      typeobjects[objects==num]= 1
    else :
      typeobjects[objects==num]= 2
  print "... OK ! (", time.time()-t1, "s )"


# create object var in new ncdf file 
t1 = time.time()
print "Write data to new netcdf file..."
objets = ncnew.createVariable('objets','f8',('vertical_levels','S_N_direction','W_E_direction',))
objets[:,:,:] = objects
if (trie):
  typeobjets = ncnew.createVariable('type_objets','f8',('vertical_levels','S_N_direction','W_E_direction',))
  typeobjets[:,:,:] = typeobjects
print "... OK ! (", time.time()-t1, "s )"

ncnew.close()

print ""
print "*** End cloud identification ***"
print "Duration : ", time.time()-t0, "seconds"
print ""

