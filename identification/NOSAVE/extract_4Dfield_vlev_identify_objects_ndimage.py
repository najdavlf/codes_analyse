#!/usr/bin/python
# -*- coding: latin-1 -*-

import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from scipy.ndimage import label, generate_binary_structure
import sys

if (len(sys.argv)<2) :
    print 'Usage : python', sys.argv[0], 'ncdfile'
    exit() 

filename = sys.argv[1]
wfile = filename.split(".nc")[0]+'.obj.nc'
print wfile

t0=time.time()

print ""
print "*** Begin cloud identification ***"
print ""

# variables
## BOOLEEN 
cyclic   = True  # force conditions cycliques aux bords
supprime = False # supprime les objets trop petits
renomme  = False # supprime les objets trop petits et renomme pour avoir des étiquettes consecutives
trie     = False # classe en deux catégories suivant un critere

# minimum number of cells in a cloud
seuil_taille=8
# minimum top height of type 2 cloud (km)
seuil_top = 4.

# generate element to represent connectivity rule (horizontal and diagonal) :
struct=generate_binary_structure(4,4)

# open ncdf file and extract dimensions and 3D field
t1 = time.time()
print "Read netcdf input data file..."
nc = ncdf.Dataset(filename,'r') # whole path must be given in filename
Tdim = nc.dimensions['time']
Zdim = nc.dimensions['vertical_levels']
Ydim = nc.dimensions['S_N_direction']
Xdim = nc.dimensions['W_E_direction']
#t = np.linspace(30,300,10) 
t = nc.variables['time'][:]
#z = nc.variables['vertical_levels'][:]
z = nc.variables['VLEV'][:,0,0]
y = nc.variables['S_N_direction'][:]
x = nc.variables['W_E_direction'][:]
rc = nc.variables['RCT'][:,:,:,:]
nc.close()
print "... OK ! (", time.time()-t1, "s )"

# create new ncdf file and set same dimensions 
t1 = time.time()
print "Create netcdf output data file..."
ncnew=ncdf.Dataset(wfile, "w", format="NETCDF4_CLASSIC")
T = ncnew.createDimension('time',None)
Z = ncnew.createDimension('vertical_levels',len(z))
Y = ncnew.createDimension('S_N_direction',len(y))
X = ncnew.createDimension('W_E_direction',len(x))
T=Tdim
Z=Zdim
Y=Ydim
X=Xdim
tt = ncnew.createVariable('time','f8',('time',))
zz = ncnew.createVariable('vertical_levels','f8',('vertical_levels',))
yy = ncnew.createVariable('S_N_direction','f8',('S_N_direction',))
xx = ncnew.createVariable('W_E_direction','f8',('W_E_direction',))
tt[:]=t
zz[:]=z
yy[:]=y
xx[:]=x
print "... OK ! (", time.time()-t1, "s )"

# create mask with condition on 4D field
#mask=np.empty(rc.shape,dtype=int)
rc[rc>10.**(-6)]=1
rc[rc<=10.**(-6)]=0

# use ndimage to identify objetcs in mask
t1 = time.time()
print "Label clouds..."
#objects, nb_objects = label(mask, structure=struct)
objects, nb_objects = label(rc, structure=struct)
print "... OK ! (", time.time()-t1, "s )"

# force cyclic conditions on lateral boundaries
if (cyclic):
  t1=time.time()
  print "Force cyclic conditions..."
  for l in range(len(t)) :
   for k in range(len(z)) :
    # bord droit : x=x[-1], (y,z)
    for j in range(len(y)) :
      #if ( (mask[l,k,j,len(x)-1]==1) and (mask[l,k,j,0]==1) and (objects[l,k,j,0]!=objects[l,k,j,len(x)-1]) ):
      if ( (rc[l,k,j,len(x)-1]==1) and (rc[l,k,j,0]==1) and (objects[l,k,j,0]!=objects[l,k,j,len(x)-1]) ):
        objects[objects==objects[l,k,j,0]] = objects[l,k,j,len(x)-1]
    # bord fond  : y=y[-1], (x,z)
    for i in range(len(x)) :
     # if ( (mask[l,k,len(y)-1,i]==1) and (mask[l,k,0,i]==1) and (objects[l,k,0,i]!=objects[l,k,len(y)-1,i]) ):
      if ( (rc[l,k,len(y)-1,i]==1) and (rc[l,k,0,i]==1) and (objects[l,k,0,i]!=objects[l,k,len(y)-1,i]) ):
        objects[objects==objects[l,k,0,i]] = objects[l,k,len(y)-1,i]
  print "... OK ! (", time.time()-t1, "s )"

# filter small objects
# and analyse each object with criterion to decide which type it is e.g. cumulus (top < 4km) vs cumulonimbus (top >= 4km)
listObj = np.unique(objects[objects>0])
if (supprime) :
  t1 = time.time()
  print "Filter clouds according to size..."
  for num in listObj :
    taille = len(objects[objects==num])
    if (taille < seuil_taille):
      objects[objects==num] = 0
  print "... OK ! (", time.time()-t1, "s )"
elif (renomme) :
  t1 = time.time()
  print "Filter and rename clouds according to size..."
  while (len(listObj)):
    num=listObj[0]
    taille = len(objects[objects==num])
    if (taille < seuil_taille):
      objects[objects==num] = 0
      objects[objects==listObj[-1]] = num
      listObj=listObj[:-1]
    else:
      listObj=listObj[1:]
  print "... OK ! (", time.time()-t1, "s )"
if (trie):
  t1 = time.time()
  print "Classify clouds according to top height..."
  typeobjects=np.zeros(rc.shape,dtype=int)
  listObj = np.unique(objects[objects>0])
  for num in listObj :
    criterion = z[np.where(objects==num)[0].max()] < seuil_top
    if (criterion):
      typeobjects[objects==num]= 1
    else :
      typeobjects[objects==num]= 2
  print "... OK ! (", time.time()-t1, "s )"

# create object var in new ncdf file 
t1 = time.time()
print "Write data to new netcdf file..."
objets = ncnew.createVariable('objets','f8',('time','vertical_levels','S_N_direction','W_E_direction',))
objets[0,:,:,:] = objects
if (trie):
  typeobjets = ncnew.createVariable('type_objets','f8',('time','vertical_levels','S_N_direction','W_E_direction',))
  typeobjets[:,:,:,:] = typeobjects
print "... OK ! (", time.time()-t1, "s )"

ncnew.close()

print ""
print "*** End cloud identification ***"
print "Duration : ", time.time()-t0, "seconds"
print ""
