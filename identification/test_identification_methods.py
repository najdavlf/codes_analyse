#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : updrafts
# 13/06/2018
# N. Villefranque

import os
import numpy as np
from identification_methods import identify

ncfile = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/ARM_LES/CERK4/CERK4.1.ARMCu.008.nc"

# copy nc file to current directory (test mode)
os.system("cp "+ncfile+" .")
ncfile = "CERK4.1.ARMCu.008.nc"

listVarsNames = ['WT','RCT']

def updrafts(listVars) :
    w = listVars[0]
    mask=w*0.
    mask[w>2.5]=1 # cell is in updraft if vertical wind > 2.5m/s
    return mask

def criteria(listVars, indsObj) :
    t,z,y,x=(indsObj)
    rc = listVars[1] 
    if any(rc[t,z,y,x]>0) : return 1 # saturated updraft
    else : return 2 # unsaturated updraft

objects, types = identify(ncfile, listVarsNames, updrafts, name="test_updrafts", delete=16,  write=True, criteria=criteria)
