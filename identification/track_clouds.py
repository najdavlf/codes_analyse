#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : track clouds
# 13/06/2018
# N. Villefranque

import os
import numpy as np
from identification_methods import identify

ncfile = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/BOMEX_LES/RDFRC/RDFRC.1.BOMEX.ALL.nc4"

listVarsNames = ['RCT'] # any size. The mask dimensions must be the same as first var in list


def clouds(listVars) :
    rc = listVars[0]
    mask=rc*0.   # same shape as RCT
    mask[rc>0]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, types = identify(ncfile, listVarsNames, clouds, name="track_clouds", delete=8, rename=True,  write=True)
