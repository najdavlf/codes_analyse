#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os
import numpy as np
from identification_methods import identify

ncfile = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/ARM_LES/CERK4/CERK4.1.ARMCu.008.nc"

# copy nc file to current directory (test mode)
os.system("cp "+ncfile+" .")
ncfile = "CERK4.1.ARMCu.008.nc"

listVarsNames = ['RCT','VLEV'] # any size. The mask dimensions must be the same as first var in list


def clouds(listVars) :
    rc = listVars[0]
    mask=rc*0.   # same shape as RCT
    mask[rc>0]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

def criteria(listVars, indsObj) :
    t,z,y,x=(indsObj)
    zz = listVars[1] 
    if max(zz[z,y,x])>2. : num=1 # top height > 2 km
    else : num=2
    if len(np.unique(z))>20 : num+=2 # depth > 500m
    # 1 : top >  2km and depth <= 500m
    # 2 : top <= 2km and depth <= 500m 
    # 3 : top >  2km and depth >  500m
    # 4 : top <= 2km and depth >  500m
    return num

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, types = identify(ncfile, listVarsNames, clouds, name="test_clouds", delete=8, rename=True,  write=True, criteria=criteria)
