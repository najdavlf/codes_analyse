#!/usr/bin/python
# -*- coding: latin-1 -*-
# Adapted from previous script 
# Now object oriented and user generic 
# 13/06/2018
# N. Villefranque

import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from scipy.ndimage import generate_binary_structure
import sys


def identify(ncdfFile, listVarNames, funcCalcMask, name="objects", diagonals=True, cyclic="horizontal", delete=0, rename=False, criteria=None, write=True):
  """identify and labels user defined objects in field, with several mandatory and optional arugments
  mandatory arguments :
      - ncdfFile : name of netCDF file containing user data. Object fields will be written in the same file.
      - listVarNames : a list of strings of any size, containing the names of the fields use to describe objects
      - funcCalcMask : a user defined function to compute a mask (0 / 1) from physical user fields. The arguments are passed in the same order as defined in listVarNames.
  optional arguments :
      - name : "objects" (default) or a string to name the object field in the netCDF file
      - diagonals : True (default) or False, to consider diagonal cells as adjacent
      - cyclic : None, "horizontal" (default) or "vertical", to force cyclic conditions
        -> horizontal assumes cyclic conditions on the last 2 dimensions
        -> vertical assumes cyclic conditions on the last dimension
      - delete : 0 (default) or integer nb>0 to delete objects containing less than nb cells 
      - rename : True or False (default) to rename objects with contiguous labels
      - crieteria : a user function that should return an integer giving the category of the object. The arguments are passed in the same order as defined in listVarNames.
      - write : True (default) or False, to write the computed fields in the netCDF file
  """
  t0 = time.time()
  print 'Begin object identification'

  t1 = time.time()
  print 'Reading',listVarNames
  listVars,dims = read(ncdfFile, listVarNames)
  print '...OK (%2.2fs)' %(time.time()-t1)
  
  t1 = time.time()
  print 'Computing mask'
  userMask = funcCalcMask(listVars)
  print '...OK (%2.2fs)' %(time.time()-t1)

  t1 = time.time()
  print 'Labelling objects'
  objects = UserObject(userMask,diagonals,name,dims)
  print '...OK (%2.2fs)' %(time.time()-t1)
  
  if not isinstance(cyclic, type(None)) : 
   t1 = time.time()
   print 'Forcing cyclic conditions type',cyclic
   objects.do_cyclic(cyclic,rename)
   print '...OK (%2.2fs)' %(time.time()-t1)
  
  if delete :
   t1 = time.time()
   print 'Deleting objects smaller than',delete
   objects.do_delete(delete, rename)
   print '...OK (%2.2fs)' %(time.time()-t1)
  
  if not isinstance(criteria, type(None)) : 
   t1 = time.time()
   print 'Sorting objects' 
   objects.do_sort(criteria, listVars) 
   print '...OK (%2.2fs)' %(time.time()-t1)
  
  if write :
   t1 = time.time()
   print 'Writing',name,'to',ncdfFile
   objects.write(ncdfFile)
   print '...OK (%2.2fs)' %(time.time()-t1)

  print 'Done. (%2.2fs)' %(time.time()-t0)
  return objects.val, objects.type


class UserObject() : 
    """ This class contains methods to define and treat a field of objects"""
    def __init__(self, mask, diagonals, name, dims) :
        self.dim = len(mask.shape)
        if (len(dims)!=self.dim) : raise StandardError("Dimensions of mask and first variable in listVarNames must be the same")
        else : self.dims=dims
        self.name = name
        self.val, self.nbr = self.label(mask,diagonals)
        self.type = None
        print '\t',self.nbr,'objects identified'

    def label(self, mask, diag) :
        if diag : struct = generate_binary_structure(self.dim,self.dim)
        else :    struct = generate_binary_structure(self.dim,1)
        objects,nb_objects = sp.ndimage.label(mask, structure=struct)
        return objects, nb_objects

    def do_cyclic(self, choice, rename) : 
        if (choice=="horizontal") : do_horiz = 1
        elif (choice=="vertical") : do_horiz = 0
        else : raise NameError(choice+" is not a valid choice for cyclic conditions.\nUse None, 'horizontal' or 'vertical'")
        objects = self.val
        shape = objects.shape
        listObj = np.unique(objects[objects>0]).tolist()
        if self.dim==4 :
          n0,n1,n2,n3 = shape
        elif self.dim==3 :
          n1,n2,n3 = shape
          n0 = 1 
          objects = objects[None,:,:,:]
        elif self.dim==2 :
          n2,n3 = objects.shape
          n0 = 1 ; n1 = 1
          objects = objects[None,None,:,:]
        for i0 in range(n0) :
          for i1 in range(n1) :
            for i2 in range(n2) :
              if (objects[i0,i1,i2,n3-1] and objects[i0,i1,i2,0] and (objects[i0,i1,i2,n3-1]!=objects[i0,i1,i2,0])) :
                  num = objects[i0,i1,i2,n3-1]
                  objects[objects==num] = objects[i0,i1,i2,0]
                  if rename : objects[objects==listObj.pop(-1)]=num
            if do_horiz : 
              for i3 in range(n3) :
                if (objects[i0,i1,n2-1,i3] and objects[i0,i1,0,i3] and (objects[i0,i1,n2-1,i3]!=objects[i0,i1,0,i3])) :
                  num = objects[i0,i1,n2-1,i3]
                  objects[objects==num] = objects[i0,i1,0,i3]
                  if rename : objects[objects==listObj.pop(-1)]=num
        self.val = objects.reshape(shape)
        nbr = len(np.unique(objects[objects>0]))
        print '\t', self.nbr - nbr, 'objects on the borders'
        self.nbr=nbr


    def do_delete(self,nbmin,rename) :
        objects = self.val
        listObj = np.unique(objects[objects>0]).tolist()
        while (len(listObj)):
          num = listObj.pop(0)                 
          nbcels = len(objects[objects==num])
          if (nbcels < nbmin):
            objects[objects==num] = 0
            if rename and len(listObj) : 
              lastnum = listObj.pop(-1)
              while (len(objects[objects==lastnum])<nbmin):
                  objects[objects==lastnum]=0
                  if (len(listObj)) : lastnum = listObj.pop(-1)
                  else : break
              objects[objects==lastnum] = num 
              listObj=[num]+listObj
        self.val = objects
        nbr = len(np.unique(objects))-1 # except 0
        print '\t', self.nbr - nbr, 'objects were too small'
        self.nbr = nbr

    def do_sort(self, criteria, listVars) :
        objects = self.val
        typeObj = np.zeros(objects.shape, dtype=int)
        listObj = np.unique(objects[objects>0]).tolist()
        for num in listObj : 
            typeObj[objects==num] = criteria(listVars, np.where(objects==num))
        for t in np.unique(typeObj[typeObj>0]) :
            print '\t',len(np.unique(objects[typeObj==t])),' objects of type',t
        self.type = typeObj

    def write(self, ncdfFile) :
        dset = ncdf.Dataset(ncdfFile,'a')
        name = check_name_in_ncdf(self.name,dset.variables.keys(),ncdfFile)
        if name!='o' : self.name = name ; obj = dset.createVariable(self.name, np.int32, self.dims)
        else : obj = dset.variables[self.name]
        if self.dim==4 : obj[:,:,:,:] = self.val
        if self.dim==3 : obj[:,:,:] = self.val
        if self.dim==2 : obj[:,:] = self.val
        if not isinstance(self.type,type(None)) :
          typename = 'type_'+self.name
          name = check_name_in_ncdf(typename,dset.variables.keys(),ncdfFile)
          if name!='o' : typename=name; typeObj = dset.createVariable(typename, np.int32, self.dims)
          else : typeObj = dset.variables[typename]
          if self.dim==4 : typeObj[:,:,:,:] = self.type
          if self.dim==3 : typeObj[:,:,:] = self.type
          if self.dim==2 : typeObj[:,:] = self.type
        dset.close()
        print '\t',self.nbr, 'objects were written'
  
def check_name_in_ncdf(myname,ncvars,ncdfFile) :
    name = myname
    while(myname in ncvars) :
        print '\t', myname, 'already exists in', ncdfFile
        name = raw_input('\toverwrite (o), rename (new name), exit (e) ?\n')
        if name=='o' : break
        elif name=='e' : exit()
        else : myname = name
    return name 

def read(ncdfFile, listVarNames) :
  dset = ncdf.Dataset(ncdfFile, 'r')
  listVars = []
  if isinstance(listVarNames, type(None)) : raise TypeError('listVarNames is empty')
  if isinstance(listVarNames, str) : listVarNames = list(listVarNames)
  for i,n in enumerate(listVarNames) : 
    v = dset.variables[n]
    listVars+=[v[:].reshape(v.shape)]
    if i==0 : dims = v.dimensions
  dset.close()
  return listVars,dims
