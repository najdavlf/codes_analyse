function [product, product_attribute, product_dimension] = generate_lwc_on_model_grid(thedate, user, institution, model, site, method_lwc);

clear product*

product = []; product_attribute = []; product_dimension = [];
%site=lower(attribute.global.location);
%thedate=sprintf('%04d%02d%02d', attribute.global.year, attribute.global.month, attribute.global.day);
version = '1.1'; % Taken in part from Malcolm's original
                 % Cv_IWC_on_model_grids.m (1 nov 2004)
                 % Added lwp (13 Apr 2005)

if nargin < 6
  method_lwc = 'lwc-scaled-adiabatic';
end
  
if strcmp(method_lwc, 'lwc-adiabatic-method')
  method_lwc = ['lwc-adiabatic-method'];
  varnames = {'lwc', 'lwc_adiabatic', 'lwc_retrieval_status', 'lwc_error', 'lwp', 'lwp_error', 'lwc_th'};
  do_adiabatic = 1;
else
  method_lwc = ['lwc-scaled-adiabatic'];
  varnames = {'lwc', 'lwc_retrieval_status', 'lwc_error', 'lwp', 'lwp_error', 'lwc_th'};
  do_adiabatic = 0;
end


lwc_units = {'kg m-3','kg m<sup>-3</sup>'};
lwp_units = {'kg m-2','kg m<sup>-2</sup>'};
missing_value = -999.0;
	
%%---- Create model grid name ----%% 
%if isempty(get_model_name(model)), return, end
model_grid_name = ['cloud-fraction-' model '-grid']; 
		 
%%---- Check for availability of essential data ----%%
if ~load_model(thedate, site, model, 'test'), return, end
if ~load_product(thedate, site, model_grid_name, 'test'), disp('No cloud fraction data'), return, end
if ~load_product(thedate, site, method_lwc, 'test'), disp('No lwc data'), return, end

%%---- load cloud fraction data ----%%  
[cf_data, cf_attribute] = load_product(thedate, site, model_grid_name,'', {'time', 'height', 'rain_rate_threshold', 'latititude', 'longitude', 'model_lwc', 'model_lwc_gridscale', 'altitude','forecast_time','model_temperature','omega_500mb'});

%%---- load lwc data ----%%
[lwc, lwc_attribute] = load_product(thedate, site, method_lwc, '', {'time', 'height','altitude', 'lwp', varnames{:}});
if load_product(prev_day(thedate), site, method_lwc,'test')
  prev_lwc = load_product(prev_day(thedate), site, method_lwc, '', {'time', 'height','altitude', 'lwp', varnames{:}});

  % concatenate data structures 
  lwc.time=[prev_lwc.time-24; lwc.time];
  
  if length(lwc.height) == length(prev_lwc.height)
    for ii=1:length(varnames)
      if isfield(lwc,varnames{ii});
        lwc = setfield(lwc, varnames{ii}, [getfield(prev_lwc,varnames{ii}); getfield(lwc,varnames{ii})]);
      end    
    end
  else
    for ii=1:length(varnames)
      if isfield(lwc,varnames{ii});    
        test = size(getfield(lwc,varnames{ii}));
        if test(1) > 1 & test(2) > 1
          if length(lwc.height) > length(prev_lwc.height)
            tmpvar = getfield(lwc,varnames{ii});
            lwc = setfield(lwc, varnames{ii}, [getfield(prev_lwc,varnames{ii}); tmpvar(:,1:length(prev_lwc.height))]);
          elseif length(lwc.height) < length(prev_lwc.height)
            tmpvar = getfield(prev_lwc,varnames{ii});
            lwc = setfield(lwc, varnames{ii}, [tmpvar(:,1:length(lwc.height)); getfield(lwc,varnames{ii})]);
          end
        else
          lwc = setfield(lwc, varnames{ii}, [getfield(prev_lwc,varnames{ii}); getfield(lwc,varnames{ii})]);
        end 
      end
    end
    if length(lwc.height) > length(prev_lwc.height)
      lwc.height = lwc.height(1:length(prev_lwc.height));
    end
  end
end

% At this point lwc.height is above msl but cf_data.height and
% product.height will be above the ground.  We therefore change
% lwc.height:
lwc.height = lwc.height - cf_data.altitude;

%%---- load model data if required ----%%
[model_data, model_attribute] = load_model(thedate, site, model, '', {'horizontal_resolution', 'height', 'uwind', 'vwind','sfc_ls_rain', 'sfc_conv_rain'});

if ~isfield(cf_data,'horizontal_resolution')
  if isfield(model_data,'horizontal_resolution')
    cf_data.horizontal_resolution = model_data.horizontal_resolution;
  else
    cf_data.horizontal_resolution = 30; % in km 
  end
end
product.horizontal_resolution = cf_data.horizontal_resolution;
product.altitude = cf_data.altitude;
product.forecast_time = cf_data.forecast_time;


%%---- Check rainrate threshold ----%%
if ~isfield(cf_data,'rain_rate_threshold')
  product.rain_rate_threshold = 2;  % in mm hr-1
else
  product.rain_rate_threshold = cf_data.rain_rate_threshold;
end

%%---- initializing outputs ----%%
% Most models have timestep every hour but met office global model is every 3 hrs
timedomain = length(cf_data.time); 
model_size = [timedomain size(cf_data.height,2)];

product.time = cf_data.time;
product.height = cf_data.height;
product.model_temperature = cf_data.model_temperature;
product.omega_500mb = cf_data.omega_500mb;

if isfield(cf_attribute,'latitude') & isfield(cf_attribute,'longitude')
  product.latitude = cf_data.latitude;  
  product.longitude = cf_data.longitude;
end

if 0
% Most models should have surface rainrate
if isfield(model_data,'sfc_ls_rain') & isfield(model_data,'sfc_conv_rain')
  product.sfc_rainrate = model_data.sfc_ls_rain(1:model_size(1)) + model_data.sfc_conv_rain(1:model_size(1));
elseif  isfield(model_data,'sfc_ls_rain') & ~isfield(model_data,'sfc_conv_rain')
  product.sfc_rainrate = model_data.sfc_ls_rain(1:model_size(1));
elseif  isfield(model_data,'sfc_ls_rain_flx') & ~isfield(model_data,'sfc_conv_rain_flx')
  % convert from kg m-2 s-1 to mm hr-1 by multiplying by 3600
  product.sfc_rainrate = (model_data.sfc_ls_rain_flx(1:model_size(1)) + model_data.sfc_conv_rain_flx(1:model_size(1)));% .* 3600;
else
  product.sfc_rainrate = ones(size(timedomain(:))) .* NaN;
end
end

product.lwc = zeros(model_size);
product.lwc_adv = zeros(model_size);

product.lwc_std = zeros(model_size);
product.lwc_adv_std = zeros(model_size);

product.lwc_std_log = zeros(model_size);
product.lwc_adv_std_log = zeros(model_size);

if do_adiabatic
  product.lwc_adiabatic = zeros(model_size);
  product.lwc_adv_adiabatic = zeros(model_size);
  product.lwc_adiabatic_inc_nolwp = zeros(model_size);
  product.lwc_adv_adiabatic_inc_nolwp = zeros(model_size);
  
  product.lwc_adiabatic_std = zeros(model_size);
  product.lwc_adv_adiabatic_std = zeros(model_size);
  product.lwc_adiabatic_inc_nolwp_std = zeros(model_size);
  product.lwc_adv_adiabatic_inc_nolwp_std = zeros(model_size);
  
  product.lwc_adiabatic_std_log = zeros(model_size);
  product.lwc_adv_adiabatic_std_log = zeros(model_size);
  product.lwc_adiabatic_inc_nolwp_std_log = zeros(model_size);
  product.lwc_adv_adiabatic_inc_nolwp_std_log = zeros(model_size);
end
%product.lwc_inc_rain = zeros(model_size);
%product.lwc_adv_inc_rain = zeros(model_size);
%product.lwc_inc_rain_std = zeros(model_size);
%product.lwc_adv_inc_rain_std = zeros(model_size);
product.lwc_th = zeros(model_size);
product.lwc_adv_th = zeros(model_size);
product.lwc_th_std = zeros(model_size);
product.lwc_adv_th_std = zeros(model_size);
product.lwc_th_std_log = zeros(model_size);
product.lwc_adv_th_std_log = zeros(model_size);


time_res = cf_data.time(2) - cf_data.time(1); % in hours.

wind_speed = sqrt(model_data.uwind.^2 + model_data.vwind.^2);
%wind_speed(model_data.height > 13000) = nan;

advect_time = product.horizontal_resolution.*1000 ./ wind_speed;
advect_time = advect_time ./ (60^2);

% 1 hour, seems a good time scale for cloud development to occur, 
% without advection
advect_time(advect_time > 1) = 1;
if strcmp(model,'arome')
  % minimum timescale, to get reasonable sample - here 5 mins.
  advect_time(advect_time < 1/12) = 1/12;
else
  % minimum timescale, to get reasonable sample - here 10 mins.
  advect_time(advect_time < 1/6) = 1/6;
end

cloud_time_res = median(diff(lwc.time));
cloud_height_res = median(diff(lwc.height));

% t, the EDGES of the grid-boxes, with time..
% r, the EDGES of the grid-boxes, with height..
t = [cf_data.time(1) - 0.5*time_res; cf_data.time(1:end) + 0.5*time_res];
r = inter_lvl_height(model_data.height',1)';


%%---- lwc retrieval status ----%%
disp('Reading lwc retrieval status ')
% 0: No liquid water
% 1: Reliable retrieval
% 2: Adiabatic retrieval: cloud top adjusted
% 3: Adiabatic retrieval: new cloud pixel
% 4: Adiabatic retrieval: no lwp
% 5: Unreliable retrieval
% 6: Rain present: no retrieval

% Convert the lwc retrieval status to a 0 1 mask
mask = lwc.lwc_retrieval_status;
mask_adiabatic = mask;
mask(mask == 1 | mask == 2 | mask == 3 ) = 1;
if do_adiabatic
  mask_adiabatic = lwc.lwc_retrieval_status;
  mask_adiabatic(mask_adiabatic == 1 | mask_adiabatic == 2 | mask_adiabatic == 3 | mask_adiabatic == 4 | mask_adiabatic == 5) = 1;
end
rain_in_profile = any(lwc.lwc_retrieval_status'==6)';
%no_lwp = any(lwc.lwc_retrieval_status'>3)';

no_lwp = (~(lwc.lwp>0) | rain_in_profile);
% [length(lwc.lwp) length(find(~(lwc.lwp>0)))  length(find(lwc.lwp>0))
% length(find(no_lwp)) length(lwc.lwp) - length(find(lwc.lwp>0))]
lwc.lwc(no_lwp,:) = NaN;
time = lwc.time(~no_lwp);
mask = lwc.lwc(~no_lwp,:);
mask_th = lwc.lwc_th(~no_lwp,:);
if do_adiabatic
  mask_adiabatic = lwc.lwc_adiabatic(~no_lwp,:);
  
  time_adiabatic_inc_nolwp = lwc.time(~rain_in_profile);
  mask_adiabatic_inc_nolwp = lwc.lwc_adiabatic(~rain_in_profile,:);
end

%%---- lwc on model grid ----%%
disp('Calculating lwc for grid boxes')
warning off
for i = 1:length(t) -1 % r and t include TOP and END edges...
  time_box = find(time >= t(i) & time < t(i+1));

  if do_adiabatic
    time_box_adiabatic_inc_nolwp = find(time_adiabatic_inc_nolwp >= t(i) & time_adiabatic_inc_nolwp < t(i+1));
  end
  
  for j = 1:size(r,2) -1
    time_box_adv = find(time >= cf_data.time(i) - (advect_time(i,j)/2) & time < cf_data.time(i) + (advect_time(i,j)/2));
    
    if do_adiabatic
      time_box_adiabatic_inc_nolwp_adv = find(time_adiabatic_inc_nolwp >= cf_data.time(i) - (advect_time(i,j)/2) & time_adiabatic_inc_nolwp < cf_data.time(i) + (advect_time(i,j)/2));
    end
    
    vert_box = find(lwc.height >= r(i,j) & lwc.height < r(i,j+1));

    mask_box = mask(time_box, vert_box);
    mask_box_adv = mask(time_box_adv, vert_box);
    
    % Data quality control, needs 90% of expected amount of data to be present
    % 120 is the number of 30 sec radar data points per hour
    % 0.06 is the 60m range resolution of the mask, in km.
    
    expected_size = floor([(r(i,j+1)-r(i,j))/cloud_height_res, time_res./cloud_time_res]);
    expected_size_adv = floor([(r(i,j+1)-r(i,j))/cloud_height_res, advect_time(i,j)./cloud_time_res]);
    lwc_box = mask(time_box, vert_box);
    lwc_box_adv = mask(time_box_adv, vert_box);
    if do_adiabatic
      lwc_box_adiabatic = mask_adiabatic(time_box, vert_box);
      lwc_box_adv_adiabatic = mask_adiabatic(time_box_adv, vert_box);
      lwc_box_adiabatic_inc_nolwp = mask_adiabatic_inc_nolwp(time_box_adiabatic_inc_nolwp, vert_box);
      lwc_box_adv_adiabatic_inc_nolwp = mask_adiabatic_inc_nolwp(time_box_adiabatic_inc_nolwp_adv, vert_box);
    end
    lwc_box_th = mask_th(time_box, vert_box);
    lwc_box_adv_th = mask_th(time_box_adv, vert_box);

    if sum(sum(~isnan(lwc_box))) < prod(floor(expected_size.*0.9)) | isempty(lwc_box)
      product.lwc(i,j) = NaN;
      product.lwc_std(i,j) = NaN;
      product.lwc_std_log(i,j) = NaN;
    else
      product.lwc(i,j) = mean(lwc_box(~isnan(lwc_box)));    
      product.lwc_std(i,j) = std(lwc_box(~isnan(lwc_box) & lwc_box > 0));       
      product.lwc_std_log(i,j) = std(log10(lwc_box(~isnan(lwc_box) & lwc_box > 0)));   
    end
        
    if sum(sum(~isnan(lwc_box_adv))) < prod(floor(expected_size_adv.*0.9)) | isempty(lwc_box_adv)
      product.lwc_adv(i,j) = NaN;
      product.lwc_adv_std(i,j) = NaN;
      product.lwc_adv_std_log(i,j) = NaN;
    else
      try
	product.lwc_adv(i,j) = mean(lwc_box_adv(~isnan(lwc_box_adv)));
	product.lwc_adv_std(i,j) = std(lwc_box_adv(~isnan(lwc_box_adv) & lwc_box_adv > 0));
	product.lwc_adv_std_log(i,j) = std(log10(lwc_box_adv(~isnan(lwc_box_adv) & lwc_box_adv > 0)));
      end
    end
    
    if do_adiabatic
      if sum(sum(~isnan(lwc_box_adiabatic))) < prod(floor(expected_size.*0.9)) | isempty(lwc_box_adiabatic)
        product.lwc_adiabatic(i,j) = NaN;
        product.lwc_adiabatic_std(i,j) = NaN;
        product.lwc_adiabatic_std_log(i,j) = NaN;
      else
        product.lwc_adiabatic(i,j) = mean(lwc_box_adiabatic(~isnan(lwc_box_adiabatic)));
        product.lwc_adiabatic_std(i,j) = std(lwc_box_adiabatic(~isnan(lwc_box_adiabatic) & lwc_box_adiabatic > 0));
        product.lwc_adiabatic_std_log(i,j) = std(log10(lwc_box_adiabatic(~isnan(lwc_box_adiabatic) & lwc_box_adiabatic > 0)));
      end
      
      if sum(sum(~isnan(lwc_box_adiabatic_inc_nolwp))) < prod(floor(expected_size.*0.9)) | isempty(lwc_box_adiabatic_inc_nolwp)
        product.lwc_adiabatic_inc_nolwp(i,j) = NaN;
        product.lwc_adiabatic_inc_nolwp_std(i,j) = NaN;
        product.lwc_adiabatic_inc_nolwp_std_log(i,j) = NaN;
      else
        product.lwc_adiabatic_inc_nolwp(i,j) = mean(lwc_box_adiabatic_inc_nolwp(~isnan(lwc_box_adiabatic_inc_nolwp)));
        product.lwc_adiabatic_inc_nolwp_std(i,j) = std(lwc_box_adiabatic_inc_nolwp(~isnan(lwc_box_adiabatic_inc_nolwp) & lwc_box_adiabatic_inc_nolwp > 0));
        product.lwc_adiabatic_inc_nolwp_std_log(i,j) = std(log10(lwc_box_adiabatic_inc_nolwp(~isnan(lwc_box_adiabatic_inc_nolwp) & lwc_box_adiabatic_inc_nolwp > 0)));
      end
    
      if sum(sum(~isnan(lwc_box_adv_adiabatic))) < prod(floor(expected_size_adv.*0.9)) | isempty(lwc_box_adv_adiabatic)
        product.lwc_adv_adiabatic(i,j) = NaN;
        product.lwc_adv_adiabatic_std(i,j) = NaN;
        product.lwc_adv_adiabatic_std_log(i,j) = NaN;
      else
        try
          product.lwc_adv_adiabatic(i,j) = mean(lwc_box_adv_adiabatic(~isnan(lwc_box_adv_adiabatic)));
          product.lwc_adv_adiabatic_std(i,j) = std(lwc_box_adv_adiabatic(~isnan(lwc_box_adv_adiabatic) & lwc_box_adv_adiabatic > 0));
          product.lwc_adv_adiabatic_std_log(i,j) = std(log10(lwc_box_adv_adiabatic(~isnan(lwc_box_adv_adiabatic) & lwc_box_adv_adiabatic > 0)));
        end
      end
      
      if sum(sum(~isnan(lwc_box_adv_adiabatic_inc_nolwp))) < prod(floor(expected_size.*0.9)) | isempty(lwc_box_adv_adiabatic_inc_nolwp)
        product.lwc_adv_adiabatic_inc_nolwp(i,j) = NaN;
        product.lwc_adv_adiabatic_inc_nolwp_std(i,j) = NaN;
        product.lwc_adv_adiabatic_inc_nolwp_std_log(i,j) = NaN;
      else
        try
          product.lwc_adv_adiabatic_inc_nolwp(i,j) = mean(lwc_box_adiabatic_inc_nolwp(~isnan(lwc_box_adiabatic_inc_nolwp)));
          product.lwc_adv_adiabatic_inc_nolwp_std(i,j) = std(lwc_box_adiabatic_inc_nolwp(~isnan(lwc_box_adiabatic_inc_nolwp) & lwc_box_adiabatic_inc_nolwp > 0));
          product.lwc_adv_adiabatic_inc_nolwp_std_log(i,j) = std(log10(lwc_box_adiabatic_inc_nolwp(~isnan(lwc_box_adiabatic_inc_nolwp) & lwc_box_adiabatic_inc_nolwp > 0)));
        end
      end  
    end
    
    if sum(sum(~isnan(lwc_box_th))) < prod(floor(expected_size.*0.9)) | isempty(lwc_box_th)
      product.lwc_th(i,j) = NaN;
      product.lwc_th_std(i,j) = NaN;
      product.lwc_th_std_log(i,j) = NaN;
    else
      product.lwc_th(i,j) = mean(lwc_box_th(~isnan(lwc_box_th)));  
      product.lwc_th_std(i,j) = std(lwc_box_th(~isnan(lwc_box_th) & lwc_box_th > 0));  
      product.lwc_th_std_log(i,j) = std(log10(lwc_box_th(~isnan(lwc_box_th) & lwc_box_th > 0)));      
    end
         
    if sum(sum(~isnan(lwc_box_adv_th))) < prod(floor(expected_size_adv.*0.9)) | isempty(lwc_box_adv_th)
      product.lwc_adv(i,j) = NaN;
      product.lwc_adv_std(i,j) = NaN;
      product.lwc_adv_std_log(i,j) = NaN;
    else
      try
	product.lwc_adv_th(i,j) = mean(lwc_box_adv_th(~isnan(lwc_box_adv_th)));
	product.lwc_adv_th_std(i,j) = std(lwc_box_adv_th(~isnan(lwc_box_adv_th) & lwc_box_adv_th > 0));
	product.lwc_adv_th_std_log(i,j) = std(log10(lwc_box_adv_th(~isnan(lwc_box_adv_th) & lwc_box_adv_th > 0)));
      end
    end
  end
end

%%---- lwp on model grid ----%%
disp('Calculating lwp for temporal grid')

product.lwp = zeros(timedomain,4);
product.lwp_adv = zeros(timedomain,4);
product.lwp_adv_3km = zeros(timedomain,4);
product.mean_wind_speed_adv = zeros(timedomain,1);
product.mean_wind_dir_adv = zeros(timedomain,1);
product.mean_wind_speed_adv_3km = zeros(timedomain,1);
product.mean_wind_dir_adv_3km = zeros(timedomain,1);

wind_dir = atan(model_data.uwind ./ model_data.vwind) .*180./pi;
%calculate mean wind speed 0-3 km for lwp
[tmp,index]=min(abs(model_data.height' - 3000));
if length(unique(index))
  product.mean_wind_speed_adv_3km = mean(wind_speed(1:end-1,1:index(1)),2);
  product.mean_wind_dir_adv_3km = mean(atan(model_data.uwind(1:end-1,1:index)./model_data.vwind(1:end-1,1:index(1))),2).*180/pi;
else
  for ii=1:size(wind_speed,1)-1
    product.mean_wind_speed_adv_3km(ii) = mean(wind_speed(ii,1:index(ii)));
    product.mean_wind_dir_adv_3km(ii) = mean(atan(model_data.uwind(ii,1:index(ii))./model_data.vwind(ii,1:index(ii)))).*180/pi;
  end
end

advect_time_lwp = product.horizontal_resolution.*1000 ./product.mean_wind_speed_adv_3km ./ 3600;
time_lwp = lwc.time(~rain_in_profile);

for i = 1:length(t) -1 % r and t include TOP and END edges...
  
  lwp_box = lwc.lwp(find(time_lwp >= t(i) & time_lwp < t(i+1)));
  lwp_box_adv_mean = lwc.lwp(find(time_lwp  >= cf_data.time(i) - (advect_time_lwp(i)/2) & time_lwp  < cf_data.time(i) + (advect_time_lwp(i)/2)));

  % calculate an accurate advection time from location of liquid water 
  height_box = find(product.lwc(i,:)>0);
  product.mean_wind_speed_adv(i) = mean(wind_speed(i,height_box));
  product.mean_wind_dir_adv(i) = mean(atan(model_data.uwind(i,height_box)./model_data.vwind(i,height_box))).*180/pi;
  accurate_advect_time_lwp = product.horizontal_resolution.*1000 ./ product.mean_wind_speed_adv(i) ./ 3600;
  if isnan(accurate_advect_time_lwp)
    accurate_advect_time_lwp = advect_time_lwp(i);
  end
  
  lwp_box_adv = lwc.lwp(find(time_lwp >= cf_data.time(i) - (accurate_advect_time_lwp/2) & time_lwp < cf_data.time(i) + (accurate_advect_time_lwp/2)));

  expected_size = floor(time_res./cloud_time_res);
  expected_size_adv = floor(accurate_advect_time_lwp./cloud_time_res);
  expected_size_adv_mean = floor(advect_time_lwp(i)./cloud_time_res);
  
  if sum(~isnan(lwp_box)) < floor(expected_size.*0.9) |sum(~isnan(lwp_box)) == 0 | isempty(lwp_box)
    product.lwp(i,:) = NaN;
%    product.lwp_min(i) = NaN
%    product.lwp_max(i) = NaN;
%    product.lwp_std(i) = NaN;
  else
    product.lwp(i,1) = mean(lwp_box(~isnan(lwp_box))); 
    product.lwp(i,2) = min(lwp_box(~isnan(lwp_box))); 
    product.lwp(i,3) = max(lwp_box(~isnan(lwp_box))); 
    product.lwp(i,4) = std(lwp_box(~isnan(lwp_box)));       
  end
 
  if sum(~isnan(lwp_box_adv)) < floor(expected_size_adv.*0.9) | sum(~isnan(lwp_box_adv)) == 0 | isempty(lwp_box_adv)
    product.lwp_adv(i,:) = NaN;
%    product.lwp_min_adv(i) = NaN
%    product.lwp_max_adv(i) = NaN;
%    product.lwp_std_adv(i) = NaN;
    product.mean_wind_speed_adv(i) = NaN;
    product.mean_wind_dir_adv(i) = NaN;
  else
    product.lwp_adv(i,1) = mean(lwp_box_adv(~isnan(lwp_box_adv))); 
    product.lwp_adv(i,2) = min(lwp_box_adv(~isnan(lwp_box_adv))); 
    product.lwp_adv(i,3) = max(lwp_box_adv(~isnan(lwp_box_adv)));
    product.lwp_adv(i,4) = std(lwp_box_adv(~isnan(lwp_box_adv)));    
  end  
  if sum(~isnan(lwp_box_adv_mean)) < floor(expected_size_adv_mean.*0.9) | sum(~isnan(lwp_box_adv_mean)) == 0 | isempty(lwp_box_adv_mean)
    product.lwp_adv_3km(i,:) = NaN;
%    product.lwp_min_adv_3km(i) = NaN
%    product.lwp_max_adv_3km(i) = NaN;
%    product.lwp_std_adv_3km(i) = NaN;
    product.mean_wind_speed_adv_3km(i) = NaN;
    product.mean_wind_dir_adv_3km(i) = NaN;
  else
    product.lwp_adv_3km(i,1) = mean(lwp_box_adv_mean(~isnan(lwp_box_adv_mean)));  
    product.lwp_adv_3km(i,2) = min(lwp_box_adv_mean(~isnan(lwp_box_adv_mean)));  
    product.lwp_adv_3km(i,3) = max(lwp_box_adv_mean(~isnan(lwp_box_adv_mean)));  
    product.lwp_adv_3km(i,4) = std(lwp_box_adv_mean(~isnan(lwp_box_adv_mean)));      
  end

end
clear lwc_box lwc_box_adv 
warning backtrace

% check that wind direction is always positive
product.mean_wind_dir_adv(find(product.mean_wind_dir_adv<0)) = 360 + product.mean_wind_dir_adv(find(product.mean_wind_dir_adv<0));
product.mean_wind_dir_adv_3km(find(product.mean_wind_dir_adv_3km<0)) = 360 + product.mean_wind_dir_adv_3km(find(product.mean_wind_dir_adv_3km<0));

%%---- Set dimensions ----%%
product_dimension.time = length(product.time);
product_dimension.height = length(product.height);
product_dimension.num_lwp_var = 4;

%%---- Set attribute ----%%
%%---- global ----%%
current_date = datestr(now);
current_date(find(current_date == '-')) = ' ';
[s, machine] = unix('uname -n'); machine = deblank(machine);

product_attribute.global.Conventions = 'CF-1.0';
product_attribute.global.title = ['Liquid water content at ' lwc_attribute.global.location ', averaged to the ' model_attribute.global.source ' grid.'];
product_attribute.global.location = lwc_attribute.global.location;
product_attribute.global.day = int16(lwc_attribute.global.day);
product_attribute.global.month = int16(lwc_attribute.global.month);
product_attribute.global.year = int16(lwc_attribute.global.year);

thehistory = [current_date ' - Generated from liquid water content data and ' model_attribute.global.source ' data'];
if exist('user','var')
  thehistory = [thehistory ' by ' user ' on ' machine];
end
product_attribute.global.history = [thehistory 10 ...
     'Liquid water content history: ' lwc_attribute.global.history];

product_attribute.global.source = [model_attribute.global.source 10 cf_attribute.global.source 10 lwc_attribute.global.source];

product_attribute.global.institution = institution ;
product_attribute.global.software_version = version ;
product_attribute.global.comment = ['This dataset contains liquid water content derived using radar/lidar cloud boundaries and liquid water path from dual-wavelength microwave radiometers, averaged on to the grid of a forecast model.' 10 ...
                   'It also contains the liquid water content and liquid water path from that model, so may be used to calculate statistics quantifying the model performance.'];


if isfield(cf_attribute,'latitude') & isfield(cf_attribute,'longitude')
  product_attribute.latitude = cf_attribute.latitude;
  product_attribute.latitude.dimensions = {};

  product_attribute.longitude = cf_attribute.longitude;
  product_attribute.longitude.dimensions = {};
end

product_attribute.time = cf_attribute.time;
product_attribute.time.dimensions = {'time'};

product_attribute.height = cf_attribute.height;
product_attribute.height.dimensions = {'time','height'};

product_attribute.model_temperature = cf_attribute.model_temperature;
product_attribute.model_temperature.dimensions = {'time','height'};

product_attribute.omega_500mb = cf_attribute.omega_500mb;
product_attribute.omega_500mb.dimensions = {'time'};

product_attribute.forecast_time = cf_attribute.forecast_time;
product_attribute.forecast_time.dimensions = {'time'};

if isfield(cf_attribute,'rain_rate_threshold')
  product_attribute.rain_rate_threshold = cf_attribute.rain_rate_threshold;
  product_attribute.rain_rate_threshold.dimensions = {};
else
  
  product_attribute.rain_rate_threshold.long_name = 'Rain rate threshold';
  product_attribute.rain_rate_threshold.units = 'mm hr-1';
  product_attribute.rain_rate_threshold.comment = ['Measurements above a surface rain rate of greater than ' num2str(product.rain_rate_threshold) ' mm h-1' 10 ...
		    'were excluded due to the possibility of strong attenuation leading to an underestimate of cloud fraction.'];
  product_attribute.rain_rate_threshold.dimensions = {};
end

if isfield(cf_attribute,'horizontal_resolution')
  product_attribute.horizontal_resolution = cf_attribute.horizontal_resolution;
  product_attribute.horizontal_resolution.dimensions = {};
else
  if isfield(model_attribute,'horizontal_resolution')
    product_attribute.horizontal_resolution = model_attribute.horizontal_resolution;
    product_attribute.horizontal_resolution.dimensions = {};
  end
end

product_attribute.altitude = cf_attribute.altitude;
product_attribute.altitude.dimensions = {};

if 0
if isfield(model_attribute,'sfc_ls_rain')
  product_attribute.sfc_rainrate = model_attribute.sfc_ls_rain;
else
  product_attribute.sfc_rainrate.comment = 'No surface rain rate available from this model';
end
product_attribute.sfc_rainrate.missing_value = -999.0;
product_attribute.sfc_rainrate.FillValue_ = -999.0;
product_attribute.sfc_rainrate.dimensions = {'time'};
end


if 0
%%---- pixel number ----%%
product_attribute.n.long_name = 'Number of radar pixels';
product_attribute.n.units = '1';
product_attribute.n.missing_value = -999.0;
product_attribute.n.FillValue_ = -999.0;
product_attribute.n.comment = ['Number of radar pixels used to make the cloud fractions by area and volume'];
product_attribute.n.plot_range = [0 1000];
product_attribute.n.plot_scale = 'linear';
product_attribute.n.dimensions = {'time','height'};


%%---- pixel number adv ----%%
product_attribute.n_adv.long_name = 'Number of radar pixels';
product_attribute.n_adv.units = '1';
product_attribute.n_adv.missing_value = -999.0;
product_attribute.n_adv.FillValue_ = -999.0;
product_attribute.n_adv.comment = ['Number of radar pixels used to make the cloud fractions by area and volume, averaging over the time taken to advect 1 model grid box, using model winds.'];
product_attribute.n_adv.plot_range = [0 1000];
product_attribute.n_adv.plot_scale = 'linear';
product_attribute.n_adv.dimensions = {'time','height'};
end

plot_atts = {[1e-8 1e-3], 'logarithmic'};

%%---- observed lwc ----%%
product_attribute.lwc = create_attributes({'time','height'},['Observed mean liquid water content, 1 hour sampling'], lwc_units, missing_value, ['This variable is the observed mean liquid water content estimated for pixels where the "categorization" data has diagnosed that liquid water is present, averaged onto the model grid with height, and 1 hour in time.' 10 .... 
                    'The model temperature and pressure were used to estimate the theoretical adiabatic liquid water content gradient for each cloud base and the adiabatic liquid water content was scaled so that its integral matches the liquid water path obtained from a coincident microwave radiometer measurement. ' 10 ...
                    'If the liquid layer is detected by the lidar only, there is the potential for cloud top height to be underestimated and if the integrated liquid water content is less than that measured by the microwave radiometer, the cloud top is extended until both agree.' 10 ...
                    'When microwave radiometer data were not available, liquid water content was not retrieved although the adiabatic value is still included in the lwc_adiabatic and lwc_adv_adiabatic variables.' 10 ...
		    'Rain present below the liquid is associated with an uncertain measurement of liquid water path and cloud boundaries so these profiles are not included.'], plot_atts);		    
  
%%---- observed lwc adv ----%%
product_attribute.lwc_adv = create_attributes({'time','height'},['Observed mean liquid water content, ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value, ['This variable is the same as the observed mean liquid water content, lwc, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

if do_adiabatic
  %%---- observed adiabatic lwc ----%%
  product_attribute.lwc_adiabatic = create_attributes({'time','height'},['Observed mean adiabatic liquid water content, 1 hour sampling'], lwc_units, missing_value,['This variable is the liquid water content that would be present if the cloud conformed to adiabatic conditions, for periods where liquid water path was available from coincident dual-wavelength microwave radiometer data.'], plot_atts);
  
  %%---- observed adiabatic lwc adv ----%%
  product_attribute.lwc_adv_adiabatic = create_attributes({'time','height'},['Observed mean adiabatic liquid water content, ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as the adiabatic mean liquid water content, lwc_adiabatic, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

  %%---- observed adiabatic_inc_nolwp lwc ----%%
  product_attribute.lwc_adiabatic_inc_nolwp = create_attributes({'time','height'},['Observed mean adiabatic liquid water content, 1 hour sampling'], lwc_units, missing_value,['This variable is the liquid water content that would be present if the cloud conformed to adiabatic conditions, including data where no liquid water path information was available.'], plot_atts);

  %%---- observed adiabatic_inc_nolwp lwc adv ----%%
  product_attribute.lwc_adv_adiabatic_inc_nolwp = create_attributes({'time','height'},['Observed mean adiabatic_inc_nolwp liquid water content, ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as the adiabatic mean liquid water content, lwc_adiabatic_inc_nolwp, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);
end

%%---- observed tophat lwc ----%%
product_attribute.lwc_th = create_attributes({'time','height'}, ['Observed mean liquid water content (tophat distribution), 1 hour sampling'], lwc_units, missing_value,['This variable is the liquid water content assuming a tophat distribution. I.e. the profile of liquid water content in each layer is constant.'], plot_atts);

%%---- observed tophat lwc adv ----%%
product_attribute.lwc_adv_th = create_attributes({'time','height'}, ['Observed mean liquid water content (tophat distribution), ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as lwc_th, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

%%----------------------%%
%%---- std. of lwc ----%%
product_attribute.lwc_std = create_attributes({'time','height'},['Std. of observed liquid water content, 1 hour sampling'], lwc_units, missing_value, ['This variable is the standard deviation of the liquid water content estimated for pixels where the "categorization" data has diagnosed that liquid water is present, averaged onto the model grid with height, and 1 hour in time.' 10 .... 
                    'The model temperature and pressure were used to estimate the theoretical adiabatic liquid water content gradient for each cloud base and the adiabatic liquid water content was scaled so that its integral matches the liquid water path obtained from a coincident microwave radiometer measurement. ' 10 ...
                    'If the liquid layer is detected by the lidar only, there is the potential for cloud top height to be underestimated and if the integrated liquid water content is less than that measured by the microwave radiometer, the cloud top is extended until both agree.' 10 ...
                    'When microwave radiometer data were not available, liquid water content was not retrieved although the adiabatic value is still included in the lwc_adiabatic and lwc_adv_adiabatic variables.' 10 ...
		    'Rain present below the liquid is associated with an uncertain measurement of liquid water path and cloud boundaries so these profiles are not included.'], plot_atts);		    
  
%%---- std. of observed lwc adv ----%%
product_attribute.lwc_adv_std = create_attributes({'time','height'},['Std. of observed liquid water content, ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value, ['This variable is the same as the standard deviation of the observed liquid water content, lwc_std, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

if do_adiabatic
  %%---- std. of observed adiabatic lwc ----%%
  product_attribute.lwc_adiabatic_std = create_attributes({'time','height'},['Std. of observed adiabatic liquid water content, 1 hour sampling'], lwc_units, missing_value,['This variable is the standard deviation of the liquid water content that would be present if the cloud conformed to adiabatic conditions, for periods where liquid water path was available from coincident dual-wavelength microwave radiometer data.'], plot_atts);

  %%---- std. of observed adiabatic lwc adv ----%%
  product_attribute.lwc_adv_adiabatic_std = create_attributes({'time','height'},['Std. of observed adiabatic liquid water content, ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as the standard deviation of the adiabatic liquid water content, lwc_adiabatic_std, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

  %%---- std. of observed adiabatic_inc_nolwp lwc ----%%
  product_attribute.lwc_adiabatic_inc_nolwp_std = create_attributes({'time','height'},['Std. of observed adiabatic liquid water content, 1 hour sampling'], lwc_units, missing_value,['This variable is the standard deviation of the liquid water content that would be present if the cloud conformed to adiabatic conditions, including data where no liquid water path information was available.'], plot_atts);

  %%---- std. of observed adiabatic_inc_nolwp lwc adv ----%%
  product_attribute.lwc_adv_adiabatic_inc_nolwp_std = create_attributes({'time','height'},['Std. of observed adiabatic_inc_nolwp liquid water content, ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as the standard deviation of the adiabatic liquid water content, lwc_adiabatic_inc_nolwp_std, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);
end

%%---- std. of observed tophat lwc ----%%
product_attribute.lwc_th_std = create_attributes({'time','height'}, ['Std. of observed liquid water content (tophat distribution), 1 hour sampling'], lwc_units, missing_value,['This variable is the standard deviation of the liquid water content assuming a tophat distribution. I.e. the profile of liquid water content in each layer is constant.'], plot_atts);

%%---- std. of observed tophat lwc adv ----%%
product_attribute.lwc_adv_th_std = create_attributes({'time','height'}, ['Std. of observed liquid water content (tophat distribution), ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as lwc_th_std, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

%%----------------------%%
%%---- std. of log10(lwc) ----%%
product_attribute.lwc_std_log = create_attributes({'time','height'},['Std. of log10(observed liquid water content), 1 hour sampling'], lwc_units, missing_value, ['This variable is the standard deviation of the log10(liquid water content) estimated for pixels where the "categorization" data has diagnosed that liquid water is present, averaged onto the model grid with height, and 1 hour in time.' 10 .... 
                    'The model temperature and pressure were used to estimate the theoretical adiabatic liquid water content gradient for each cloud base and the adiabatic liquid water content was scaled so that its integral matches the liquid water path obtained from a coincident microwave radiometer measurement. ' 10 ...
                    'If the liquid layer is detected by the lidar only, there is the potential for cloud top height to be underestimated and if the integrated liquid water content is less than that measured by the microwave radiometer, the cloud top is extended until both agree.' 10 ...
                    'When microwave radiometer data were not available, liquid water content was not retrieved although the adiabatic value is still included in the lwc_adiabatic and lwc_adv_adiabatic variables.' 10 ...
		    'Rain present below the liquid is associated with an uncertain measurement of liquid water path and cloud boundaries so these profiles are not included.'], plot_atts);		    
  
%%---- std. of observed lwc adv ----%%
product_attribute.lwc_adv_std_log = create_attributes({'time','height'},['Std. of log10(observed liquid water content), ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value, ['This variable is the same as the standard deviation of the log10(observed liquid water content), lwc_std_log, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

if do_adiabatic
  %%---- std. of log10(observed adiabatic lwc) ----%%
  product_attribute.lwc_adiabatic_std_log = create_attributes({'time','height'},['Std. of log10(observed adiabatic liquid water content), 1 hour sampling'], lwc_units, missing_value,['This variable is the standard deviation of the log10(liquid water content) that would be present if the cloud conformed to adiabatic conditions, for periods where liquid water path was available from coincident dual-wavelength microwave radiometer data.'], plot_atts);
  
  %%---- std. of log10(observed adiabatic lwc adv ----%%
  product_attribute.lwc_adv_adiabatic_std_log = create_attributes({'time','height'},['Std. of log10(observed adiabatic liquid water content), ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as the standard deviation of the adiabatic log10(liquid water content), lwc_adiabatic_std_log, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);
  
  %%---- std. of log10(observed adiabatic_inc_nolwp lwc ----%%
  product_attribute.lwc_adiabatic_inc_nolwp_std_log = create_attributes({'time','height'},['Std. of log10(observed adiabatic liquid water content), 1 hour sampling'], lwc_units, missing_value,['This variable is the standard deviation of the log10(liquid water content) that would be present if the cloud conformed to adiabatic conditions, including data where no liquid water path information was available.'], plot_atts);
  
  %%---- std. of log10(observed adiabatic_inc_nolwp lwc adv ----%%
  product_attribute.lwc_adv_adiabatic_inc_nolwp_std_log = create_attributes({'time','height'},['Std. of log10(observed adiabatic_inc_nolwp liquid water content), ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as the standard deviation of the adiabatic log10(liquid water content), lwc_adiabatic_inc_nolwp_std_log, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);
end


%%---- std. of log10(observed tophat lwc ----%%
product_attribute.lwc_th_std_log = create_attributes({'time','height'}, ['Std. of log10(observed liquid water content) (tophat distribution), 1 hour sampling'], lwc_units, missing_value,['This variable is the standard deviation of the log10(liquid water content) assuming a tophat distribution. I.e. the profile of liquid water content in each layer is constant.'], plot_atts);

%%---- std. of log10(observed tophat lwc adv ----%%
product_attribute.lwc_adv_th_std_log = create_attributes({'time','height'}, ['Std. of log (observed liquid water content) (tophat distribution), ' num2str(product.horizontal_resolution) ' km.'], lwc_units, missing_value,['This variable is the same as lwc_th_std_log, except that model winds were used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes).'], plot_atts);

%%----------------------%%
%%---- observed lwp ----%%
product_attribute.lwp = create_attributes({'time','num_lwp_var'}, ['Observed mean liquid water path, 1 hour sampling'], lwp_units, missing_value, ['This variable is the observed [mean, min, max, std] of liquid water path retrieved from dual-wavelength microwave radiometers, averaged 1 hour in time.' 10 .... 
		    'Rain present below liquid layers is associated with an uncertain measurement of liquid water path and cloud boundaries so these profiles are not included.'], {[-50 1000], 'linear'});		    
  
%%---- observed lwp adv using mean winds where lwc present----%%
product_attribute.lwp_adv = create_attributes({'time','num_lwp_var'}, ['Observed mean liquid water path, ' num2str(product.horizontal_resolution) ' km'], lwp_units, missing_value, ['This variable is the same as the observed liquid water path, lwp, except that model winds were used to estimate the time taken ' 10 ...
                    'to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes). The mean wind, mean_wind_speed_adv, was calculated from the ' 10 ...
                    'mean of model winds in the grid boxes in the profile that contain liquid water.'], {[-50 1000], 'linear'});

%%---- model mean winds where lwc present----%%
product_attribute.mean_wind_speed_adv = create_attributes({'time'}, ['Mean model winds where liquid water path is present in profile'], {'m s-1', 'm s<sup>-1</sup>'}, missing_value, ['This variable is the mean model wind used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km. ' 10 ...
                    'It was calulated from the mean of the magnitude of the model winds in the grid boxes that contain liquid water.'], {[0 50], 'linear'});

%%---- model mean wind direction where lwc present----%%
product_attribute.mean_wind_dir_adv = create_attributes({'time'}, ['Mean model wind direction where liquid water path is present in profile'], 'degrees', missing_value, ['This variable is the mean of the model wind direction in the grid boxes that contain liquid water.'], {[0 360], 'linear'});

%%---- observed lwp adv using mean winds 0-3km ----%%
product_attribute.lwp_adv_3km = create_attributes({'time','num_lwp_var'}, ['Observed mean liquid water path, ' num2str(product.horizontal_resolution) ' km'], lwp_units, missing_value, ['This variable is the same as the observed liquid water path, lwp, except that model winds were used to estimate the time taken ' 10 ...
                    'to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km (max 1 hour, min 10 minutes). The mean wind, mean_wind_speed_adv_3km, was calculated from the ' 10 ...
                    'mean of the magnitude of the model winds from 0 to 3 km in the vertical.'], {[-50 1000], 'linear'});

%%---- model mean winds where lwc present----%%
product_attribute.mean_wind_speed_adv_3km = create_attributes({'time'}, ['Mean model winds from 0 to 3 km'], {'m s-1', 'm s<sup>-1</sup>'}, missing_value, ['This variable is the mean model wind used to estimate the time taken to advect the model''s horizontal resolution of ' num2str(product.horizontal_resolution) ' km. It was calulated from the mean of the model winds from 0 to 3 km in the vertical.'], {[0 50], 'linear'});

%%---- model mean wind direction where lwc present----%%
product_attribute.mean_wind_dir_adv_3km = create_attributes({'time'}, ['Mean model wind direction from 0 to 3 km'], 'degrees', missing_value, ['This variable is the mean of the model wind direction from 0 to 3 km in the vertical.'], {[0 360], 'linear'});
%%---- LWC model ----%%
if isfield(cf_data,'model_lwc')
  product.model_lwc = cf_data.model_lwc;
  product.model_lwp = sum(diff(r(1:end-1,:)')'.*cf_data.model_lwc,2); %kg m-3
end
if isfield(cf_attribute,'model_lwc')
  product_attribute.model_lwc = cf_attribute.model_lwc;
  product_attribute.model_lwc.long_name = 'Model liquid water content';
  product_attribute.model_lwc.dimensions = {'time','height'};
  
  product_attribute.model_lwp = create_attributes({'time'}, 'Model liquid water path', lwp_units, missing_value, [], {[-50 1000], 'linear'});
end

%%---- LWC model sub-gridscale (DWD) ----%%
if isfield(cf_data,'model_lwc_gridscale')
  product.model_lwc_gridscale = cf_data.model_lwc_gridscale;
  product.model_lwp_gridscale = sum(diff(r(1:end-1,:)')'.*cf_data.model_lwc_gridscale,2); %kg m-3
end
if isfield(cf_attribute,'model_lwc_gridscale')
  product_attribute.model_lwc_gridscale = cf_attribute.model_lwc_gridscale;
  product_attribute.model_lwc_gridscale.dimensions = {'time','height'};
  
  product_attribute.model_lwp_gridscale = create_attributes({'time'}, 'Model grid-scale liquid water path', lwp_units, missing_value, [], {[-50 1000], 'linear'});
end

%%---- ----%%
function [out] = inter_lvl_height(varargin);

if length(varargin) == 0
  help(mfilename)
  return
end

if length(varargin) == 1
  in = varargin{1};
  tran_flag = 0;
elseif length(varargin) == 2
  if varargin{2} == 2
    in = varargin{1}';
    tran_flag = 1;
  elseif varargin{2} == 1
    in = varargin{1};
    tran_flag = 0;
  else
    help(mfilename)
    return
  end
else
  help(mfilename)
  return
end

out(1,:) = in(1,:) -  0.5.*(in(2,:) - in(1,:));
out(out < 0) = 0;

for i = 1:size(in, 1)
  out(i+1, :) = 2*in(i,:) - out(i,:);
end

if tran_flag == 1
  out = out';
end
