function generate_product(site, DATES, method, model, user, institution, test)
%  generate_product(site, DATES, method, model, user, institution)


if nargin < 3 , return, end;

if ~isnumeric(DATES)
  disp(['Prefer numeric dates']);
  return;
end

if ~get_site_name(site)
  return;
end

if nargin < 5
  %user = 'Robin Hogan <r.j.hogan@reading.ac.uk>';
  user = 'Ewan O''Connor <e.j.oconnor@reading.ac.uk>';
  institution = 'Data processed at the Department of Meteorology, University of Reading.';
end
if nargin < 6
  institution = 'Data processed at the Department of Meteorology, University of Reading.';
end

if nargin < 7
  test = 0; % not testing
end


if length(DATES) == 2 
  if DATES(1) < DATES(2)
    DATES=DATES(1):DATES(2);
  end
end

directory = [get_data_dir '/' site '/processed/categorize/'];

if ~test
  output_dir = [get_data_dir '/' site '/products'];
else
  output_dir = [get_data_dir '/' site '/products/test_output'];
end
  
for DATE=DATES  
  if rem(DATE,100) > 31 | rem(DATE,10000) > 1231 | rem(DATE,100) < 1
    continue;
  end

  thedate = num2str(DATE);

  if ~any(strcmp(method,{'cloud-fraction-arscl-grid','sfcrad-model-grid'}))
    switch site
     case {'astex','fire','blindtest-cabauw','blindtest-cabauw_nodrizzle'}
      filename = (['/home/radar/data/blindtest_lwc/' site '_categorize.nc']);
      thedate = [];
     otherwise
      filename = ([directory thedate(1:4) '/' thedate '_' site '_categorize.nc']);
    end
    if ~load_product(thedate, site, 'categorize', 'test')
     disp(['*** Categorization data from ' thedate ' not found']);
     continue
   end
  end
  
  clear product*
  
  switch method
   case 'iwc-Z-T-method'
    method_name = method;
    [product, product_attribute, product_dimension] = generate_iwc(thedate, user, institution, site);
    
   case 'iwc-Matrosov-method'
    method_name = method;
    [product, product_attribute, product_dimension] = generate_iwc_matrosov(thedate, user, institution, site);
       
   case 'classification'
    method_name = method;
    [product, product_attribute, product_dimension] = generate_noddy(thedate, user, institution, site);
   
   case 'epsilon-sigma-v-bar-method'
    % Turbulent eddy dissipation rate
    method_name = method;
    [product, product_attribute, product_dimension] = generate_epsilon(thedate, user, institution, site);
   
   case {'lwc','lwc-scaled-adiabatic','lwc-adiabatic-method'}
    
    if strcmp(method, 'lwc-adiabatic-method')
      method_name = ['lwc-adiabatic-method'];
    else
      method_name = ['lwc-scaled-adiabatic'];
    end
      
    [product, product_attribute, product_dimension] = generate_simple_lwc(thedate, user, institution, site, method_name);
    
   case {'lwf','lwf-Z-rainrate'}
    method_name = ['lwf-Z-rainrate'];
    [product, product_attribute, product_dimension] = generate_simple_lwf(thedate, user, institution, site);
    
   case 'drizzle'
    % Drizzle products
    method_name = method;
    [product, product_attribute, product_dimension] = generate_drizzle_products(thedate, user, institution, site);
  case 'drizzle-test'
    % Drizzle products
    method_name = method;
    [product, product_attribute, product_dimension] = generate_drizzle_products_new(thedate, user, institution, site);

    
   case {'cloud-fraction-model-grid'}
    if ~exist('model','var'), return, end
%    if ~any(strcmp({'ecmwf', 'meteo-france','racmo','met-office-global'}, model) | strncmp('met-office-',model, 10) | strncmp('dwd-lm',model, 6) | strncmp('smhi-rca',model, 8)|strncmp('ncep',model,4)), return, end

    method_name = ['cloud-fraction-' model '-grid'];

    [product, product_attribute, product_dimension] = generate_Cv_on_model_grid(thedate, user, institution, model, site); 
    
   case {'high-cloud-fraction-model-grid'}
    if ~exist('model','var'), return, end
 %   if ~any(strcmp({'ecmwf', 'meteo-france','racmo','met-office-global'}, model) | strncmp('met-office-',model, 10) | strncmp('dwd-lm',model, 6) | strncmp('smhi-rca',model, 8),strncmp('ncep',model,4), return, end

    method_name = ['high-cloud-fraction-' model '-grid'];

    [product, product_attribute, product_dimension] = generate_highCv_on_model_grid(thedate, user, institution, model, site);       
   
   case {'iwc-model-grid','iwc-Z-T-model-grid'}
    if ~exist('model','var'), return, end
  %  if ~any(strcmp({'ecmwf', 'meteo-france','racmo','met-office-global'}, model) | strncmp('met-office-',model, 10) | strncmp('dwd-lm',model, 6) | strncmp('smhi-rca',model, 8)), return, end

    method_name = ['iwc-Z-T-' model '-grid'];

    [product, product_attribute, product_dimension] = generate_iwc_on_model_grid(thedate, user, institution, model, site); 
    
   case {'lwc-model-grid','lwc-scaled-model-grid','lwc-adiabatic-model-grid'}
    if ~exist('model','var'), return, end
%    if ~any(strcmp({'ecmwf', 'meteo-france','racmo','met-office-global'}, model) | strncmp('met-office-',model, 10) | strncmp('dwd-lm',model, 6) | strncmp('smhi-rca',model, 8)), return, end

    if strcmp(method, 'lwc-adiabatic-model-grid')
      method_lwc = ['lwc-adiabatic-method'];
      method_name = ['lwc-adiabatic-' model '-grid'];
    else
      method_lwc = ['lwc-scaled-adiabatic'];
      method_name = ['lwc-scaled-' model '-grid'];
    end

    [product, product_attribute, product_dimension] = generate_lwc_on_model_grid(thedate, user, institution, model, site, method_lwc); 
    
   case {'lwf-model-grid'}
    if ~exist('model','var'), return, end
%    if ~any(strcmp({'ecmwf', 'meteo-france','met-office-global'}, model) | strncmp('met-office-',model, 10) | strncmp('dwd-lm',model, 6) | strncmp('smhi-rca',model, 8)), return, end

    method_name = ['lwf-' model '-grid'];

    [product, product_attribute, product_dimension] = generate_lwf_on_model_grid(thedate, user, institution, model, site); 
    
   case {'lwf-length-model-grid'}
    if ~exist('model','var'), return, end
%    if ~any(strcmp({'ecmwf', 'meteo-france','met-office-global'}, model) | strncmp('met-office-',model, 10) | strncmp('dwd-lm',model, 6) | strncmp('smhi-rca',model, 8)), return, end

    method_name = ['lwf-length-' model '-grid'];

    [product, product_attribute, product_dimension] = generate_lwf_length_model_grid(thedate, user, institution, model, site); 
    
   case {'sfcrad-model-grid'}
    if ~exist('model','var'), return, end
    method_name = ['sfcrad-' model '-grid'];
    [product, product_attribute, product_dimension] = generate_sfcrad_on_model_grid(thedate, user, institution, model, site);

   case {'w-model-grid','w-lidar-model-grid'}
    if ~exist('model','var'), return, end
    
    if strcmp(method, 'w-lidar-model-grid')
      method_w = ['lidar'];
      method_name = ['w-lidar' model '-grid'];
    else
      method_w = ['radar'];
      method_name = ['w-' model '-grid'];
    end
    
    [product, product_attribute, product_dimension] = generate_w_on_model_grid(thedate, user, institution, model, site, method_w); 
   
   case {'epsilon-model-grid','epsilon-sigma-v-bar-model-grid','epsilon-lidar-model-grid'}
    if ~exist('model','var'), return, end
    
    if strcmp(method, 'epsilon-lidar-model-grid')
      method_epsilon = ['epsilon-lidar'];
      method_name = ['epsilon-lidar' model '-grid'];
    else
      method_epsilon = ['epsilon-sigma-v-bar-method'];
      method_name = ['epsilon-' model '-grid'];
    end
    
    [product, product_attribute, product_dimension] = generate_epsilon_on_model_grid(thedate, user, institution, model, site, method_epsilon); 
    
   case {'pdf-model-grid'}
    if ~exist('model','var'), return, end
%    if ~any(strcmp({'standard', 'ecmwf', 'meteo-france','racmo','met-office-global'}, model) | strncmp('met-office-',model, 10) | strncmp('dwd-lm',model, 6) | strncmp('smhi-rca',model, 8)), return, end

    method_name = ['pdf-' model '-grid'];

    [product, product_attribute, product_dimension] = generate_pdf_on_model_grid(thedate, user, institution, model, site); 

   case 'cloud-fraction-arscl-grid'    
    [product, product_attribute, product_dimension] = generate_Cv_arscl_model_grid(thedate, user, institution, model, site);
    method_name = ['cloud-fraction-' model '-grid'];
   
   case {'cloud-fraction-standard-grid'}
    [product, product_attribute, product_dimension] = generate_Cv_on_standard_grid(thedate, user, institution, site);
    method_name = ['cloud-fraction-standard-grid'];
    
   case {'Z_R_beta_R','Z-R_beta-R'}
    % Z-R and beta-R relationships
    method_name = ['Z-R_beta-R'];
    [product, product_attribute, product_dimension] = generate_Z_R_beta_R(thedate, site, user, institution);
    
   case 'highres_lidar'
    instrument = model;
    method_name = [instrument '_original-res'];
    [product, product_attribute, product_dimension] = generate_highres_lidar(thedate, user, institution, site, instrument);   
   otherwise
    disp(['Product ' method ' does not exist']);
  end      
  
  if exist('product','var') 
    if ~isempty(product)
      output_filename = sprintf('%s/%s/%04d/%04d%02d%02d_%s_%s.nc', output_dir, method_name, ...
                                double(product_attribute.global.year), double(product_attribute.global.year), ...
                                double(product_attribute.global.month), double(product_attribute.global.day), site, method_name);
      
      %check directories exist
      status = 1;
      if ~exist([output_dir '/' method_name],'dir')
        [status, msg] = mkdir(output_dir, method_name);
      end
      
      if status & ~exist([output_dir '/' method_name '/' num2str(double(product_attribute.global.year))])
        [status, msg] = mkdir([output_dir '/' method_name], num2str(double(product_attribute.global.year)));
      end      

      if status
        write_nc_struct(output_filename, product_dimension, product, product_attribute);
      else
        error('error creating directory');
      end	    
    end
  end
end 


