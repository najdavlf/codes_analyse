#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm
import time, datetime
from mycolors import *

path="/home/villefranquen/Work/NCDF/ARMCU/"

# sensib simu
simuId = np.array(["CERK4",
           "CEN2D","WENO3","WENO5",
           "D100m","DX40m","DZ40m","DZVAR",
           "L10km","L25km","L51km",
           "MREVE","SCOND","TDELT",
           "NWIND","2WIND"])
"""simuId = np.array(["CERK4",
           "CEN2D","WENO3",
           "DX40m","DZ40m","DZVAR",
           "L10km","L25km","L51km",
           "MREVE","SCOND","TDELT",
           "NWIND","2WIND"])"""
simuGp = np.array([0,
           10, 10, 10,
           20, 20, 20, 20,
           30, 30, 30,
           40, 40, 40,
           50, 50])
"""simuGp = np.array([0,
           10, 10, 
           20, 20, 20, 
           30, 30, 30, 
           40, 40, 40,
           50,50])"""

sizeDm = np.array([6.4,
           6.4,6.4,6.4,
           6.4,6.4,6.4,6.4,
           10.,25.6,51.2,
           6.4,6.4,6.4,
           51.2,51.2])


advIds = simuId[simuGp==10]
resIds = simuId[simuGp==20]
domIds = simuId[simuGp==30]
parIds = simuId[simuGp==40]
wndIds = simuId[simuGp==50]

nbSimu = len(simuId)
nbTime = 15

nbClds = np.zeros((nbSimu,nbTime))
dsClds = np.zeros((nbSimu,nbTime))

for (nsimu,simu) in enumerate(simuId) :
  print simu
  for time in range(nbTime) :
      ntime = "%03i" %(time+1)
      fName = path+simu+"/"+simu+".1.ARMCu."+ntime+".obj_carac_4D.nc"
      ncData = ncdf.Dataset(fName,"r")
      dimCld = ncData.dimensions["cloud"]
      nbClds[nsimu, time] = dimCld.size
      dsClds[nsimu, time] = dimCld.size/(sizeDm[nsimu]**2)
      ncData.close()

nbCldMax = np.max(nbClds)


def get_var(fName,name, carac, lev) :
    # get the field name, 
    # carac, lev = 0 => mean horiz, mean vert
    # carac, lev = 1 => min horiz, min vert
    # carac, lev = 2 => max horiz, max vert
    # carac, lev = 3 => std horiz, std vert

    ncData = ncdf.Dataset(fName,"r")
    if (carac>-1) :
        if (lev>-1) :
            tmp = ncData.variables[name][:,carac,:,0]
            toto = np.ma.masked_values(tmp,0.)
            if (lev==0) : # mean along z axis
              var = toto.mean(axis=0)
            elif (lev==1) : # min along z axis
              var = toto.min(axis=0)
            elif (lev==2) : # max along z axis
              var = toto.max(axis=0)
            elif (lev==3) : # std along z axis 
              var = toto.std(axis=0)
        else :
              var = ncData.variables[name][carac,:,0]
    else :
              var = ncData.variables[name][:,0]

    ncData.close()
    return var

def plot_evol(array, leg, title, ylab, figname) :
  nb1 = array.shape[0]
  nb2 = array.shape[1]
  time=["%02ih" %(i+1) for i in range(nb2)]
  plt.figure(figsize=(12,6))
  for i in range(nb1) :
      plt.plot(array[i,:], linestyle=linestyles[i], linewidth=2, color=rainbow15[i])
  plt.xlim([-1,17])
  plt.title("Evolution of "+title)
  plt.xlabel("Simulation duration")
  plt.ylabel(ylab)
  plt.xticks(range(nb2), time)
  plt.legend(leg,loc="best")
  plt.savefig(figname,bbox_inches='tight')
  plt.close()

def plot_distrib_depth(varname, carac, lev, title, xlab, figname) :
  plt.figure(figsize=(12,6))
  kernel = []
  varmax = -999999.
  varmin =  999999. 
  for (nsimu,simu) in enumerate(simuId) :
    print simu
    var=[]
    for time in range(nbTime) : 
      if (nbClds[nsimu,time] > 1) :
        ntime = "%03i" %(time+1)
        fName = path+simu+"/"+simu+".1.ARMCu."+ntime+".obj_carac_4D.nc"
        var = np.append(var,(get_var(fName,"top",carac,lev)+3*get_var(fName,"top",3,lev)-get_var(fName,"base",carac,lev)-3*get_var(fName,"base",3,lev)))
    kernel = np.append(kernel,stats.gaussian_kde(var))
    if (var.max() > varmax) : 
        varmax = var.max()
    if (var.min() < varmin) : 
        varmin = var.min() 
  varmin = (varmin)
  varmax = (varmax)
  print varmin, varmax
  varvec = np.linspace(varmin,varmax,200)
  smallvarvec = range(0,200,20)
  ticks = [r'%g' %(varvec[i]) for i in smallvarvec]
  for (nsimu,simu) in enumerate(simuId) :
    plt.plot(kernel[nsimu](varvec),linestyle=linestyles[nsimu],linewidth=2,color=rainbow15[nsimu])
  plt.title("Density of "+title)
  plt.xlabel(xlab)
  plt.ylabel("Density")
  plt.xticks(smallvarvec,ticks)
  plt.legend(simuId,loc="best")
  plt.savefig(figname,bbox_inches='tight')
  plt.close()

def plot_distrib(varname, carac, lev, title, xlab, figname) :
  plt.figure(figsize=(12,6))
  kernel = []
  varmax = -999999.
  varmin =  999999. 
  for (nsimu,simu) in enumerate(simuId) :
    print simu
    var=[]
    for time in range(nbTime) : 
      if (nbClds[nsimu,time] > 1) :
        ntime = "%03i" %(time+1)
        fName = path+simu+"/"+simu+".1.ARMCu."+ntime+".obj_carac_4D.nc"
        var = np.append(var,get_var(fName,varname,carac,lev))
    kernel = np.append(kernel,stats.gaussian_kde(var))
    if (var.max() > varmax) : 
        varmax = var.max()
    if (var.min() < varmin) : 
        varmin = var.min() 
  varmin = (varmin)
  varmax = (varmax)
  varvec = np.linspace(varmin,varmax,200)
  smallvarvec = range(0,200,20)
  ticks = [r'%g' %(varvec[i]) for i in smallvarvec]
  for (nsimu,simu) in enumerate(simuId) :
    plt.plot(kernel[nsimu](varvec),linestyle=linestyles[nsimu],linewidth=2,color=rainbow15[nsimu])
  plt.title("Density of "+title)
  plt.xlabel(xlab)
  plt.ylabel("Density")
  plt.xticks(smallvarvec,ticks)
  plt.legend(simuId,loc="best")
  plt.savefig(figname,bbox_inches='tight')
  plt.close()




def plot_distrib_log(varname, carac, lev, title, xlab, figname) :
  plt.figure(figsize=(12,6))
  kernel = []
  varmax = -10.
  varmin = 10. 
  for (nsimu,simu) in enumerate(simuId) :
    print simu
    var=[]
    for time in range(nbTime) : 
      if (nbClds[nsimu,time] > 1) :
        ntime = "%03i" %(time+1)
        fName = path+simu+"/"+simu+".1.ARMCu."+ntime+".obj_carac_4D.nc"
        var = np.append(var,get_var(fName,varname,carac,lev))
    var = np.log10(var)
    kernel = np.append(kernel,stats.gaussian_kde(var))
    if (var.max() > varmax) : 
        varmax = var.max()
    if (var.min() < varmin) : 
        varmin = var.min() 
  varmin = int(varmin)-2
  varmax = int(varmax)+2
  varvec = np.linspace(varmin,varmax,-10*varmin+1)
  smallvarvec = range(0,-10*varmin,10)
  ticks = [r'10$^{%i}$' %(varvec[i]) for i in smallvarvec]
  for (nsimu,simu) in enumerate(simuId) :
    plt.plot(kernel[nsimu](varvec),linestyle=linestyles[nsimu],linewidth=2,color=rainbow15[nsimu])
  plt.title("Density of "+title)
  plt.xlabel(xlab)
  plt.ylabel("Density")
  plt.xticks(smallvarvec,ticks)
  plt.legend(simuId,loc="best")
  plt.savefig(figname,bbox_inches='tight')
  plt.close()



def figs1() :
  print "Evol nb clouds"
  plot_evol(nbClds, simuId, "the number of clouds", "Number of clouds", "EvolCloudNumb.pdf")
  plot_evol(dsClds, simuId, "the density of clouds", "Number of clouds / Domain area", "EvolCloudDens.pdf")
  plot_evol(nbClds[sizeDm<11,:], simuId[sizeDm<11], "the number of clouds", "Number of clouds", "EvolCloudNumbSmallDomain.pdf")
  plot_evol(nbClds[sizeDm>11,:], simuId[sizeDm>11], "the number of clouds", "Number of clouds", "EvolCloudNumbLargeDomain.pdf")

def figs2() :
  print "Distrib RCT"
  plot_distrib_log("RCT",0,0,"cloud mean liquid water mixing ratio",r"r$_c$ (kg/kg)","DistribRC00.pdf")
  print "Distrib RVT"
  plot_distrib_log("RVT",0,0,"cloud mean specific humidity",r"r$_v$ (kg/kg)","DistribRV00.pdf")
  print "Distrib THT"
  plot_distrib("THT",0,0,"cloud mean temperature",r"$\theta$ (K)","DistribTH00.pdf")
  print "Distrib cover"
  plot_distrib_log("cover",-1,-1,"cloud cover", "Cloud Cover", "DistribCC.pdf")

def figs3() :
  print "Distrib epaisseurs"
  plot_distrib_depth("top",0,-1,"cloud depth","Depth (km)", "DistribDepth.pdf")

figs1()
figs2()
figs3()
