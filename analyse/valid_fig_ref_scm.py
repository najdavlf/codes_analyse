#!/usr/bin/python
# -*- coding: latin-1 -*-

# 03 / 2017 #
# N. Villefranque

# Extraire les champs moyens de la simulation LES BOMEX
# Tracer les figures du papier de référence Siebesma et al. 2003
# pour validation de la simu contre référence

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime

g = 9.81

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

def smooth(var, delta):
    nx = len(var)
    newvar = np.array(var)
    newvar[delta/2:-delta/2] = [np.mean(var[i-delta/2:i+delta/2]) for i in range(delta/2,nx-delta/2)]
    newvar[:delta/2]  = [np.mean(var[:delta/2+i]) for i in range(delta/2)]
    newvar[-delta/2:] = [np.mean(var[i-delta/2:]) for i in range(delta/2)]
    return newvar

def profile(ax, var, time, zmax, col, typ):
    ax.plot(var[time,:(int)(zmax/25)], np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0
def profilethick(ax, var, time, zmax, col, typ, lwd):
    ax.plot(var[time,:(int)(zmax/25)], np.arange(25./2.,zmax+25./2.,25),col,ls=typ,lw=lwd)
    return 0


def meanprofile(ax, var,t0,t1,zmax, col, typ):
    ax.plot(np.mean(var[t0:t1, :(int)(zmax/25)],axis=0),np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0
def meanprofile2(ax, var, t0,t1, zmin,zmax, col, typ):
    ax.plot(np.mean(var[t0:t1, (int)(zmin/25):(int)(zmax/25)],axis=0),np.arange(zmin,zmax,25),col,ls=typ)
    return 0

def setratio(ax, ratio) :
    ratio_default=(ax.get_xlim()[1]-ax.get_xlim()[0])/(ax.get_ylim()[1]-ax.get_ylim()[0])
    ax.set_aspect(ratio*ratio_default)
    return 0

def gradz(var) :
    nz = len(var[0,:])
    gradz = 0.*var
    gradz[:,0 ] =   (-var[:, 0]+var[:, 1])/25.
    gradz[:,nz-1] = ( var[:,nz-1]-var[:,nz-2])/25.
    gradz[:,1:-1] = ( var[:,2:nz]-var[:,0:nz-2])/50.
    return gradz

def entrainement(var1, var4) :
    grad = gradz(var4)
    return -grad / (var4 - var1)

def detrainement(eps,mfx) :
    grad = gradz(mfx)
    return eps - grad/mfx

ncfile = ncdf.Dataset("CERK4.1.SCM25.000KCL.nc", "r")
t = var1d(ncfile,"time")


heures=[]
for tt in t:                                              
    date = datetime.timedelta(seconds=int(tt))             
    day = date.days
    ss = tt - day*24.*3600. - 60                               
    heures.append(str(datetime.timedelta(seconds=int(ss)))) # format H:M:S

th1 = var2d(ncfile, "MEAN_THp1")
th2 = var2d(ncfile, "MEAN_THp2")
th5 = var2d(ncfile, "MEAN_THp5")
thv1 = var2d(ncfile, "MEAN_THVp1")
thv2 = var2d(ncfile, "MEAN_THVp2")
thv4 = var2d(ncfile, "MEAN_THVp4")
thv5 = var2d(ncfile, "MEAN_THVp5")
thv6 = var2d(ncfile, "MEAN_THVp6")
thl1 = var2d(ncfile, "MEAN_THLp1")
thl2 = var2d(ncfile, "MEAN_THLp2")
thl4 = var2d(ncfile, "MEAN_THLp4")
thl5 = var2d(ncfile, "MEAN_THLp5")
thl6 = var2d(ncfile, "MEAN_THLp6")
rt1 = var2d(ncfile, "MEAN_RTp1")*1000.
rt2 = var2d(ncfile, "MEAN_RTp2")
rt4 = var2d(ncfile, "MEAN_RTp4")
rt5 = var2d(ncfile, "MEAN_RTp5")
rt6 = var2d(ncfile, "MEAN_RTp6")
rc1 = var2d(ncfile, "MEAN_RCp1")*1000.
rc2 = var2d(ncfile, "MEAN_RCp2")
rc4 = var2d(ncfile, "MEAN_RCp4")
rc5 = var2d(ncfile, "MEAN_RCp5")
rc6 = var2d(ncfile, "MEAN_RCp6")
rv1 = var2d(ncfile, "MEAN_RVp1")*1000.
rr1 = var2d(ncfile, "MEAN_RRp1")*1000.
u1 = var2d(ncfile, "MEAN_Up1")
v1 = var2d(ncfile, "MEAN_Vp1")
w1 = var2d(ncfile, "MEAN_Wp1") 
w2 = var2d(ncfile, "MEAN_Wp2") 
w4 = var2d(ncfile, "MEAN_Wp4") 
w6 = var2d(ncfile, "MEAN_Wp6") 
tke1 = var2d(ncfile, "RES_KEp1")
cf = var2d(ncfile, "MEAN_CFp1")
rho1 = var2d(ncfile, "MEAN_RHOp1")
ww1 = var2d(ncfile, "RES_W2p1")
ww2 = var2d(ncfile, "RES_W2p2")
wrc = var2d(ncfile, "RES_WRCp1")
#wrr = var2d(ncfile, "RES_WRRp1")  ## Flux de precip dispo dans prochaine sortie rico
wrt = var2d(ncfile, "RES_WRTp1")
wrt4 = var2d(ncfile, "RES_WRTp4")
uu1 = var2d(ncfile, "RES_U2p1")
uw = var2d(ncfile, "RES_WUp1")
wthl = var2d(ncfile, "RES_WTHLp1")
wthl4 = var2d(ncfile, "RES_WTHLp4")
wthv = var2d(ncfile, "RES_WTHVp1")
wrt = var2d(ncfile, "RES_WRTp1")
wrc = var2d(ncfile, "RES_WRCp1")
pr1 = var2d(ncfile, "MEAN_PREp1")

pts1 = var2d(ncfile, "AVG_PTSp1")
pts2 = var2d(ncfile, "AVG_PTSp2")
pts4 = var2d(ncfile, "AVG_PTSp4")
pts6 = var2d(ncfile, "AVG_PTSp6")

e0 = var1d(ncfile, "E0")
q0 = var1d(ncfile, "Q0")
cftot = var1d(ncfile, "ZCFTOT")
cbh = var1d(ncfile, "ZCB")
lwp = var1d(ncfile, "LWP")
rwp = var1d(ncfile, "LWP")
inttke = var1d(ncfile, "INT_TKE")
blh = var1d(ncfile, "BL_H")

rho0 = rho1[:,0].min()
e0 = e0*2.47*1000000.*rho0
q0 = q0*1015*rho0

clfra = pts2/pts1
cofra = pts4/pts1
thfra = pts6/pts1

wrr = 1.*wrc ## !!! A virer quand récup flux de precip
wrr1 = 1.*wrr
wrt1 = 1.*wrt
wrc1 = 1.*wrc
wrt = wrt*2.47*1000000.*rho0
wrc = wrc*2.47*1000000.*rho0
wrr = wrr*2.47*1000000.*rho0

cfmax_height = 12.5+np.array(rc1).argmax(axis=1)*25. # hauteur du max de cloud fraction
cfmask = np.ones(np.array(rc1).shape)*np.array(rc1>0) # 1 si rc > 0 et 0 sinon
indmask = np.where(cfmask==1)
cth = e0*0.
cbh = e0*0.
cth[np.unique(indmask[0])] = [25.*max(indmask[1][indmask[0] == ind]) for ind in np.unique(indmask[0])]
cbh[np.unique(indmask[0])] = [25.*min(indmask[1][indmask[0] == ind]) for ind in np.unique(indmask[0])]

base=smooth(cbh,60)[540]
top =smooth(cth,60)[540] 

buoyancy = 0.*wthv
buoyancy2 = 0.*wthv
buoyancy[:,:] = [g*wthv[i,:]*rho1[i,:]/thv1[i,0] + g*wrc[i,:] for i in range(len(e0))]
buoyancy2[:,:] = [g*wthv[i,:]*rho1[i,:]/thv1[i,0] for i in range(len(e0))]

indminbu = np.argmin(buoyancy, axis=1)
heiminbu = 12.5 + 25.*indminbu
indminbu2 = np.argmin(buoyancy2, axis=1)
heiminbu2 = 12.5 + 25.*indminbu2

msflx2 = rho1*clfra*w2
msflx4 = rho1*cofra*w4
msflx6 = rho1*thfra*w6

qt = (rt1/1000.) / (1. + (rt1/1000.))
ql = (rc1/1000.) / (1. + (rc1/1000.))
qv = (rv1/1000.) / (1. + (rv1/1000.))
qr = (rr1/1000.) / (1. + (rr1/1000.))

the = th1+ qv*2.5*1000.*((1000./pr1)**0.286)

qt1 = qt*1000.
ql1 = ql*1000.

qt2 = rt2/(1.+rt2)*1000.
qt4 = rt4/(1.+rt4)*1000.
qt5 = rt5/(1.+rt5)*1000.
qt6 = rt6/(1.+rt6)*1000.

ql2 = rc2/(1.+rc2)*1000.
ql4 = rc4/(1.+rc4)*1000.
ql6 = rc6/(1.+rc6)*1000.

# epsilon entrainement ; delta detrainement
epsqt = entrainement(qt1, qt4)
epstl = entrainement(thl1, thl4)
delqt = detrainement(epsqt, msflx4)
deltl = detrainement(epstl, msflx4)

# indice du dernier niveau de fraction nuageuse (core et thermiques) > 1% (max entre 1800 et 2100)
indfra2 = np.where(clfra>=0.01)
indmax2 = (int)(np.max([np.max(indfra2[1][indfra2[0] == ind]) for ind in range(360,540)]))
hmax2 = indmax2*25.
indmin2 = (int)(np.min([np.min(indfra2[1][indfra2[0] == ind]) for ind in range(360,540)]))
hmin2 = indmin2*25.

indfra4 = np.where(cofra>=0.01)
indmax4 = (int)(np.max([np.max(indfra4[1][indfra4[0] == ind]) for ind in range(360,540)]))
hmax4 = indmax4*25.
indmin4 = (int)(np.min([np.min(indfra4[1][indfra4[0] == ind]) for ind in range(360,540)]))
hmin4 = indmin4*25.

indfra6 = np.where(thfra>=0.01)
indmax6 = (int)(np.max([np.max(indfra6[1][indfra6[0] == ind]) for ind in range(360,540)]))
hmax6 = indmax6*25.
indmin6 = (int)(np.min([np.min(indfra6[1][indfra6[0] == ind]) for ind in range(360,540)]))
hmin6 = indmin6*25.

def fig5():
    fig = plt.figure(figsize=(18,15))
    ratio = 2

    ax1 = fig.add_subplot(121)
    ax2 = ax1.twiny()
    ax3 = fig.add_subplot(122)

    profile(ax1, th1, 0, 3000, 'k', '-')
    profile(ax2, qt1, 0, 3000, 'k', '--')
    ax3.plot(range(720), q0, color='k', linestyle='-')
    ax3.plot(range(720), e0, color='k', linestyle='--')

    ax1.set_xlabel(r"$\theta$ (K)")
    ax2.set_xlabel(r"$q_t$ (g/kg)")
    ax3.set_xlabel("time (UTC)")
    
    ax1.set_ylabel("height (m)")
    ax3.set_ylabel(r"Surface heat fluxes (W/m$^2$)")

    ax1.set_xlim(xmin=295, xmax=313)
    ax2.set_xlim(xmin=0, xmax=22 )
    ax3.set_xlim(xmin=-60, xmax=780)
    
    ax1.set_ylim(ymin= 0, ymax=3000)
    ax3.set_ylim(ymin=-50, ymax=450)

    ax3.set_xticks(range(0,721,180))
    ax3.set_xticklabels(["12","15","18","21","24"])
    
    ax1.legend([r"$\theta$ "],loc="center left")
    ax2.legend([r"$q_t$ "],loc="right")
    ax3.legend(["Sensible","Latent"], loc="best")

    plt.savefig("neggersetal_fig05.pdf")
    plt.close()

def fig6():
    fig = plt.figure(figsize=(15,15))
    ratio = 1.8

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    profilethick(ax1, th1, 0  , 3000, 'k', '-', 2)
    profile(ax1, th1, 90 , 3000, 'k', '-')
    profile(ax1, th1, 210, 3000, 'k', '--')
    profile(ax1, th1, 330, 3000, 'k', '-.')
    profile(ax1, th1, 450, 3000, 'k', ':')
    profile(ax1, th1, 570, 3000, 'k+', '-')
    profile(ax1, th1, 690, 3000, 'k.', '-')

    profilethick(ax2, qt1, 0, 3000, 'k', '-', 2)
    profile(ax2, qt1, 90 , 3000, 'k', '-') 
    profile(ax2, qt1, 210, 3000, 'k', '--') 
    profile(ax2, qt1, 330, 3000, 'k', '-.') 
    profile(ax2, qt1, 450, 3000, 'k', ':') 
    profile(ax2, qt1, 570, 3000, 'k+', '-') 
    profile(ax2, qt1, 690, 3000, 'k.', '-') 

    ax1.legend(["Initial 1200 UTC", "1330 UTC", "1530 UTC", "1730 UTC", "1930 UTC", "2130 UTC", "2330 UTC"], loc = "best")
    ax2.legend(["Initial 1200 UTC", "1330 UTC", "1530 UTC", "1730 UTC", "1930 UTC", "2130 UTC", "2330 UTC"], loc = "best")

    ax1.set_xlabel(r"$\theta$ (K)")
    ax2.set_xlabel(r"$q_t$ (g/kg)")

    ax1.set_label("height (m)")
    ax2.set_label("height (m)")

    ax1.set_xlim(xmin=296 , xmax=313)
    ax2.set_xlim(xmin=0   , xmax=20 )

    ax1.set_ylim(ymin=0   , ymax=3000)
    ax2.set_ylim(ymin=0   , ymax=3000)

    setratio(ax1,ratio)
    setratio(ax2,ratio)
    
    plt.savefig("neggersetal_fig06.pdf")
    plt.close()

def fig7():
    fig = plt.figure(figsize=(15,13))

    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax3 = ax2.twinx()

    ax1.plot(range(720), smooth(cth, 60), color='k', linestyle=':')
    ax1.plot(range(690), smooth(blh, 60)[:690], color="k", linestyle='-')
    ax1.plot(range(690), smooth(heiminbu, 60)[:690], color="k", linestyle='-.')
    ax1.plot(range(690), smooth(heiminbu2, 60)[:690], color="k", linestyle='--')
    ax1.fill_between(range(720), cbh, cth, color='gray', alpha=.5)
    ax2.plot(range(720), cftot*100, color="k", linestyle='-')
    ax3.plot(range(720), lwp*1000, color="k", linestyle='--')
    ax1.axvline(360,color="k",linestyle=':')
    ax1.axvline(540,color="k",linestyle=':')
    ax2.axvline(360,color="k",linestyle=':')
    ax2.axvline(540,color="k",linestyle=':')
    ax1.set_xlim(xmin=-180, xmax=780)
    ax2.set_xlim(xmin=-180, xmax=780)

    ax1.set_ylim(ymin=0,ymax=3500 )
    ax2.set_ylim(ymin=0,ymax=50 )
    ax3.set_ylim(ymin=0,ymax=100)

    ax1.set_xticks(range(-120,750,120))
    ax1.set_xticklabels(["10","12","14","16","18","20","22","24"])
    ax2.set_xticks(range(-120,750,120))
    ax2.set_xticklabels(["10","12","14","16","18","20","22","24"])

    ax1.set_ylabel("height (m)")
    ax2.set_ylabel("Projected cloud fraction (%)")
    ax3.set_ylabel("Liquid Water Path (g/kg)")
    ax2.set_xlabel("Time (UTC)")
    
    ax1.legend(["cloud top height smoothed over one hour","boundary layer height",r"level of min buoyancy flux (= $\theta_v$ flux + $r_c$ flux)", r"level of min buoyancy flux (= $\theta_v$ flux)", "clouds"],loc="upper left")
    ax2.legend(["Projected Cloud Fraction"],loc="upper left")
    ax3.legend(["Liquid Water Path"], loc="upper right")

    plt.savefig("neggersetal_fig07.pdf")
    plt.close()

def fig8():
    fig = plt.figure(figsize=(12,10))

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    ax1.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax1.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax1.axhspan(hmin4,hmax4,color="gray",alpha=.2)
    
    ax2.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax2.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax2.axhspan(hmin4,hmax4,color="gray",alpha=.2)

    meanprofile(ax1, thl1, 360,540, 3000, 'k', '-.' )
    meanprofile2(ax1, thl2, 360,540, hmin2,hmax2, 'k', '--')
    meanprofile2(ax1, thl4, 360,540, hmin4,hmax4, 'k', '-')
    meanprofile2(ax1, thl6, 360,540, hmin6,hmax6, 'k', ':' )
    
    meanprofile(ax2, qt1, 360,540, 3000, 'k', '-.' )
    meanprofile2(ax2, qt2, 360,540, hmin2,hmax2, 'k', '--')
    meanprofile2(ax2, qt4, 360,540, hmin4,hmax4, 'k', '-')
    meanprofile2(ax2, qt6, 360,540, hmin6,hmax6, 'k', ':' )

    ax1.set_xlim(xmin=297, xmax=315)
    ax2.set_xlim(xmin=0  , xmax=20 )

    ax1.set_ylim(ymin=0  , ymax=3000)
    ax2.set_ylim(ymin=0  , ymax=3000)

    ax1.set_xlabel(r"$\theta_l$ (K)")
    ax2.set_xlabel(r"$q_t$ (g/kg)")

    ax1.set_ylabel("Height (m)")

    ax1.legend(["mean","cloud","core","thermal"],loc="best")
    ax2.legend(["mean","cloud","core","thermal"],loc="best")

    plt.savefig("neggersetal_fig08.pdf")
    plt.close()


def fig9() :
    fig = plt.figure(figsize=(15,22))
    ratio = 1.5

    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)

    ax1.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax1.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax1.axhspan(hmin4,hmax4,color="gray",alpha=.2)
    
    ax2.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax2.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax2.axhspan(hmin4,hmax4,color="gray",alpha=.2)
   
    ax3.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax3.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax3.axhspan(hmin4,hmax4,color="gray",alpha=.2)

    ax4.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax4.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax4.axhspan(hmin4,hmax4,color="gray",alpha=.2)


    meanprofile(ax1, thl2-thl5, 360,540, 2500, 'k', '--')
    meanprofile(ax1, thl4-thl5, 360,540, 2500, 'k', '-')
    meanprofile(ax1, thl6-thl5, 360,540, 2500, 'k', '-.' )

    meanprofile(ax2, qt2-qt5, 360,540, 2500, 'k', '--')
    meanprofile(ax2, qt4-qt5, 360,540, 2500, 'k', '-')
    meanprofile(ax2, qt6-qt5, 360,540, 2500, 'k', '-.' )
    
    meanprofile(ax3, gradz(thl2), 360,540, 2500, 'k', '--')
    meanprofile(ax3, gradz(thl4), 360,540, 2500, 'k', '-')
    meanprofile(ax3, gradz(thl6), 360,540, 2500, 'k', '-.' )

    meanprofile(ax4, gradz(qt2), 360,540, 2500, 'k', '--')
    meanprofile(ax4, gradz(qt4), 360,540, 2500, 'k', '-')
    meanprofile(ax4, gradz(qt6), 360,540, 2500, 'k', '-.' )

    ax3.axvline(color="k", linestyle=':')
    ax4.axvline(color="k", linestyle=':')

    ax1.set_ylim(ymin=250, ymax=2500)
    ax2.set_ylim(ymin=250, ymax=2500)
    ax3.set_ylim(ymin=250, ymax=2500)
    ax4.set_ylim(ymin=250, ymax=2500)
    
    ax1.set_xlim(xmin=-5   , xmax=0   )
    ax2.set_xlim(xmin= 0   , xmax=7   )
    ax3.set_xlim(xmin=-.002, xmax=.008)
    ax4.set_xlim(xmin=-.008, xmax=.002)

    ax1.set_xlabel(r"$\theta_l^c - \overline{\theta_l}$ (K)")
    ax2.set_xlabel(r"$q_t^c - \overline{q_t}$ (g/kg)")
    ax3.set_xlabel(r"$\partial\theta_l^c / \partial z$ (K/m)")
    ax4.set_xlabel(r"$\partial q_t^c / \partial z$ (g/kg/m)")

    ax1.legend(["cloud", "core", "thermal"], loc="best")
    ax2.legend(["cloud", "core", "thermal"], loc="best")
    ax3.legend(["cloud", "core", "thermal"], loc="best")
    ax4.legend(["cloud", "core", "thermal"], loc="best")

    setratio(ax1,ratio)
    setratio(ax2,ratio)
    setratio(ax3,ratio)
    setratio(ax4,ratio)

    plt.savefig("neggersetal_fig09.pdf")
    plt.close()

def fig10():
    fig = plt.figure(figsize=(14,14))
    ratio = 1.8

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    ax1.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax1.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax1.axhspan(hmin4,hmax4,color="gray",alpha=.2)
    ax2.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax2.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax2.axhspan(hmin4,hmax4,color="gray",alpha=.2)
    

    meanprofile(ax1, ql2, 360,540, 2500, 'k', '--')
    meanprofile(ax1, ql4, 360,540, 2500, 'k', '-')
    meanprofile(ax1, ql6, 360,540, 2500, 'k', ':' )

    meanprofile(ax2, cf*100, 360,540, 2500, 'k', '--')
    meanprofile(ax2, cofra*100, 360,540, 2500, 'k', '-')
    meanprofile(ax2, thfra*100, 360,540, 2500, 'k', ':' )

    ax1.set_xlabel(r"$q_l$ (g/kg)")
    ax2.set_xlabel("Fraction (%)")

    ax1.set_ylabel("Height (m)")
    ax2.set_ylabel("Height (m)")

    ax1.set_xlim(xmin=0, xmax=4)
    ax2.set_xlim(xmin=0, xmax=30)
    ax1.set_ylim(ymin=250, ymax=2500)
    ax2.set_ylim(ymin=250, ymax=2500)
   
    ax1.legend(["cloud","core","thermal"],loc="best")
    ax2.legend(["cloud","core","thermal"],loc="best")

    setratio(ax1,ratio)
    setratio(ax2,ratio)

    plt.savefig("neggersetal_fig10.pdf")    
    plt.close()

def fig11():
    fig = plt.figure(figsize=(14,14))
    ratio = 1.8

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    ax2.axhspan(hmin2,hmax2,color="gray",alpha=.5)
    ax1.axhspan(hmin2,hmax2,color="gray",alpha=.5)
    meanprofile(ax1, epstl, 360,540, 2500, "k", "-" )
    meanprofile(ax1, epsqt, 360,540, 2500, "k", "--")
    meanprofile(ax2, deltl, 360,540, 2500, "k", "-" )
    meanprofile(ax2, delqt, 360,540, 2500, "k", "--")
    ax1.axvline(color="k",linestyle=":")

    ax1.set_xlabel(r"$\epsilon$ ($m^{-1}$)")
    ax2.set_xlabel(r"$\delta$ ($m^{-1}$)")

    ax1.set_ylabel("height (m)")
    ax2.set_ylabel("height (m)")

    ax1.set_xlim(xmin=-0.001, xmax=0.005)
    ax2.set_xlim(xmin=0, xmax=0.005)
    
    ax1.set_ylim(ymin=250, ymax=2500)
    ax2.set_ylim(ymin=250, ymax=2500)

    ax1.legend([r"$\epsilon(\theta_l)$", r"$\epsilon(q_t)$"], loc="best")
    ax2.legend([r"$\delta(\theta_l)$", r"$\delta(q_t)$"], loc="best")

    plt.savefig("neggersetal_fig11.pdf")
    plt.close()

def fig12():
    fig = plt.figure(figsize=(07,12))
    ratio = 1.8
    ax = fig.add_subplot(111)
    meanprofile(ax, th1, 360,540, 2500, "k", "-" )
    meanprofile(ax, th2, 360,540, 2500, "k", "--")
    ax.axhspan(hmin2,hmax2,color="gray",alpha=.5)

    ax.set_xlim(xmin=297.2, xmax=310)
    ax.set_ylim(ymin=0  , ymax=2500)

    ax.set_xlabel(r"$\theta$ (K)")
    ax.set_ylabel("height (m)")

    ax.legend(["mean","cloud"],loc="best")

    plt.savefig("neggersetal_fig12.pdf")
    plt.close()

def fig13():
    fig = plt.figure(figsize=(14,14))
    ratio=1.8

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    meanprofile(ax1, w2, 360,540, 2500, 'k', '--')
    meanprofile(ax1, w4, 360,540, 2500, "k", "-" )
    meanprofile(ax1, w6, 360,540, 2500, "k", ":")
    
    ax1.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax1.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax1.axhspan(hmin4,hmax4,color="gray",alpha=.2)


    meanprofile(ax2, msflx2, 360,540, 2500, "k", "--")

    ax1.set_ylim(ymin=250, ymax=2500)
    ax1.set_xlim(xmin=0, xmax=4.5 )
    ax2.set_ylim(ymin=250, ymax=2500)
    ax2.set_xlim(xmin=0, xmax=.17 )

    ax1.set_xlabel("w (m/s)")
    ax2.set_xlabel("Mass flux (m/s)")
    ax1.set_ylabel("height (m)")
    ax2.set_ylabel("height (m)")

    ax1.legend(["cloud","core","thermal"],loc="best")

    setratio(ax1,ratio)
    setratio(ax2,ratio)

    plt.savefig("neggersetal_fig13.pdf")
    plt.close()

def fig14():
    fig = plt.figure(figsize=(10,15))
    ratio = 1.8

    ax = fig.add_subplot(111)

    ax.axhspan(hmin6,hmax6,color="gray",alpha=.2)
    ax.axhspan(hmin2,hmax2,color="gray",alpha=.2)
    ax.axhspan(hmin4,hmax4,color="gray",alpha=.2)
    
    meanprofile(ax, thv2-thv5, 360,540, 2500, "k", "--")
    meanprofile(ax, thv4-thv5, 360,540, 2500, "k", "-" )
    meanprofile(ax, thv6-thv5, 360,540, 2500, "k", "-." )

    ax.axvline(color="k",linestyle=':')
    
    ax.set_xlabel(r"$\theta_v^c - \overline{\theta_v}$ (K)")
    ax.legend(["cloud","core","thermal"],loc="best")

    ax.set_xlim(xmin=-1.5,xmax=2)
    ax.set_ylim(ymin=250,ymax=2500)

    setratio(ax,ratio)

    plt.savefig("neggersetal_fig14.pdf")
    plt.close()

def fig15():
    fig = plt.figure(figsize=(14,14))
    ratio = 1.8

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    meanprofile(ax1, ww1, 360,540, 3000, "k", "-")
    meanprofile(ax2, ww2, 360,540, 3000, "k", "-")
    ax2.axhspan(hmin2,hmax2,color="gray",alpha=.5)

    ax1.set_xlabel(r"$\sigma_w^2$  ($m^2/s^2$)")
    ax2.set_xlabel(r"$(\sigma_w^c)^2$  ($m^2/s^2$)")

    ax1.set_ylabel("height (m)")
    ax2.set_ylabel("height (m)")

    ax1.set_xlim(xmin=0, xmax=2)
    ax2.set_xlim(xmin=0, xmax=12)

    ax1.set_ylim(ymin=0,ymax=3000)
    ax2.set_ylim(ymin=0,ymax=3000)
   
    plt.savefig("neggersetal_fig15.pdf")
    plt.close()


fig5()
fig6()
fig7()
fig8()
fig9()
fig10()
fig11()
fig12()
fig13()
fig14()
fig15()
