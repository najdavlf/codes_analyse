#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib
from mycolors import *

path="/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/"

simuId = np.array(["REF","MIC","MAC"])
simuCo = np.array([basic9[4],basic10[7],basic10[5]])

REF = path+"ARM_LES/WRONG_FORC/"+"CERK4.1.ARMWF.16h30.nc"

REFdat = ncdf.Dataset(REF,"r")

REFrct = REFdat.variables["RCT"][0,:,:,:]
REFmic = REFdat.variables["cloud_mean_rct"][0,:,:,:]
maxlon = int(3.2/0.025)
indlat = int(4./0.025)-1

REFrct2 = (1000.*REFrct[:,indlat,0:maxlon])
REFmic2 = (1000.*REFmic[:,indlat,0:maxlon])

#vmin = min(np.min(REFrct2),np.min(REFmic2))
vmin = 0.000001
vmax = max(np.max(REFrct2),np.max(REFmic2))
print vmin,vmax


matplotlib.rcParams.update({'font.size': 18})
cmap = plt.get_cmap("jet")
cmap.set_under("w")
w, h = plt.figaspect(1./2.)
plt.figure(figsize=(1.5*w,h))

plt.subplot(121)
plt.imshow(REFrct2,vmin=vmin,vmax=vmax,interpolation=None, origin="lower")
locs,labs = plt.xticks()
newlabs = ["%g" %(g*25./1000.) for g in locs]
plt.xticks(locs,newlabs)
plt.xlabel("Longitude [km]")
plt.xlim(0,maxlon)
locs,labs = plt.yticks()
newlabs = ["%g" %(g*25./1000.) for g in locs]
plt.yticks(locs,newlabs)
plt.ylabel("Height [km]")
plt.ylim(20,60)

plt.subplot(122)
plt.imshow(REFmic2,vmin=vmin,vmax=vmax,interpolation=None, origin="lower")
locs,labs = plt.xticks()
newlabs = ["%g" %(g*25./1000.) for g in locs]
plt.xticks(locs,newlabs)
plt.xlabel("Longitude [km]")
plt.xlim(0,maxlon)
locs,labs = plt.yticks()
newlabs = ["%g" %(g*25./1000.) for g in locs]
plt.yticks(locs,newlabs)
plt.ylabel("Height [km]")
plt.ylim(20,60)
plt.colorbar(orientation="horizontal",aspect=10)

plt.savefig("vert_sec_HETER_HOMOG.png")
