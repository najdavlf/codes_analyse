#!/usr/bin/python
# -*- coding: latin-1 -*-

# 03 / 2017 #
# N. Villefranque

# Extraire les champs moyens de la simulation LES BOMEX
# Tracer les figures du papier de référence Siebesma et al. 2003
# pour validation de la simu contre référence

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime

g = 9.81

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

def smooth(var, delta):
    nx = len(var)
    newvar = np.array(var)
    newvar[delta/2:-delta/2] = [np.mean(var[i-delta/2:i+delta/2]) for i in range(delta/2,nx-delta/2)]
    newvar[:delta/2]  = [np.mean(var[:delta/2+i]) for i in range(delta/2)]
    newvar[-delta/2:] = [np.mean(var[i-delta/2:]) for i in range(delta/2)]
    return newvar

def profile(ax, var, time, zmax, col, typ):
    ax.plot(var[time,:(int)(zmax/25)], np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0

def meanprofile(ax, var,t0,t1,zmax, col, typ):
    ax.plot(np.mean(var[t0:t1, :(int)(zmax/25)],axis=0),np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0

def setratio(ax, ratio) :
    ratio_default=(ax.get_xlim()[1]-ax.get_xlim()[0])/(ax.get_ylim()[1]-ax.get_ylim()[0])
    ax.set_aspect(ratio*ratio_default)
    return 0

def gradz(var) :
    nz = len(var[0,:])
    gradz = 0.*var
    gradz[:,0 ] =   (-var[:, 0]+var[:, 1])/25.
    gradz[:,nz-1] = ( var[:,nz-1]-var[:,nz-2])/25.
    gradz[:,1:-1] = ( var[:,2:nz]-var[:,0:nz-2])/50.
    return gradz

def entrainement(var1, var4) :
    grad = gradz(var4)
    return -grad / (var4 - var1)

def detrainement(eps,mfx) :
    grad = gradz(mfx)
    return eps - grad/mfx

ncfile = ncdf.Dataset("THL3D.1.RIC.000.nc", "r")
t = var1d(ncfile,"time")


heures=[]
for tt in t:                                              
    date = datetime.timedelta(seconds=int(tt))             
    day = date.days
    ss = tt - day*24.*3600. - 60                               
    heures.append(str(datetime.timedelta(seconds=int(ss)))) # format H:M:S

th1 = var2d(ncfile, "MEAN_THp1")
thv1 = var2d(ncfile, "MEAN_THVp1")
thv2 = var2d(ncfile, "MEAN_THVp2")
thv4 = var2d(ncfile, "MEAN_THVp4")
thv6 = var2d(ncfile, "MEAN_THVp6")
thl1 = var2d(ncfile, "MEAN_THLp1")
thl2 = var2d(ncfile, "MEAN_THLp2")
thl4 = var2d(ncfile, "MEAN_THLp4")
thl6 = var2d(ncfile, "MEAN_THLp6")
rt1 = var2d(ncfile, "MEAN_RTp1")*1000.
rt2 = var2d(ncfile, "MEAN_RTp2")
rt4 = var2d(ncfile, "MEAN_RTp4")
rt6 = var2d(ncfile, "MEAN_RTp6")
rc1 = var2d(ncfile, "MEAN_RCp1")*1000.
rc2 = var2d(ncfile, "MEAN_RCp2")
rc4 = var2d(ncfile, "MEAN_RCp4")
rc6 = var2d(ncfile, "MEAN_RCp6")
rv1 = var2d(ncfile, "MEAN_RVp1")*1000.
rr1 = var2d(ncfile, "MEAN_RRp1")*1000.
u1 = var2d(ncfile, "MEAN_Up1")
v1 = var2d(ncfile, "MEAN_Vp1")
w1 = var2d(ncfile, "MEAN_Wp1") 
w2 = var2d(ncfile, "MEAN_Wp2") 
w4 = var2d(ncfile, "MEAN_Wp4") 
w6 = var2d(ncfile, "MEAN_Wp6") 
tke1 = var2d(ncfile, "RES_KEp1")
cf = var2d(ncfile, "MEAN_CFp1")
rho1 = var2d(ncfile, "MEAN_RHOp1")
ww1 = var2d(ncfile, "RES_W2p1")
wrc = var2d(ncfile, "RES_WRCp1")
wrr = var2d(ncfile, "RES_WRRp1")  
wrt = var2d(ncfile, "RES_WRTp1")
wrt4 = var2d(ncfile, "RES_WRTp4")
uu1 = var2d(ncfile, "RES_U2p1")
uw = var2d(ncfile, "RES_WUp1")
wthl = var2d(ncfile, "RES_WTHLp1")
wthl4 = var2d(ncfile, "RES_WTHLp4")
wthv = var2d(ncfile, "RES_WTHVp1")
wrt = var2d(ncfile, "RES_WRTp1")
wrc = var2d(ncfile, "RES_WRCp1")
pr1 = var2d(ncfile, "MEAN_PREp1")

pts1 = var2d(ncfile, "AVG_PTSp1")
pts2 = var2d(ncfile, "AVG_PTSp2")
pts4 = var2d(ncfile, "AVG_PTSp4")
pts6 = var2d(ncfile, "AVG_PTSp6")

e0 = var1d(ncfile, "E0")
q0 = var1d(ncfile, "Q0")
cftot = var1d(ncfile, "ZCFTOT")
cbh = var1d(ncfile, "ZCB")
lwp = var1d(ncfile, "LWP")
rwp = var1d(ncfile, "LWP")
inttke = var1d(ncfile, "INT_TKE")

rho0 = rho1[:,0].min()
e0 = e0*2.47*1000000.*rho0
q0 = q0*1004*rho0

clfra = pts2/pts1
cofra = pts4/pts1
thfra = pts6/pts1

wrr1 = 1.*wrr
wrt1 = 1.*wrt
wrc1 = 1.*wrc
wrt = wrt*2.47*1000000.*rho0
wrc = wrc*2.47*1000000.*rho0
wrr = wrr*2.47*1000000.*rho0

cfmax_height = np.array(rc1).argmax(axis=1)*25. # hauteur du max de cloud fraction
cfmask = np.ones(np.array(rc1).shape)*np.array(rc1>0) # 1 si rc > 0 et 0 sinon
indmask = np.where(cfmask==1)
cth = e0*0.
cbh = e0*0.
cth[np.unique(indmask[0])] = [25.*max(indmask[1][indmask[0] == ind])+12.5 for ind in np.unique(indmask[0])]
cbh[np.unique(indmask[0])] = [25.*min(indmask[1][indmask[0] == ind])-12.5 for ind in np.unique(indmask[0])]

base=smooth(cbh,60)[540]
top =smooth(cth,60)[540] 

buoyancy = 0.*wthv
buoyancy[:,:] = [g*wthv[i,:]*rho1[i,:]/thv1[i,0] + g*wrc[i,:] for i in range(len(e0))]

msflx2 = rho1*clfra*w2
msflx4 = rho1*cofra*w4
msflx6 = rho1*thfra*w6

qt = (rt1/1000.) / (1. + (rt1/1000.))
ql = (rc1/1000.) / (1. + (rc1/1000.))
qv = (rv1/1000.) / (1. + (rv1/1000.))
qr = (rr1/1000.) / (1. + (rr1/1000.))

the = th1+ qv*2.5*1000.*((1000./pr1)**0.286)

qt1 = qt*1000.
ql1 = ql*1000.

qt2 = rt2/(1.+rt2)*1000.
qt4 = rt4/(1.+rt4)*1000.
qt6 = rt6/(1.+rt6)*1000.

ql2 = rc2/(1.+rc2)*1000.
ql4 = rc4/(1.+rc4)*1000.
ql6 = rc6/(1.+rc6)*1000.

# epsilon entrainement ; delta detrainement
epsqt = entrainement(qt1, qt4)
epstl = entrainement(thl1, thl4)
delqt = detrainement(epsqt, msflx4)
deltl = detrainement(epstl, msflx4)

def fig2(th1,qv,u1,v1):
    fig = plt.figure(figsize=(20,5))
    ratio = 1.0

    ax1 = fig.add_subplot(141)
    ax2 = fig.add_subplot(142, sharey=ax1)
   #plt.setp(ax2.get_yticklabels(), visible=False)
    ax3 = fig.add_subplot(143, sharey=ax1)
   #plt.setp(ax3.get_yticklabels(), visible=False)
    ax4 = fig.add_subplot(144, sharey=ax1)
   #plt.setp(ax4.get_yticklabels(), visible=False)
    
    profile(ax1, th1, 0, 4000, 'k', '-')
    profile(ax2, qv*1000., 0, 4000, 'k', '-') 
    profile(ax3, u1, 0, 4000, 'k', '-')
    profile(ax4, v1, 0, 4000, 'k', '-')
   
    ax1.set_xlabel(r"$\theta$ (K)")
    ax1.set_ylabel("height (m)")
    ax2.set_xlabel(r"q$_v$ (g/kg)")
    ax3.set_xlabel("u (m/s)")
    ax4.set_xlabel("v (m/s)")

    ax1.set_xlim(xmin=293,xmax=320)
    ax2.set_xlim(xmin=0,xmax=22)
    ax3.set_xlim(xmin=-11, xmax=0)
    ax4.set_xlim(xmin=-15, xmax=0)
    
   #plt.subplots_adjust(wspace = -.059)
    
   #setratio(ax1,ratio)
   #setratio(ax2,ratio)
   #setratio(ax3,ratio)
   #setratio(ax4,ratio)
    plt.savefig("vanzantenetal_fig02.pdf")
    plt.close()



def fig3(cbh, eo, qo, cftot, lwp, rwp) :
    fig = plt.figure(figsize=(20,15))
    ratio = .3

    ax5 = fig.add_subplot(325)
    ax6 = fig.add_subplot(326)

    ax1 = fig.add_subplot(321, sharex = ax5)
    ax2 = fig.add_subplot(322, sharex = ax6)
    ax3 = fig.add_subplot(323, sharex = ax5)
    ax4 = fig.add_subplot(324, sharex = ax6)

    ax1.plot(range(1440),smooth(cbh,60), "k")
    ax1.set_ylim(ymin=0,ymax=900)
    ax1.annotate("Cloud Base Height (m)", xy=(240,700))
    plt.setp(ax1.get_xticklabels(), visible=False)
    setratio(ax1,ratio)
    ax3.plot(range(1440),smooth(q0,60), "k")
    ax3.set_ylim(ymin=0,ymax=16)
    ax3.annotate(r"Sensible Heat Flux (W/m$^2$)", xy=(240,14.5))
    plt.setp(ax3.get_xticklabels(), visible=False)
    setratio(ax3,ratio)
    ax5.plot(range(1440),smooth(e0,60), "k")
    ax5.set_ylim(ymin=100,ymax=200)
    ax5.annotate(r"Latent Heat Flux (W/m$^2$)", xy=(240,189))
    ax5.set_xlim(xmin=0,xmax=1440)
    ax5.set_xlabel("Time (h)")
    ax5.set_xticks(range(0,1441,120))
    ax5.set_xticklabels(["0","","4","","8","","12","","16","","20","","24"])
    setratio(ax5,ratio)

    ax2.plot(range(1440),smooth(cftot,60), "k")
    ax2.set_ylim(ymin=0,ymax=.4)
    ax2.annotate("Cloud Cover (-)", xy=(240,.38))
    plt.setp(ax2.get_xticklabels(), visible=False)
    setratio(ax2,ratio)
    ax4.plot(range(1440),smooth(lwp*1000,60), "k")
    ax4.set_ylim(ymin=0,ymax=50)
    ax4.annotate(r"Liquid Water Path (g/m$^2$)", xy=(240,47))
    plt.setp(ax4.get_xticklabels(), visible=False)
    setratio(ax4,ratio)
    ax6.plot(range(1440),smooth(rwp*1000,60), "k")
    ax6.set_ylim(ymin=0,ymax=21)
    ax6.annotate(r"Rain Water Path (g/m$^2$)", xy=(240,18))
    ax6.set_xlim(xmin=0,xmax=1440)
    ax6.set_xlabel("Time (h)")
    ax6.set_xticks(range(0,1441,120))
    ax6.set_xticklabels(["0","","4","","8","","12","","16","","20","","24"])
    setratio(ax6,ratio)

    plt.savefig("vanzantenetal_fig03.pdf")

    plt.close()


def fig4(thl,qt,u,v,qr,ql,cf) :
    fig = plt.figure(figsize=(20,10))
    ratio = 1.

    ax1 = fig.add_subplot(231)
    ax2 = fig.add_subplot(232, sharey = ax1)
    ax3 = fig.add_subplot(233, sharey = ax1)
    ax4 = fig.add_subplot(234)
    ax5 = fig.add_subplot(235, sharey = ax4)

    meanprofile(ax1, thl, 1200, 1440, 4000, "k", "-")
    meanprofile(ax2, qt*1000, 1200, 1440, 4000, "k", "-")
    meanprofile(ax3, u, 1200, 1440, 4000, "k", "-")
    meanprofile(ax3, v, 1200, 1440, 4000, "k", "-")
    meanprofile(ax4, qr*1000000, 1200, 1440, 4000, "k", "-")
    meanprofile(ax4, ql*1000000, 1200, 1440, 4000, "k", "--")
    meanprofile(ax5, cf, 1200, 1440, 4000, "k", "-")
    meanprofile(ax5, cofra, 1200, 1440, 4000, "k", "-.")
    meanprofile(ax5, thfra, 1200, 1440, 4000, "k", ":")

    profile(ax1, thl, 0, 4000, 'k', '--')
    profile(ax2, qt*1000, 0, 4000, 'k', '--')
    profile(ax3, u, 0, 4000, 'k', '--')
    profile(ax3, v, 0, 4000, 'k', '--')

    ax1.set_xlim(xmin=296, xmax=316)
    ax2.set_xlim(xmin=0  , xmax=16.)
    ax3.set_xlim(xmin=-10, xmax=0.0)
    ax4.set_xlim(xmin=-2 , xmax=15.)
    ax5.set_xlim(xmin=-.01, xmax=0.3)

    ax1.set_xlabel(r'$\theta_l$ (K)')
    ax2.set_xlabel(r'q$_t$ (g/kg)')
    ax3.set_xlabel("u, v (m/s)")
    ax4.set_xlabel(r"q$_r$, q$_l$ (mg/kg)")
    ax5.set_xlabel("Fraction")

    ax1.set_ylabel("Height (m)")
    ax4.set_ylabel("Height (m)")

    ax1.legend(["Mean 4 last hours", "Initial"],loc="best")
    ax5.legend(["Cloud", "Core", "Thermals"],loc="best")
    ax4.legend([r"$q_r$", r"$q_l$"],loc="best")

    plt.savefig("vanzantenetal_fig04.pdf")
    plt.close()


def fig5(w2, w4) :
    fig = plt.figure(figsize=(15,15))
    ratio = 1. 

    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)

    meanprofile(ax1, msflx4, 1200,1440,2500, 'k', '-')
    meanprofile(ax1, msflx6, 1200,1440,2500, 'k', '--')
    meanprofile(ax2, w2, 1200, 1440, 2500, 'k', '-')
    meanprofile(ax2, w4, 1200, 1440, 2500, 'k', '--')
    meanprofile(ax2, w6, 1200, 1440, 2500, 'k', '--')
    meanprofile(ax3, epsqt*1000., 1200,1440, 2500, 'k', '-')
    ax3.axvline(x=1.3,linestyle='--',color="k")
    meanprofile(ax4, delqt*1000., 1200,1440, 2500, 'k', '-')
    ax4.axvline(x=1.6,linestyle='--',color="k")
    
    ax1.legend(["core","thermal"], loc="best")
    ax2.legend(["cloud","core","thermal"], loc="best")
    
    ax1.set_xlabel("Massflux (m/s)")
    ax2.set_xlabel("w (m/s)")
    ax3.set_xlabel(r"$\epsilon$ (km$^{-1}$)")
    ax4.set_xlabel(r"$\delta$ (km$^{-1}$)")

    ax1.set_ylim(ymin=0, ymax=3000)
    ax2.set_ylim(ymin=0, ymax=3000)
    ax3.set_ylim(ymin=0, ymax=3000)
    ax4.set_ylim(ymin=0, ymax=3000)
    
   #ax1.set_xlim(xmin=0, xmax=0.03)
    ax2.set_xlim(xmin=0, xmax=3)
    ax3.set_xlim(xmin=0, xmax=4)
    ax4.set_xlim(xmin=0, xmax=4)

    plt.savefig("vanzantenetal_fig05.pdf")
    plt.close()

def fig6() :
    fig = plt.figure(figsize=(15,8))
    ratio = 1.

    ax1 = fig.add_subplot(121)
    meanprofile(ax1, wrr, 1200,1440, 4000, 'k', '-')
    ax1.axvline(linestyle=':',color="k")
    ax1.set_xlim(xmax=5)
    ax1.set_ylim(ymin=0, ymax=4000)
    ax1.set_xlabel(r"Prec (W/m$^2$)")
    ax1.set_ylabel("Height (m)")

    ax2 = fig.add_subplot(122)
    meanprofile(ax2, wrr1/qr, 1200,1440, 4000, 'k', '-')
    ax2.axvline(linestyle=':',color="k")
    ax2.set_xlim(xmax=5)
    ax2.set_ylim(ymin=0, ymax=4000)
    ax2.set_xlabel(r"Prec/q$_r$ (m.s$^{-1}$)")
    ax2.set_ylabel("Height (m)")

    plt.savefig("vanzantenetal_fig06.pdf")
    plt.show()



fig2(th1,qv,u1,v1)
fig3(cbh,e0,q0,cftot,lwp,rwp)
fig4(thl1,qt,u1,v1,qr,ql,cf)
fig5(w2,w4)
fig6()
