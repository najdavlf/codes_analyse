
# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
import netCDF4
import scipy.ndimage as nd
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage import measure
from skimage.draw import ellipsoid
import sys

datafile = sys.argv[1]

p = netCDF4.Dataset(datafile,'r')
clouds = p.variables["objets"][:,:,:]

cmap = cm.get_cmap('nipy_spectral') # colormap to select consecutive colors in loop 

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111, projection='3d')
nobjs = len(np.unique(clouds[clouds>0]))
print nobjs," clouds in the field"

for k in np.unique(clouds[clouds>0]):
    print k

    # get surface from 3D volume object
    verts, faces = measure.marching_cubes_classic(clouds==k,0,spacing=(1,1,1)) # 1 only for pixels in the kth cloud

    # Fancy indexing: `verts[faces]` to generate a collection of triangles
    mesh = Poly3DCollection(verts[faces],color="white",edgecolor=cmap(float(k)/nobjs),linewidths=0.1,alpha=1)

    #mesh.set_edgecolor('k')
    ax.add_collection3d(mesh)

#   ax.set_xlim(0, 512)
#   ax.set_ylim(0, 512)
#   ax.set_zlim(0, 160)


ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
#

plt.show()
