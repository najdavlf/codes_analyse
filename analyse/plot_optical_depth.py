
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import colors as clr
import numpy as np
import netCDF4 as nc

ncfl='/home/villefranquen/Work/NCDF/ARMCU/ARMCU.008.nc'
ncfl='/home/villefranquen/Work/NCDF/ARMCU/TIMESTEP/ARM1630.nc'
dset = nc.Dataset(ncfl, 'r')
var = dset.variables['RCT']
rcz = var[0,:,:,:]
print rcz.shape

rc  = np.sum(rcz,axis=0)
print rc.shape

rho_w = 1000.
r_eff = 10.0e-6
ke  = 3./2.*rc/(rho_w*r_eff)
od  = ke*25. # meters (vertical resolution)
print np.max(od)
x=np.linspace(0,6.4,257)
y=np.linspace(0,6.4,257)
r=range(0,256,40)
print r
print x[r]
f=plt.figure()
ax=f.add_subplot(111)
plt.imshow(od,cmap=mpl.cm.Blues,norm=clr.LogNorm(vmin=1,vmax=10),origin='lower')
plt.plot([0,128], [4/0.025]*2,'k--',linewidth=3)
plt.xlim(0,256)
plt.ylim(0,256)
ax.set_aspect('equal','box')
plt.xticks(r,x[r])
plt.yticks(r,y[r])
cbar=plt.colorbar()
cbar.ax.tick_params(labelsize=18)
plt.tight_layout()
fig='optical_depth_ARMCu008.png'
fig='optical_depth_ARM1630.png'
plt.savefig(fig)
plt.show()
