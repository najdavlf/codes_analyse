#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm
import time, datetime
import six
from matplotlib import colors
import matplotlib.patches as mpatches
import atpy

dfile = sys.argv[1]
fidir = sys.argv[2]

surfdom = 6.4*6.4 # en km

print "Lecture donnees en cours ..."

tdata = atpy.Table(dfile, type='ascii') 

simus = np.unique(tdata.ID)
nb_simus = len(simus)


nb_nuage = []
nb_times = []
min_time = []
max_time = []

for s in simus :
    min_time.append(min(tdata.where(tdata.ID == s).nH))
    max_time.append(max(tdata.where(tdata.ID == s).nH))

min_nh = max(15, min(min_time))
max_nh = min(max_time)

# Restriction de la donnee en enlevant les premiers pas de temps 
tdata = tdata.where((tdata.nH > min_nh) & (tdata.nH <= max_nh))

for s in simus :
    nb_nuage.append(len(tdata.where(tdata.ID == s)))
    nb_times.append(len(np.unique(tdata.where(tdata.ID == s).nH)))

nh = np.unique(tdata.nH)
nb_nh = len(nh)
tot_nb_nuage = sum(nb_nuage)

vec = np.sqrt(tdata.HAUTEUR_max / tdata.ETENDUE_max)
tdata.add_column("Aspect_Ratio_Hmax_Emax", vec)
vec = (tdata.TOP_max - tdata.BASE_min)/np.sqrt(tdata.ETENDUE_proj)
tdata.add_column("Aspect_Ratio_TopBase_Eproj", vec)
vec = (tdata.xmax - tdata.xmin)*(tdata.ymax-tdata.ymin)
tdata.add_column("Eproj_minmax", vec)
vec = tdata.ETENDUE_max / tdata.Eproj_minmax
tdata.add_column("Emax_Eproj", vec)
vec = np.sqrt(tdata.Eproj_minmax)
tdata.add_column("Longueur_eq_minmax",vec)
vec = np.sqrt(tdata.ETENDUE_proj)
tdata.add_column("Longueur_eq",vec)
vec = tdata.ETENDUE_proj/(surfdom)
tdata.add_column("Cloud_Fraction",vec)

heads = np.array(tdata.columns)

# Reperer la premiere colonne "variable"
i, = np.where(heads=="VOLUME")
variables = heads[i[0]:]

print "Lecture donnee terminee !"

print "Analyse evolutions en cours ..."
nb_nuages = np.zeros((nb_nh,nb_simus))
tabmoy = np.zeros((nb_nh,nb_simus,len(variables)))
tabmax = np.zeros((nb_nh,nb_simus,len(variables)))
tabmin = np.zeros((nb_nh,nb_simus,len(variables)))
tabstd = np.zeros((nb_nh,nb_simus,len(variables)))
tabsum = np.zeros((nb_nh,nb_simus,len(variables)))

# Evolution du nombre de nuages
for (i,n) in enumerate(nh) :
    for (j,s) in enumerate(simus) :
        tmp = tdata.where((tdata.ID == s) & (tdata.nH == n))
        if (tmp) :
          nb_nuages[i,j] = max(tmp.N)
          for (k,v) in enumerate(variables) :
              tabmoy[i,j,k] = np.sum(tmp[v])/nb_nuages[i,j]
              tabmax[i,j,k] = np.amax(tmp[v])
              tabmin[i,j,k] = np.amin(tmp[v])
              tabstd[i,j,k] = np.std(tmp[v])
              tabsum[i,j,k] = np.sum(tmp[v])
        else :
          nb_nuages[i,j] = 0
print "Analyse evolutions terminee !"


def main() :
  # print "Plot distributions en cours ..."
  # for (k,v) in enumerate(variables) :
  #   plot_histo(tdata,v)
  """
  print "Plot densite taille en cours ..."
  vec = tdata.Longueur_eq*1000
# vec = tdata.Longueur_eq/tdata.BASE_min
# vec = tdata.Longueur_eq 
  base = tdata.BASE_min
  ind  = tdata.nH
  indh = np.unique(ind)
  vecarm = vec[tdata.ID=="ARM"]
  indarm = ind[tdata.ID=="ARM"]
  basarm = base[tdata.ID=="ARM"]
  vecbmx = vec[tdata.ID=="BMX"]
  indbmx = ind[tdata.ID=="BMX"]
  basbmx = base[tdata.ID=="BMX"]
  vecric = vec[tdata.ID=="RIC"]
  indric = ind[tdata.ID=="RIC"]
  basric = base[tdata.ID=="RIC"]
  vecscm = vec[tdata.ID=="SCM"]
  indscm = ind[tdata.ID=="SCM"]
  basscm = base[tdata.ID=="SCM"]
  for h in indh :
      if (len(basarm[indarm==h])) : basarm[indarm==h] = np.mean(basarm[indarm==h])
      if (len(basbmx[indbmx==h])) : basbmx[indbmx==h] = np.mean(basbmx[indbmx==h])
      if (len(basric[indric==h])) : basric[indric==h] = np.mean(basric[indric==h])
      if (len(basscm[indscm==h])) : basscm[indscm==h] = np.mean(basscm[indscm==h])

# vec = vec[vec>80]
# vecarm = vecarm[vecarm>80]
# vecbmx = vecbmx[vecbmx>80]
# vecric = vecric[vecric>80]
# vecscm = vecscm[vecscm>80]
  nbins=15
  tovec = vec
  hist,b = np.histogram(vec, bins=nbins)
  nstar = b[:-1]*hist*np.log(10)
  N = len(vec)
  plt.figure()
# hist,q = np.histogram(vecarm/basarm, bins=nbins)
  hist,q = np.histogram(vecarm, bins=nbins)
  nstar = 1.*hist
  N = len(vecarm)
  plt.plot(q[:-1],nstar/N,"g--")
# hist,q = np.histogram(vecbmx/basbmx, bins=nbins)
  hist,q = np.histogram(vecbmx, bins=nbins)
  nstar = 1.*hist
  N = len(vecbmx)
  plt.plot(q[:-1],nstar/N,"b--")
# hist,q = np.histogram(vecric/basric, bins=nbins)
  hist,q = np.histogram(vecric, bins=nbins)
  nstar = 1.*hist
  N = len(vecric)
  plt.plot(q[:-1],nstar/N,"b-")
# hist,q = np.histogram(vecscm/basscm, bins=nbins)
  hist,q = np.histogram(vecscm, bins=nbins)
  nstar = 1.*hist
  N = len(vecscm)
  plt.plot(q[:-1],nstar/N,"g-")
# plt.plot([100,2000],[1.121-.70*2,1.121-.7*np.log10(2000)],"k")
  plt.gca().set_xscale("log")
  plt.gca().set_yscale("log")
  plt.xlim(30,5000)
  plt.legend(["ARM","BMX","RIC","SCM"])#,"Fit Neggers et al. 2003"])
# plt.xlabel("Longueur equivalente / Hauteur de base de nuage")
# plt.xlabel("Longueur equivalente / Hauteur de couche limite")
  plt.xlabel("Longueur equivalente (m)")
  plt.ylabel("N(l)/N")
# plt.title("Densite de taille de nuages \nrapportee a la hauteur de base de nuage")
# plt.title("Densite de taille de nuages \nrapportee a la hauteur de couche limite")
  plt.title("Densite de taille de nuages")
# plt.savefig(fidir+"/densite_taille_BASE.pdf")
# plt.savefig(fidir+"/densite_taille_ZCB.pdf")
  plt.savefig(fidir+"/densite_taille.pdf")
  plt.close()
  print "Plot densite taille termine !"
  print "Plot evolutions en cours ..."
  plot_evolution(nb_nuages,"nombre_nuages")
  for (k,v) in enumerate(variables) :
       plot_evolution(tabsum[:,:,k],"Total_"+v)
       plot_evolution(tabmoy[:,:,k],v+"_moy")
       plot_evolution(tabmax[:,:,k],v+"_max")
       plot_evolution(tabmin[:,:,k],v+"_min")
       plot_evolution(tabstd[:,:,k],v+"_std")
  print "Plot evolutions termine !"
  """
  print "Plot densite aspect ratio en cours ..."
  e = tdata.Longueur_eq
  h = tdata.TOP_max - tdata.BASE_min
  emin = min(e)
  emax = max(e)
  hmin = min(h)
  hmax = max(h)
  ne = 50
  nh = 50
  evec = np.linspace(0.,max(emax,hmax),num=ne)
  hvec = np.linspace(0.,max(emax,hmax),num=nh)
  maparm = np.zeros( (ne,nh) )
  mapbmx = np.zeros( (ne,nh) )
  mapric = np.zeros( (ne,nh) )
  mapscm = np.zeros( (ne,nh) )
  earm = e[tdata.ID=="ARM"]
  harm = h[tdata.ID=="ARM"]
  ebmx = e[tdata.ID=="BMX"]
  eric = e[tdata.ID=="RIC"]
  escm = e[tdata.ID=="SCM"]
  hbmx = h[tdata.ID=="BMX"]
  hric = h[tdata.ID=="RIC"]
  hscm = h[tdata.ID=="SCM"]
  for (i,ei) in enumerate(evec[:-1]) :
    ei_suiv = evec[i+1]
    for (j, hj) in enumerate(hvec[:-1]) :
      hj_suiv = hvec[j+1]
      maparm[j,i] = float(len(earm[(earm>=ei) & (earm<ei_suiv) & (harm>=hj) & (harm<hj_suiv)]))/len(earm)
      mapbmx[j,i] = float(len(ebmx[(ebmx>=ei) & (ebmx<ei_suiv) & (hbmx>=hj) & (hbmx<hj_suiv)]))/len(ebmx)
      mapric[j,i] = float(len(eric[(eric>=ei) & (eric<ei_suiv) & (hric>=hj) & (hric<hj_suiv)]))/len(eric)
      mapscm[j,i] = float(len(escm[(escm>=ei) & (escm<ei_suiv) & (hscm>=hj) & (hscm<hj_suiv)]))/len(escm)
  print "figures !"
  plt.figure()
  im=plt.imshow(maparm,origin="lower",cmap="Reds",norm=LogNorm(vmin=0.00005,vmax=1.))
  plt.xlabel("Longueur equivalente (km)")
  elab = ["%.2g" %(i) for i in evec[0:len(evec)-1:5]]
  hlab = ["%.2g" %(i) for i in hvec[0:len(hvec)-1:5]]
  plt.xticks(range(0,len(evec),5),elab)
  plt.yticks(range(0,len(hvec),5),hlab)
  plt.ylabel("Distance base sommet (km)")
  plt.title("Repartition en densite des rapports d'aspects des nuages de ARM") 
  plt.colorbar(im)
  plt.show()
  plt.figure()
  im=plt.imshow(mapbmx,origin="lower",cmap="Reds",norm=LogNorm(vmin=0.00005,vmax=1.))
  plt.xlabel("Longueur equivalente (km)")
  plt.ylabel("Distance base sommet (km)")
  plt.title("Repartition en densite des rapports d'aspects des nuages de BOMEX")
  plt.colorbar(im)
  plt.xticks(range(0,len(evec),5),elab)
  plt.yticks(range(0,len(hvec),5),hlab)
  plt.show()
  plt.figure()
  im=plt.imshow(mapric,origin="lower",cmap="Reds",norm=LogNorm(vmin=0.00005,vmax=1.))
  plt.xlabel("Longueur equivalente (km)")
  plt.ylabel("Distance base sommet (km)")
  plt.title("Repartition en densite des rapports d'aspects des nuages de RICO")
  plt.colorbar(im)
  plt.xticks(range(0,len(evec),5),elab)
  plt.yticks(range(0,len(hvec),5),hlab)
  plt.show()
  plt.figure()
  im=plt.imshow(mapscm,origin="lower",cmap="Reds",norm=LogNorm(vmin=0.00001,vmax=1.))
  plt.xlabel("Longueur equivalente (km)")
  plt.ylabel("Distance base sommet (km)")
  plt.title("Repartition en densite des rapports d'aspects des nuages de SCMS")
  plt.colorbar(im)
  plt.xticks(range(0,len(evec),5),elab)
  plt.yticks(range(0,len(hvec),5),hlab)
  plt.show()
  print "Plot densite aspect ratio termine !"

###########################
# function plot evolution #
###########################

def plot_evolution(var,name) :
  yyy = name.split("_")
  if (len(yyy) >= 1) :
    ylabl = yyy[0]+" "
  if (len(yyy) >= 2) :
    ylabl += yyy[1]+" "
  if (len(yyy) >= 3) :
    ylabl += yyy[2]+" "
  color = ["green","blue","blue","green"]
  lstyl = ["--","--","-","-"]
  leg = []
  fig = plt.figure()
  for (i,s) in enumerate(simus) :
    plt.plot(range(min_nh,max_nh),var[:,i],color=color[i],linestyle=lstyl[i])
    leg.append(s+" - "+str(nb_nuage[i])+" clouds")
  plt.legend(leg,loc="best")
  plt.xticks(range(0,161,12), ("0","1h","2h","3h","4h","5h","6h","7h","8h","9h","10h","11h","12h","13h"))
  plt.xlabel("Temps de simulation")
  plt.ylabel(ylabl)
  plt.title("Evolution de "+ylabl)
  plt.savefig(fidir+"/Evolution_"+name+".pdf")
 #plt.close(fig)

  leg = []
  fig = plt.figure()
  moy_var = np.copy(var)
  for (i,n) in enumerate(nh) :
      if (i>5 & i<max(nh)-5) :
          moy_var[i,:] = (np.sum(var[i-6:i+6,:],axis=0)+2*var[i,:])/14
  
  for (i,s) in enumerate(simus) :
    plt.plot(range(min_nh,max_nh),moy_var[:,i],color=color[i],linestyle=lstyl[i])
    leg.append(s+" - "+str(nb_nuage[i])+" clouds")
  plt.legend(leg,loc="best")
  plt.xlim([0,max_nh+6])
  plt.xticks(range(0,max_nh,12), ("0","1h","2h","3h","4h","5h","6h","7h","8h","9h","10h","11h","12h","13h","14h","15h","16h","17h","18h"))
  plt.xlabel("Temps de simulation")
  plt.ylabel(ylabl)
  plt.title("Evolution de "+ylabl)
  plt.savefig(fidir+"/Evolution_"+name+"_lisse.pdf")
  plt.close(fig)


def plot_histo(tdata,name) :
  nbins = 100
  yyy = name.split("_")
  if (len(yyy) >= 1) :
    ylabl = yyy[0]+" "
  if (len(yyy) >= 2) :
    ylabl += yyy[1]+" "
  if (len(yyy) >= 3) :
    ylabl += yyy[2]+" "
  color = ["green","blue","cyan","lime"]
  markr = ["o","*","+","x"]
  leg = []
  var = tdata[name]
  bbins = np.linspace(var.min(),var.max(),nbins)
  hist_tot,b,p = plt.hist(var, bins=nbins,histtype='step',normed=True)
  hist_arm,a,p = plt.hist(tdata.where(tdata.ID == "ARM")[name],bins=b,histtype='step',normed=True)
  hist_scm,a,p = plt.hist(tdata.where(tdata.ID == "SCM")[name],bins=b,histtype='step',normed=True)
  hist_bmx,a,p = plt.hist(tdata.where(tdata.ID == "BMX")[name],bins=b,histtype='step',normed=True)
  hist_ric,a,p = plt.hist(tdata.where(tdata.ID == "RIC")[name],bins=b,histtype='step',normed=True)
  plt.close()
  plt.figure()
# plt.plot(hist_tot)
  plt.plot(b[:-1],hist_arm)
  plt.plot(b[:-1],hist_scm)
  plt.plot(b[:-1],hist_bmx)
  plt.plot(b[:-1],hist_ric) 
  plt.xlabel(ylabl)
  plt.ylabel("Distribution de densite")
  plt.legend(["ARM","SCM","BMX","RIC"],loc="best")
  plt.title("Density distribution of "+ylabl)
  plt.savefig(fidir+"/Distribution_"+name+".pdf")
  plt.close()

main()
