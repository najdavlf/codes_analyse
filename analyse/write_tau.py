#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib
from mycolors import *

path="/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/"
case="ARM_LES/"
dire="WRONG_FORC/"
simu="CERK4.1.ARMWF.16h30.nc"

file=path+case+dire+simu

dat = ncdf.Dataset(file, "r")

refrc = dat.variables["RCT"][0,:,:,:]
moyrc = dat.variables["cloud_mean_rct"][0,:,:,:]

dat.close()

reff = 10.e-6
rhow = 1.e3
rhoa = 1.2

dy=6.4/256 # [km]
dx=dy      # [km]
dz=25 # [m]

yval=4.
xmin=0.
xmax=3.2

yind=int(yval/dy)-1
ximi=int(xmin/dx)
xima=int(xmax/dx)

reflwp = np.sum(refrc[:,yind,ximi:xima], axis=0)*dz
moylwp = np.sum(moyrc[:,yind,ximi:xima], axis=0)*dz

reftau = reflwp*3./2./(reff*rhow)*rhoa
moytau = moylwp*3./2./(reff*rhow)*rhoa

reftwr = ""
for t in reftau :
  thist = "%g " %(t)
  reftwr = reftwr+thist

moytwr = ""
for t in moytau :
  thist = "%g " %(t)
  moytwr = moytwr+thist


ref = open("reftau.txt","w")
ref.write(reftwr)
ref.close()

moy = open("moytau.txt","w")
moy.write(moytwr)
moy.close()
