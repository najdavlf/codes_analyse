#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm
import time, datetime
from mycolors import *
import matplotlib as mpl
mpl.rcParams.update({'font.size': 16})

path="/home/villefranquen/Work/NCDF/ARMCU/"
simu="CERK4/CERK4.1.ARMCu.007.obj_carac_4D.nc"

data = ncdf.Dataset(path+simu, "r")
RCT  = data.variables["RCT"][:,0,:,0]
data.close()
print RCT.shape

LWP  = np.log10(np.sum(RCT,axis=0)*25.*1000.)

print LWP

kern = stats.gaussian_kde(LWP)
vmax = int(max(LWP))+2
vmin = int(min(LWP))-2
print vmin,vmax
vvec = np.linspace(vmin,vmax,70)
lvec = np.linspace(vmin,vmax,7)

plt.figure()
plt.plot(vvec, kern(vvec), color=basic2[1],linewidth=3)
labs = [r'10$^{%i}$' %(g) for g in lvec]
plt.xticks(lvec,labs)
plt.savefig("distrib_LWP.pdf")
plt.show()
