#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors

if (len(sys.argv) < 3) :
  sys.exit('Usage : python plot_scatter_clouds.py OBJ FIGURES/')
else :    
  filename = sys.argv[1] # nom fichier à traiter
  figuresd = sys.argv[2] # dossier où garder les figures

nbin=500

ncfile = ncdf.Dataset(filename,"r")
varplt = ncfile.variables["DTHRAD"][:,:,:,:]
varplt = varplt*60.*60.*24.
#varplt = ncfile.variables["RCT"][:,:,:,:]
#varplt = varplt*1000.

nt,nz,ny,nx = varplt.shape
vecalt = np.linspace(0.,4.,nz+1)
vecal2 = np.linspace(0.0125,3.9875,nz)
pdfmap = np.empty((nz,nbin))
vecmap = np.empty((nz+1,nbin+1))
meanhr = np.empty(nz)
numfig = 250
fig=plt.figure(figsize=(25,15))
for t in range(nt) :
    vmin = varplt[:,:,:,:].min()
    vmax = varplt[:,:,:,:].max()
    vecbin = np.linspace(vmin,vmax,nbin+1)
    vecmap[-1,:]=0*vecbin
    for z in range(nz) :
        hist,b = np.histogram(varplt[t,z,:,:].reshape(nx*ny),bins=nbin,range=(vmin,vmax))
        pdfmap[z,:]=hist
        vecmap[z,:]=b
        meanhr[z]  =np.mean(varplt[t,z,:,:,].reshape(nx*ny))
    
    xbin,yalt = np.meshgrid(vecbin,vecalt)
    numfig=numfig+1
    if (numfig==260):
        ax=fig.add_subplot(2,5,10)
    else :
        ax=fig.add_subplot(numfig)
    pc=ax.pcolor(vecmap,yalt,np.ma.masked_where(pdfmap<1,np.log10(pdfmap,where=pdfmap>0)),vmax=np.log10(pdfmap.max()),cmap="rainbow")#int(np.log10(pdfmap.max()))+1,cmap="rainbow")
    ax.plot(meanhr,vecal2,"k--")
#   ax.set_xlim(vmin,vmax)
    ax.set_xlim(-500.,100)
    ax.set_title("time "+str(30*(t+1))+"sec")
    if (numfig==253):
      plt.title(u'BOMEX heating rates occurency (2.62x10$^{5}$ cells)\n\ntime'+str(30*(t+1))+'sec')
    if (numfig>255):
      ax.set_xlabel("Heating rate (K/day)")
#     ax.set_xlabel("Cloud Water mixing ratio (g/kg)")
    if (numfig==251 or numfig==256):
      ax.set_ylabel("Height (km)")
    if (numfig==255 or numfig==260):
      cb=plt.colorbar(pc)
      ticks=range(1,int(np.log10(pdfmap.max()))+1)
      cb.set_ticks(ticks)
      labels=[(r"10$^{%s}$" %(str(i))) for i in ticks]
      cb.set_ticklabels(labels)
      cb.set_label("Count")
#   plt.savefig(figuresd+"bomex_rc_panel.pdf")
    plt.savefig(figuresd+"/bomex_heatrates_panel.pdf")
plt.show()
