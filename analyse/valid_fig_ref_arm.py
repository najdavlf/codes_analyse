#!/usr/bin/python
# -*- coding: latin-1 -*-

# 03 / 2017 #
# N. Villefranque

# Extraire les champs moyens de la simulation LES ARM
# Tracer les figures du papier de référence Brown et al. 2002
# pour validation de la simu contre référence

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime

g = 9.81

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

def smooth(var, delta):
    nx = len(var)
    newvar = np.array(var)
    newvar[delta/2:-delta/2] = [np.mean(var[i-delta/2:i+delta/2]) for i in range(delta/2,nx-delta/2)]
    newvar[:delta/2]  = [np.mean(var[:delta/2+i]) for i in range(delta/2)]
    newvar[-delta/2:] = [np.mean(var[i-delta/2:]) for i in range(delta/2)]
    return newvar

def profile(ax, var, time, zmax, col, typ):
    ax.plot(var[time,:(int)(zmax/25)], np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0

def meanprofile(ax, var,t0,t1,zmax, col, typ):
    ax.plot(np.mean(var[t0:t1, :(int)(zmax/25)],axis=0),np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0

def setratio(ax, ratio) :
    ratio_default=(ax.get_xlim()[1]-ax.get_xlim()[0])/(ax.get_ylim()[1]-ax.get_ylim()[0])
    ax.set_aspect(ratio*ratio_default)
    return 0

def gradz(var) :
    nz = len(var[0,:])
    gradz = 0.*var
    gradz[:,0 ] =   (-var[:, 0]+var[:, 1])/25.
    gradz[:,nz-1] = ( var[:,nz-1]-var[:,nz-2])/25.
    gradz[:,1:-1] = ( var[:,2:nz]-var[:,0:nz-2])/50.
    return gradz

def entrainement(var1, var4) :
    grad = gradz(var4)
    return -grad / (var4 - var1)

def detrainement(eps,mfx) :
    grad = gradz(mfx)
    return eps - grad/mfx


ncfile = ncdf.Dataset("CERK4.1.ARM.000.ALL.nc", "r")
t = var1d(ncfile,"time")


heures=[]
for tt in t:                                              
    date = datetime.timedelta(seconds=int(tt))             
    day = date.days
    ss = tt - day*24.*3600. - 60                               
    heures.append(str(datetime.timedelta(seconds=int(ss)))) # format H:M:S

th1 = var2d(ncfile, "MEAN_THp1")
thv1 = var2d(ncfile, "MEAN_THVp1")
thv2 = var2d(ncfile, "MEAN_THVp2")
thv4 = var2d(ncfile, "MEAN_THVp4")
thl1 = var2d(ncfile, "MEAN_THLp1")
thl2 = var2d(ncfile, "MEAN_THLp2")
thl4 = var2d(ncfile, "MEAN_THLp4")
rt1 = var2d(ncfile, "MEAN_RTp1")*1000.
rt2 = var2d(ncfile, "MEAN_RTp2")
rt4 = var2d(ncfile, "MEAN_RTp4")
rc1 = var2d(ncfile, "MEAN_RCp1")*1000.
rc2 = var2d(ncfile, "MEAN_RCp2")
rc4 = var2d(ncfile, "MEAN_RCp4")
rv1 = var2d(ncfile, "MEAN_RVp1")*1000.
u1 = var2d(ncfile, "MEAN_Up1")
v1 = var2d(ncfile, "MEAN_Vp1")
w1 = var2d(ncfile, "MEAN_Wp1") 
w2 = var2d(ncfile, "MEAN_Wp2") 
w4 = var2d(ncfile, "MEAN_Wp4") 
tke1 = var2d(ncfile, "RES_KEp1")
cf = var2d(ncfile, "MEAN_CFp1")
rho1 = var2d(ncfile, "MEAN_RHOp1")
ww1 = var2d(ncfile, "RES_W2p1")
wrc = var2d(ncfile, "RES_WRCp1")
wrt = var2d(ncfile, "RES_WRTp1")
uu1 = var2d(ncfile, "RES_U2p1")
uw = var2d(ncfile, "RES_WUp1")
wthl = var2d(ncfile, "RES_WTHLp1")
wthv = var2d(ncfile, "RES_WTHVp1")
pr1 = var2d(ncfile, "MEAN_PREp1")

pts1 = var2d(ncfile, "AVG_PTSp1")
pts2 = var2d(ncfile, "AVG_PTSp2")
pts4 = var2d(ncfile, "AVG_PTSp4")

e0 = var1d(ncfile, "E0")
q0 = var1d(ncfile, "Q0")
cftot = var1d(ncfile, "ZCFTOT")
cbh = var1d(ncfile, "ZCB")
lwp = var1d(ncfile, "LWP")
inttke = var1d(ncfile, "INT_TKE")

rho0 = rho1[:,0].min()
e0 = e0*2.47*1000000.*rho0
q0 = q0*1015*rho0

clfra = pts2/pts1
cofra = pts4/pts1

wrt = wrt*2.47*1000000.*rho0
wrc = wrc*2.47*1000000.*rho0

cfmax_height = np.array(rc1).argmax(axis=1)*25. # hauteur du max de cloud fraction
cfmask = np.ones(np.array(rc1).shape)*np.array(rc1>0) # 1 si rc > 0 et 0 sinon
indmask = np.where(cfmask==1)
cth = e0*0.
cbh = e0*0.
cth[np.unique(indmask[0])] = [25.*max(indmask[1][indmask[0] == ind])+12.5 for ind in np.unique(indmask[0])]
cbh[np.unique(indmask[0])] = [25.*min(indmask[1][indmask[0] == ind])-12.5 for ind in np.unique(indmask[0])]

base=smooth(cbh,60)[540]
top =smooth(cth,60)[540] 

buoyancy = 0.*wthv
buoyancy[:,:] = [g*wthv[i,:]*rho1[i,:]/thv1[i,0] + g*wrc[i,:] for i in range(len(e0))]

msflx = rho1*cofra*w4

qt = (rt1/1000.) / (1. + (rt1/1000.))
ql = (rc1/1000.) / (1. + (rc1/1000.))
qv = (rv1/1000.) / (1. + (rv1/1000.))
the = th1+ qv*2.5*1000.*((1000./pr1)**0.286)

qt1 = qt*1000.
ql1 = ql*1000.

qt2 = rt2/(1.+rt2)*1000.
qt4 = rt4/(1.+rt4)*1000.

ql2 = rc2/(1.+rc2)*1000.
ql4 = rc4/(1.+rc4)*1000.


# epsilon entrainement ; delta detrainement
epsqt = entrainement(qt1, qt4)
epstl = entrainement(thl1, thl4)
delqt = detrainement(epsqt, msflx)
deltl = detrainement(epstl, msflx)

the = th1+ qv*2.5*1000.*((1000./pr1)**0.286)

def fig3(e0,q0):
  fig = plt.figure()
  ratio = 1 

  ax = fig.add_subplot(111)
  plt.plot(e0[:871],color="k") 
  plt.plot(q0[:871],color="k")
  plt.xticks(range(30,871,120),["12","14","16","18","20","22","00","02"])
  plt.ylim([-100,600])
  ax.annotate("LATENT",xy=(140,300),xytext=(140,300))
  ax.annotate("SENSIBLE", xy=(360,160),xytext=(360,160))
  plt.xlabel("Time (UTC)")
  plt.ylabel(r'Surface Fluxes (Wm$^{-2}$)')
  setratio(ax,ratio)
  plt.savefig("brownetal_fig3.pdf")
  plt.close()



def fig4(th1,rt1):
  ltitle=["1430","1730","2030","2330"]
  fig = plt.figure(figsize=(20,13))
  ratio = 1.8
  for i in [1,2,3,4]:
    ax=fig.add_subplot(2,4,i)
    profile(ax, th1, 0*60, 3000, "k" , "--")
    profile(ax, th1, (i*3)*60, 3000, "k", "-")
    plt.legend(["initial","mean"],loc="best")
    plt.title(ltitle[i-1])
    plt.xlabel(r'$<\theta> (K)$')
    plt.ylabel('z (m)')
    setratio(ax,ratio)

    ax=fig.add_subplot(2,4,i+4)
    profile(ax, rt1, 0*60, 3000, "k" , "--")
    profile(ax, rt1, (i*3)*60, 3000, "k", "-")
    plt.xlim([0,20])
    plt.legend(["initial","mean"],loc="best")
    plt.title(ltitle[i-1])
    plt.xlabel(r'$<r_t> (g/kg)$')
    plt.ylabel('z (m)')
    setratio(ax,ratio)
  plt.savefig("brownetal_fig4.pdf")
  plt.close()


def fig5(cf,cftot,cbh,cth,indmask):
  fig = plt.figure(figsize=(15,15))
  ratio=1.
 
  ####### a) total cloud fraction ######
  ax = fig.add_subplot(221)
  scfmax = smooth(cftot, 60)
  plt.plot(indmask[0], scfmax[indmask[0]], "k")
  plt.xlim(0,900)
  plt.xticks(range(30,871,120),["12","14","16","18","20","22","00","02"])
  plt.ylim([0,0.5])
  plt.xlabel("Time (UTC)")
  plt.ylabel("Total Cloud Fraction")
  setratio(ax,ratio) 

  ####### b) maximum cloud fraction ######
  ax = fig.add_subplot(222)
  scfmax = smooth(np.array(cf[:,:]).max(axis=1) , 60)
  plt.plot(indmask[0], scfmax[indmask[0]], "k")
  plt.xlim(0,900)
  plt.xticks(range(30,871,120),["12","14","16","18","20","22","00","02"])
  plt.ylim([0,0.5])
  plt.xlabel("Time (UTC)")
  plt.ylabel("Maximum Cloud Fraction")
  setratio(ax,ratio) 

  ####### c) cloud base height ######
  ax = fig.add_subplot(223)
  scbh = smooth(cbh[np.unique(indmask[0])], 60)
  plt.plot(np.unique(indmask[0]), scbh, "k")
  plt.xticks(range(30,871,120),["12","14","16","18","20","22","00","02"])
  plt.ylim([0,4000])
  plt.xlabel("Time (UTC)")
  plt.ylabel("Cloud-base height (m)")
  setratio(ax,ratio) 

  ####### d) cloud top height ######
  ax = fig.add_subplot(224)
  scth = smooth(cth[np.unique(indmask[0])] , 60)
  plt.plot(np.unique(indmask[0]), scth,'k')
  plt.xlim([0,900])
  plt.xticks(range(30,871,120),["12","14","16","18","20","22","00","02"])
  plt.ylim([0,4000])
  plt.xlabel("Time (UTC)")
  plt.ylabel("Cloud-top height (m)")
  setratio(ax,ratio) 


  plt.savefig("brownetal_fig5.pdf")
  plt.close()


def fig6(ww,uu,base,top) :
  fig = plt.figure(figsize=(15,15))
  ratio=1.

  ### a) <w'w'> 14h30 ###
  ax = fig.add_subplot(221)
  profile(ax, ww, 3*60, 1500, "k" , "-")
  plt.xlabel(r"<w'w'> (m$^2$s$^{-2}$)")
  plt.ylabel("z (m)")
  plt.ylim([0,1500])
  plt.xlim([0,1])
  plt.title("1430")
  setratio(ax,ratio) 

  ### b) <u'u'> 14h30 ###
  ax = fig.add_subplot(222)
  profile(ax, uu, 3*60, 1500, "k", "-")
  plt.xlabel(r"<u'u'> (m$^2$s$^{-2}$)")
  plt.ylabel("z (m)")
  plt.ylim([0,1500])
  plt.xlim([0,2])
  plt.title("1430")
  setratio(ax,ratio)  

  ### c) <w'w'> 20h30 ###
  ax = fig.add_subplot(223)
  profile(ax, ww, 9*60, 3000, "k", "-")
  ax.fill_between(np.linspace(0,2), base,top, color="gray", alpha=.5)
  plt.xlabel(r"<w'w'> (m$^2$s$^{-2}$)")
  plt.ylabel("z (m)")
  plt.xlim([0,2])
  plt.title("2030")
  setratio(ax,ratio)  

  ### d) <u'u'> 20h30 ###
  ax = fig.add_subplot(224)
  profile(ax, uu, 9*60, 3000, "k", "-")
  ax.fill_between(np.linspace(0,2), base,top, color="gray", alpha=.5)  
  plt.xlabel(r"<u'u'> (m$^2$s$^{-2}$)")
  plt.ylabel("z (m)")
  plt.xlim([0,2])
  plt.title("2030")
  setratio(ax,ratio)  

  plt.savefig("brownetal_fig6.pdf")
  plt.close()


def fig7(buoyancy):
    fig = plt.figure(figsize=(20,8))
    ratio = 1.

    ax = fig.add_subplot(121)
    profile(ax, buoyancy, 9*60, 3000, 'k', '-')
    plt.axvline(x=0, ymin=0, ymax = 3000, color='k', linewidth=.5)
    plt.xlim([-.005,.005])
    plt.ylim([0,3000])
    plt.xlabel(r"<$\rho$w'b'> (kg.m$^{-1}$.s$^{-3}$)")
    plt.ylabel("z (m)")
    plt.title("2030")
    setratio(ax,ratio)

    ax = fig.add_subplot(122)
    sbuo = smooth(buoyancy.min(axis=1), 60)
    plt.plot(range(3*60,750),sbuo[60*3:750],color="k")
    plt.xlim([150,750])
    plt.ylim([-0.0015,0])
    plt.xlabel('Time (UTC)')
    plt.ylabel(r"<$\rho$w'b'>$_{min}$ (kg.m$^{-1}$.s$^{-3}$)")
    plt.xticks(range(150,751,120),["14","16","18","20","22","24"])
    setratio(ax,ratio)

    plt.savefig("brownetal_fig7.pdf")
    plt.close()


def fig8(cofra,msflx,epsqt,delqt,epstl,deltl) :
    fig = plt.figure(figsize=(8,32))
    ratio = 1.

    ax1 = fig.add_subplot(421)
    ax2 = fig.add_subplot(422)
    ax3 = fig.add_subplot(423)
    ax4 = fig.add_subplot(424)
    ax5 = fig.add_subplot(425)
    ax6 = fig.add_subplot(426)
    ax7 = fig.add_subplot(427)
    ax8 = fig.add_subplot(428)

    profile(ax1, cofra, 420, 3000, 'k', '-')
    profile(ax2, cofra, 540, 3000, 'k', '-')
    profile(ax3, msflx, 420, 3000, 'k', '-')
    profile(ax4, msflx, 540, 3000, 'k', '-')
    profile(ax5, epsqt, 420, 3000, 'k', '-')
    profile(ax6, epsqt, 540, 3000, 'k', '-')
    profile(ax7, delqt, 420, 3000, 'k', '-')
    profile(ax8, delqt, 540, 3000, 'k', '-')

    ax1.set_title("1830")
    ax2.set_title("2030")

    ax1.set_xlabel("a(core)")
    ax2.set_xlabel("a(core)")
    ax3.set_xlabel("M(core)")
    ax4.set_xlabel("M(core)")
    ax5.set_xlabel(r"$\epsilon$(core)")
    ax6.set_xlabel(r"$\epsilon$(core)")
    ax7.set_xlabel(r"$\delta$(core)")
    ax8.set_xlabel(r"$\delta$(core)")

    ax1.set_xlim(xmin=0, xmax=0.08)
    ax2.set_xlim(xmin=0, xmax=0.08)
    ax3.set_xlim(xmin=0, xmax=0.10)
    ax4.set_xlim(xmin=0, xmax=0.10)
    ax5.set_xlim(xmin=0, xmax=0.0025)
    ax6.set_xlim(xmin=0, xmax=0.0025)
    ax7.set_xlim(xmin=0, xmax=0.0025)
    
    ax1.set_ylim(ymin=0, ymax=3000)
    ax2.set_ylim(ymin=0, ymax=3000)
    ax3.set_ylim(ymin=0, ymax=3000)
    ax4.set_ylim(ymin=0, ymax=3000)
    ax5.set_ylim(ymin=0, ymax=3000)
    ax6.set_ylim(ymin=0, ymax=3000)
    ax7.set_ylim(ymin=0, ymax=3000)
    ax8.set_ylim(ymin=0, ymax=3000)

    plt.savefig("brownetal_fig8.pdf")




def fig9(thv,the) :
    fig = plt.figure(figsize=(16,48))
    ratio = 1.

    ax = fig.add_subplot(121)
    profile(ax, thv, 300, 4000, 'k', '-')
    plt.xlim([305,330])
    plt.ylim([0,4000])
    plt.xlabel(r"<$\theta_v$> (K)")
    plt.ylabel("z (m)")
    plt.title("1630")
    setratio(ax,ratio)

    ax = fig.add_subplot(122)
    profile(ax, thv, 540, 4000, 'k', '-')
    plt.xlim([305,330])
    plt.ylim([0,4000])
    plt.xlabel(r"<$\theta_v$> (K)")
    plt.ylabel("z (m)")
    plt.title("2030")
    setratio(ax,ratio)

    plt.savefig("brownetal_fig9.pdf")
    plt.close()

fig3(e0,q0)
fig4(th1,rt1)
fig5(cf,cftot,cbh,cth, indmask)
fig6(ww1,uu1,base,top)
fig7(buoyancy)
fig8(cofra,msflx,epsqt,epstl,delqt,deltl) 
fig9(thv1,the) 
