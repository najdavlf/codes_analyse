#!/usr/bin/python
# -*- coding: latin-1 -*-

# 03 / 2017 #
# N. Villefranque

# Extraire les champs moyens de la simulation LES BOMEX
# Tracer les figures du papier de référence Siebesma et al. 2003
# pour validation de la simu contre référence

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime

g = 9.81

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

def smooth(var, delta):
    nx = len(var)
    newvar = np.array(var)
    newvar[delta/2:-delta/2] = [np.mean(var[i-delta/2:i+delta/2]) for i in range(delta/2,nx-delta/2)]
    newvar[:delta/2]  = [np.mean(var[:delta/2+i]) for i in range(delta/2)]
    newvar[-delta/2:] = [np.mean(var[i-delta/2:]) for i in range(delta/2)]
    return newvar

def profile(ax, var, time, zmax, col, typ):
    ax.plot(var[time,:(int)(zmax/25)], np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0

def meanprofile(ax, var,t0,t1,zmax, col, typ):
    ax.plot(np.mean(var[t0:t1, :(int)(zmax/25)],axis=0),np.arange(25./2.,zmax+25./2.,25),col,ls=typ)
    return 0

def meanprofile2(ax, var,t0,t1, zmin,zmax, col, typ):
    ax.plot(np.mean(var[t0:t1, (int)(zmin/25):(int)(zmax/25)],axis=0),np.arange(zmin+25./2.,zmax+25./2.,25),col,ls=typ)
    return 0


def setratio(ax, ratio) :
    ratio_default=(ax.get_xlim()[1]-ax.get_xlim()[0])/(ax.get_ylim()[1]-ax.get_ylim()[0])
    ax.set_aspect(ratio*ratio_default)
    return 0

def gradz(var) :
    nz = len(var[0,:])
    gradz = 0.*var
    gradz[:,0 ] =   (-var[:, 0]+var[:, 1])/25.
    gradz[:,nz-1] = ( var[:,nz-1]-var[:,nz-2])/25.
    gradz[:,1:-1] = ( var[:,2:nz]-var[:,0:nz-2])/50.
    return gradz

def entrainement(var1, var4) :
    grad = gradz(var4)
    return -grad / (var4 - var1)

def detrainement(eps,mfx) :
    grad = gradz(mfx)
    return eps - grad/mfx


ncfile = ncdf.Dataset("VMFTH.1.BMX.000.nc", "r")
t = var1d(ncfile,"time")


heures=[]
for tt in t:                                              
    date = datetime.timedelta(seconds=int(tt))             
    day = date.days
    ss = tt - day*24.*3600. - 60                               
    heures.append(str(datetime.timedelta(seconds=int(ss)))) # format H:M:S

th1 = var2d(ncfile, "MEAN_THp1")
thv1 = var2d(ncfile, "MEAN_THVp1")
thv2 = var2d(ncfile, "MEAN_THVp2")
thv4 = var2d(ncfile, "MEAN_THVp4")
thv6 = var2d(ncfile, "MEAN_THVp6")
thl1 = var2d(ncfile, "MEAN_THLp1")
thl2 = var2d(ncfile, "MEAN_THLp2")
thl4 = var2d(ncfile, "MEAN_THLp4")
thl6 = var2d(ncfile, "MEAN_THLp6")
rt1 = var2d(ncfile, "MEAN_RTp1")*1000.
rt2 = var2d(ncfile, "MEAN_RTp2")
rt4 = var2d(ncfile, "MEAN_RTp4")
rt6 = var2d(ncfile, "MEAN_RTp6")
rc1 = var2d(ncfile, "MEAN_RCp1")*1000.
rc2 = var2d(ncfile, "MEAN_RCp2")
rc4 = var2d(ncfile, "MEAN_RCp4")
rc6 = var2d(ncfile, "MEAN_RCp6")
rv1 = var2d(ncfile, "MEAN_RVp1")*1000.
u1 = var2d(ncfile, "MEAN_Up1")
v1 = var2d(ncfile, "MEAN_Vp1")
w1 = var2d(ncfile, "MEAN_Wp1") 
w2 = var2d(ncfile, "MEAN_Wp2") 
w4 = var2d(ncfile, "MEAN_Wp4") 
w6 = var2d(ncfile, "MEAN_Wp6") 
tke1 = var2d(ncfile, "RES_KEp1")
cf = var2d(ncfile, "MEAN_CFp1")
rho1 = var2d(ncfile, "MEAN_RHOp1")
ww1 = var2d(ncfile, "RES_W2p1")
wrc = var2d(ncfile, "RES_WRCp1")
wrt = var2d(ncfile, "RES_WRTp1")
wrt4 = var2d(ncfile, "RES_WRTp4")
uu1 = var2d(ncfile, "RES_U2p1")
uw = var2d(ncfile, "RES_WUp1")
wthl = var2d(ncfile, "RES_WTHLp1")
wthl4 = var2d(ncfile, "RES_WTHLp4")
wthv = var2d(ncfile, "RES_WTHVp1")
wrt = var2d(ncfile, "RES_WRTp1")
wrc = var2d(ncfile, "RES_WRCp1")
pr1 = var2d(ncfile, "MEAN_PREp1")

pts1 = var2d(ncfile, "AVG_PTSp1")
pts2 = var2d(ncfile, "AVG_PTSp2")
pts4 = var2d(ncfile, "AVG_PTSp4")
pts6 = var2d(ncfile, "AVG_PTSp6")

e0 = var1d(ncfile, "E0")
q0 = var1d(ncfile, "Q0")
cftot = var1d(ncfile, "ZCFTOT")
cbh = var1d(ncfile, "ZCB")
lwp = var1d(ncfile, "LWP")
inttke = var1d(ncfile, "INT_TKE")

rho0 = rho1[:,0].min()
e0 = e0*2.47*1000000.*rho0
q0 = q0*1015*rho0

clfra = pts2/pts1
cofra = pts4/pts1
thfra = pts6/pts1


wrt1 = 1.*wrt
wrc1 = 1.*wrc
wrt = wrt*2.47*1000000.*rho0
wrc = wrc*2.47*1000000.*rho0

cfmax_height = np.array(rc1).argmax(axis=1)*25. # hauteur du max de cloud fraction
cfmask = np.ones(np.array(rc1).shape)*np.array(rc1>0) # 1 si rc > 0 et 0 sinon
indmask = np.where(cfmask==1)
cth = e0*0.
cbh = e0*0.
cth[np.unique(indmask[0])] = [25.*max(indmask[1][indmask[0] == ind])+12.5 for ind in np.unique(indmask[0])]
cbh[np.unique(indmask[0])] = [25.*min(indmask[1][indmask[0] == ind])-12.5 for ind in np.unique(indmask[0])]

base=smooth(cbh,60)[540]
top =smooth(cth,60)[540] 

buoyancy = 0.*wthv
buoyancy[:,:] = [g*wthv[i,:]*rho1[i,:]/thv1[i,0] + g*wrc[i,:] for i in range(len(e0))]

msflx2 = rho1*clfra*w2
msflx4 = rho1*cofra*w4
msflx6 = rho1*thfra*w6

qt = (rt1/1000.) / (1. + (rt1/1000.))
ql = (rc1/1000.) / (1. + (rc1/1000.))
qv = (rv1/1000.) / (1. + (rv1/1000.))
the = th1+ qv*2.5*1000.*((1000./pr1)**0.286)

qt1 = qt*1000.
ql1 = ql*1000.

qt2 = rt2/(1.+rt2)*1000.
qt4 = rt4/(1.+rt4)*1000.
qt6 = rt6/(1.+rt6)*1000.

ql2 = rc2/(1.+rc2)*1000.
ql4 = rc4/(1.+rc4)*1000.
ql6 = rc6/(1.+rc6)*1000.

# epsilon entrainement ; delta detrainement
epsqt = entrainement(qt1, qt4)
epstl = entrainement(thl1, thl4)
delqt = detrainement(epsqt, msflx4)
deltl = detrainement(epstl, msflx4)

# Eddy diffusivity
kqt = - wrt1/ gradz(qt)
ktl = - wthl/ gradz(thl1)

the = th1+ qv*2.5*1000.*((1000./pr1)**0.286)

def fig1(thl1,qt,u1,v1):
    fig = plt.figure(figsize=(9.5,12.5))
    ratio = 3.0

    ax1 = fig.add_subplot(121)
    ax2 = ax1.twiny()
    profile(ax1, thl1, 0, 2500, 'k', '-')
    profile(ax2, qt*1000., 0, 2500, 'k', '-') 
    ax1.set_xlabel(r"$\theta_l$ (K)")
    ax1.set_ylabel("height (m)")
    ax2.set_xlabel(r"q$_t$ (g/kg)")
    ax1.set_xlim(xmin=298,xmax=310)
    ax2.set_xlim(xmin=2,xmax=18)
    ax1.annotate(r"$\theta_l$",xy=(308,2100),xytext=(308,2100))
    ax1.annotate(r"q$_t$", xy=(300,2100),xytext=(300,2100))
    ax1.fill_between(np.linspace(298,310), 500,1500, color="gray", alpha=.5)
    setratio(ax1,ratio)
    setratio(ax2,ratio)

    ax1 = fig.add_subplot(122)
    ax2 = ax1.twiny()
    profile(ax1, u1, 0, 2500, 'k', '-')
    profile(ax2, v1, 0, 2500, 'k', '--')
    ax1.set_xlabel("U (m/s)")
    ax2.set_xlabel("V (m/s)")
    ax1.set_xlim(xmin=-10,xmax=2)
    ax2.set_xlim(xmin=-10,xmax=2)
    ax1.annotate("U",xy=(-8,2100),xytext=(-8,2100))
    ax1.annotate("V",xy=(-1,2100),xytext=(-1,2100))
    setratio(ax1,ratio)
    setratio(ax2,ratio)

    plt.savefig("vmfth_siebesmaetal_fig1.pdf")
    plt.close()


def fig2(cftot, lwp, inttke) :
    fig = plt.figure(figsize=(6,10))
    ratio = .5

    ax = fig.add_subplot(311)
    plt.plot(range(360),cftot[:360], "k")
    plt.xlim([0,361])
    plt.ylim([0,0.2])
    plt.ylabel("total cloud cover")
    setratio(ax,ratio)

    ax = fig.add_subplot(312)
    plt.plot(range(360),lwp[:360]*1000., "k")
    plt.xlim([0,361])
    plt.ylim([0,20])
    plt.ylabel(r'LWP (g.m$^{-2}$)')
    setratio(ax,ratio)

    ax = fig.add_subplot(313)
    plt.plot(range(360),inttke[:360], "k")
    plt.ylim([0,750])
    plt.xlim([0,361])
    plt.ylabel(r"TKE (kg.m$^{-1}$.s$^{-2}$)")
    plt.xlabel("Time (min)")
    setratio(ax,ratio)

    plt.savefig("vmfth_siebesmaetal_fig2.pdf")
    plt.close()


def fig3(th,qv,u,v,ql) :
    fig = plt.figure(figsize=(12,12))
    ratio = 1.

    ax = fig.add_subplot(221)
    profile(ax, th, 0, 2500, 'k', '--')
    meanprofile(ax, th, 300,360, 2500, 'k', '-')
    plt.xlim([298,310])
    plt.ylim([0,2500])
    plt.xlabel(r'$\theta$')
    plt.ylabel("height (m)")
    ax.grid(linestyle=':')
    ax.annotate("a)", xy=(299,2200),xytext=(299,2200))
    setratio(ax,ratio)

    ax = fig.add_subplot(222)
    profile(ax, qv*1000., 0, 2500, 'k', '--')
    meanprofile(ax, qv*1000., 300,360, 2500, 'k', '-')
    plt.xlim([4,18])
    plt.ylim([0,2500])
    plt.xlabel(r'q$_v$ (g/kg)')
    plt.ylabel("height (m)")
    ax.grid(linestyle=':')
    ax.annotate("b)", xy=(5,2200),xytext=(5,2200))
    setratio(ax, ratio)

    ax = fig.add_subplot(223)
    profile(ax, u, 0, 2500, 'k', '--')
    meanprofile(ax, u, 300,360, 2500,'k','-')
    profile(ax, v, 0, 2500, 'k', '--')
    meanprofile(ax, v, 300,360, 2500,'k','-')
    plt.xlabel("u (m/s)       v (m/s)")
    plt.ylabel("height (m)")
    plt.xlim([-10,2])
    plt.ylim([0,2500])
    ax.grid(linestyle=':')
    ax.annotate("c)",xy=(-9,2200),xytext=(-9,2200))
    setratio(ax, ratio)

    ax = fig.add_subplot(224)
    profile(ax, ql*1000., 0, 2500, 'k', '--')
    meanprofile(ax, ql*1000., 300,360, 2500,'k','-')
    plt.xlabel(r"q$_l$ (g/kg)")
    plt.ylabel("height (m)")
    plt.xlim([0,0.01])
    plt.ylim([0,2500])
    ax.grid(linestyle=':')
    ax.annotate("d)", xy=(0.001,2200), xytext=(0.001,2200))
    setratio(ax, ratio)

    plt.savefig("vmfth_siebesmaetal_fig3.pdf")
    plt.close()

def fig4(wrt, wthl, wrc, wthv, uw) :
    fig = plt.figure(figsize=(11,15))
    ratio = 1.

    ax = fig.add_subplot(321)
    meanprofile(ax,wrt,180,360,2500,'k','-')
    plt.xlim([0,175])
    plt.ylim([0,2500])
    plt.xlabel(r"$\overline{w'q_t'}$ (W/m$^2$)")
    plt.ylabel("height (m)")
    ax.annotate("a)",xy=(8,2200))
    ax.grid(linestyle=':')
    setratio(ax,ratio)

    ax = fig.add_subplot(322)
    meanprofile(ax,wthl*1000.,180,360,2500,'k','-')
    plt.xlim([-40,10])
    plt.ylim([0,2500])
    plt.xlabel(r"$\overline{w'\theta_l'}$ (W/m$^2$)")
    plt.ylabel("height (m)")
    ax.grid(linestyle=':')
    ax.annotate("b)",xy=(-37,2200))
    setratio(ax,ratio)

    ax = fig.add_subplot(323)
    meanprofile(ax,wrc,180,360,2500,'k','-')
    plt.xlim([0,40])
    plt.ylim([0,2500])
    plt.xlabel(r"$\overline{w'q_l'}$ (W/m$^2$)")
    plt.ylabel("height (m)")
    ax.grid(linestyle=':')
    ax.annotate("c)",xy=(3,2200))
    setratio(ax,ratio)

    ax = fig.add_subplot(324)
    meanprofile(ax,wthv*1000.,180,360,2500,'k','-')
    plt.xlim([-10,30])
    plt.ylim([0,2500])
    plt.xlabel(r"$\overline{w'\theta_v'}$ (W/m$^2$)")
    plt.ylabel("height (m)")
    ax.annotate("d)",xy=(-7,2200))
    ax.grid(linestyle=':')
    setratio(ax,ratio)

    ax = fig.add_subplot(325)
    meanprofile(ax,uw,180,360,2500,'k','-')
    plt.xlim([-0.05,0.1])
    plt.ylim([0,2500])
    plt.xlabel(r"$\overline{u'w'}$ (m$^2$/s$^2$)")
    plt.ylabel("height (m)")
    ax.annotate("e)",xy=(-0.04,2200))
    ax.grid(linestyle=':')
    setratio(ax,ratio)

    plt.savefig("vmfth_siebesmaetal_fig4.pdf")
    plt.close()

def fig5(tke, ww):
    fig = plt.figure()
    ratio = 1.

    ax = fig.add_subplot(121)
    meanprofile(ax, tke, 180,360, 2500, 'k', '-')
    plt.xlim([0,0.5])
    plt.ylim([0,2500])
    plt.xlabel(r"TKE (m$^2$.s$^{-2}$)")
    plt.ylabel("height (m)")
    ax.annotate("a)", xy=(0.41,2200))
    ax.grid(linestyle=':')
    setratio(ax,ratio)

    ax = fig.add_subplot(122)
    meanprofile(ax, ww, 180,360, 2500, 'k', '-')
    plt.xlim([0,0.3])
    plt.ylim([0,2500])
    plt.xlabel(r"vertical velocity variance (m$^2$.s$^{-2}$)")
    ax.annotate("b)", xy=(0.255,2200))
    ax.grid(linestyle=':')
    setratio(ax,ratio)

    plt.savefig("vmfth_siebesmaetal_fig5.pdf")
    plt.close()

def fig6(cf, cofra, thfra):
    fig = plt.figure()
    ratio = 1.

    ax = fig.add_subplot(111)
    meanprofile(ax, cf, 180,360, 2500, 'k', '--')
    meanprofile(ax, cofra, 180,360, 2500, 'k', '-.')
    meanprofile(ax, thfra, 180,360, 2500, 'k', ':')
    plt.xlim([0,0.26])
    plt.ylim([0,2500])
    plt.xlabel("fraction")
    plt.ylabel("height (m)")
    #ax.annotate("cloud",xy=(0.017,1500))
    #ax.annotate("core", xy=(0.005,750))
    ax.legend(["cloud","core","thermals"], loc="best")
    setratio(ax,ratio)

    plt.savefig("vmfth_siebesmaetal_fig6.pdf")
    plt.close()

def fig7(thl1,thl2,thl4,thl6,qt1,qt2,qt4,qt6,thv1,thv2,thv4,thv6,ql1,ql2,ql4,ql6,w1,w2,w4,w6):
    fig = plt.figure(figsize=(11,15))
    ratio = 1.

    ax = fig.add_subplot(321)
    meanprofile(ax,thl1,180,360,2500,'k','-')
    meanprofile(ax,thl2,180,360,2000,'k','--')
    meanprofile(ax,thl4,180,360,1800,'k','-.')
    meanprofile(ax,thl6,180,360,2000,'k',':')
    plt.xlim([298,308])
    plt.ylim([0,2500])
    plt.xlabel(r"$\theta_l$ (K)")
    plt.ylabel("height (m)")
    ax.annotate("a)",xy=(307,2200))
    plt.legend(["mean","cloud","core", "thermals"],loc="best")
    setratio(ax,ratio)

    ax = fig.add_subplot(322)
    meanprofile(ax,qt1,180,360,2500,'k','-')
    meanprofile(ax,qt2,180,360,2000,'k','--')
    meanprofile(ax,qt4,180,360,1800,'k','-.')
    meanprofile(ax,qt6,180,360,2000,'k',':')
    plt.xlim([4,18])
    plt.ylim([0,2500])
    plt.xlabel(r"$q_t$ (g/kg)")
    ax.annotate("b)",xy=(16,2200))
    plt.legend(["mean","cloud","core","thermals"],loc="best")
    setratio(ax,ratio)

    ax = fig.add_subplot(323)
    meanprofile(ax,thv1,180,360,2500,'k','-')
    meanprofile(ax,thv2,180,360,2000,'k','--')
    meanprofile(ax,thv4,180,360,1800,'k','-.')
    meanprofile(ax,thv6,180,360,2000,'k',':')
    plt.xlim([301,309])
    plt.ylim([0,2500])
    plt.xlabel(r"$\theta_v$ (K)")
    plt.ylabel("height (m)")
    ax.annotate("c)",xy=(308.,2200))
    plt.legend(["mean","cloud","core","thermals"],loc="best")
    setratio(ax,ratio)

    ax = fig.add_subplot(324)
    meanprofile(ax,ql2,180,360,2000,'k','--')
    meanprofile(ax,ql4,180,360,1800,'k','-.')
    meanprofile(ax,ql6,180,360,2000,'k',':')
    plt.xlim([0,3])
    plt.ylim([0,2500])
    plt.xlabel(r"$q_l$ (g/kg)")
    ax.annotate("d)",xy=(2.6,2200))
    plt.legend(["cloud","core","thermals"],loc="lower right")
    setratio(ax,ratio)

    ax = fig.add_subplot(325)
    meanprofile(ax,w2,180,360,2000,'k','--')
    meanprofile(ax,w4,180,360,1800,'k','-.')
    meanprofile(ax,w6,180,360,2000,'k',':')
    plt.xlim([0,4])
    plt.ylim([0,2500])
    plt.xlabel("vertical velocity (m/s)")
    plt.ylabel("height (m)")
    ax.annotate("e)",xy=(3.4,2200))
    plt.legend(["cloud","core","thermals"],loc="lower right")
    setratio(ax,ratio)

    plt.savefig("vmfth_siebesmaetal_fig7.pdf")
    plt.close()

def fig8(msflx4, qt1, qt4, wrt, thl1, thl4, wthl, rho1) :
    fig = plt.figure(figsize=(18,9))
    ratio=1.
    
    varqt = msflx4*(qt4-qt1)/1000./(wrt*rho1)
    varth = msflx4*(thl4-thl1)/(wthl*rho1)

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    meanprofile(ax1,msflx4, 180,360, 2500, 'k', '-')
    meanprofile(ax2,varqt,  180,360, 2500, 'k', '-')
    meanprofile(ax2,varth,  180,360, 2500, 'k', '--')

    ax1.set_ylim(ymin=0,ymax=2500)
    ax2.set_ylim(ymin=0,ymax=2500)
    ax1.set_xlim(xmin=0,xmax=0.03)
    ax2.set_xlim(xmin=.4,xmax=1.)
    ax1.set_xlabel("Mass flux (m/s)")
    ax2.set_xlabel(r"M($\Phi_{co} - \Phi$)/$\overline{w'\Phi'}$")
    ax2.legend([r"$\Phi=q_t$", r"$\Phi=\theta_l$"], loc="best")
    ax1.annotate("a)",xy=(0.002,2300))
    ax2.annotate("b)",xy=(0.44,2300))
    
    ax1.set_ylabel("height (m)")
    ax2.set_ylabel("height (m)")

    setratio(ax1,ratio)
    setratio(ax2,ratio)

    plt.savefig("vmfth_siebesmaetal_fig8.pdf")
    plt.close()

def fig9(epsqt, epstl, delqt, delql):
    fig = plt.figure(figsize=(18,9))
    ratio = 1.

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    meanprofile(ax1,epsqt, 180,360, 2500, 'k', '-')
    meanprofile(ax1,epstl, 180,360, 2500, 'k', '--')
    meanprofile(ax2,delqt, 180,360, 2500, 'k', '-')
    meanprofile(ax2,deltl, 180,360, 2500, 'k', '--')

    ax1.set_ylim(ymin=0,ymax=2500)
    ax2.set_ylim(ymin=0,ymax=2500)
    ax1.set_xlim(xmin=0,xmax=0.006)
    ax2.set_xlim(xmin=0,xmax=0.006)

    ax1.legend([r"$\epsilon_{q_t}$", r"$\epsilon_{\theta_l}$"], loc="best")
    ax2.legend([r"$\epsilon_{q_t}$", r"$\epsilon_{\theta_l}$"], loc="best")

    ax1.annotate("a)",xy=(0.001,2300))
    ax2.annotate("b)",xy=(0.001,2300))

    ax1.set_xlabel(r"$\epsilon (m^{-1})$")
    ax2.set_xlabel(r"$\delta (m^{-1})$")
    ax1.set_ylabel("height (m)")
    ax2.set_ylabel("height (m)")

    setratio(ax1,ratio)
    setratio(ax2,ratio)

    plt.savefig("vmfth_siebesmaetal_fig9.pdf")
    plt.close()

def fig10(kqt, ktl):
    fig = plt.figure()
    ratio = 1.
    ax = fig.add_subplot(111)
    meanprofile(ax, kqt, 180, 360, 2500, 'k', '-')
    meanprofile2(ax, ktl, 180, 360, 250,2500, 'k', '--')
    ax.legend([r"$K_{q_t}$", r"$K_{\theta_l}$"], loc="best")
    ax.set_xlabel(r"K$_\Phi$ (m$^2$/s)")
    ax.set_ylabel("height (m)")
    ax.set_ylim(ymin=0,ymax=2500)
    ax.set_xlim(xmin=0,xmax=100)

    plt.savefig("vmfth_siebesmaetal_fig11.pdf")
    plt.close()

fig1(thl1,qt,u1,v1)
fig2(cftot,lwp,inttke)
fig3(th1,qv,u1,v1,ql)
fig4(wrt,wthl,wrc,wthv,uw)
fig5(tke1, ww1)
fig6(cf,cofra,thfra)
fig7(thl1,thl2,thl4,thl6,qt1,qt2,qt4,qt6,thv1,thv2,thv4,thv6,ql1,ql2,ql4,ql6,w1,w2,w4,w6)
fig8(msflx4, qt1, qt4, wrt1, thl1, thl4, wthl, rho1)
fig9(epsqt, epstl, delqt, deltl)
fig10(kqt, ktl)
