#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as nc
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib
from mycolors import *

col = {}
col["REF"] = basic9[4]
col["NWD"] = basic9[7]
col["MIC"] = basic10[7]
col["MAC"] = basic10[5]

matplotlib.rcParams.update({'font.size': 18})

f = open("/home/villefranquen/Work/SCART/scart_project/build/GASS_RES","r")

simu=[]
sza=[]
trans=[]
sd_trans=[]
albed=[]
sd_albed=[]

f.readline()
for (i,l) in enumerate(f) :
    vec = l.split("\t")
    simu.append(vec[0])
    sza.append(float(vec[1]))
    trans.append(float(vec[2])*100.)
    sd_trans.append(float(vec[3])*100.)
    albed.append(float(vec[4])*100.)
    sd_albed.append(float(vec[5])*100.)

leg=[]
plt.figure()
for s in np.unique(simu) :
    indpt = (np.where(np.array(simu)==s)[0])
    thissza = [sza[i] for i in indpt]
    thistra = [trans[i] for i in indpt]
    thisstd = [sd_trans[i] for i in indpt]
    plt.plot(thissza,thistra,color=col[s],linewidth=.5)
    plt.fill_between(thissza, np.array(thistra)-3*np.array(thisstd), np.array(thistra)+3*np.array(thisstd), color=col[s], alpha=.2)
    leg.append(s)
plt.legend(leg)
plt.title("Transmission in ARMCu 1830 UTC")
plt.xlabel(r"Solar Zenithal Angle [$^o$]")
plt.ylabel("SW Transmission [%]")
plt.savefig("RES_trans.pdf")

leg=[]
plt.figure()
for s in np.unique(simu) :
    indpt = (np.where(np.array(simu)==s)[0])
    thissza = [sza[i] for i in indpt]
    thisalb = [albed[i] for i in indpt]
    thisstd = [sd_trans[i] for i in indpt]
    plt.plot(thissza,thisalb,color=col[s],linewidth=.5)
    plt.fill_between(thissza, np.array(thisalb)-3*np.array(thisstd), np.array(thisalb)+3*np.array(thisstd), color=col[s], alpha=.2)
    leg.append(s)
plt.legend(leg)
plt.title("Albedo in ARMCu 1830 UTC")
plt.xlabel(r"Solar Zenithal Angle [$^o$]")
plt.ylabel("SW Albedo [%]")
plt.savefig("RES_albed.pdf")
