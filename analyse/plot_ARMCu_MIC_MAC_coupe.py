#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib
from mycolors import *

path="/home/villefranquen/Work/NCDF/ARMCU/"

simuId = np.array(["REF","MIC","MAC"])
simuCo = np.array([basic9[4],basic10[7],basic10[5]])

REF = path+"L51km/L51km.1.ARMCu.007.nc"

REFdat = ncdf.Dataset(REF,"r")

REFrct = REFdat.variables["RCT"][0,:,:,:]
REFmic = REFdat.variables["cloud_mean_rct"][0,:,:,:]
REFmac = REFdat.variables["field_mean_RCT"][0,:,:,:]
print REFmic.shape
maxlon = 150

REFrct2 = np.max(1000.*REFrct[:,35:40,0:maxlon],axis=1)
REFmic2 = np.max(1000.*REFmic[:,35:40,0:maxlon],axis=1)
REFmac2 = np.max(1000.*REFmac[:,35:40,0:maxlon],axis=1)

vmin = min(np.min(REFrct2),np.min(REFmic2),np.min(REFmac2))
vmax = max(np.max(REFrct2),np.max(REFmic2),np.max(REFmac2))
print vmin,vmax


matplotlib.rcParams.update({'font.size': 18})
w, h = plt.figaspect(1./2.)
plt.figure(figsize=(2.*w,h))

plt.subplot(131)
plt.imshow(REFrct2,vmin=vmin,vmax=vmax,interpolation=None, origin="lower")
locs,labs = plt.xticks()
newlabs = ["%g" %(g/10.) for g in locs]
plt.xticks(locs,newlabs)
plt.xlabel("Longitude [km]")
plt.xlim(0,maxlon)
locs,labs = plt.yticks()
newlabs = ["%g" %(g*40./1000.) for g in locs]
plt.yticks(locs,newlabs)
plt.ylabel("Height [km]")
plt.ylim(0,100)
plt.colorbar(shrink=1.5,orientation="horizontal")

plt.subplot(132)
plt.imshow(REFmic2,vmin=vmin,vmax=vmax,interpolation=None, origin="lower")
locs,labs = plt.xticks()
newlabs = ["%g" %(g/10.) for g in locs]
plt.xticks(locs,newlabs)
plt.xlabel("Longitude [km]")
plt.xlim(0,maxlon)
locs,labs = plt.yticks()
newlabs = ["%g" %(g*40./1000.) for g in locs]
plt.yticks(locs,newlabs)
plt.ylabel("Height [km]")
plt.ylim(0,100)
plt.colorbar(shrink=1.5,orientation="horizontal")

plt.subplot(133)
plt.imshow(REFmac2,vmin=vmin,vmax=vmax,interpolation=None, origin="lower")
locs,labs = plt.xticks()
newlabs = ["%g" %(g/10.) for g in locs]
plt.xticks(locs,newlabs)
plt.xlabel("Longitude [km]")
plt.xlim(0,maxlon)
locs,labs = plt.yticks()
newlabs = ["%g" %(g*40./1000.) for g in locs]
plt.yticks(locs,newlabs)
plt.ylabel("Height [km]")
plt.ylim(0,100)
plt.colorbar(shrink=1.5,orientation="horizontal")
plt.savefig("vert_sec_REF_MIC_MAC.pdf")
