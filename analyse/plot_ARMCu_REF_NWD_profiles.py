#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib
from mycolors import *

path="/home/villefranquen/Work/NCDF/ARMCU/"

simuId = np.array(["REF","NWD"])
simuCo = np.array([basic9[4],basic9[7]])

REF = path+"L51km.1.ARMCu.000KCL.nc"
NWD = path+"NWIND.1.ARMCu.000KCL.nc"

REFdat = ncdf.Dataset(REF,"r")
NWDdat = ncdf.Dataset(NWD,"r")

time = REFdat.variables["time"][:]
tmin = (time-time[0]+300.)/60.
theu = tmin/60.

zlev = REFdat.variables["vertical_levels"][:]

REFlwp = REFdat.variables["LWP"][:,0,0]
NWDlwp = NWDdat.variables["LWP"][:,0,0]

hx = 10
ix = np.where(theu==hx)[0][0]
print ix

REFql = REFdat.variables["ql"][ix,:,0,0]
NWDql = NWDdat.variables["ql"][ix,:,0,0]
REFcf = REFdat.variables["rneb"][ix,:,0,0]
NWDcf = NWDdat.variables["rneb"][ix,:,0,0]

h0 = 3.5
h1 = 13
i0 = np.where(theu==h0)[0][0]
i1 = np.where(theu==h1)[0][0]

matplotlib.rcParams.update({'font.size': 18})

w, h = plt.figaspect(1./3.)
plt.figure(figsize=(w,1.1*h))
plt.plot(theu[i0:i1], REFlwp[i0:i1]*1000., color=simuCo[0],linewidth=2.)
plt.plot(theu[i0:i1], NWDlwp[i0:i1]*1000., color=simuCo[1],linewidth=2.)
plt.title("LWP evolution for two ARMCu simulations")
plt.xlim(h0,h1)
plt.xlabel("Time [UTC]")
locs,labs = plt.xticks()
plt.xticks(locs[1:],('1530','1730','1930','2130','2330'))
plt.ylabel(r"Liquid Water Path [g.m$^{-2}$]")
plt.legend(simuId)
plt.vlines(hx,0.,18.,linestyle="--",linewidth=2.)
plt.savefig("lwp_compare_REF_NWD_21h30.pdf")
plt.close()

w, h = plt.figaspect(1.2)
plt.figure(figsize=(1.3*w,1.3*h))
plt.plot(REFql*1000000., zlev/1000., color=simuCo[0],linewidth=2.)
plt.plot(NWDql*1000000., zlev/1000., color=simuCo[1],linewidth=2.)
plt.legend(simuId)
plt.text(10,0.5,r"q$_l$",fontsize=70)
plt.title("Mean cloud water at 2130 UTC")
plt.xlabel(r"q$_l$ [mg.kg$^{-1}$]")
plt.ylabel("Height [km]")
plt.savefig("ql_compare_REF_NWD_21h30.pdf")
plt.close()

w, h = plt.figaspect(1.2)
plt.figure(figsize=(1.3*w,1.3*h))
plt.plot(REFcf*100., zlev/1000., color=simuCo[0],linewidth=2.)
plt.plot(NWDcf*100., zlev/1000., color=simuCo[1],linewidth=2.)
plt.legend(simuId)
plt.text(4,0.3,r"cf",fontsize=70)
plt.title("Cloud fraction at 2130 UTC")
plt.xlabel("cf [%]")
plt.ylabel("Height [km]")
plt.savefig("cf_compare_REF_NWD_21h30.pdf")
plt.close()
