library("ncdf4") # to manipulate ncdf
library(sm)  	# pour ploter plusieurs densités sur le même graph
library(MASS)

# RAINBOW 15
red = c(0,17,68,119,17,68,153,119,170,221,119,170,221,119,170,221,221,140,66,255)
green = c(0,68,119,170,119,170,204,119,170,221,17,68,119,17,68,119,221,140,66,255)
blue = c(0,119,170,221,85,136,187,19,68,119,17,68,119,68,119,170,221,140,66,255)
# RAINBOW 6
red = c(0,60,73,99,190,230,217,221,140,66,255)
green = c(0,64,140,173,188,139,33,221,140,66,255)
blue = c(0,150,194,153,72,51,32,221,140,66,255)
# RAINBOW 5
red = c(0,64,82,125,227,217,221,140,66,255)
green = c(0,64,157,184,156,33,221,140,66,255)
blue = c(0,150,183,116,55,32,221,140,66,255)

cols=(rgb(red,green,blue, max=255))
simuId = c("CERK4",
           #"CEN2D","WENO3","WENO5",
           #"D100m","DX40m","DZ40m","DZVAR",
           "L10km","L25km","L51km",
           #"MREVE","SCOND","TDELT",
           "NWIND","2WIND")

simuGp = c(0,
           #10, 10, 10, 
           #20, 20, 20, 20,
           30, 30, 30,
           #40, 40, 40,
           50, 50)

sizeDm = c(6.4,
           #6.4,6.4,6.4,
           #6.4,6.4,6.4,6.4,
           10.,25.6,51.2,
           #6.4,6.4,6.4,
           51.2,51.2)

nbSimu = length(simuId)
nbTime = 15

getvar <- function(fname,vname)
{
  nc = nc_open(fname)
  x = ncvar_get(nc,vname)
  var = x[,1]
  return(var)
}


getNN <- function(fname,dim) {
 xbary = getvar(fname,"W_E_barycenter")
 ybary = getvar(fname,"S_N_barycenter")
 nbrobj=length(xbary)
 numobj=1:nbrobj
 colo = rep("black",nbrobj)
 ListVois = rep(0,nbrobj)
 mode(ListVois) = "list"
 
 dist = matrix(data=NA,nrow=nbrobj,ncol=nbrobj)
 NN = 0.*numobj
 for (obj in numobj){
  for (vois in numobj) {
    xdist = abs(xbary[obj]-xbary[vois])
    ydist = abs(ybary[obj]-ybary[vois])
    if (xdist>dim/2.){
      xdist=dim-xdist
    }
    if (ydist>dim/2.){
      ydist=dim-ydist
    }
    dist[obj,vois] = sqrt(xdist*xdist+ydist*ydist)
  }
  NN[obj]=min(dist[obj,dist[obj,]>0.])
  ListVois[obj]=list(which(dist[obj,]<(1/sqrt(nbrobj/(dim*dim)*pi))*gamma(1+1./2.)))
 }
 
 clust = rep(0,nbrobj)
#nclust=0
#for (obj in numobj){
#  if (clust[obj]==0) {
#    nclust=nclust+1
#    thisClust = nclust
#  } else {
#    thisClust=clust[obj]
#  }
#  clust[ListVois[[obj]]]=thisClust
#  for (vois in ListVois[[obj]]) {
#    clust[ListVois[[vois]]]=thisClust
#    for (hisvois in ListVois[[vois]]) {
#      clust[ListVois[[hisvois]]]=thisClust
#    }
#  }
#}   
 
#colo = colors()[clust]
 return(list(NN,colo,clust,xbary,ybary,dist,numobj))
}

#cols=colors()[sample(15:150,nbSimu,rep=F)]
iorg=matrix(0,nrow=nbSimu,ncol=nbTime)
path="/home/villefranquen/Work/NCDF/ARMCU/"

for (time in 04:12) {
  pdf(paste("NNcdf",time,".pdf",sep=""))
  plot(c(0,1),c(0,1),col="black",type="l",main="Cumulative of Nearest Neighbour Distance pdf \n Organization diagnostic as in Tompkins & Semie",xlab = "randCDF(r)",ylab="CDF(r)")
  for (nsimu in 1:nbSimu) {
    ntime = sprintf("%03i" , time)
    fName = paste(path,simuId[nsimu],"/",simuId[nsimu],".1.ARMCu.",ntime,".obj_carac_4D.nc",sep="")
    print(fName)
    res = getNN(fName, sizeDm[nsimu])
    NN = res[[1]]
    rdata = density(NN)$x
    NNcdf = cumsum(density(NN)$y*(rdata[2]-rdata[1]))
    weibu = 1.-exp(-length(res[[7]])/(sizeDm[nsimu]*sizeDm[nsimu])*pi*rdata[rdata>0]*rdata[rdata>0])
    lines(weibu, NNcdf[rdata>0], type="l", col=cols[nsimu])
    iorg[nsimu,time]=cumsum(NNcdf[rdata>0][2:length(weibu)]*abs(weibu[1:length(weibu)-1]-weibu[2:length(weibu)]))[length(weibu)-1]
  }
  legend("topleft",simuId,col=cols,lty=1)
  dev.off()
}

 pdf("EvolIorg.pdf")
plot(c(0,nbTime),c(0.5,0.5),col="black",type="l",main="Evolution of the organization index \n Organization diagnostic as in Tompkins & Semie",xlab = "Time (h)",ylab="Iorg")
for (nsimu in 1:nbSimu) {
  lines(4:12, iorg[nsimu,4:12], col=cols[nsimu], type="l")
}
legend("topleft",simuId,col=cols,lty=1)
 dev.off()
