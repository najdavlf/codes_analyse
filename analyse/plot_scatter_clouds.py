 #!/usr/bin/python
# -*- coding: latin-1 -*-

# 22 / 12 / 2016

"""
Lit le masque objets 
Plot les nuages en scatter plot
Sauve les figures dans figuresd/CLOUD_SCATTER
"""

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
import six
from matplotlib import colors
import matplotlib.patches as mpatches

if (len(sys.argv) < 3) :
  sys.exit('Usage : python plot_scatter_clouds.py OBJ FIGURES/')
else :    
  objfile = sys.argv[1]  # extension du nom de ton fichier avec les objets 'OBJ'
  figuresd = sys.argv[2] # dossier où tu veux garder tes figures

colors_ = list(six.iteritems(colors.cnames))

def extrait_t_x_y_z(filename):
  ncfile = ncdf.Dataset(filename,"r")
  t = ncfile.variables["time"][:]
  z = ncfile.variables['VLEV'][:,0,0]
  y = ncfile.variables['S_N_direction'][:]
  x = ncfile.variables['W_E_direction'][:]
  ncfile.close()
  return [t , z , y , x]

def extrait_obj(filename,indt):
  ncfile = ncdf.Dataset(filename,"r")
  obj = ncfile.variables["objets"][indt,:,:,:]
  ncfile.close()
  return obj 

def plot_clouds(path,id_simu,filename,t,z,y,x):
  for (indt,t) in enumerate(t) :
   if (indt>0):
    id_heure = "%03i" %(indt)
    objets = extrait_obj(filename,indt)
    nb_obj = int(objets.max())
    if (nb_obj>0) :
      fig = plt.figure(figsize=(8,8))
      ax = fig.add_subplot(111, projection='3d')
      X,Y=np.meshgrid(x,y)
      ax.set_zlim3d(0, z[-1])
      ax.set_ylim3d(0, y[-1])
      ax.set_xlim3d(0, x[-1])

      for iz in range(len(z)):
        tmp = np.array(objets[iz,:,:])
        if (tmp.max() > 0) :
          for obj in range(1,nb_obj+1) :
            ncolor = np.mod(obj-1,len(colors_))
#           ax.scatter(X[tmp==obj], Y[tmp==obj], tmp[tmp==obj]/obj*z[iz], c="gray",marker=".",alpha=0.4)
            ax.scatter(X[tmp==obj], Y[tmp==obj], tmp[tmp==obj]/obj*z[iz], c=colors_[ncolor],marker=".",alpha=0.4)
      ax.set_xlabel("X (km)")
      ax.set_ylabel("Y (km)")
      ax.set_zlabel("Z (km)")
      ax.set_title(id_simu+" clouds at timestep "+id_heure)
      
      for angle in range(0,360,30) :
        ax.view_init(30, angle)
        plt.savefig(path+figuresd+'/CLOUD_SCATTER/cloud_'+id_simu+'_'+id_heure+'_'+str(angle)+'.png')

      ax.view_init(0,0)
      plt.savefig(path+figuresd+'/CLOUD_SCATTER/cloud_'+id_simu+'_'+id_heure+'_up1.png')
      ax.view_init(0,90)
      plt.savefig(path+figuresd+'/CLOUD_SCATTER/cloud_'+id_simu+'_'+id_heure+'_up2.png')
      ax.view_init(90, 0)
      plt.savefig(path+figuresd+'/CLOUD_SCATTER/cloud_'+id_simu+'_'+id_heure+'_haut.png')
      plt.close()


def run_plot_scatter_cloud():
  path = "/cnrm/moana/user/villefranquen/CAS_LES/SIMULATIONS/"
  list_path_simu = [
                   "ARM_LES/NWIND.1.",
                   "ARM_LES/CERK4.1.",
                   "SCMS_LES/CER25.1.",
                   "BOMEX_LES/VMFTH.1.",
                   "RICO_LES/THL3D.1."
                   ]
  list_id_simu = [
                   "ARMCU",
                   "ARM",
                   "SCM",
                   "BMX",
                   "RIC"
                 ]
  couleurs = ["black","darkgreen","darkred"]
  for(path_simu, id_simu, col) in zip(list_path_simu,list_id_simu, couleurs):
    print ""
    print "**************"+id_simu+"**************"
    print ""
    filename_rc = path+path_simu+id_simu+".ALL.nc"
    filename_ob = path+path_simu+id_simu+"."+objfile+".nc"
    print "Lecture t"
    [t, z, y, x] = extrait_t_x_y_z(filename_rc)
    nt = len(t) 

    print "Plot scatter clouds"
    plot_clouds(path,id_simu,filename_ob,t,z,y,x)


run_plot_scatter_cloud()
