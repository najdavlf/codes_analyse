
import numpy   as np
import netCDF4 as nc
from matplotlib import pyplot as plt

obspath = '/home/villefranquen/Work/NCDF/CLOUDMASK/'
obsfile = 'cloudmask3D_20170822112138.nc'
obs = nc.Dataset(obspath+obsfile, "r")

vlev_obs = np.linspace(0,2000.,41)
vlev_LES = np.linspace(0,4000.,160)

# vertical levels
plt.figure()
plt.plot(vlev_LES,'b',label='LES')
plt.plot(vlev_obs,'r',label='Cloud3D') 
plt.legend()
plt.savefig('levels_les_obs.png')
plt.close()

# cloud profiles in Cloudnet
var='Cloud_Mask'
mask_obs = obs.variables[var][:].reshape(obs.variables[var].shape)
print mask_obs.shape
nb_heights = mask_obs.shape[0]
pts_in_dom = mask_obs.shape[1]*mask_obs.shape[2]

def cloudfrac_ratio(nbz):
  cloudfrac_s = np.zeros(nb_heights-nbz)
  cloudfrac_v = np.zeros(nb_heights-nbz)
  for jcf in range(nb_heights-nbz):
      this = mask_obs[jcf:jcf+nbz,:,:]
      cloudfrac_v[jcf] = np.sum(this)*1.0/(pts_in_dom*nbz)
      cloudfrac_s[jcf] = len(np.where(np.sum(this,axis=0)>0)[0])*1.0/(pts_in_dom)
  return cloudfrac_v,cloudfrac_s

dz = vlev_obs[2]-vlev_obs[1]
beta=[]
for nbz in range(1,7):
    print 'delta z',dz*nbz
    cfv,cfs = cloudfrac_ratio(nbz)
    ratio = cfv
    ratio[cfs>0]/=cfs[cfs>0]
    ratio[cfs==0]=np.float('nan')
    print 'mean cfv/cfs', np.nanmean(ratio)
    # csurf/cvol=1+beta*dz
    beta.append((1./np.nanmean(ratio) - 1.)/(nbz*dz)) 
print beta
