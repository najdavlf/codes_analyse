#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib
from mycolors import *

path="/home/villefranquen/Work/NCDF/ARMCU/"

simuId = np.array(["REF","NWD"])
simuCo = np.array([basic9[4],basic9[7]])

REF = path+"L51km/L51km.1.ARMCu.007.obj_carac_4D.nc"
REF = path+"CERK4/CERK4.1.ARMCu.007_carac_4D.nc"
NWD = path+"NWIND/NWIND.1.ARMCu.007.obj_carac_4D.nc"
NWD = path+"NWDSD/NWDSD.1.ARMCu.007_carac.nc"

REFdat = ncdf.Dataset(REF,"r")
NWDdat = ncdf.Dataset(NWD,"r")

REFcov = REFdat.variables["cover"][:,0]
NWDcov = NWDdat.variables["cover"][:,0]
REFleq = np.sqrt(REFcov)*1000.
NWDleq = np.sqrt(NWDcov)*1000.

REFmax = max(REFleq)
NWDmax = max(NWDleq)
minleq = min(np.min(REFleq),np.min(NWDleq))
maxleq = max(np.max(REFleq),np.max(NWDleq))
print minleq, maxleq


REFlog = np.log10(REFleq)
NWDlog = np.log10(NWDleq)
minlog = np.log10(minleq)*0.99
maxlog = np.log10(maxleq)*1.01

bins = np.linspace(minleq,maxleq,num=15, endpoint=True)
midp = bins[:-1] + (bins[1:]-bins[:-1])/2.
print zip(bins,midp)
REFnbr = np.zeros(len(midp))
NWDnbr = np.zeros(len(midp))

for (i,(inf,sup)) in enumerate(zip(bins[:-1], bins[1:])) :
    print i
    REFnbr[i] = len(REFleq[(REFleq>inf) & (REFleq <= sup)])
    NWDnbr[i] = len(NWDleq[(NWDleq>inf) & (NWDleq <= sup)])

matplotlib.rcParams.update({'font.size': 18})
print len(REFleq)
print len(NWDleq)
print REFnbr/len(REFleq)
print NWDnbr/len(NWDleq)
plt.figure()
plt.plot(np.log10(midp),np.log10(REFnbr/len(REFleq)),color=simuCo[0],linewidth=2.)
plt.plot(np.log10(midp[NWDnbr>0]),np.log10(NWDnbr[NWDnbr>0]/len(NWDleq)),color=simuCo[1],linewidth=2.)
plt.title("Cloud size distribution at 1830 UTC")
plt.xlabel("Equivalent length [m]")
plt.ylabel("Density of cloud")
plt.xlim(2.,np.log10(4000))
plt.ylim(-3,0)
ticks = [100,200,500,1000,2000,4000]
newlocs = [np.log10(i) for i in ticks]
newlabs = ["%s" %(i) for i in ticks]
plt.xticks(newlocs,newlabs)
ticks = [.1,.01,.001]
newlocs = [0]
newlocs = np.append(newlocs,[np.log10(i) for i in ticks])
newlabs = ["0"]
newlabs = np.append(newlabs,[r"10$^{%01i}$" %(np.log10(i)) for i in ticks])
plt.yticks(newlocs,newlabs)
leg = ["%s : %i clouds" %(sim,nb) for (sim,nb) in zip(simuId, [len(REFleq),len(NWDleq)])]
plt.legend(leg)
plt.savefig("size_dens_compare_small_REF_NWD.pdf")
