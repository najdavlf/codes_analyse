#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib as mpl
from mycolors import *

def ave(field, delta) :
    new = 1.*field 
    for i in range(delta/2,field.shape[0]-delta/2+1) :
        for j in range(delta/2, field.shape[1]-delta/2+1) : 
            new[i,j] = np.mean(field[i-delta/2 : i+delta/2 , j-delta/2 : j+delta/2])
    return new 

path = "/cnrm/tropics/user/villefranquen/scart_project/build/"
path = "./"

REF = path+"REFSD_mie_SZA45_4096.nc"
MIC = path+"MICSD_mie_SZA45_4096.nc"
MAC = path+"MACSD_mie_SZA45_4096.nc"

REFdat = ncdf.Dataset(REF,"r")
MICdat = ncdf.Dataset(MIC,"r")
MACdat = ncdf.Dataset(MAC,"r")

REFdnfo = REFdat.variables["Downward_fluxes"][0,:,:] / 1368.*100.
MICdnfo = MICdat.variables["Downward_fluxes"][0,:,:] / 1368.*100.
MACdnfo = MACdat.variables["Downward_fluxes"][0,:,:] / 1368.*100.
REFsdnf = REFdat.variables["Downward_fluxes_STD"][0,:,:] / 1368.*100. 
MICsdnf = MICdat.variables["Downward_fluxes_STD"][0,:,:] / 1368.*100.
MACsdnf = MACdat.variables["Downward_fluxes_STD"][0,:,:] / 1368.*100.

REFdat.close()
MICdat.close()
MACdat.close()

REFdnf = ave(REFdnfo,2)
MICdnf = ave(MICdnfo,2)
MACdnf = ave(MACdnfo,2)

vmin = min(np.min(REFdnf), np.min(MICdnf), np.min(MACdnf))
vmax = max(np.max(REFdnf), np.max(MICdnf), np.max(MACdnf))

mpl.rcParams.update({'font.size': 18})
w, h = plt.figaspect(1./1.5)
plt.figure(figsize=(2.*w,h))
plt.subplot(131)
plt.imshow(REFdnf, cmap=mpl.cm.coolwarm, interpolation="none",origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.title("REF")
plt.subplot(132)
plt.imshow(MICdnf, cmap=mpl.cm.coolwarm, interpolation="none", origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.title("MIC")
plt.subplot(133)
plt.imshow(MACdnf, cmap=mpl.cm.coolwarm, interpolation="none",origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.title("MAC")
plt.colorbar()
plt.savefig("ARMCu_REF_MIC_MAC_map.pdf")
plt.show()

vmin = min(np.min(REFsdnf), np.min(MICsdnf), np.min(MACsdnf))
vmax = max(np.max(REFsdnf), np.max(MICsdnf), np.max(MACsdnf))

mpl.rcParams.update({'font.size': 18})
w, h = plt.figaspect(1./1.5)
plt.figure(figsize=(2.*w,h))
plt.subplot(131)
plt.imshow(REFsdnf, cmap=mpl.cm.coolwarm, interpolation="none",origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.subplot(132)
plt.imshow(MICsdnf, cmap=mpl.cm.coolwarm, interpolation="none", origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.subplot(133)
plt.imshow(MACsdnf, cmap=mpl.cm.coolwarm, interpolation="none",origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.colorbar()
plt.savefig("ARMCu_REF_MIC_MAC_STD_map.pdf")
plt.show()

DIFFmic = (REFdnfo-MICdnfo)
DIFFmac = (REFdnfo-MACdnfo)
vmin = min(np.min(DIFFmac), np.min(DIFFmac))
vmax =  -vmin # max(np.max(DIFFmic), np.max(DIFFmac))

DIFFmac[DIFFmac > REFsdnf+MACsdnf] = 0.
DIFFmac[DIFFmac < -REFsdnf-MACsdnf] = 0.


"""
plt.figure(figsize=(2.*w,h))
plt.subplot(121)
plt.imshow(DIFFmic, cmap=mpl.cm.coolwarm, origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.colorbar()
plt.subplot(122)
"""
plt.figure()
plt.imshow(DIFFmac, cmap=mpl.cm.coolwarm, origin="lower", vmin=vmin, vmax=vmax)
locs,labs = plt.xticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.xticks(locs,newlabs)
plt.xlim(0,63)
locs,labs = plt.yticks()
newlabs = ["%g" %(l/10.) for l in locs]
plt.yticks(locs,newlabs)
plt.ylim(0,63)
plt.xlabel("x [km]")
plt.ylabel("y [km]")
plt.colorbar()
plt.show()
