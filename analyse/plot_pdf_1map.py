#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors

if (len(sys.argv) < 3) :
  sys.exit('Usage : python plot_scatter_clouds.py OBJ FIGURES/')
else :    
  filename = sys.argv[1] # nom fichier à traiter
  figuresd = sys.argv[2] # dossier où garder les figures

nbin=500

ncfile = ncdf.Dataset(filename,"r")
varplt = ncfile.variables["DTHRAD"][:,:,:,:]
varplt = varplt*60.*60.*24.
#varplt = ncfile.variables["RCT"][:,:,:,:]
#varplt = varplt*1000.

# domain definition
nt,nz,ny,nx = varplt.shape
zmin = 0.
zmax = 4.
zdel = .025
vecalt = np.linspace(zmin,zmax,nz+1) # layer boundaries
vecal2 = np.linspace(zmin+zdel/2.,zmax-zdel/2.,nz) # layer mid height

pdfmap = np.empty((nz,nbin))
vecmap = np.empty((nz+1,nbin+1))
meanhr = np.empty(nz)

# histogram range
vmin = varplt[:,:,:,:].min()
vmax = varplt[:,:,:,:].max()
vecbin = np.linspace(vmin,vmax,nbin+1)
vecmap[-1,:]=0*vecbin

fig=plt.figure()
for z in range(nz) :
    hist,b = np.histogram(varplt[:,z,:,:].reshape(nt*nx*ny),bins=nbin,range=(vmin,vmax))
    pdfmap[z,:]=hist
    vecmap[z,:]=b
    meanhr[z]  =np.mean(varplt[:,z,:,:,].reshape(nt*nx*ny))
    
xbin,yalt = np.meshgrid(vecbin,vecalt)
ax=fig.add_subplot(111)
pc=ax.pcolor(vecmap,yalt,np.ma.masked_where(pdfmap<1,np.log10(pdfmap,where=pdfmap>0)),vmax=np.log10(pdfmap.max()),cmap="rainbow")
# ax.set_xlim(-500.,100)
ax.set_title(u'BOMEX heating rates occurency\n(2.62x10$^{5}$ cells x 10 timesteps)')
# ax.set_title(u'ARMCu liquid water mixing ratio occurency\n(2.62x10$^{5}$ cells x 10 timesteps)')
ax.set_xlabel("Heating rate (K/day)")
# ax.set_xlabel("Cloud Water mixing ratio (g/kg)")
ax.set_ylabel("Height (km)")
cb=plt.colorbar(pc)
ticks=range(1,int(np.log10(pdfmap.max()))+1)
cb.set_ticks(ticks)
labels=[(r"10$^{%s}$" %(str(i))) for i in ticks]
cb.set_ticklabels(labels)
cb.set_label("Count")
# plt.savefig(figuresd+"armcu_rc_alltimes.pdf")
plt.savefig(figuresd+"/bomex_heatrates_alltimes2.pdf")
plt.show()
