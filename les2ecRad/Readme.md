
# On veut créer des profils 1D jusqu'au sommet de l'atmosphère pour chaque champ LES 3D pour computer les propriétés optiques des gaz
# et au passage on veut diagnostiquer les paramètres nuageux taille de nuages, fsd et overlap

# On a donc besoin d'avoir identifié et caractérisé au minimum
# les nuages en tant qu'objets
# les profils d'eau dans les nuages
# les profils d'aires

Il faut donc
- récupérer les scripts d'identification et de caractérisation
- modifier le script de caractérisation pour qu'il écrive une info verticale
- écrire le script qui créé les profils 1D 
	- en moyennant les profils 3D des LES 
	- en rallongeant les profils 1D au dessus de la LES (cf Quentin script htune)
	- en diagnosticant les paramètres nuageux  
- faire tourner identification, caractérisation et make1Dprof sur tous les champs 3D [ de référence, avec MNH 5.4.1 ?? ]
- faire tourner ecRad avec les outils make_opt_prop de Vincent Eymet sur tous les profils 1D (34M un profil)


TODO :
dans make1Dprof, corriger la transition LES / atmo pour smoother
carac avec info verticale
