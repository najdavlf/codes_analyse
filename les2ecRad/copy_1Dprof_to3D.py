import os
import sys
import numpy as np
import netCDF4 as nc

# we have the 1D ecRad profile made from 1D LES
# we have N LES columns from 3D file
# we want to copy the 1D ecRad profile to dimension N
# and modify the profiles under 4km to match each column

ecRad1d = sys.argv[1]
ecRad2d = ecRad1d.split(".nc")[0]+"_Ncolumns.nc"
LES3d   = sys.argv[2]

nc1d = nc.Dataset(ecRad1d)
nc3d = nc.Dataset(LES3d)

th = nc3d.variables["THT"][:]
pr = nc3d.variables["PABST"][:]
t = th*(pr/100000.)**(2./7.)
Nx = nc3d.variables["W_E_direction"].shape[0]
Ny = nc3d.variables["S_N_direction"].shape[0]

print "duplicating profiles"
os.system("ncrcat -O -n "+str(Nx*Ny)+",3,0 "+ecRad1d+" "+ecRad2d)
nc2d = nc.Dataset(ecRad2d,"a")

print "begin loop"
k=0
nz = len(nc2d.variables["q"][0,:])
for i in range(Nx) :
  print i
  for j in range(Ny) :
    for ecvar,lesvar in zip(["q_liquid","q"],["RCT","RVT"]):
      var = nc3d.variables[lesvar][0,:,j,i]
      nc2d.variables[ecvar][k,nz-len(var):]=var[::-1]
    # need to deal with T and P
    ecvar="temperature_hl"
    var = t[0,:,j,i]
    var=np.concatenate((var[0][None],0.5*(var[1:]+var[:-1])))
    nc2d.variables[ecvar][k,nz+1-len(var):]=var[::-1]
    ecvar="pressure_hl"
    var = pr[0,:,j,i]
    var=np.concatenate((var[0][None],0.5*(var[1:]+var[:-1])))
    nc2d.variables[ecvar][k,nz+1-len(var):]=var[::-1]
    k+=1

ql = nc2d.variables["q_liquid"][:]
cf = nc2d.variables["cloud_fraction"][:]
cf = 0.*cf
cf[ql>0]=1.
nc2d.variables["cloud_fraction"][:]=cf
nc2d.variables["re_liquid"][:]=cf*10*1.e-6
nc2d.variables["fractional_std"][:]=cf*0.
nc2d.close()
