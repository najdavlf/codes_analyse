
import sys
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl
from utils import *
from pylab import *

from createEcRadFile import createEcRadFile
from ecRad import *

ref1d = "/home/villefranquen/Work/NCDF/ECRAD/i3rc_mls_cumulus.nc"
listVarNames=["THT","PABST","RCT","RVT","VLEV"]
def make1Dprof(ncfile,characs,output):
    print "1D profiles from 3D fields"
    # Need areas for cloud effective length
    dset = nc.Dataset(characs)
    area = np.squeeze(np.take(dset.variables["area"][:],0,axis=-1))
    dset.close()

    # 3D fields
    dset = nc.Dataset(ncfile, "r")
    rc3d = get_var4D(dset,"RCT")
    # Average relevant 3D fields in ncfile: RCT, THT, PABST, RVT
    prof1d = {}
    for var in listVarNames:
        prof1d[var] = np.mean(dset.variables[var][:],axis=(-1,-2))
        prof1d[var] = prof1d[var].reshape(prof1d[var].size)
    dset.close()

    # Add 1D profiles of cloud fraction and temperature
    prof1d["CF"] = cloud_fra(rc3d)
    prof1d["T"]  = prof1d["THT"]*(prof1d["PABST"]/100000.)**(2./7.)

    # Diagnose cloud related parameters: overlap_param, fsd, cloud scale
    prof1d["ovp"] = overlap_param(prof1d["CF"],rc3d)
    prof1d["fsd"] = std_frac(rc3d)
    prof1d["csc"] = inv_cld_scale(area)

    # Concatenate with upper atmosphere profiles
    dset = nc.Dataset(ref1d, "r")
    profref={}
    for v in dset.variables.keys():
        profref[v]=dset.variables[v][:]
        profref[v]=profref[v].reshape(profref[v].size)
    dset.close()

    """
    # Full level height
    orig_pr = prof1d["PABST"]*1.
    pmin = orig_pr.min()
    prhl = profref["pressure_hl"]
    prfl = 0.5*(prhl[1:]+prhl[:-1]) # computing full level pressure
    jor=searchsorted(prfl,pmin)     # searching pmin in prfl
    orig_pr = np.concatenate((prfl[:jor],orig_pr[::-1]))
    """

    # z,T,P need to be computed on the half level grids and include surf point (1st level...)
    # We assume that dz is small enough so that linear interpolation is ok
    # At the ground level, z=0, t=t[1]-dz*gradt[1], p=p[1]-dz*gradp[1]
    hallev = (prof1d["VLEV"][1:]+prof1d["VLEV"][:-1])/2.
    haltem = (prof1d["T"][1:]+prof1d["T"][:-1])/2.
    halpre = (prof1d["PABST"][1:]+prof1d["PABST"][:-1])/2.
    lev0 = 0
    tem0 = haltem[0] - (haltem[1]-haltem[0])/(hallev[1]-hallev[0])*(hallev[0]-lev0)
    pre0 = halpre[0] - (halpre[1]-halpre[0])/(hallev[1]-hallev[0])*(hallev[0]-lev0)
    prof1d["VLEV"]  = np.concatenate((np.array([lev0]),hallev))*1e3
    prof1d["T"] =     np.concatenate((np.array([tem0]),haltem))
    prof1d["PABST"] = np.concatenate((np.array([pre0]),halpre))

    # Get the height of the top of the LES field
    zmax = prof1d["VLEV"].max()
    # j is the index such as if zmax is inserted in z[::-1] before z[::-1][j],
    # then the order of z[::-1] is preserved
    j=searchsorted(profref["height_hl"][::-1],zmax) # need z in increasing order 
    # This means z[::-1][:j]+[zmax]+z[::-1][j:] is sorted in increasing order
    # We want to concatenate z_les + z[::-1][j:]

    # concatenate z half levels : this is straightforward
    v="height_hl"
    z=prof1d[dicVars[v]]           # LES column
    z_orig=profref[v][::-1]
    z_orig_fl=.5*(z_orig[1:]+z_orig[:-1])
    prof1d[dicVars[v]]=np.concatenate((prof1d[dicVars[v]],profref[v][::-1][j:]))[None,::-1] # height_hl in decreasing order
    z_above=profref[v][::-1][j:]
    zzz=prof1d[dicVars[v]][0,::-1] # whole column

    # concatenate P,T,q : more difficult...

    # First, extrapolate above the LES, such as gradT(z>4km) = gradT(4km), 
    # until T meets the reference temperature profile : while under 12km, 
    # use this extrapolation, above 12km, use linear interpolation between T(12km) and Tles(4km)
    v="temperature_hl" #------------------------------ T --------------------------------------
    t=prof1d[dicVars[v]] 
    t_above=profref[v][::-1][j:]*1.
    gt=(t[-1]-t[-2])/(z[-1]-z[-2]) # grad T at 4km height
    tz=t[-1]
    zbelow=z[-1]
    i=0
    while z_above[i]<12000.:
        dt = gt*(z_above[i]-zbelow)
        tz = tz+dt
        if (tz>t_above[i]) :
          t_above[i]=tz # replace the reference values by the extrapolated values
        else :
          break # break as soon as the extrapolated profile meets the reference profile
        zbelow=z_above[i]
        i+=1
        jpress=i 
    if z_above[i] >= 12000. : # This means the profiles did not meet before 12km,
      # let's perform a linear interpolation betwen 4 and 12km
      gt=(t_above[i]-t[-1])/(z_above[i]-z[-1])
      i=0
      tz=t[-1]
      zbelow=z[-1]
      while z_above[i]<12000.:
        dt = gt*(z_above[i]-zbelow)
        tz = tz+dt
        t_above[i]=tz
        zbelow=z_above[i]
        i+=1

    prof1d[dicVars[v]]=np.concatenate((prof1d[dicVars[v]],t_above))[None,::-1] # height_hl in decreasing order
    #--------------------------------- T is set -------------------------------#

    # Assume linear pressure profile between 4km and z[jpress]
    v="pressure_hl" #------------------------------ P -------------------------#
    p=prof1d[dicVars[v]]
    p_orig=profref[v][:]
    p_above=profref[v][::-1][j:]*1.
    gp=(p_above[jpress]-p[-1])/(z_above[jpress]-z[-1])
    pz=p[-1]
    zbelow=z[-1]
    i=0
    while z_above[i]<z_above[jpress]:
        dp = gp*(z_above[i]-zbelow)
        pz=pz+dp
        p_above[i]=pz
        zbelow=z_above[i]
        i+=1

    prof1d[dicVars[v]]=np.concatenate((prof1d[dicVars[v]],p_above))[None,::-1] # height_hl in decreasing order
    #--------------------------------- P is set -------------------------------#

    
    # Assume negative exponential specific humidity profile between z[i] such as gq[i]~0 
    # and z[j] such as ref specific humidity is ~ 0 
    v="q"  #--------------------------------------- q -------------------------#
    zf=.5*(zzz[1:]+zzz[:-1])
    jf = np.searchsorted(zf,zmax)
    q=prof1d[dicVars[v]][:-1] # the last value of q, between 3.975 and 4 km 
    q_above=profref["q"][::-1][j-1:]*1. # is replaced by the 1st value of q_above

    z_fl = zf[:jf]
    z_above_fl = zf[jf:]
    j12k=np.searchsorted(z_above_fl,12000.) 
    z2 = z_above_fl[j12k] # right above 12km
    q2 = q_above[j12k]    # ref humidity at level right above 12km
    iz=5
    while (iz<len(q)-2) & ((z_fl[iz]<2000.) | (abs((q[iz+1]-q[iz-1])/(z_fl[iz+1]-z[iz-1])) >= .2*abs((q[iz]-q[iz-5])/(z_fl[iz]-z_fl[iz-5])))):
        iz+=1 
    iz-=1
    z1 = z_fl[iz]
    q1 = q[iz]

    k=-np.log(q1/q2)/(z1-z2)
    q0=q1*np.exp(k*z1)
    
    for i in range(iz,len(q)):
        q[i] = q0*np.exp(-k*z_fl[i])
    for i in range(j12k) :
        q_above[i]=q0*np.exp(-k*z_above_fl[i])

    prof1d[dicVars[v]]=np.concatenate((q,q_above))[None,::-1] # height_hl in decreasing order
    #--------------------------------- q is set -------------------------------#
    
    ncol = None
    nlev = len(zf) # full level nb is one less than half level nb
    nb_above = nlev-len(q)
    for v in varsClouds:
     if v!="q":
      if dicVars[v] in prof1d:
          # here we concatenate directly in reverse order : j+1 zeros with reverse LES profile except higher component
          prof1d[dicVars[v]]=np.concatenate((np.zeros(nb_above),prof1d[dicVars[v]][::-1][1:]))[None,:]
      else:
          prof1d[dicVars[v]]=np.zeros((1,nlev))

    v="o3_mmr"
    prof1d[dicVars[v]] = interp(zf[::-1],z_orig_fl,profref[v])[None,:]

    # Correct re_liquid and re_ice 
    ql=prof1d[dicVars["q_liquid"]]
    rl=prof1d[dicVars["re_liquid"]]
    if rl.max()==0. : rl[ql>0]=10.*1e-6
    qi=prof1d[dicVars["q_ice"]]
    ri=prof1d[dicVars["re_ice"]]
    if ri.max()==0. : ri[qi>0]=10.*1e-6

    # Write into new netcdf file
    dims={"column":ncol,
          "level":nlev,
          "half_level":nlev+1,
          "mid_level":nlev-1}

    createEcRadFile(output, dims,profref,prof1d)
    print "Done."
    print ""
    
if __name__ == "__main__":
    ncfile = "/home/villefranquen/Work/NCDF/LES3D/SCMS/CER25.1.SCMS.009.nc"
    ncfiles=["/home/villefranquen/Work/NCDF/LES3D/%s.1.%s.001.nc"%(pref,case) for pref,case in zip(["ARMCu/CERK4","BOMEX/VMFTH","RICO/THL3D","SCMS/CER25"],["ARMCu","BOMEX","RICO","SCMS"])]
    ncfiles+=["/home/villefranquen/Work/NCDF/LES3D/%s.1.%s.010.nc"%(pref,case) for pref,case in zip(["ARMCu/CERK4","BOMEX/VMFTH","RICO/THL3D","SCMS/CER25"],["ARMCu","BOMEX","RICO","SCMS"])]
    charac = "/home/villefranquen/Work/NCDF/LEScharacs/CER25.1.SCMS.009_charac.nc"
    characs=["/home/villefranquen/Work/NCDF/LEScharacs/%s.1.%s.001_charac.nc"%(pref,case) for pref,case in zip(["CERK4","VMFTH","THL3D","CER25"],["ARMCu","BOMEX","RICO","SCMS"])]
    characs+=["/home/villefranquen/Work/NCDF/LEScharacs/%s.1.%s.010_charac.nc"%(pref,case) for pref,case in zip(["CERK4","VMFTH","THL3D","CER25"],["ARMCu","BOMEX","RICO","SCMS"])]
    output = "test_SCMS.009.nc"
    output="test.nc"
    #   outputs=["test_ARMCu.0%02i.nc"%i for i in range(4,14)]
    for ncfile,charac in zip(ncfiles,characs):
      make1Dprof(ncfile,charac,output)
