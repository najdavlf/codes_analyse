import numpy as np

def get_var4D(dset,name) :
  var=dset.variables[name] 
  if len(var.shape)!=4 : 
    print "Error :",name," is not 4D : ", var.shape
    exit()
  else :
    return var[0,:,:,:]

def cloud_fra(rc) :
  mask = np.zeros(rc.shape,dtype=np.int8)
  mask[rc>0.] = 1
  if len(np.unique(mask))>2 :
    print "Error : ",set(mask)," should be 0,1"
    exit()
  else :
    return 1.*np.sum(mask,axis=(1,2))/(rc.shape[1]*rc.shape[2])

def std_frac(rc) :
  std=np.zeros(rc.shape[0],dtype=np.float64)
  for i in range(len(rc[:,0,0])):
      rci=rc[i,:,:]
      if len(rci[rci>0])>2 :
        std[i] = np.std(rci[rci>0])/np.mean(rci[rci>0])
  return std

def inv_cld_scale(area) :
  nclouds = len(area[0,:])
  # perim = 2 pi r 
  # area =    pi r r
  domain=6.4*6.4 # km2
  cf = np.sum(area,axis=1)/domain
  perim = 2.*np.sqrt(np.pi*area) # kmeters 
  frac_peri = perim/domain/1000. # meters-1

  # each cloud has an area and a perimeter (the perimeter of a circle of the same area)
  # the scale is 4 * area (1-area) / perim
  # area is the cloud fraction of the column at height z
  # perim is the total perimeter of the clouds in the column at height z

  tot_frac_peri = np.nansum(frac_peri, axis=1, dtype=np.float64)
  tot_frac_peri[tot_frac_peri==0] = np.float('nan')
  scale = 4.*(cf*(1.-cf))/tot_frac_peri
  inv_scale = 1./scale 
  inv_scale[np.isnan(tot_frac_peri)] = 0.
  return inv_scale

def eq_eps(a,b) :
  return abs(a-b)<1.e-8

def same_order(z1,z2) :
  if (z1[1]-z1[0])*(z2[1]-z2[0]) < 0 :
      z2 = np.flipud(z2)
  return z2 

def ave_to_ecrad_grid(var, z1, z2) :
  # this could be way better ! But this simple version gives matching profiles 
  newvar=0.*z2[:-1]
  midz1 = .5 * (z1[1:]+z1[:-1])
  for indz,(hmin,hmax) in enumerate(zip(z2[:-1],z2[1:])) :
    ind = np.where((midz1>=hmin)&(midz1<=hmax))
    nbz1 = len(ind[0])
    if nbz1 > 0 :
      newvar[indz] = np.mean(var[ind], dtype=np.float64)
  return newvar

def cld_frac_on_ecrad_grid(rc3d, z1, z2) :
  # if there is more than 1 LES layer in a ecRad layer
  # the cloud fraction is the "true" cloud fraction of the combined levels
  # it can only be more than the cloud fraction profile on the LES grid 
  cf=0.*z2[:-1]
  for indz,(hmin,hmax) in enumerate(zip(z2[:-1],z2[1:])) :
    ind = np.where((z1>=hmin)&(z1<=hmax))[0]
    nbz1 = len(ind)
    if nbz1 > 0 :
      rcslice = np.max(rc3d[ind,:,:],axis=0)
      ncloudy = len(np.where(rcslice>0)[0])
      cf[indz] = ncloudy/(256.*256)
  return cf

def rc3d_on_ecrad_grid(rc3d, z1, z2) :
  shape3d = rc3d.shape
  newshape = (len(z2)-1, shape3d[1], shape3d[2])
  new = np.zeros(newshape)
  for indz,(hmin,hmax) in enumerate(zip(z2[:-1],z2[1:])) :
    ind = np.where((z1>=hmin)&(z1<=hmax))[0]
    nbz1 = len(ind)
    if nbz1 > 0 :
      new[indz] = np.max(rc3d[ind,:,:],axis=0)
  return new

from matplotlib import pyplot as plt
def overlap_param(cf,rc3d) :
    # ctrue = a cmax + (1-a) cran = a(cmax-cran) + cran
    # a = (ctrue - cran) / (cmax - cran)
    # at each level interface (mid levels) 
    # cf and rc3d should be defined on the ecmwf grid before doing this
    # cf on the ecmwf grid is max cf
    ctrue = 0.*cf[1:]
    crand = 0.*cf[1:]
    cmaxi = 0.*cf[1:]
    #fig,axes =plt.subplots(nrows=len(cf[cf>0])+1,ncols=3, figsize=(8,100))
    k=0
    maxrc = 1.0e-6
    npts = 256.*256.
    for i in range(len(ctrue)) :
      ra = rc3d[i,:,:]
      rb = rc3d[i+1,:,:]
      rc = np.max(rc3d[i:i+2,:,:],axis=0)
      ca = len(ra[ra>maxrc])/npts
      cb = len(rb[rb>maxrc])/npts
      crand[i] = ca+cb-ca*cb
      cmaxi[i] = max(ca,cb)
      ctrue[i] = len(rc[rc>maxrc])/npts
      if 0:
        a1=axes[k,0]
        a2=axes[k,1]
        a3=axes[k,2]
        k+=1
        a1.imshow(rc3d[i,:,:])
        a1.set_title(str(len(np.where(rc3d[i,:,:]>1.0e-6)[0]/(256.*256.))))
        a2.imshow(rc3d[i+1,:,:])
        a2.set_title(str(len(np.where(rc3d[i+1,:,:]>1.0e-6)[0]/(256.*256.))))
        a3.imshow(rc)
        a3.set_title(str(len(np.where(rc>1.0e-6)[0]/(256.*256.))))
    #plt.tight_layout()
    #plt.savefig("test_overlap2.png")
    #plt.show()
    ov_param = (ctrue-crand)/(cmaxi-crand)
    ov_param[np.isnan(ov_param)]=1
    ov_param[ov_param<=0]=1
    return  ov_param 

