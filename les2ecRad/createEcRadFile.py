# author liboisq
# modifs 
# 15/11/2018  N. Villefranque return dict

import sys
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl
from ecRad import longnames,units
from ecRad import varsThermo,varsClouds,varsRad,varsGas
from ecRad import dicVars

def createEcRadFile(ncfile,dims,profref,prof1d):
    # Defining the new ecRad profile
    ecVars={}

    # Creating a new .nc file
    dset = nc.Dataset(ncfile, "w")
     
    # Creating dimensions : column, level, half-level, mid_level
    for dim in dims:
        dset.createDimension(dim,dims[dim])
       
    # Creating variables
    # Thermo 
    for var in varsThermo:
      ecVars[var] = dset.createVariable(var,"f4",("column","half_level",))
    
    # Clouds
    for var in varsClouds:
     if var != "overlap_param":
      ecVars[var] = dset.createVariable(var,"f4",("column","level",))
     else:
      ecVars[var] = dset.createVariable(var,"f4",("column","mid_level",))
    
    # Aerosols 
    dset.createDimension('aerosols', 12)
    dset.createVariable('aerosol_mmr', 'f4', ('column','aerosols','level'))
    """ aerosols types:
        1 = Sea_Salt_bin1, 2 = Sea_Salt_bin2, 3 = Sea_Salt_bin3
        4 = Mineral_Dust_bin1, 5 = Mineral_Dust_bin2, 6 = Mineral_Dust_bin3
        7 = Organic_Matter_hydrophilic, 8 = Organic_Matter_hydrophobic
        9 = Black_Carbon_hydrophilic, 10 = Black_Carbon_hydrophobic
        11 = Sulfates, 12 = Stratospheric Sulfates """

    # Surface properties and illumination
    for var in varsRad:
      ecVars[var] = dset.createVariable(var,"f4",("column",))
    
    # Trace gases
    ecVars["o3_mmr"] = dset.createVariable("o3_mmr","f4",("column","level",))
    for var in varsGas:
      ecVars[var] = dset.createVariable(var,"f4",)

    for var in ecVars:
      ecVars[var].long_name = longnames[var]
      ecVars[var].units = units[var]

      sh=ecVars[var].shape
      if len(sh)==0:               # scalar values
        if dicVars[var] in prof1d:
          ecVars[var].assignValue(prof1d[dicVars[var]])
        else: ecVars[var].assignValue(profref[var])
      else :
        if dicVars[var] in prof1d: # use 3D averaged profile
          ecVars[var][:] = prof1d[dicVars[var]][:]
        else: ecVars[var][:] = profref[var][:] # use 1D reference
    dset.close()
