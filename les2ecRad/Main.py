
import sys
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl

from identify_clouds import idClouds
from characterize_clouds import chClouds
from make1Dprof import make1Dprof

dirIn =  "/home/villefranquen/Work/NCDF/LES3D"
dirCh = "/home/villefranquen/Work/NCDF/LEScharacs/"
dirOut = "/home/villefranquen/Work/NCDF/LES1D/"

for (dirpath,dirnames,filesnames) in walk(dirIn):
  for file in filesnames:
    if (".nc" in file):
      fi = file.split(".nc")[0]
      print fi
      ncfile = '/'.join([dirpath,file])
      idClouds(ncfile)                  # identify clouds in field 
      ncchar = dirCh+fi+"_charac.nc"
      #chClouds(ncfile,ncchar)           # compute characteristics
      nc1d = dirOut+fi+"_1D.nc"
      make1Dprof(ncfile,ncchar,nc1d)    # compute 1D profiles
