
import sys
from os import walk
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib as mpl

dirObj = "/home/villefranquen/Work/codes_analyse/scripts_objects/src"
sys.path.append(dirObj)

from identification_methods import identify

def clouds(dictVars) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>0]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

def idClouds(ncfile):
  listVarsNames=["RCT"]
  identify(ncfile, listVarsNames, clouds, name="clouds", overwrite=True)
