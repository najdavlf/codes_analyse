import netCDF4 as nc
ref1D = "/home/villefranquen/Work/NCDF/ECRAD/i3rc_mls_cumulus.nc" 

dset = nc.Dataset(ref1D,"r")

f=open("ecRad.py","a")

f.write("longnames={")
for v in dset.variables:
  f.write('"'+v+'":"'+dset.variables[v].long_name+'",')
f.write("}")
f.write("\n")
f.write("units={")
for v in dset.variables:
  f.write('"'+v+'":"'+dset.variables[v].units+'",')
f.write("}")

f.close()

