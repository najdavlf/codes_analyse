
Projet git pour tous les scripts qui traitent les nuages
identification : identifie et numerote les nuages dans une scene LES => from .nc to .nc
caracterisation : calcule des caracteristiques pour chaque nuage identifies d'une LES => from obj.nc & .nc to txt 
analyse : scripts pour tracer des diagnostics des simulations et des nuages identifies et caracterises => from .nc & txt to .pdf
manipulation : modifier des champs LES, des dimensions, renommer des variables...

Pour traiter une série de fichiers netcdf 2D produits par MesoNH sous beaufix quand la sortie est .lfi
- 1. sous beaufix :
-- 1. avec les données script/DATA/dirextract_000_ALL_reduction_Fleur
      convertir le 000.lfi en 000KCL.nc (extrait seulement les variables d'intérêt)
-- 2. faire ncks -Oh -d time,0,179 fich000KCL.nc fich000KCL.nc si la dimension temporelle est trop grande... bizarre 
-- 3. faire un get avec lftp pour récupérer le fichier en local

- 2. en local :
-- 1. avec le script manipulation/script_rename_manip_var
      appeler avec en argument le fichier 000KCL.nc
      renomme certaines var au format commun et calcule des nouveaux diagnostics
      écrit un fichier 000KCL.nc.nc
-- 2. avec le script manipulation/ncdf_2d_to_4d.py
      appeler avec en argument le 000KCL.nc.nc
      écrit dans un fichier 000KCL_4D.nc les variables en 4d avec x et y un seul point (valeur 0.)

Pour traiter une série de fichiers netcdf 3D produits par MesoNH sous beaufix quand la sortie est .nc4
- 1. sous beaufix : 
-- 1. avec le script script/script_rename_dim
      appeler avec en argument la liste des fichiers .nc4
      extrait les variables d'intérêt, aux points de grilles,
      attention ix0, ix1,... iz1 écrits en dur dans le script (1, 512,... 100)
      attention conversion du nc4 en nc3 64 bit (option -6) car sinon bug sur rename
      renomme les variables et dimensions,
      modifie les vecteurs x,y,z pour être au milieu des mailles suivant la convention des .lfi
      attention, dx/2, dy/2, dz/2 en dur dans le script (50,50,20)
-- 2. avec le script script/add_time
      appeler avec en argument la liste des fichiers .nc
      ajoute la dimension temporelle UNLIMITED
      attention overwrite fichier d'origine et 
      attention temps initial en dur (77977800) et dt en dur (3600)
-- 3. faire un mget en lftp pour récupérer les fichiers en local

- 2. en local :
-- 1. avec le script manipulation/add_time_dim_to_var.py
      tourner sur tous les fichiers 3D (z,y,x) du dossier
      créé un fichier 4D (t,z,y,x) identique par ailleurs
-- 2. avec le script identification/extract_4Dfield_identify_objects_ndimage.py
      tourner sur tous les fichiers 4D (t,z,y,x) du dossier
      créé un fichier .obj.nc avec le champ "objets" qui identifie les nuages
-- 3. avec le script caracterisation/extract_obj4d_write_carac.py
      tourner sur tous les fichiers .obj.nc du dossier
      créé un fichier _carac_4D.nc avec pour dimensions 
      time, numero du nuage, carac (mean, min, max, std), vertical levels
      et tous les champs morpho, thermodyn, dyn du fichier associé au .obj.nc
