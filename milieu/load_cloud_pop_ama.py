#!/usr/bin/python
# -*- coding: latin-1 -*-

# 14/01/2019 
# N. Villefranque

import sys, os
p='/home/villefranquen/objects/src/'
p='/cnrm/tropics/commun/DATACOMMUN/OBJECTS/objects/src/'
sys.path.append(p)
import netCDF4 as nc
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime
from identification_methods import identify
from mycolors import basic4 as cols
from mycolors import basic2 as cols
import matplotlib as mpl
from constants import *
import pickle

f=open("data.txt","r")

plt.style.use("presentation")
mpl.rcParams["axes.edgecolor"]=cols[-3]
mpl.rcParams["grid.color"]=cols[-4]
mpl.rcParams["grid.linestyle"]="-"

markers =  {'.': 'point', ',': 'pixel', 'o': 'circle', 'v': 'triangle_down', '^': 'triangle_up', '<': 'triangle_left', '>': 'triangle_right', '1': 'tri_down', '2': 'tri_up', '3': 'tri_left', '4': 'tri_right', '8': 'octagon', 's': 'square', 'p': 'pentagon', '*': 'star', 'h': 'hexagon1', 'H': 'hexagon2', '+': 'plus', 'x': 'x', 'D': 'diamond', 'd': 'thin_diamond', '|': 'vline', '_': 'hline',  0: 'tickleft', 1: 'tickright', 2: 'tickup', 3: 'tickdown', 4: 'caretleft', 5: 'caretright', 6: 'caretup', 7: 'caretdown',  10: 'caretupbase'}
mymarkers = [m for m in list(markers)]

# Read vars

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

def cloudMask(dictVars,thrs) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>thrs]=1 # cell is in cloud if liquid water mixing ratio is greater than thrs
    return mask

listVarsNames = ["RCT"]
path = "/home/villefranquen/Work/NCDF/LES3D/"
path = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/"

maxz = {"ARMCu" : 116,
        "BOMEX" : 85,
        "RICO"  : 102,
        "SCMS"  : 114
        }
minz = {"ARMCu" : 28,
        "BOMEX" : 17,
        "RICO"  : 21,
        "SCMS"  : 16
        }

# To find all the files
reps = ["ARM_LES"]
reps = ["ARMCu","BOMEX","RICO","SCMS"]
reps = ["ARM_LES","BOMEX_LES","RICO_LES","SCMS_LES"]
pref = ["CERK4"]
pref = ["CERK4","VMFTH","THL3D","CER25"]
case = ["ARMCu"]
case = ["ARMCu","BOMEX","RICO","SCMS"]
nbfs = [4,4,4,4]
nbfs = [14,15,15,12]

w,h = plt.figaspect(1)
fig = plt.figure(figsize=(w*2,h*2.5))
ax01 = fig.add_subplot(421)  # smallest big cloud eq radius
ax02 = fig.add_subplot(422)  # percent remaining clouds 
ax11 = fig.add_subplot(423)  # cloud number
ax12 = fig.add_subplot(424,sharey=ax11) # big cloud number
ax21 = fig.add_subplot(425)  # cloud cover
ax22 = fig.add_subplot(426,sharey=ax21) # big cloud cover
ax31 = fig.add_subplot(427)  # base top 
ax32 = fig.add_subplot(428)  # big base top

fig1 = plt.figure(figsize=(h*2.,0.5*h))
ax41 = fig1.add_subplot(131) # cloud number
ax42 = fig1.add_subplot(132) # cloud cover
ax99 = fig1.add_subplot(133) # scatter cover - number

fig2 = plt.figure(figsize=(w*1,h))
ax43 = fig2.add_subplot(111) # smallest big cloud eq radius

fig3 = plt.figure(figsize=(w*2.,h))
ax44 = fig3.add_subplot(121) # percent of remaining clouds
ax45 = fig3.add_subplot(122) # number of remaining clouds 

fig4 = plt.figure(figsize=(w*2.,h))
ax46 = fig4.add_subplot(121) # cloud cover big clouds
ax47 = fig4.add_subplot(122) # cloud volume fraction big clouds

fig5 = plt.figure(figsize=(w*2.,h))
ax48 = fig5.add_subplot(121) # all clouds base and top
ax49 = fig5.add_subplot(122) # big clouds base and top

figvol = plt.figure(figsize=(w*2.0,h*2.0))
ax51 = figvol.add_subplot(221) # cumulative volume fraction at each timestep
ax52 = figvol.add_subplot(222) # cumulative volume fraction at each timestep
ax53 = figvol.add_subplot(223) # cumulative volume fraction at each timestep
ax54 = figvol.add_subplot(224) # cumulative volume fraction at each timestep
axes5 = {case[0]:ax51,case[1]:ax52,case[2]:ax53,case[3]:ax54}

figasp = plt.figure(figsize=(w*2.0,h*2.0)) # aspect ratio scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax61 = figasp.add_subplot(gs[1:4,0:3])
ax61x = figasp.add_subplot(gs[0,0:3],sharex=ax61)
ax61y = figasp.add_subplot(gs[1:4,3],sharey=ax61) 
ax62 = figasp.add_subplot(gs[1:4,5:8]) 
ax62x = figasp.add_subplot(gs[0,5:8],sharex=ax62) 
ax62y = figasp.add_subplot(gs[1:4,8],sharey=ax62) 
ax63 = figasp.add_subplot(gs[6:9,0:3]) 
ax63x = figasp.add_subplot(gs[5,0:3],sharex=ax63) 
ax63y = figasp.add_subplot(gs[6:9,3],sharey=ax63) 
ax64 = figasp.add_subplot(gs[6:9,5:8]) 
ax64x = figasp.add_subplot(gs[5,5:8],sharex=ax64) 
ax64y = figasp.add_subplot(gs[6:9,8],sharey=ax64) 
axes6 = {case[0]:(ax61,ax61x,ax61y),case[1]:(ax62,ax62x,ax62y),case[2]:(ax63,ax63x,ax63y),case[3]:(ax64,ax64x,ax64y)}

figtop = plt.figure(figsize=(w*2.0,h*2.0)) # tops scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax71 = figtop.add_subplot(gs[1:4,0:3])
ax71x = figtop.add_subplot(gs[0,0:3],sharex=ax71)
ax71y = figtop.add_subplot(gs[1:4,3],sharey=ax71) 
ax72 = figtop.add_subplot(gs[1:4,5:8]) 
ax72x = figtop.add_subplot(gs[0,5:8],sharex=ax72) 
ax72y = figtop.add_subplot(gs[1:4,8],sharey=ax72) 
ax73 = figtop.add_subplot(gs[6:9,0:3]) 
ax73x = figtop.add_subplot(gs[5,0:3],sharex=ax73) 
ax73y = figtop.add_subplot(gs[6:9,3],sharey=ax73) 
ax74 = figtop.add_subplot(gs[6:9,5:8]) 
ax74x = figtop.add_subplot(gs[5,5:8],sharex=ax74) 
ax74y = figtop.add_subplot(gs[6:9,8],sharey=ax74) 
axes7 = {case[0]:(ax71,ax71x,ax71y),case[1]:(ax72,ax72x,ax72y),case[2]:(ax73,ax73x,ax73y),case[3]:(ax74,ax74x,ax74y)}

figdep = plt.figure(figsize=(w*2.0,h*2.0))  # depth scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax81 = figdep.add_subplot(gs[1:4,0:3])
ax81x = figdep.add_subplot(gs[0,0:3],sharex=ax81)
ax81y = figdep.add_subplot(gs[1:4,3],sharey=ax81) 
ax82 = figdep.add_subplot(gs[1:4,5:8]) 
ax82x = figdep.add_subplot(gs[0,5:8],sharex=ax82) 
ax82y = figdep.add_subplot(gs[1:4,8],sharey=ax82) 
ax83 = figdep.add_subplot(gs[6:9,0:3]) 
ax83x = figdep.add_subplot(gs[5,0:3],sharex=ax83) 
ax83y = figdep.add_subplot(gs[6:9,3],sharey=ax83) 
ax84 = figdep.add_subplot(gs[6:9,5:8]) 
ax84x = figdep.add_subplot(gs[5,5:8],sharex=ax84) 
ax84y = figdep.add_subplot(gs[6:9,8],sharey=ax84) 
axes8 = {case[0]:(ax81,ax81x,ax81y),case[1]:(ax82,ax82x,ax82y),case[2]:(ax83,ax83x,ax83y),case[3]:(ax84,ax84x,ax84y)}


figbas = plt.figure(figsize=(w*2.0,h*2.0)) # base scatterplot
gs = mpl.gridspec.GridSpec(9,9)
ax91 = figbas.add_subplot(gs[1:4,0:3])
ax91x = figbas.add_subplot(gs[0,0:3],sharex=ax91)
ax91y = figbas.add_subplot(gs[1:4,3],sharey=ax91) 
ax92 = figbas.add_subplot(gs[1:4,5:8]) 
ax92x = figbas.add_subplot(gs[0,5:8],sharex=ax92) 
ax92y = figbas.add_subplot(gs[1:4,8],sharey=ax92) 
ax93 = figbas.add_subplot(gs[6:9,0:3]) 
ax93x = figbas.add_subplot(gs[5,0:3],sharex=ax93) 
ax93y = figbas.add_subplot(gs[6:9,3],sharey=ax93) 
ax94 = figbas.add_subplot(gs[6:9,5:8]) 
ax94x = figbas.add_subplot(gs[5,5:8],sharex=ax94) 
ax94y = figbas.add_subplot(gs[6:9,8],sharey=ax94) 
axes9 = {case[0]:(ax91,ax91x,ax91y),case[1]:(ax92,ax92x,ax92y),case[2]:(ax93,ax93x,ax93y),case[3]:(ax94,ax94x,ax94y)}

i=0
dx=0.025
dy=0.025
dz=0.025
for i,(rep,pre,cas,nb) in enumerate(zip(reps,pref,case,nbfs)):
  tsteps = range(1,nb+1)
  nbf=len(tsteps)
  nc3dList = [path+rep+"/"+pre+".1."+cas+".%03i"%(h)+".nc" for h in tsteps]
  nc3dList = [path+rep+"/"+pre+"/"+pre+".1."+cas+".%03i"%(h)+".nc" for h in tsteps]
  # Characteristics
  print pre,cas
  for i3d,nc3d in enumerate(nc3dList) :
    clouds=nc.Dataset(nc3d).variables["clouds"][:]
    if np.max(clouds)>0 :
      ax5=axes5[cas] # volumes cumul
      (ax6,ax6x,ax6y)=axes6[cas] # aspect ratios
      (ax7,ax7x,ax7y)=axes7[cas] # tops 
      (ax8,ax8x,ax8y)=axes8[cas] # depth
      (ax9,ax9x,ax9y)=axes9[cas] # base

      x=pickle.load(f)
      y=pickle.load(f)
      ax5.plot(x,y,linewidth=.5+.1*i3d,color=cols[i+1])
      x=pickle.load(f)
      y=pickle.load(f)
      ax5.plot(x,y,marker='+',color="black",markersize=2*i3d)
      ax5.set_title(cas)
      ax5.set_xlabel("Cloud [\% of number of clouds]")
      ax5.set_ylabel("Cumul vol frac [\%]")

      x=pickle.load(f)
      y=pickle.load(f)
      ax6.plot(x,y, linestyle="", marker='+',markersize=1.5*i3d,color=cols[i+1])
      ax6.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markerfacecolor='gray',alpha=1-3.*np.sqrt(np.std(x)*np.std(y)))
      ax6.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markeredgecolor='black',markerfacecolor='none',markeredgewidth=1.)
      ax6.set_xlabel("Equivalent length [km]")
      ax6.set_ylabel("Depth [km]")

      x=pickle.load(f)
      y=pickle.load(f)
      ax7.plot(x,y,linestyle="", marker="+",markersize=1.5*i3d,color=cols[i+1])
      ax7.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markerfacecolor='gray',alpha=1-3.*np.sqrt(np.std(x)*np.std(y)))
      ax7.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markeredgecolor='black',markerfacecolor='none',markeredgewidth=1.)
      ax7.set_xlabel("Mean top [km]")
      ax7.set_ylabel("Max top [km]")
      
      x=pickle.load(f)
      y=pickle.load(f)
      ax8.plot(x,y,linestyle="", marker='+',markersize=1.5*i3d,color=cols[i+1])
      ax8.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markerfacecolor='gray',alpha=1-3.*np.sqrt(np.std(x)*np.std(y)))
      ax8.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markeredgecolor='black',markerfacecolor='none',markeredgewidth=1.)
      ax8.set_xlabel("Mean depth [km]")
      ax8.set_ylabel("Max depth [km]")

      x=pickle.load(f)
      y=pickle.load(f)
      ax9.plot(x,y,linestyle="", marker='+',markersize=1.5*i3d,color=cols[i+1])
      ax9.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markerfacecolor='gray',alpha=1-3.*np.sqrt(np.std(x)*np.std(y)))
      ax9.plot(np.mean(x),np.mean(y),linestyle="",marker='o',markersize=1.5*i3d,markeredgecolor='black',markerfacecolor='none',markeredgewidth=1.)
      ax9.set_xlabel("Min base [km]")
      ax9.set_ylabel("Mean base [km]")

    if i3d==nbf-1 :
      lab=pickle.load(f)
      ax6x.set_title(cas+" - %i big clouds"%lab)
      x=pickle.load(f)
      y=pickle.load(f)
      ax6.plot(x,y,linewidth=1.,color=cols[-3])
      ax6.set_xlim(0.,2.)
      ax6.set_ylim(0.,2.)
      x=pickle.load(f)
      y=pickle.load(f)
      ax6x.hist(x,bins="auto",color=cols[i+1],alpha=.9)
      ax6y.hist(y,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
      plt.setp(ax6x.get_xticklabels(),visible=False)
      plt.setp(ax6y.get_yticklabels(),visible=False)
      [label.set_visible(False) for label in ax6x.get_yticklabels()[:-1]]
      [label.set_visible(False) for label in ax6y.get_xticklabels()[:-1]]

      lab=pickle.load(f)
      ax7x.set_title(cas+" - %i big clouds"%lab)
      x=pickle.load(f)
      y=pickle.load(f)
      ax7.plot(x,y,linewidth=1.,color=cols[-3])
      ax7.set_xlim(0.,3.)
      ax7.set_ylim(0.,3.)
      x=pickle.load(f)
      y=pickle.load(f)
      ax7x.hist(x,bins="auto",color=cols[i+1],alpha=.9)
      ax7y.hist(y,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
      plt.setp(ax7x.get_xticklabels(),visible=False)
      plt.setp(ax7y.get_yticklabels(),visible=False)
      [label.set_visible(False) for label in ax7x.get_yticklabels()[:-1]]
      [label.set_visible(False) for label in ax7y.get_xticklabels()[:-1]]

      lab=pickle.load(f)
      ax8x.set_title(cas+" - %i big clouds"%lab)
      x=pickle.load(f)
      y=pickle.load(f)
      ax8.plot(x,y,linewidth=1.,color=cols[-3])
      ax8.set_xlim(0.,1.8)
      ax8.set_ylim(0.,1.8)
      x=pickle.load(f)
      y=pickle.load(f)
      ax8x.hist(x,bins="auto",color=cols[i+1],alpha=.9)
      ax8y.hist(y,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
      plt.setp(ax8x.get_xticklabels(),visible=False)
      plt.setp(ax8y.get_yticklabels(),visible=False)
      [label.set_visible(False) for label in ax8x.get_yticklabels()[:-1]]
      [label.set_visible(False) for label in ax8y.get_xticklabels()[:-1]]

      lab=pickle.load(f)
      ax9x.set_title(cas+" - %i big clouds"%lab)
      x=pickle.load(f)
      y=pickle.load(f)
      ax9.plot(x,y,linewidth=1.,color=cols[-3])
      ax9.set_xlim(0.4,2.)
      ax9.set_ylim(0.4,2.)
      x=pickle.load(f)
      y=pickle.load(f)
      ax9x.hist(x,bins="auto",color=cols[i+1],alpha=.9)
      ax9y.hist(y,bins="auto",orientation="horizontal",color=cols[i+1],alpha=.9)
      plt.setp(ax9x.get_xticklabels(),visible=False)
      plt.setp(ax9y.get_yticklabels(),visible=False)
      [label.set_visible(False) for label in ax9x.get_yticklabels()[:-1]]
      [label.set_visible(False) for label in ax9y.get_xticklabels()[:-1]]
      #ax9x.set_ylim(0,max(x))
      #ax9y.set_xlim(0,max(y))
 
  if cas=="ARMCu":begin=4;end=-2
  if cas=="BOMEX":begin=7;end=11
  # volume threshold and percentage of remaining clouds
  x=pickle.load(f)
  y=pickle.load(f)
  ax01.plot(x,y, color=cols[i+1],label=cas)
  x=pickle.load(f)
  y=pickle.load(f)
  ax02.plot(x,y, color=cols[i+1],label=cas)
  ax02.fill_between(x,y, color=cols[i+1],label=cas,alpha=.33)
  # same but on separate fig 
  x=pickle.load(f)
  y=pickle.load(f)
  ax43.plot(x,y, color=cols[i+1],label=cas)
  x=pickle.load(f)
  y=pickle.load(f)
  ax44.plot(x,y, color=cols[i+1],label=cas)

  # number total and number of 90% total volume
  x=pickle.load(f)
  y=pickle.load(f)
  ax11.plot(x,y,color=cols[i+1],label=cas)
  x=pickle.load(f)
  y=pickle.load(f)
  ax12.plot(x,y,color=cols[i+1],label=cas)
  # same but on separate fig 
  x=pickle.load(f)
  y=pickle.load(f)
  if cas=="ARMCu" or cas=="BOMEX" :
    number=y
    ax41.plot(x,y,color=cols[i+1],label=cas)
    if len(sys.argv)>1 and int(sys.argv[1])>=begin and sys.argv[2]==cas:
      thisp = int(sys.argv[1])
      ax41.plot(x[begin:thisp],y[begin:thisp],linestyle="",marker="x",color=cols[i+1],mec=cols[i+1],mew=3)
      ax41.plot(x[thisp],y[thisp],linestyle="",marker="o",fillstyle='none',mec=cols[i+1],mew=3,ms=10)
    else : 
     if not (sys.argv[2]=="ARMCu" and cas=="BOMEX") :
      ax41.plot(x[begin:end],y[begin:end],linestyle="",marker="x",color=cols[i+1],mec=cols[i+1],mew=3)
  x=pickle.load(f)
  y=pickle.load(f)
  ax45.plot(x,y,color=cols[i+1],label=cas)

  # cover total and cover of 90% total volume
  x=pickle.load(f)
  y=pickle.load(f)
  ax21.plot(x,y,color=cols[i+1],label=cas)
  x=pickle.load(f)
  y=pickle.load(f)
  ax22.plot(x,y,color=cols[i+1],label=cas,linewidth=1.)
  x=pickle.load(f)
  y=pickle.load(f)
  ax22.plot(x,y,color=cols[i+1],label=cas)
  # same but on separate fig 
  x=pickle.load(f)
  y=pickle.load(f)
  if cas=="ARMCu" or cas=="BOMEX" :
    cover=y
    ax42.plot(x,y,color=cols[i+1],label=cas)
    if len(sys.argv) and int(sys.argv[1])>=begin and sys.argv[2]==cas :
      thisp = int(sys.argv[1])
      ax42.plot(x[begin:thisp],y[begin:thisp],linestyle="",marker="x",color=cols[i+1],mew=3)
      ax42.plot(x[thisp],y[thisp],linestyle="",marker="o",fillstyle='none',mec=cols[i+1],mew=3,ms=10)
      ax99.plot(cover[begin:thisp],number[begin:thisp],color=cols[i+1],linestyle="",marker="x",mew=3)
      ax99.plot(cover[thisp],number[thisp],linestyle="",marker="o",fillstyle='none',mec=cols[i+1],mew=3,ms=10)
    else : 
     if not(sys.argv[2]=="ARMCu" and cas=="BOMEX") :
      ax42.plot(x[begin:end],y[begin:end],linestyle="",marker="x",color=cols[i+1],mew=3)
      ax99.plot(cover[begin:end],number[begin:end],color=cols[i+1],linestyle="",marker="x",mew=3)
  x=pickle.load(f)
  y=pickle.load(f)
  ax46.plot(x,y,color=cols[i+1],label=cas,linewidth=.5)
  x=pickle.load(f)
  y=pickle.load(f)
  ax46.plot(x,y,color=cols[i+1],label=cas)


  # base min / top max all clouds
  x=pickle.load(f)
  y=pickle.load(f)
  ax31.plot(x,y,color=cols[i+1],label=cas)
  x=pickle.load(f)
  z=pickle.load(f)
  ax31.plot(x,z,color=cols[i+1],label=cas)
  ax31.fill_between(x,y,z,color=cols[i+1],label=cas,alpha=.33)
  ax31.grid(axis="y")
  # same but on separate fig
  x=pickle.load(f)
  y=pickle.load(f)
  ax48.plot(x,y,color=cols[i+1],label=cas)
  x=pickle.load(f)
  z=pickle.load(f)
  ax48.plot(x,z,color=cols[i+1],label=cas)
  ax48.fill_between(x,y,z,color=cols[i+1],label=cas,alpha=.33)
  ax48.grid(axis="y")

  # base mean / top mean, clouds 90% total volume
  x=pickle.load(f)
  y=pickle.load(f)
  ax32.plot(x,y,color=cols[i+1],label=cas)
  x=pickle.load(f)
  z=pickle.load(f)
  ax32.plot(x,z,color=cols[i+1],label=cas)
  ax32.fill_between(x,y,z,color=cols[i+1],label=cas,alpha=.33)
  ax32.grid(axis="y")
  # same but separated fig
  x=pickle.load(f)
  y=pickle.load(f)
  ax49.plot(x,y,color=cols[i+1],label=cas)
  x=pickle.load(f)
  z=pickle.load(f)
  ax49.plot(x,z,color=cols[i+1])
  ax49.fill_between(x,y,z,color=cols[i+1],alpha=.33)
  ax49.grid(axis="y")

#figdep.tight_layout()
figdep.savefig("clouds_depths.svg")

#figtop.tight_layout()
figtop.savefig("clouds_tops.svg")

#figbas.tight_layout()
figbas.savefig("clouds_bases.svg")

#figasp.tight_layout()
figasp.savefig("clouds_aspect_ratios.svg")

figvol.tight_layout()
figvol.savefig("clouds_cumul_volumes.svg")
#----------#
# ax1  ax3 #
# ax2  ax4 #
#----------#
ax12.legend(loc="best")
ax01.set_ylabel("Smallest big cloud\neq. sphere radius [m]")
ax02.set_ylabel("\% of big clouds")
ax02.yaxis.tick_right()
ax02.yaxis.set_label_position("right")
ax11.set_title("All clouds")
ax12.set_title("Biggest clouds")
ax12.yaxis.tick_right()
ax12.yaxis.set_label_position("right")
ax11.set_ylabel("Number of clouds")
ax21.set_ylabel("Cloud cover [\%]")
ax22.yaxis.tick_right()
ax31.set_ylabel("Cloud min base and\nmax top heights [km]")
ax32.set_ylabel("Big clouds mean base\nand top heights [km]")
ax32.yaxis.tick_right()
ax32.yaxis.set_label_position("right")
ax31.set_xlabel("Simulation hour [\#]")
ax32.set_xlabel("Simulation hour [\#]")
fig.tight_layout()
fig.savefig("clouds_cloud_pop.svg")


ax41.set_ylabel("Number of clouds")
ax41.set_xlabel("Simulation hour [\#]")
ax42.set_ylabel("Cloud cover [\%]")
ax42.set_xlabel("Simulation hour [\#]")
ax99.set_xlabel("Cloud cover [\%]")
ax99.set_ylabel("Number of clouds")
ax99.set_xlim(ax42.get_ylim())
ax99.set_ylim(ax41.get_ylim())
ax41.legend(loc="lower left")

ax43.set_ylabel("Smallest big cloud\neq. sphere radius [m]")
ax43.set_xlabel("Simulation hour [\#]")
ax43.legend(loc="best")

ax44.set_ylabel("\% of big clouds")
ax45.set_ylabel("Number of big clouds")
ax44.set_xlabel("Simulation hour [\#]")
ax45.set_xlabel("Simulation hour [\#]")
ax44.legend(loc="best")

ax46.set_ylabel("Cloud cover [\%]")
ax47.set_ylabel("Cloud volume fraction [\%]")
ax46.set_xlabel("Simulation hour [\#]")
ax47.set_xlabel("Simulation hour [\#]")
ax47.legend(loc='best')

ax48.set_title("All clouds")
ax48.set_ylabel("Min base - Max top")
ax49.set_title("Big clouds")
ax49.set_ylabel("Mean base - mean top")
ax48.set_xlabel("Simulation hour [\#]")
ax49.set_xlabel("Simulation hour [\#]")
ax49.legend(loc="best")

fig1.tight_layout()
if len(sys.argv)>1 :
  fig1.savefig(sys.argv[1]+sys.argv[2]+"clouds_cloud_num_cov.svg")
else :
  fig1.savefig("clouds_cloud_num_cov.svg")
fig2.tight_layout()
fig2.savefig("clouds_smallest_big_radius.svg")
fig3.tight_layout()
fig3.savefig("clouds_remaining_big_clouds.svg")
fig4.tight_layout()
fig4.savefig("clouds_cover_big_clouds.svg")
fig5.tight_layout()
fig5.savefig("clouds_bases_tops.svg")

#plt.show()
f.close()
