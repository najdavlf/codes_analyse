#!/usr/bin/python
# -*- coding: latin-1 -*-

# 14/01/2019 
# N. Villefranque

import sys, os
p='/home/villefranquen/objects/src/'
p='/cnrm/tropics/commun/DATACOMMUN/OBJECTS/objects/src/'
sys.path.append(p)
import netCDF4 as nc
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime
from identification_methods import identify
from mycolors import basic4 as cols
import matplotlib as mpl
from constants import *
import pickle

f=open("data.txt","w")

plt.style.use("manuscript")
mpl.rcParams["axes.edgecolor"]=cols[-3]
mpl.rcParams["grid.color"]=cols[-4]
mpl.rcParams["grid.linestyle"]="-"

markers =  {'.': 'point', ',': 'pixel', 'o': 'circle', 'v': 'triangle_down', '^': 'triangle_up', '<': 'triangle_left', '>': 'triangle_right', '1': 'tri_down', '2': 'tri_up', '3': 'tri_left', '4': 'tri_right', '8': 'octagon', 's': 'square', 'p': 'pentagon', '*': 'star', 'h': 'hexagon1', 'H': 'hexagon2', '+': 'plus', 'x': 'x', 'D': 'diamond', 'd': 'thin_diamond', '|': 'vline', '_': 'hline',  0: 'tickleft', 1: 'tickright', 2: 'tickup', 3: 'tickdown', 4: 'caretleft', 5: 'caretright', 6: 'caretup', 7: 'caretdown',  10: 'caretupbase'}
mymarkers = [m for m in list(markers)]

# Read vars

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

def cloudMask(dictVars,thrs) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>thrs]=1 # cell is in cloud if liquid water mixing ratio is greater than thrs
    return mask

listVarsNames = ["RCT"]
path = "/home/villefranquen/Work/NCDF/LES3D/"
path = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/"

maxz = {"ARMCu" : 116,
        "BOMEX" : 85,
        "RICO"  : 102,
        "SCMS"  : 114
        }
minz = {"ARMCu" : 28,
        "BOMEX" : 17,
        "RICO"  : 21,
        "SCMS"  : 16
        }

# To find all the files
reps = ["ARM_LES"]
reps = ["ARMCu","BOMEX","RICO","SCMS"]
reps = ["ARM_LES","BOMEX_LES","RICO_LES","SCMS_LES"]
pref = ["CERK4"]
pref = ["CERK4","VMFTH","THL3D","CER25"]
case = ["ARMCu"]
case = ["ARMCu","BOMEX","RICO","SCMS"]
nbfs = [4,4,4,4]
nbfs = [14,15,15,12]

i=0
dx=0.025
dy=0.025
dz=0.025
for i,(rep,pre,cas,nb) in enumerate(zip(reps,pref,case,nbfs)):
  tsteps = range(1,nb+1)
  nbf=len(tsteps)
  nc3dList = [path+rep+"/"+pre+".1."+cas+".%03i"%(h)+".nc" for h in tsteps]
  nc3dList = [path+rep+"/"+pre+"/"+pre+".1."+cas+".%03i"%(h)+".nc" for h in tsteps]
  # Characteristics
  number=np.zeros(nbf,dtype=int) # Number of clouds at a timestep
  bignumber=np.zeros(nbf,dtype=int) # Number of big clouds at a timestep
  tccovr=np.zeros(nbf) # Total cloud cover at a timestep
  bigtccovr=np.zeros(nbf) # Big cloud cover at a timestep
  minbas=np.zeros(nbf) # Min cloud base at each timestep
  maxtop=np.zeros(nbf) # Max cloud top at each timestep
  bigbas=np.zeros(nbf) # Min cloud base of the 50% bigger clouds at each timestep
  bigtop=np.zeros(nbf) # Max cloud top of the 50% bigger clouds at each timestep
  thresh=np.zeros(nbf) # Threshold in volume for the 90 percentile
  percen=np.zeros(nbf) # Percentage of clouds that make the 90% of the volume
  alleqlen=[]
  alldepth=[]
  allmeantp=[]
  allmaxtop=[]
  allmdep=[]
  allmeanbs=[]
  allminbas=[]
  for i3d,nc3d in enumerate(nc3dList) :
    print pre,cas,i3d
    print nc3d
    clouds=nc.Dataset(nc3d).variables["clouds"][:]
    if np.max(clouds)>0 :
      nbCellsDomainCover = clouds.shape[-1]*clouds.shape[-2]
      nbCellsDomainVolume= clouds.shape[-1]*clouds.shape[-2]*clouds.shape[-3]
      number[i3d] = len(np.unique(clouds[clouds>0]))
      # Total cover and fraction are in %
      tccovr[i3d] = 100.*float(len(np.where(np.sum(clouds,axis=-3)>0)[0]))/nbCellsDomainCover
      nbcols = np.zeros((number[i3d])) # List of cloud number of columns at this timestep
      nbcell = np.zeros((number[i3d])) # List of cloud number of cells at this timestep
      basesh = np.zeros((number[i3d])) # List of cloud base heights at this timestep
      ctopsh = np.zeros((number[i3d])) # List of cloud top heights at this timestep
      basesh = np.zeros((number[i3d])) # List of cloud base heights at this timestep
      ctopsh = np.zeros((number[i3d])) # List of cloud max top heights at this timestep
      meantp = np.zeros((number[i3d])) # List of cloud mean top heights at this timestep
      meanbs = np.zeros((number[i3d])) # List of cloud mean base heights at this timestep
      bigclouds = 1.*clouds
      for k,c in enumerate(np.unique(clouds[clouds>0])) : # 1 value per cloud
        inds = np.where(clouds==c)
        uniq_cols = list(set(zip(inds[-1],inds[-2])))
        nbcols[k] = len(uniq_cols)
        nbcell[k] = len(inds[0])
        alltops = dz*np.array([max([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))])
        allbase = dz*np.array([min([inds[-3][j] for j in np.where((np.array(inds[-1])==uniq_cols[u][0]) & (np.array(inds[-2])==uniq_cols[u][1]))[0]]) for u in range(len(uniq_cols))])
        basesh[k] = dz*1000.*min(inds[-3])
        ctopsh[k] = dz*1000.*max(inds[-3])
        meantp[k] = 1000.*np.mean(alltops)
        meanbs[k] = 1000.*np.mean(allbase)
     
      # Cumulative sum of volumic cloud fractions : the last point is the total volumic cloud fraction at the timestep
      nbCellsDomainVolume=nbCellsDomainCover*(np.max(ctopsh)/dz/1000. - np.min(basesh)/dz/1000.)
      y=np.cumsum((np.sort(nbcell)[::-1])/float(nbCellsDomainVolume)*100.) # cumulative sorted from biggest to smallest of volume cloud fraction in %
      j90=np.searchsorted(y,.9*max(y))                                     # where cumulated volume fraction is ~ 90% of total volume fraction
      nbcell90 = np.sort(nbcell)[::-1][j90]                                # size (nb of cells) of the cloud just before that 90% percentile
      thresh[i3d]=(nbcell90*dx*dy*dz*3./4./np.pi)**(1./3.)*1000.           # radius [m] of a sphere of same volume than the smallest cloud in the 90% volume fraction pool
      x=np.array(range(number[i3d]))/float(number[i3d])*100.
      percen[i3d] = x[j90]                                                 # Percentage of clouds in the 90% pool
      pickle.dump(x,f)
      pickle.dump(y,f)
      pickle.dump(x[j90],f)
      pickle.dump(y[j90],f)

      for k,c in enumerate(np.unique(clouds[clouds>0])) : 
        if nbcell[k]<=nbcell90 : bigclouds[bigclouds==c]=0
      bignumber[i3d] = len(nbcell[nbcell>nbcell90])
      bigtccovr[i3d] = 100.*float(len(np.where(np.sum(bigclouds,axis=-3)>0)[0]))/nbCellsDomainCover

      # list of '1 value per cloud' values at each timestep
      nbcols=list(nbcols)
      nbcell=list(nbcell)
      # one value per timestep
      minbas[i3d] = np.min(basesh)/1000.
      maxtop[i3d] = np.max(ctopsh)/1000.
      bigbas[i3d] = np.mean(basesh[nbcell>nbcell90])/1000.
      bigtop[i3d] = np.mean(ctopsh[nbcell>nbcell90])/1000.

      depth = (ctopsh[nbcell>nbcell90] - basesh[nbcell>nbcell90])*1e-3
      nbcols=np.array(nbcols); nbcell=np.array(nbcell)
      areas=nbcols[nbcell>nbcell90]*dx*dy
      alleqlen+=list(np.sqrt(areas))
      alldepth+=list(depth)
      pickle.dump(np.sqrt(areas),f)
      pickle.dump(depth,f)

      moy = meantp[nbcell>nbcell90]*1.e-3
      top = ctopsh[nbcell>nbcell90]*1.e-3
      allmeantp+=list(moy)
      allmaxtop+=list(top)
      pickle.dump(moy,f)
      pickle.dump(top,f)
      
      mdep = (meantp[nbcell>nbcell90]-meanbs[nbcell>nbcell90])
      allmdep+=list(mdep*1.e-3)
      pickle.dump(mdep*1.e-3,f)
      pickle.dump(depth,f)

      moy = meanbs[nbcell>nbcell90]*1.e-3
      bas = basesh[nbcell>nbcell90]*1.e-3
      allmeanbs+=list(moy)
      allminbas+=list(bas)
      pickle.dump(bas,f)
      pickle.dump(moy,f)

    if i3d==nbf-1 :
      pickle.dump(sum(bignumber),f)
      pickle.dump([0.,2.],f)
      pickle.dump([0.,2.],f)
      pickle.dump(alleqlen,f)
      pickle.dump(alldepth,f)
      pickle.dump(sum(bignumber),f)
      pickle.dump([0.,3.],f)
      pickle.dump([0.,3.],f)
      pickle.dump(allmeantp,f)
      pickle.dump(allmaxtop,f)
      pickle.dump(sum(bignumber),f)
      pickle.dump([0.,1.8],f)
      pickle.dump([0.,1.8],f)
      pickle.dump(allmdep,f)
      pickle.dump(alldepth,f)
      pickle.dump(sum(bignumber),f)
      pickle.dump([0.4,2.],f)
      pickle.dump([0.4,2.],f)
      pickle.dump(allminbas,f)
      pickle.dump(allmeanbs,f)
 
  # volume threshold and percentage of remaining clouds
  pickle.dump(tsteps,f)
  pickle.dump(thresh,f)
  pickle.dump(tsteps,f)
  pickle.dump(percen,f)
  # same but on separate fig 
  pickle.dump(tsteps,f)
  pickle.dump(thresh,f)
  pickle.dump(tsteps,f)
  pickle.dump(percen,f)

  # number total and number of 90% total volume
  pickle.dump(tsteps,f)
  pickle.dump(number,f)
  pickle.dump(tsteps,f)
  pickle.dump(bignumber,f)
  # same but on separate fig 
  pickle.dump(tsteps,f)
  pickle.dump(number,f)
  pickle.dump(tsteps,f)
  pickle.dump(bignumber,f)

  # cover total and cover of 90% total volume
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  pickle.dump(tsteps,f)
  pickle.dump(bigtccovr,f)
  # same but on separate fig 
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  pickle.dump(tsteps,f)
  pickle.dump(tccovr,f)
  pickle.dump(tsteps,f)
  pickle.dump(bigtccovr,f)

  # base min / top max all clouds
  pickle.dump(tsteps,f)
  pickle.dump(minbas,f)
  pickle.dump(tsteps,f)
  pickle.dump(maxtop,f)
  # same but on separate fig
  pickle.dump(tsteps,f)
  pickle.dump(minbas,f)
  pickle.dump(tsteps,f)
  pickle.dump(maxtop,f)

  # base mean / top mean, clouds 90% total volume
  pickle.dump(tsteps,f)
  pickle.dump(bigbas,f)
  pickle.dump(tsteps,f)
  pickle.dump(bigtop,f)
  # same but separated fig
  pickle.dump(tsteps,f)
  pickle.dump(bigbas,f)
  pickle.dump(tsteps,f)
  pickle.dump(bigtop,f)

f.close()
