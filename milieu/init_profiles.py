#!/usr/bin/python
# -*- coding: latin-1 -*-

# 04 / 2017 #
# N. Villefranque

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime
from mycolors import basic4 as cols
import matplotlib as mpl
from constants import *

plt.style.use("manuscript")
mpl.rcParams["axes.edgecolor"]=cols[-3]
mpl.rcParams["grid.color"]=cols[-4]
mpl.rcParams["grid.linestyle"]="-"

# Read vars

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

# Compute thermodynamics
def rel_hum(pv,ps):
    return pv/ps*100.

def pvap(P,r) :
    # inputs
    # P : pressure [Pa]
    # r : mixing ratio [kg/kg]
    # output
    # Vapor pressure [hPa]
    return (P/100.)*r/(0.622+r)

def psat(T) :  
    # inputs
    # T : temperature [°C]
    # output 
    # Saturation vapour pressure [hPa]
    return 6.1121*np.exp((18.678-T/234.5)*(T/(T+257.14))) # Buck formula

def T(th,P) :
    # inputs
    # th : potential temperature [K]
    # P  : pressure [Pa]
    # output
    # Temperature [°C]
    return th*(P/100./Ps_)**(R_/Ma_/Cp_)-Tk_


ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/CERK4.1.ARM.000.ALL.nc", "r")
th1 = var2d(ncfile, "MEAN_THp1")
rt1 = var2d(ncfile, "MEAN_RTp1")*1000.
rv1 = var2d(ncfile, "MEAN_RVp1")
u1  = var2d(ncfile, "MEAN_Up1")
v1  = var2d(ncfile, "MEAN_Vp1")
p1  = var2d(ncfile, "MEAN_PREp1")
ncfile.close()
ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/VMFTH.1.BMX.000.ALL.nc", "r")
th2 = var2d(ncfile, "MEAN_THp1")
rt2 = var2d(ncfile, "MEAN_RTp1")*1000.
rv2 = var2d(ncfile, "MEAN_RVp1")
u2  = var2d(ncfile, "MEAN_Up1")
v2  = var2d(ncfile, "MEAN_Vp1")
p2  = var2d(ncfile, "MEAN_PREp1")
ncfile.close()
ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/THL3D.1.RIC.000.ALL.nc", "r")
th3 = var2d(ncfile, "MEAN_THp1")
rt3 = var2d(ncfile, "MEAN_RTp1")*1000.
rv3 = var2d(ncfile, "MEAN_RVp1")
u3  = var2d(ncfile, "MEAN_Up1")
v3  = var2d(ncfile, "MEAN_Vp1")
p3  = var2d(ncfile, "MEAN_PREp1")
ncfile.close()
ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/CER25.1.SCM.000.ALL.nc", "r")
th4 = var2d(ncfile, "MEAN_THp1")
rt4 = var2d(ncfile, "MEAN_RTp1")*1000.
rv4 = var2d(ncfile, "MEAN_RVp1")
u4  = var2d(ncfile, "MEAN_Up1")
v4  = var2d(ncfile, "MEAN_Vp1")
p4  = var2d(ncfile, "MEAN_PREp1")
ncfile.close()

rh1 = rel_hum(pvap(p1,rv1),psat(T(th1,p1)))
rh2 = rel_hum(pvap(p2,rv2),psat(T(th2,p2)))
rh3 = rel_hum(pvap(p3,rv3),psat(T(th3,p3)))
rh4 = rel_hum(pvap(p4,rv4),psat(T(th4,p4)))

w1 = np.sqrt(u1**2+v1**2)
w2 = np.sqrt(u2**2+v2**2)
w3 = np.sqrt(u3**2+v3**2)
w4 = np.sqrt(u4**2+v4**2)

w,h = plt.figaspect(1.)
fig = plt.figure(figsize=(w*1.8,h))

th = fig.add_subplot(131)
for i,(var,lab) in enumerate(zip([th1,th2,th3,th4],["ARMCu","BOMEX","RICO","SCMS"])):
    th.plot(var[0,:120],np.linspace(0,3,120),color=cols[i+1],label=lab)
th.set_ylabel("Height [km]")
th.set_xlabel("Potential temperature [K]")

rh = fig.add_subplot(132)
for i,(var,lab) in enumerate(zip([rh1,rh2,rh3,rh4],["ARMCu","BOMEX","RICO","SCMS"])):
    rh.plot(var[0,:120],np.linspace(0,3,120),color=cols[i+1],label=lab)
rh.set_xlabel("Relative humidity [%]")
rh.set_yticklabels([])
plt.legend(loc="best")

ww = fig.add_subplot(133)
for i,(var,lab) in enumerate(zip([w1,w2,w3,w4],["ARMCu","BOMEX","RICO","SCMS"])):
    ww.plot(var[0,:120],np.linspace(0,3,120),color=cols[i+1],label=lab)
ww.set_xlabel("Wind speed [m/s]")
ww.yaxis.tick_right()

plt.tight_layout()
plt.savefig("init_profiles.svg")
plt.show()
