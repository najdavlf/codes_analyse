#!/usr/bin/python
# -*- coding: latin-1 -*-

# 14/01/2019 
# N. Villefranque

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import time, datetime
from mycolors import basic4 as cols
import matplotlib as mpl
from constants import *

plt.style.use("manuscript")
mpl.rcParams["axes.edgecolor"]=cols[-3]
mpl.rcParams["grid.color"]=cols[-4]
mpl.rcParams["grid.linestyle"]="-"

# Read vars

def var2d(ncfile, varname):
    var = ncfile.variables[varname][:,:]
    return var

def var1d(ncfile, varname):
    var = ncfile.variables[varname][:]
    return var

ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/CERK4.1.ARM.000.ALL.nc", "r")
r1=var2d(ncfile,"MEAN_RHOp1")
q1=var1d(ncfile, "Q0")
e1=var1d(ncfile, "E0")
ncfile.close()
ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/VMFTH.1.BMX.000.ALL.nc", "r")
r2=var2d(ncfile,"MEAN_RHOp1")
q2=var1d(ncfile, "Q0")
e2=var1d(ncfile, "E0")
ncfile.close()
ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/THL3D.1.RIC.000.ALL.nc", "r")
r3=var2d(ncfile,"MEAN_RHOp1")
q3=var1d(ncfile, "Q0")
e3=var1d(ncfile, "E0")
ncfile.close()
ncfile = ncdf.Dataset("/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/CER25.1.SCM.000.ALL.nc", "r")
r4=var2d(ncfile,"MEAN_RHOp1")
q4=var1d(ncfile, "Q0")
e4=var1d(ncfile, "E0")
ncfile.close()

zarm = [0,1000,2000,4000]
zscm = [0,1500,4000]
zbmx = [0,1500,3000]
zric = [0,740,3000]

tarm = np.linspace(1,900,900)+350
tscm = np.linspace(1,720,720)+420
tbmx = np.linspace(1,900,900)
tric = np.linspace(1,1440,1440)

varm = [-3,-3,0,0]
varmfin = [-1.2,-1.2,0,0]
vscm = [-3,0,0]
vbmx = [-2,-2,0]
vric = [-2,-1,0]

w,h = plt.figaspect(1.)
fig = plt.figure(figsize=(w*1.8,h))

a1 = plt.subplot2grid((2,3),(0,0),rowspan=2,colspan=1)
for i,(z,v,lab) in enumerate(zip([zarm,zbmx,zric,zscm],[varm,vbmx,vric,vscm],["ARMCu","BOMEX","RICO","SCMS"])):
    a1.plot(v,z,color=cols[i+1],label=lab)
a1.plot([0,0],[0,3000],color=cols[6],lw=1.,ls=':')
a1.set_title(r"Radiative cooling [K/d]")
a1.set_xlabel(r"$\partial_t\theta$")
a1.set_ylabel("Height [m]")
a1.set_xlim(-3.5,0.5)
a1.set_ylim(0,3000)

locs = a1.get_xticks()
labs = a1.get_xticklabels()
a1.set_xticks(locs[::2])
a1.set_xticklabels("%.2f" %f for f in locs[::2])

a2 = plt.subplot2grid((2,3),(0,1),rowspan=1,colspan=2)
a2.set_title(r"Surface heat fluxes [W/m$^2$]")
for i,(v,r,t,lab) in enumerate(zip([q1,q2,q3,q4],[r1,r2,r3,r4],[tarm,tbmx,tric,tscm],["ARMCu","BOMEX","RICO","SCMS"])):
    a2.plot(t/60.,v*1015.*r[:,0].min(),color=cols[i+1],label=lab)
plt.legend(loc="best")
a2.plot([0,25],[0,0],color=cols[6],lw=1.,ls=':')
a2.text(2,400,"Sensible",fontsize=15)
a2.set_ylim(-50,500)
a2.yaxis.set_label_position("right")
a2.yaxis.tick_right()

a3 = plt.subplot2grid((2,3),(1,1),rowspan=1,colspan=2,sharex=a2)
for i,(v,r,t,lab) in enumerate(zip([e1,e2,e3,e4],[r1,r2,r3,r4],[tarm,tbmx,tric,tscm],["ARMCu","BOMEX","RICO","SCMS"])):
    a3.plot(t/60.,v*2.47e6*r[:,0].min(),color=cols[i+1],label=lab)
a3.plot([0,25],[0,0],color=cols[6],lw=1.,ls=':')
a3.text(2,400,"Latent",fontsize=15)
a3.set_ylim(-50,500)
a3.yaxis.set_label_position("right")
a3.yaxis.tick_right()
a3.set_xlabel("Local Time [h]")

plt.tight_layout()
plt.savefig("rad_forcings.svg")
plt.show()
