
  # hist of eqsizes 
  alleqsize = [e for sube in eqsize for e in sube]
  minsize=np.min(alleqsize)
  maxsize=np.max(alleqsize)
  bins=np.arange(minsize,maxsize+100.,100.)
  hist,edges = np.histogram(alleqsize,bins=bins)
  mid_edges=.5*(edges[1:]+edges[:-1])

  nlog = mid_edges*np.log(10.)*hist # l*ln(10)*N(l)
  nlog[nlog==0.]=np.float('nan')
  y = np.log10(nlog/np.nansum(nlog))
  ax3.semilogx(mid_edges,y,color=cols[i+1],label=cas)

  # hist of normalized eqsizes 
  alleqsize = [e for sube in normes for e in sube]
  minsize=np.min(alleqsize)
  maxsize=np.max(alleqsize)
  bins=np.arange(minsize,maxsize+.1,.1)
  hist,edges = np.histogram(alleqsize,bins=50)
  mid_edges=.5*(edges[1:]+edges[:-1])

  nlog = mid_edges*np.log(10.)*hist # l*ln(10)*N(l)
  nlog[nlog==0.]=np.float('nan')
  y = np.log10(nlog/np.nansum(nlog))
  ax4.semilogx(mid_edges,y,color=cols[i+1],label=cas)


