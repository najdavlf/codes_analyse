#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time, datetime
import six
from matplotlib import colors
import matplotlib.patches as mpatches
import atpy

print ""
print "Debut programme !"
print ""

dfile = sys.argv[1]
fname = "ARM_LES/CERK4.1.ARM.OBJ.nc"
fname2 = "ARM_LES/CERK4.1.ARM.ALL.nc"
wname = "ARM_LES/CERK4.1.ARM.RC.nc"

print "Lecture nectdf en cours ..."
ncfile=ncdf.Dataset(fname, "r", format="NETCDF3_CLASSIC")
ncfile2=ncdf.Dataset(fname2, "r", format="NETCDF3_CLASSIC")
ncnew =ncdf.Dataset(wname, "w", format="NETCDF3_CLASSIC")

t = ncfile.variables['time'][:]
x = ncfile2.variables['W_E_direction'][:]
y = ncfile2.variables['S_N_direction'][:]
z = ncfile2.variables['VLEV'][:,:,:]

Tdim = ncfile.dimensions['time']
Zdim = ncfile2.dimensions['vertical_levels']
Ydim = ncfile2.dimensions['S_N_direction']
Xdim = ncfile2.dimensions['W_E_direction']

T = ncnew.createDimension('time',180)
Z = ncnew.createDimension('vertical_levels',160)
Y = ncnew.createDimension('S_N_direction',256)
X = ncnew.createDimension('W_E_direction',256)

T = Tdim
Z = Zdim
Y = Ydim
X = Xdim

time = ncnew.createVariable('time','f8',('time',))
time.units = "seconds since 2004-01-01 00:00:00 UTC"
time[:] = t

xvec = ncnew.createVariable('W_E_direction','f8',('W_E_direction',))
xvec.units = "km"
xvec[:] = x

yvec = ncnew.createVariable('S_N_direction','f8',('S_N_direction',))
yvec.units = "km"
yvec[:] = y

#zvec = ncnew.createVariable('VLEV','f8',('vertical_levels,S_N_direction,W_E_direction',))
#zvec.units = "km"
#zvec[:,:,:] = z

ncfile2.close()

RCT_mean = ncnew.createVariable('RCT', 'f8', ('time','vertical_levels','S_N_direction','W_E_direction',))

print "Lecture netcdf terminée !"
print ""

surfdom = 6.4*6.4 # en km

print "Lecture données en cours ..."

tdata = atpy.Table(dfile, type='ascii') 

simus = np.unique(tdata.ID)
nb_simus = len(simus)

rc = tdata.RC_mean
nh = tdata.nH
nu = tdata.N
ns = tdata.ID

print "Lecture données terminée !"
print ""

print "Boucle sur datafile en cours ..."

nn = 0
# l numero de ligne dans le fichier
# n numero de nuage dans l'instant
# nh[l] numero d'instant entre 1 et size(t)
# rc[l] rc moyen du nuage

for (l,n) in enumerate(nu) : # parcours du fichier de données 
    if (ns[l] == "ARM") :    # uniquement pour ARM
        if (nh[l]!=nn) :     # instant suivant 
            objets = ncfile.variables['objets'][nh[l]-1,:,:,:] # recupere la matrice objets de l'instant nn
            rc3d = objets*0. 
            nn = nh[l]
            print nn, objets.max()
        rc3d[objets==n] = rc[l] # le nuage n de l'instant nn prend partout la valeur mean_rc(n)
        RCT_mean[nh[l]-1,:,:,:] = rc3d

ncfile.close()
ncnew.close()

print "Boucle sur datafile terminée !"
print ""
print "Fin programme !"
print ""
