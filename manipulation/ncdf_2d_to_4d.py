#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import time, datetime

ncfile = sys.argv[1] 
prefix = ncfile.split('.nc')[0]
ncfnew = prefix+'_4D.nc'

data = ncdf.Dataset(ncfile,'r')
dnew = ncdf.Dataset(ncfnew,'w')

Xdim = data.dimensions['W_E_direction']
Ydim = data.dimensions['S_N_direction']
Zdim = data.dimensions['vertical_levels']
Tdim = data.dimensions['time']

Xlen = Xdim.size
Ylen = Ydim.size
Zlen = Zdim.size
Tlen = Tdim.size

Tnew = dnew.createDimension('time',None)
Znew = dnew.createDimension('vertical_levels',Zlen)
Ynew = dnew.createDimension('S_N_direction',Ylen)
Xnew = dnew.createDimension('W_E_direction',Xlen)

Tnew = Tdim
Znew = Zdim
Ynew = Ydim
Xnew = Xdim

Tvar = data.variables['time']
Zvar = data.variables['vertical_levels']
Yvar = data.variables['S_N_direction']
Xvar = data.variables['W_E_direction']

Tvar_new = dnew.createVariable('time',Tvar.dtype,('time',))
Zvar_new = dnew.createVariable('vertical_levels',Zvar.dtype,('vertical_levels',))
Yvar_new = dnew.createVariable('S_N_direction',Yvar.dtype,('S_N_direction',))
Xvar_new = dnew.createVariable('W_E_direction',Zvar.dtype,('W_E_direction',))

for (var,new_var) in zip([Tvar,Zvar,Yvar,Xvar],[Tvar_new,Zvar_new,Yvar_new,Xvar_new]):
    for att in var.ncattrs() :
      new_var.setncattr(att, var.getncattr(att))

Tvar_new[:] = Tvar[:]
Zvar_new[:] = Zvar[:]
Yvar_new[:] = 0.
Xvar_new[:] = 0.

for key in data.variables.keys():
    mask = 0
    var = data.variables[key]
    var_shape = var.shape
    new_shape = var_shape+(Ylen,Xlen)
    tmp_dim = var.dimensions
    if ('mask' in tmp_dim) :
        mask = 1 
        new_shape = tuple(np.delete(list(new_shape), tmp_dim.index('mask')))
        tmp_dim = tuple(np.delete(list(tmp_dim),tmp_dim.index('mask')))
    dimensions = tmp_dim + ('S_N_direction','W_E_direction',)

    if ( (key!="time") & (key!='vertical_levels') & (key!="S_N_direction") & (key!="W_E_direction") ) :
        var_tab = var[:]
        new_var = dnew.createVariable(key,var.dtype,dimensions)
        for att in var.ncattrs() :
            new_var.setncattr(att, var.getncattr(att))
        if (len(new_shape)==2):
            var_tab = var[:]
            new_var[0,0] = var_tab
        elif (len(new_shape)==3):
            var_tab = var[:]
            new_var[:,0,0] = var_tab
        elif (len(new_shape)==4):
            var_tab = var[:,:]
            new_var[:,:,0,0] = var_tab
    
data.close()
dnew.close()
