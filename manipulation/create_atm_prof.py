import numpy as np
import netCDF4 as nc
import sys

if (len(sys.argv) < 3) :
    print "Usage : python", sys.argv[0]," textfile height_top_les(km)"
    quit() 
else :
    textfile = sys.argv[1]
    topheight= float(sys.argv[2])

ncfile = textfile.split(".")[0]+".nc"

f = open(textfile, "r")

print f.readline()

nz = 31
zvec=np.zeros(nz)
tvec=np.zeros(nz)
pvec=np.zeros(nz)
dvec=np.zeros(nz)
vvec=np.zeros(nz)
ovec=np.zeros(nz)

for i in range(nz) :
    z,p,t,d,v,o = f.readline().split(",")
    zvec[i] = float(z)
    tvec[i] = float(t)
    pvec[i] = float(p)
    dvec[i] = float(d)
    vvec[i] = float(v)
    ovec[i] = float(o)
f.close()

topz = zvec[zvec>=topheight]
topt = tvec[zvec>=topheight]
topp = pvec[zvec>=topheight]
topd = dvec[zvec>=topheight]
topv = vvec[zvec>=topheight]
topo = ovec[zvec>=topheight]

n = nc.Dataset(ncfile, "w")

print ncfile
n.createDimension("vertical_levels",len(topz))

zz=n.createVariable("vertical_levels", 'f8', ('vertical_levels',))
tt=n.createVariable("MEAN_T", 'f8', ('vertical_levels',))
pp=n.createVariable("MEAN_P", 'f8', ('vertical_levels',))
dd=n.createVariable("MEAN_RHO", 'f8', ('vertical_levels',))
vv=n.createVariable("MEAN_VC", 'f8', ('vertical_levels',))
oo=n.createVariable("MEAN_OC", 'f8', ('vertical_levels',))

zz[:]=topz
tt[:]=topt
pp[:]=topp
dd[:]=topd
vv[:]=topv
oo[:]=topo

n.close()
