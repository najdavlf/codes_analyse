import numpy as np
from netCDF4 import Dataset
import sys

if (len(sys.argv) < 2) :
    print "Argument missing (tau,cf)"
    exit()
else :
    tau = sys.argv[1]
    cf  = sys.argv[2]

xmax = 6.4
ymax = 6.4
zmax = 4.0 # km
NLON = 256
NLAT = 256
NLVL = 160

area = xmax*ymax
couv = float(cf)*area
xcov = np.sqrt(couv)
ycov = np.sqrt(couv)

dx = xmax/NLON
dy = ymax/NLAT
dz = zmax/NLVL

x = np.linspace(dx/2.,xmax-dx/2.,num=NLON)
y = np.linspace(dy/2.,ymax-dy/2.,num=NLAT)
z = np.linspace(dz/2.,zmax-dz/2.,num=NLVL)

ncfile = Dataset('slab_cloud_tau'+tau+'_cf'+cf+'.nc', 'w', format='NETCDF3_64BIT')
ncfile.createDimension('W_E_direction',NLON)
ncfile.createDimension('S_N_direction',NLAT)
ncfile.createDimension('vertical_levels',NLVL)
ncfile.createDimension('time',1)

horizontalx = ncfile.createVariable('W_E_direction', 'f4', ('W_E_direction',))
horizontaly = ncfile.createVariable('S_N_direction', 'f4', ('S_N_direction',))
vertical = ncfile.createVariable('vertical_levels', 'f4', ('vertical_levels',))
time = ncfile.createVariable('time', 'f4', ('time',))

rcvar  = ncfile.createVariable('RCT', 'f4', ('time', 'vertical_levels', 'S_N_direction', 'W_E_direction'))

horizontalx[:] = x
horizontaly[:] = y
vertical[:] = z
time[:] = 0.

# On veut une epaisseur optique de tau sur 1 km d'epaisseur
k = float(tau)/1. # km-1

rho_eau = 1000. # kg / m3 (a 4 dC)
rho_air = 1.204 # kg / m3 (a 20 dC)
Reff = 10*10**(-9) # effective radius (km) over land

# 800 nm : massic cross section mse = 153.4 m^2/kg d'eau 
mse = 153.4 
rc = k/(mse*rho_air)/1000. # rc en kg/kg

zmin = 0.750
zmax = zmin + 1.
imin = int(zmin/dz)
imax = int(zmax/dz)
xmax = int(xcov/dx)
ymax = int(ycov/dy)

rcvar[:,:,:,:] = 0.
rcvar[0,imin:imax,:ymax,:xmax] = rc 

ncfile.close()
