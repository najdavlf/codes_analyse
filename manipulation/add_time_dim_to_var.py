#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import time, datetime

if (len(sys.argv) < 2) :
    print 'Usage : python', sys.argv[0], ' ncfile'
    exit()

ncfile = sys.argv[1] 
prefix = ncfile.split('.nc')[0]
ncfnew = prefix+'_4D.nc'

data = ncdf.Dataset(ncfile,'r')
dnew = ncdf.Dataset(ncfnew,'w')

Xdim = data.dimensions['W_E_direction']
Ydim = data.dimensions['S_N_direction']
Zdim = data.dimensions['vertical_levels']
Tdim = data.dimensions['time']

Xlen = Xdim.size
Ylen = Ydim.size
Zlen = Zdim.size
Tlen = Tdim.size

Tnew = dnew.createDimension('time',None)
Znew = dnew.createDimension('vertical_levels',Zlen)
Ynew = dnew.createDimension('S_N_direction',Ylen)
Xnew = dnew.createDimension('W_E_direction',Xlen)

Tnew = Tdim
Znew = Zdim
Ynew = Ydim
Xnew = Xdim

Tvar = data.variables['time']
Zvar = data.variables['vertical_levels']
Yvar = data.variables['S_N_direction']
Xvar = data.variables['W_E_direction']

Tvar_new = dnew.createVariable('time',Tvar.dtype,('time',))
Zvar_new = dnew.createVariable('vertical_levels','f8',('vertical_levels',))
Yvar_new = dnew.createVariable('S_N_direction','f8',('S_N_direction',))
Xvar_new = dnew.createVariable('W_E_direction','f8',('W_E_direction',))

for (var,new_var) in zip([Tvar,Zvar,Yvar,Xvar],[Tvar_new,Zvar_new,Yvar_new,Xvar_new]):
    for att in var.ncattrs() :
      new_var.setncattr(att, var.getncattr(att))


Tvar_new[:] = Tvar[:]
Zvar_new[:] = Zvar[:]
Yvar_new[:] = Yvar[:]
Xvar_new[:] = Xvar[:]

for key in data.variables.keys():
    var = data.variables[key]
    var_shape = var.shape
    new_shape = (1,)+var_shape
    tmp_dim = var.dimensions
    dimensions = ('time',)+tmp_dim
    if ( (key!="time") & (key!='vertical_levels') & (key!="S_N_direction") & (key!="W_E_direction") ) :
        new_var = dnew.createVariable(key,var.dtype,dimensions)
        for att in var.ncattrs() :
            new_var.setncattr(att, var.getncattr(att))
        new_var[0,:,:,:] = var[:,:,:]
    
data.close()
dnew.close()
