
import sys
import numpy as np
import netCDF4 as nc

from matplotlib import pyplot as plt

if len(sys.argv)<2 : print "Usage:",sys.argv[0],"file" ; exit()

f=sys.argv[1]
rd=nc.Dataset(f,"r")
wd=nc.Dataset(f.split(".nc")[0]+'_cyclic.nc','w')

print rd
for dim in rd.dimensions :
    print rd.dimensions[dim].size*3

rd.close()
wd.close()
