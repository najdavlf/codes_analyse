#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time, datetime
import six
from matplotlib import colors
import matplotlib.patches as mpatches
import atpy

print ""
print "Debut programme !"
print ""

fname = "ARM_LES/CERK4.1.ARM.ALL.nc"
wname = "ARM_LES/CERK4.1.ARM.GCM2.nc"
mname = "CERK4.1.ARM.000.ALL.nc"

print "Lecture nectdf en cours ..."
ncfile=ncdf.Dataset(fname, "r", format="NETCDF3_CLASSIC")
ncnew =ncdf.Dataset(wname, "w", format="NETCDF3_CLASSIC")
ncmean=ncdf.Dataset(mname, "r", format="NETCDF3_CLASSIC")

t = ncfile.variables['time'][:]

Tdim = ncfile.dimensions['time']
Zdim = ncfile.dimensions['vertical_levels']
Ydim = ncfile.dimensions['S_N_direction']
Xdim = ncfile.dimensions['W_E_direction']

T = ncnew.createDimension('time',180)
Z = ncnew.createDimension('vertical_levels',160)
Y = ncnew.createDimension('S_N_direction',256)
X = ncnew.createDimension('W_E_direction',256)

T = Tdim
Z = Zdim
Y = Ydim
X = Xdim

time = ncnew.createVariable('time','f8',('time',))
time.units = "seconds since 2004-01-01 00:00:00 UTC"
time[:] = t

#RCT_mean = ncnew.createVariable('RCT_mean', 'f8', ('time','vertical_levels','S_N_direction','W_E_direction',))
RCT_ov = ncnew.createVariable('RCT_ov', 'f8', ('time','vertical_levels','S_N_direction','W_E_direction',))
cf = ncmean.variables['MEAN_CFp1'][:,:]
ccf = np.zeros((180,160))

for (i,k) in enumerate(range(0,900,5)) : # boucle pas de temps
    ccf[i,:] = np.mean(cf[k:k+5,:],axis=0) # t , z

print "Lecture netcdf terminée !"
print ""

print "Boucle sur vec temps en cours ..."

for (l,n) in enumerate(t) :
    # l numero de ligne dans le fichier
    # n numero de nuage dans l'instant
    # nh[l] numero d'instant 
    # rc[l] rc moyen du nuage
    print l
    rct = ncfile.variables['RCT'][l,:,:,:]
    rc1d = np.mean(rct,axis=(1,2))
    for z in range(160) :
        nb_cel = int(np.sqrt(ccf[l,z]*256*256))
        #RCT_mean[l,z,:,:] = rc1d[z]
        RCT_ov[l,z,:,:] = 0.
        RCT_ov[l,z,:nb_cel,:nb_cel] = rc1d[z]


ncfile.close()
ncnew.close()
ncmean.close()

print "Boucle sur datafile terminée !"
print ""
print "Fin programme !"
print ""


