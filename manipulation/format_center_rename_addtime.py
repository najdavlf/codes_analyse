import netCDF4 as nc 
import sys,os  

if len(sys.argv)<2 :
    print "Usage: ",sys.argv[0],"ncfile [RITtoRCT]"
    exit(1)

fi = sys.argv[1]
f = nc.Dataset(fi,'r')

if len(f.variables) > 10 :
  f.close()
  print "Extracting relevant fields"
  os.system("ncks -O -v XHAT,YHAT,ZHAT,THT,PABST,RVT,RCT,.RIT %s %s" %(fi,"ex_"+fi))
  f = nc.Dataset("ex_"+fi,"r")

g = nc.Dataset('formatted_'+fi,'w')
if len(sys.argv)==2 : do_RITtoRCT=0 
else : do_RITtoRCT=1


changeVar = {"X":"W_E_direction","Y":"S_N_direction","Z":"vertical_levels","XHAT":"W_E_direction","YHAT":"S_N_direction","ZHAT":"vertical_levels"}

# To copy the global attributes of the netCDF file  
print "Copying global attributes"
for attname in f.ncattrs():
    setattr(g,attname,getattr(f,attname))

# To copy the dimension of the netCDF file
print "Copying dimensions"
g.createDimension("time",None)
for dimname,dim in f.dimensions.iteritems():
        npts = len(dim)
        if dimname in changeVar:
            npts -= 2 # Remove bounds (HALO points)
            g.createDimension(changeVar[dimname],npts)
        else : 
            g.createDimension(dimname,npts)
        print dimname, "of length", npts

# To copy the variables of the netCDF file
print "Copying fields"
for varname,ncvar in f.variables.iteritems():
       print varname
       dims=[changeVar[d] if d in changeVar else d for d in ncvar.dimensions]
       if varname in changeVar:
         var = g.createVariable(changeVar[varname],ncvar.dtype,dims)
       else:
         var = g.createVariable(varname,ncvar.dtype,['time']+dims)

       #Proceed to copy the variable attributes
       for attname in ncvar.ncattrs():  
          setattr(var,attname,getattr(ncvar,attname))
       #Finally copy the variable data to the new created variable
       if len(var.shape)==4:
           var[:,:,:,:] = [ncvar[1:-1,1:-1,1:-1]]
       elif len(var.shape)==1 : 
           var[:]= 0.5*(ncvar[1:-1] + ncvar[2:])
       else : print varname, "is of shape", var.shape

if do_RITtoRCT: g.variables["RCT"][:] += g.variables["RIT"][:]

f.close()
g.close()
