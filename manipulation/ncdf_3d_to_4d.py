#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import time, datetime

ncfile = sys.argv[1] 
prefix = ncfile.split('.nc')[0]
ncfnew = prefix+'_4D.nc'

data = ncdf.Dataset(ncfile,'r')
dnew = ncdf.Dataset(ncfnew,'w')

Xdim = data.dimensions['W_E_direction']
Ydim = data.dimensions['S_N_direction']
Zdim = data.dimensions['vertical_levels']
Tdim = data.dimensions['time']

Xlen = Xdim.size
Ylen = Ydim.size
Zlen = Zdim.size
Tlen = Tdim.size

Tnew = dnew.createDimension('time',None)
Znew = dnew.createDimension('vertical_levels',Zlen)
Ynew = dnew.createDimension('S_N_direction',Ylen)
Xnew = dnew.createDimension('W_E_direction',Xlen)

Tnew = Tdim
Znew = Zdim
Ynew = Ydim
Xnew = Xdim

Tvar = data.variables['time']

Tvar_new = dnew.createVariable('time',Tvar.dtype,('time',))
Zvar_new = dnew.createVariable('vertical_levels','f8',('vertical_levels',))
Yvar_new = dnew.createVariable('S_N_direction','f8',('S_N_direction',))
Xvar_new = dnew.createVariable('W_E_direction','f8',('W_E_direction',))

for (var,new_var) in zip([Tvar],[Tvar_new]):
    for att in var.ncattrs() :
      new_var.setncattr(att, var.getncattr(att))


x0=0.
x1=12.8
nx=512
dx=(x1-x0)/nx
y0=0.
y1=12.8
ny=512
dy=(y1-y0)/ny
z0=0.
z1=4.
nz=160
dz=(z1-z0)/nz

Tvar_new[:] = Tvar[:]
Zvar_new[:] = np.linspace(x0+dx/2. , x1-dx/2. , num=nx)
Yvar_new[:] = np.linspace(y0+dy/2. , y1-dy/2. , num=ny)
Xvar_new[:] = np.linspace(z0+dz/2. , z1-dz/2. , num=nz)

for key in data.variables.keys():
    var = data.variables[key]
    var_shape = var.shape
    new_shape = (1,)+var_shape
    tmp_dim = var.dimensions
    dimensions = ('time',)+tmp_dim
    if ( (key!="time") & (key!='vertical_levels') & (key!="S_N_direction") & (key!="W_E_direction") ) :
        var_tab = var[:]
        new_var = dnew.createVariable(key,var.dtype,dimensions)
        for att in var.ncattrs() :
            new_var.setncattr(att, var.getncattr(att))
        var_tab = var[:,:,:]
        new_var[0,:,:,:] = var_tab
    
data.close()
dnew.close()
