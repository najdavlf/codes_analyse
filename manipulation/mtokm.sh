#!/bin/bash

for f in *carac_4D.nc 
do
  ncap2 -s "W_E_barycenter=W_E_barycenter/1000." $f -O $f
  ncap2 -s "S_N_barycenter=S_N_barycenter/1000." $f -O $f
  ncap2 -s "W_E_direction=W_E_direction/1000." $f -O $f
  ncap2 -s "S_N_direction=S_N_direction/1000." $f -O $f
  ncap2 -s "area=area/1000000." $f -O $f
  ncap2 -s "cover=cover/1000000." $f -O $f
  ncap2 -s "volume=volume/1000000000." $f -O $f
done
