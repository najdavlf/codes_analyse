#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys, os
import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from matplotlib import pyplot as plt 

print ""
print "Begin", sys.argv[0]
print ""

if (len(sys.argv)<2) :
    print "Error :", sys.argv[0], "needs at least one argument (netCDF LES file name)"
    print ""
    print "Exiting"
    print ""
    exit()

# nb heures dans la simulation
LENSIM = 15

def main(listFiles):
  for file in listFiles :
    add_mean_cloud_RC(file)
#   add_mean_field_RC(file)

" at each level, each cloud cell takes the <ql(z)>_c value of the matching cloud "
def add_mean_cloud_RC(file) :
    print "add_mean_cloud_RC", file

    # retrieve carac files names
    carfile = file.split(".nc")[0]+".carac.nc"
    objfile = file.split(".nc")[0]+".obj.nc"

    # open all datasets
    datacar = ncdf.Dataset(carfile,"r")
    dataLES = ncdf.Dataset(file,"a")
    dataobj = ncdf.Dataset(objfile,"r")

    # retrieve objects, rc[:,0,:,:] (vlevs, mean, clouds, time) 
    objects = dataobj.variables["objets"][:,:,:,:]
    nt,nz,ny,nx = objects.shape
    objects = np.array(objects).reshape(objects.shape)
    RCcloud = datacar.variables["RCT"][:,0,:,:]
    nz2,nc,nt2 = RCcloud.shape
    RCcloud = np.array(RCcloud).reshape(RCcloud.shape)

    # loop over levels and clouds and fill meanRC var
    listObj = np.unique(objects[objects>0])
    meanRC = np.zeros(objects.shape)
    for z in range(nz) :
        if (np.max(objects[:,z,:,:])>0) :
            print "level", z
            zobj = objects[:,z,:,:]
            zRCm = zobj*0.
            for (ic,c) in enumerate(listObj) :
                if (len(zobj[zobj==c]) > 0) :
                    zRCm[zobj==c] = RCcloud[z,ic,:]

            meanRC[:,z,:,:] = zRCm

    # create var in LES dataset
    cloud_mean_rct = dataLES.createVariable("cloud_mean_rct","f8",("time","vertical_levels","S_N_direction","W_E_direction",))
    cloud_mean_rct[:,:,:,:] = meanRC[:,:,:,:]

    # close datasets
    datacar.close()
    dataLES.close()
#   dataobj.close()

    return 0

" at each level, each cloud cell takes the <ql(z)> value of whole field "
def add_mean_field_RC(file) :
    print "add_mean_field_RC", file

    # retrieve mean file name and sim time in hour
    meanfile = file.split(".0")[0]+".000KCL.nc"
    ninstant = int(file.split(".0")[1].split(".nc")[0])

    # open mean dataset 
    data000 = ncdf.Dataset(meanfile, "r")

    # retrieve time dim and instant 
    timedm = data000.dimensions["time"]
    nbtime = timedm.size     # number of points in the simulation
    tmstep = nbtime/LENSIM   # number of points per hour of simulation
    thistm = tmstep*ninstant - 1 # point matching the sim time
    print thistm

    tmvar0 = data000.variables["time"][:]

    # retrieve <ql> and cf 
    meanql = data000.variables["ql"][thistm,:]
    profcf = data000.variables["rneb"][thistm][:]
    meanql = np.array(meanql).reshape(len(meanql))
    profcf = np.array(profcf).reshape(len(profcf)) 

    # close mean dataset
    data000.close()

    # compute constant value of rc to give to each cloudy cell in file
    meanrc = meanql/(1.-meanql)
    rc_cld = profcf*1.
    rc_cld[profcf>0] = meanrc[profcf>0]/profcf[profcf>0]

    # open LES dataset
    dataLES = ncdf.Dataset(file, "a")

    # retrieve time and RCT field
    tmvar1 = dataLES.variables["time"][:]
    RCT = dataLES.variables["RCT"][:,:,:,:]
    nt,nz,ny,nx = RCT.shape
    RCT = np.array(RCT,dtype=float).reshape(RCT.shape)
    no0RCT = np.where(RCT>0.)

    # check if time is the same
    if (tmvar0[thistm] != tmvar1) :
      print "Error : not the same time step"
      print "In mean : ", tmvar0[thistm],thistm 
      print "In inst :" , tmvar1[0],ninstant 
      exit()

    # loop over levels and fill meanRC var
    meanRC = np.zeros(RCT.shape)
    meanRC[no0RCT] = 1
    for z in range(nz) :
        print "level", z
        if (rc_cld[z]>0) :
            meanRC[:,z,:,:] = meanRC[:,z,:,:] * rc_cld[z]
    
    # create new variable in LES dataset
    field_mean_RCT = dataLES.createVariable("field_mean_rct","f8",("time","vertical_levels","S_N_direction","W_E_direction",))
    field_mean_RCT[:,:,:,:] = meanRC[:,:,:,:]

    # close LES dataset
    dataLES.close()

    return 0


main(sys.argv[1:])
