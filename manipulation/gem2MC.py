from matplotlib import pyplot as plt
import numpy as np 
from scipy.interpolate import RegularGridInterpolator as rgi
import netCDF4 as nc
import sys

r_over_cp = 0.286

# arg : gem file to convert to 3D regular
ncfile = sys.argv[1]
# name of fields in input file
rcname = 'q_liquid'
prname = 'pressure'
tpname = 'temperature'
mzname = 'height' # mid height

fname = ncfile.split('.nc')[0]+'_3DMC.nc'
print fname
def get_var(dset,name):
  units = dset.variables[name].units
  var = dset.variables[name][:].reshape(dset.variables[name].shape)
  return [var,units]
def set_dim(dset,dim,n):
  dset.createDimension(dim,n)
def set_var(dset,name,dims,var,units):
  dvar = dset.createVariable(name,'f8',dims)
  dvar.units = units
  if len(var.shape)==3 :
    dvar[:,:,:] = var
  if len(var.shape)==1 :
    dvar[:] = var

# get dataset
dset = nc.Dataset(ncfile)
# get q_liquid
rc,rcunits = get_var(dset,rcname)
# get pressure
pr,prunits = get_var(dset,prname)
# get temperature
tp,tpunits = get_var(dset,tpname)
# get mid heights
mz,mzunits = get_var(dset,mzname)
if mzunits=='m' : mz/=1000. # heights in km
# close dataset
dset.close()

# set resols and dims
dx=.25
dy=.25
nx=rc.shape[2]
ny=rc.shape[1]
nz=rc.shape[0]
mx=np.linspace(0.+dx/2.,dx*nx-dx/2.,nx)
my=np.linspace(0.+dx/2.,dy*ny-dx/2.,ny)

# increasing height
rc=np.flipud(rc)
pr=np.flipud(pr)
tp=np.flipud(tp)
mz=np.flipud(mz)

# top of the LES domain ?
rc1d = np.mean(rc,axis=(1,2))
zmax = np.ceil(max(mz[rc1d>0]))

# new vertical resolution ?
nz = 160
dz = zmax/160.
newz = np.linspace(0+dz/2.,zmax-dz/2.,nz)
npts = [[z,y,x] for z in newz for y in my for x in mx]

# interpol rc
interpol_func = rgi((mz,my,mx),rc)
newrc=interpol_func(npts).reshape((nz,ny,nx))

# interpol pr
tmp = rc*0.
for iz in range(len(mz)):
  tmp[iz,:,:] = pr[iz]
interpol_func = rgi((mz,my,mx),tmp)
newpr=interpol_func(npts).reshape((nz,ny,nx))

# interpol tp
th=tp*(pr[0]/pr)**r_over_cp
tmp = rc*0.
for iz in range(len(mz)):
  tmp[iz,:,:] = th[iz]
interpol_func = rgi((mz,my,mx),tmp)
newth=interpol_func(npts).reshape((nz,ny,nx))

# write to new ncdf
dset = nc.Dataset(fname,"w")
xname = "W_E_direction"
yname = "S_N_direction"
zname = "vertical_levels"
set_dim(dset,xname,nx)
set_dim(dset,yname,ny)
set_dim(dset,zname,nz)
rcname = "RCT"
prname = "PABST"
thname = "THT"
set_var(dset,xname,(xname,),mx,'km')
set_var(dset,yname,(yname,),my,'km')
set_var(dset,zname,(zname,),newz,'km')
set_var(dset,rcname,(zname,yname,xname,),newrc,rcunits)
set_var(dset,prname,(zname,yname,xname,),newpr,prunits)
set_var(dset,thname,(zname,yname,xname,),newth,tpunits)
dset.close()

if 1 :
  mask = rc*0.
  mask[rc>0]=1
  fig,(ax1,ax2) = plt.subplots(nrows=1,ncols=2,sharey=True)
  ax1.plot(np.mean(rc*1000.,axis=(1,2)),mz)
  ax1.plot(np.mean(newrc*1000.,axis=(1,2)),newz,'r--')
  ax1.set_xlabel("Mean liquid water mixing ratio [g/kg]")
  ax1.set_ylabel("z [km]")
  ax1.set_ylim([0,20])
  ax2.plot(np.sum(mask,axis=(1,2))/(nx*ny),mz)
  ax1.set_xlabel("Cloud fraction")
  plt.savefig('rc_prof_'+ncfile.split('.nc')[0].split('/')[-1]+'.png')
